<div class="page-content about-content about-gallery-content">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('О клинике'),
            ['controller' => 'Abouts', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <div class="border-top">
            <h1>Галерея</h1>
<div class="grid">
  <!-- .grid-sizer empty element, only used for element sizing -->
  <div class="grid-sizer"></div>
  <?php foreach ($row['attachment_images'] as $k => $image) {
                           
                            ?>
                            <div class="grid-item <?php if ($image['big']) {echo 'grid-item--width2';}?>">
                                <div class="inner-wrap">
                                    <a class="fancybox" href="/files/attachment-images/<?=$image['image']?>" rel="gallery1">
                                        <img src="/files/attachment-images/<?php if ($image['big']) {echo $image['thumbnail_570x570'];} else {echo $image['thumbnail_270x270'];}?>" alt="">
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
</div>

       <!--      <div class="row list">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <div class="row list">
                        <?php foreach ($row['attachment_images'] as $k => $image) {
                            if ($k == 7) break;
                            ?>
                            <div class="col-md-<?php if ($image['big']) {echo '12';} else {echo '6';}?> col-sm-12 col-xs-12 ">
                                <div class="inner-wrap">
                                    <a class="fancybox" href="/files/attachment-images/<?=$image['image']?>" rel="gallery1">
                                        <img src="/files/attachment-images/<?php if ($image['big']) {echo $image['thumbnail_570x570'];} else {echo $image['thumbnail_270x270'];}?>" alt="">
                                    </a>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <div class="row list">
                        <?php foreach ($row['attachment_images'] as $k => $image) {
                            if ($k == 14) break;
                            if ($k >= 7){
                            ?>
                            <div class="col-md-<?php if ($image['big']) {echo '12';} else {echo '6';}?> col-sm-12 col-xs-12 ">
                                <div class="inner-wrap">
                                    <a class="fancybox" href="/files/attachment-images/<?=$image['image']?>" rel="gallery1">
                                        <img src="/files/attachment-images/<?php if ($image['big']) {echo $image['thumbnail_570x570'];} else {echo $image['thumbnail_270x270'];}?>" alt="">
                                    </a>
                                </div>
                            </div>
                        <?php }
                        } ?>

                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
