<footer class="footer">
    <div class="footer-btm">
        <div class="wrapper">
            <div class="footer-btm__links">
                <ul>
                    <li><p class="copy-text"><span class="copy">&copy;</span><?=$settings['bf587ceef5']?></p></li>
                    <li><a href="<?=$this->Url->build(['controller' => 'Pages', 'action' => 'politics'])?>"><?= __('Политика конфиденциальности'); ?></a></li>
                    <li><a href="<?=$this->Url->build(['controller' => 'Misc', 'action' => 'sitemap'])?>"><?= __('Карта сайта'); ?></a></li>
                    <li class="siluet-li">
                        <p class="siluet">
                            <a target="_blank" href="http://siluetstudio.com"><?=$settings['copyright']?></a>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div id="call-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="img/cross.png"></button>
                <h4 class="modal-title">Заказать звонок</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <label for="name2">ФИО<span>*</span></label>
                <input class="modal-input" id="name2">
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input" id="organization2">
                <div class="btns">
                    <div class="row">
                        <div class="col-md-9">
                            <label class="checkbox">
                                <input type="checkbox">
                                <div class="checkbox__text">Я соглашаюсь с <a href="#">Политикой
                                        конфиденциальности</a></div>
                            </label>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="btn btn-modal">Отправить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
