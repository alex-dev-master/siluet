<div class="page-content sitemap-page">
    <div class="container">
        <div class="col-12">
<h3 class="page-title">Карта сайта</h3>
<ul>
    <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'mainpage']); ?>">Главная</a></li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'Abouts', 'action' => 'index']); ?>">Кто мы</a></li>
    <li>
        <a href="<?php echo $this->Url->build(['controller' => 'Services', 'action' => 'index']); ?>">Что мы делаем</a>
        <ul>
        <?php foreach ($services as $service) {?>
            <li><a href="<?=$this->Url->build(['controller' => 'Services', 'action' => 'view', $service['slug']]); ?>"><?=$service['caption']?></a></li>
        <?php } ?>
        </ul>
    </li>
    <li>
        <a href="<?php echo $this->Url->build(['controller' => 'Experiences', 'action' => 'index'], true); ?>">Наш опыт</a>
        <ul>
            <?php foreach($experiences as $experience) { ?>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Experiences', 'action' => 'view', $experience['slug']]); ?>"><?=$experience['caption']?></a></li>
            <?php } ?>
        </ul>
    </li>
    <li><a href="<?php echo $this->Url->build(['controller' => 'Contacts', 'action' => 'index']); ?>">Контакты</a></li>
</ul>
        </div>
    </div>
</div>
