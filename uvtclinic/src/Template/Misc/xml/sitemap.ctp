<?= '<?xml version="1.0" encoding="utf-8"?>';?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>/</loc>
        <changefreq>monthly</changefreq>
        <priority>1</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'abouts'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'aboutMedicalActivities'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'regulations'], true) ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'orderRules'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'rightsObligations'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'jobs'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'services'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <?php foreach ($itemRubrics as $itemRubric) { ?>
        <url>
            <loc><?= $this->Url->build(["controller" => "ItemRubrics","action" => "index", $itemRubric['slug']], true) ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.8</priority>
        </url>
        <?php foreach ($itemRubric['items'] as $items) { ?>
            <url>
                <loc><?= $this->Url->build(["controller" => "Items","action" => "view", $items['slug']], true) ?></loc>
                <changefreq>monthly</changefreq>
                <priority>0.8</priority>
            </url>
        <?php } ?>

    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'experts'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <?php foreach ($experts as $expert) { ?>
        <url>
            <loc><?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']],true) ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.8</priority>
        </url>
    <?php } ?>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'timetable'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'costs'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'contacts'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'questionnaire'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'articles'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <?php foreach ($articleRubrics as $articleRubric) { ?>
        <url>
            <loc><?php echo $this->Url->build(['controller' => 'ArticleRubrics', 'action' => 'index'], true); ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.8</priority>
        </url>
            <?php foreach ($articleRubric['articles'] as $article) { ?>
            <url>
                <loc><?php echo $this->Url->build(['controller' => 'Articles', 'action' => 'view', $article['slug']], true); ?></loc>
                <changefreq>monthly</changefreq>
                <priority>0.8</priority>
            </url>
            <?php } ?>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Articles', 'action' => 'reviews'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
</urlset>
