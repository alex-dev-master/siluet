<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use SiluetCms\Traits\FrontEndTrait;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    use FrontEndTrait {
        initialize as protected initializeFrontEnd;
    }

    protected $formMessage = [];

    public function initialize()
    {
        parent::initialize();
        $this->initializeFrontEnd();
        $this->setLang();


        $this->formMessage['notEmpty'] = __('Поле обязательно для заполнения');
        $this->formMessage['emailValidFormat'] = __('Неправильный формат электронной почты');
        $this->formMessage['phoneValidFormat'] = __('Неправильный формат телефона');
        $this->formMessage['privatePolicyRequire'] = __('Вы должны согласиться с политикой конфиденциальности');

    }


    private function setLang()
    {
        $languages = Configure::read('Project.languages');

        $langs = [];
        $url = [
            'controller' => $this->request->getParam('controller'),
            'action' => $this->request->getParam('action')
        ];
        if ($slug = $this->request->getParam('slug')) {
            $url['slug'] = $slug;
        }
        foreach ($languages as $lang => $option) {
            $selected = ($this->getRequest()->getParam('lang') == $option['prefix']);
            $langs[$lang] = [
                'title' => $option['title'],
                'url' => $url + ['lang' => $option['prefix']],
                'selected' => $selected
            ];
        }

        $this->set(compact('langs'));
    }


    protected function sendCakeEmail($emails, $subject, $template, $data)
    {
        $Email = new \Cake\Mailer\Email();
        $Email
            ->setTo($emails)
            ->setFrom('no_reply@' . env('SERVER_NAME'))
            ->setSubject($subject . ' на сайте ' . env('SERVER_NAME'))
            ->setTemplate($template)
            ->emailFormat('both')
            ->setViewVars(compact('data'))
            ->send();
    }


}
