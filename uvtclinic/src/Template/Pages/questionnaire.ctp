<div class="page-content questionnaire-content">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('О клинике'),
            ['controller' => 'Abouts', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <?= $this->Form->create(['schema' => []], ['id' => 'sendQuestion', 'class' => 'form']); ?>
        <ol class="list-ol">
            <?php foreach ($row['attachment_tests'] as $j => $question) {?>
            <li class="list-item">
                <p class="question"><?=$question['caption']?></p>
                <?php
                    $options = preg_split("/[0-9]\s/", $question['options']);
                ?>
                <?php foreach ($options as  $k => $option) { ?>
                <?php if (!empty($option)) { ?>
                <label class="radio">
                    <input type="radio" value="<?=trim($option)?>" name="<?=$question['caption']?>" id="q-<?=$j?>-<?=$k?>" <?php if ($k == 1) echo ' checked';?>>
                    <div class="radio__text"><?=trim($option)?></div>
                </label>
                <?php } ?>
                <?php } ?>
            </li>
            <?php } ?>

            <li class="list-item">
                <p class="question">Мы благодарим Вас за участие! Если Вы хотите оставить предложения по работе данной медицинской организации, пожалуйста, напишите свои предложения</p>
                <label class="textarea-label" for="organization2">Текст сообщения</label>
                <textarea class="textarea" id="organization2" name="Текст" placeholder="Поделитесь вашими мыслями"></textarea>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-form"><img src="/img/mail.png">Отправить</button>
                </div>
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
            </li>

        </ol>
        <?= $this->Form->end(); ?>


    </div>
</div>
