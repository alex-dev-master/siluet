<?php
return [
    /**
     * Debug Level:
     *
     * Production Mode:
     * false: No error messages, errors, or warnings shown.
     *
     * Development Mode:
     * true: Errors and warnings shown.
     */
    'debug' => true,
    /**
	* Force DebugKit to display. This can be used to make sure DebugKit displays on hosts it otherwise determines unsafe
	* See https://book.cakephp.org/3.0/en/debug-kit.html#configuration
	*     https://stackoverflow.com/questions/50108060/cake-php-3-debug-kit-panel
	*/
	'DebugKit' => [
		'forceEnable' => true
	],
    /**
     * Project settings
     */
    'Project' => [
        /**
         * if true all queries automatically cached in client part
         */
        'cacheQueries' => false,
        'cacheRoutes' => false,

        /*'languages' => [
            'ru' => [
                'locale' => 'ru_RU',
                'label' => 'Русский',
                'short' => 'RU',
                'prefix' => false
            ],
            'en' => [
                'locale' => 'en_US',
                'label' => 'English US',
                'short' => 'EN',
                'icon' => 'us',
                'prefix' => 'en',
            ]
        ]*/
    ],
    'Cache' => [
        '_routes_' => [
            'className' => 'File',
            'prefix' => 'siluet_routes_',
            'path' => CACHE . 'routes' . DS,
            'serialize' => true,
            'duration' => '+1 years'
        ]
    ]
];
