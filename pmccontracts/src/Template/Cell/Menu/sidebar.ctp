
        <?php  foreach ($menus as $menu) { ?>
            <?php if (isset($menu['submenu']) && !empty($menu['current']) && $menu['current']) { ?>
            <?php foreach ($menu['submenu'] as $sidebar) { ?>
                <li class="<?php if (!empty($sidebar['current'])) { echo 'active'; } ?>"><a href="<?= $this->Url->build($sidebar['url']); ?>"><?= $sidebar['caption'] ?></a></li>
            <?php } ?>
            <?php } ?>
        <?php } ?>

