<div class="sitemap">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <div class="text">
            <h1>Карта сайта</h1>
            <ul class="sitemap-list">
                <li><a href="/"><?=__('Главная');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'abouts']); ?>"><?=__('О клинике');?></a>
                    <ul>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'aboutMedicalActivities']); ?>"><?=__('О медицинской деятельности');?></a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'regulations']); ?>"><?=__('Нормативные документы');?></a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'orderRules']); ?>"><?=__('Правила внутреннего распорядка');?></a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'rightsObligations']); ?>"><?=__('О правах и обязанностях граждан в сфере охраны здоровья');?></a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'jobs']); ?>"><?=__('Вакансии');?></a></li>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'services']); ?>"><?=__('Услуги');?></a>
                    <ul>
                        <?php foreach ($itemRubrics as $itemRubric) { ?>
                            <li><a href="<?= $this->Url->build(["controller" => "ItemRubrics","action" => "index", $itemRubric['slug']]) ?>"><?=$itemRubric['caption'];?></a>
                                <ul>
                                    <?php foreach ($itemRubric['items'] as $items) { ?>
                                        <li>
                                            <a href="<?= $this->Url->build(["controller" => "Items","action" => "view", $items['slug']]) ?>"><?=$items['caption'];?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'experts']); ?>"><?=__('Наши специалисты');?></a>
                    <ul>
                        <?php foreach ($experts as $expert) { ?>
                            <li><a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>"><?=$expert['caption'];?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'timetable']); ?>"><?=__('Расписание приема');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'costs']); ?>"><?=__('Стоимость');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'contacts']); ?>"><?=__('Контакты');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'questionnaire']); ?>"><?=__('Оценка качества обслуживания');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'articles']); ?>"><?=__('Полезная информация');?></a>
                    <ul>
                        <?php foreach ($articleRubrics as $articleRubric) { ?>
                            <li><a href="<?= $this->Url->build(["controller" => "ArticleRubrics","action" => "index", $articleRubric['slug']]) ?>"><?=$articleRubric['caption'];?></a>
                                <ul>
                                    <?php foreach ($articleRubric['articles'] as $article) { ?>
                                        <li>
                                            <a href="<?= $this->Url->build(["controller" => "Articles","action" => "view", $article['slug']]) ?>"><?=$article['caption'];?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'reviews']); ?>"><?=__('Отзывы');?></a></li>
            </ul>
        </div>
    </div>
</div>

