<?= '<?xml version="1.0" encoding="utf-8"?>';?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>/</loc>
        <changefreq>monthly</changefreq>
        <priority>1</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Abouts', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Manufactures', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Deliveries', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Shops', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Portfolios', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach ($items as $item) { ?>
        <url>
            <loc><?= $this->Url->build(["controller" => "Portfolios","action" => "index", $item['slug']], true) ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.7</priority>
        </url>

        <?php foreach ($item['items'] as $product) { ?>
            <url>
                <loc><?= $this->Url->build(["controller" => "Portfolios","action" => "view", $product['slug']], true) ?></loc>
                <changefreq>monthly</changefreq>
                <priority>0.6</priority>
            </url>
        <?php } ?>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Services', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach ($services as $service) { ?>
        <url>
            <loc><?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']], true) ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Works', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Questions', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Contacts', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Jobs', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <?php foreach ($jobs as $job) { ?>
        <url>
            <loc><?= $this->Url->build(["controller" => "Jobs","action" => "view", $job['slug']], true) ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>

    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Blogs', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

    <?php foreach ($blogs as $blog) { ?>
        <url>
            <loc><?= $this->Url->build(["controller" => "Blogs","action" => "view", $blog['slug']], true) ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Reviews', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>

</urlset>
