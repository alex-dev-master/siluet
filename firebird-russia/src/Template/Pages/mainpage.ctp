<?= $this->element('slider', [$sliders])?>

<div class="main-icons">
    <div class="wrapper">
        <div class="row list">
            <?php foreach ($AttachmentAdvantages as $attachment_advantage) { ?>
            <div class="col-md-2 col-md-4 col-xs-6">
                <div class="item">
                    <div class="img-container">
                        <img src="/files/attachment-advantages/<?=$attachment_advantage['image']?>">
                    </div>
                    <div class="text">
                        <p><?=$attachment_advantage['caption']?></p>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php foreach ($category_items as $k=>$category) { ?>
<div class="main-product" style="background: radial-gradient(400px circle at 67% 50%, #<?=$category['color']?>, #<?=$category['color2']?>);">
    <div class="wrapper parent">
        <div class="row child">
            <div class="<?php if ($k%2 == 0) {echo 'col-md-6  col-sm-6';} else {echo 'col-md-6 col-md-push-6  col-sm-6 col-sm-push-6';} ?>">
                <div class="img-container">
                    <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "index", $category['slug']]) ?>"> <img src="/files/item-rubrics/<?=$category['image']?>"></a>
                </div>
            </div>
            <div class="<?php if ($k%2 == 0) {echo 'col-md-6 col-sm-6';} else {echo 'col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6';} ?>">
                <div class="text ">
                    <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "index", $category['slug']]) ?>"><p class="title"><?=$category['caption']?></p></a>
                    <p class="special"><?=$category['content']?></p>
                    <a href="#order-modal" data-toggle="modal" class="btn order-btn"><?=__('Заявка на расчет')?></a>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="products matr-products">
    <div class="wrapper">
        <div class="row list">
            <?php
            $items_count = count($category['items'])-1;
            $limit = 4;
            if ($items_count >= 3) {
            $max_num = $items_count;
            $used_nums = array();
            while (1) {
            $item = rand(0, $max_num);
            if (!in_array($item, $used_nums)) {
            $used_nums[] = $item;
        }
        if (count($used_nums) == $limit) {
        break;
    }
} ?>
<?php
foreach ($used_nums as $num) { ?>
<?php if ($category['items'][$num]['to_main']) { ?>
<div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
    <div class="item">

            <div class="img-container">
             <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "view", $category['items'][$num]['slug']]) ?>">
                <img src="/files/items/<?=$category['items'][$num]['thumbnail_226x226']?>">
                <div class="more-arrow" style="background: #<?=$category['color']?>"><img src="/img/arr-w.png"></div>
                </a>
            </div>
            <div class="name">
                <p><?=$category['items'][$num]['caption']?></p>
            </div>

    </div>
</div>
<?php } ?>
<?php }?>
<?php } else { ?>
<?php foreach ($category['items'] as $item) { ?>
<?php if ($item['to_main']) { ?>
<div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
    <div class="item">

        <div class="img-container">
            <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "view", $item['slug']]) ?>">
                <img src="/files/items/<?=$item['thumbnail_226x226']?>">
                <div class="more-arrow" style="background: #<?=$category['color']?>"><img src="/img/arr-w.png"></div>
            </a>
        </div>
        <div class="name">
            <p><?=$item['caption']?></p>
        </div>

    </div>
</div>
<?php } ?>
<?php } ?>
<?php }  ?>

</div>
</div>
</div>
<?php } ?>


<div class="how-works">
    <div class="wrapper">
        <h2><?=__('Как мы работаем?')?></h2>
        <div class="how-works__list">
            <div class="row list">
                <?php foreach ($works as $work) { ?>
                <div class="item">
                    <div class="img-container">
                        <img src="/files/works/<?=$work['image_icon']?>">
                    </div>
                    <div class="text">
                        <p><?=$work['caption_3']?></p>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="clients">
    <div class="wrapper">
        <h2><?=__('Наши клиенты')?></h2>
        <div class="clients__slider-block">
            <div class="clients__slider">
                <?php foreach ($clients as $client) { ?>
                <div class="slide">
                    <a <?php if (!empty($client['url'])) { ?>href="<?=$client['url']?>" <?php } ?>>
                        <div class="img-container">
                            <img src="/files/clients/<?=$client['image']?>">
                        </div>
                    </a>
                    <div class="text">
                        <a <?php if (!empty($client['url'])) { ?>href="<?=$client['url']?>" <?php } ?>><p><?=$client['caption']?></p></a>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="clients__slider__nav">
                <a href="#" class="slide-nav-arrow left"><img src="/img/jumb-arr-l.png"> </a>
                <a href="#" class="slide-nav-arrow right"><img src="/img/jumb-arr-r.png"> </a>
            </div>
        </div>
    </div>
</div>
<div class="about">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="text">
                    <h2><?=__('О компании')?></h2>
                    <?=$page['content']?>
                </div>
                <a href="/abouts" class="btn arr-btn"><?=__('Подробнее')?></a>
                <a href="/reviews" class="btn about-test-btn"><?=__('Отзывы')?></a>
            </div>
            <div class="col-md-6  col-sm-6">
                <div class="img-container">
                    <img src="/files/pages/<?=$page['image']?>">
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>
<div id="back-top">
    <a href="#top"><img src="/img/up.png"> </a>
</div>

