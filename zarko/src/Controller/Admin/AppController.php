<?php
namespace App\Controller\Admin;

//use App\Traits\ControllerTrait;
use SiluetCms\Traits\ControllerTrait;
use Cake\Controller\Controller;

class AppController extends Controller {
    use ControllerTrait;
}