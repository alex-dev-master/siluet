<div class="page-content about-content about-doc-content">
    <img class="content-bg" src="/files/pages/<?=$row['thumbnail_1440x540']?>" alt="">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('О клинике'),
            ['controller' => 'Abouts', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <?=$row['content']?>
        <ul class="list-ul">
            <?php foreach ($row['attachment_files'] as $file) { ?>
            <li class="list-item">
                <h5 class="doc-title"><?=$file['caption']?></h5>
                <a class="doc-link" href="/files/attachment-files/<?=$file['file']?>" download><?=$file['file']?></a>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
