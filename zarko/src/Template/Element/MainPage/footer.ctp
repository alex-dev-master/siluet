<footer class="footer footer-main">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a href="mailto:<?= $settings['email'] ?>" class="footer-main__link"><?= $settings['email'] ?></a>
                <a href="tel:<?= $settings['phone'] ?>" class="footer-main__link"><?= $settings['phone'] ?></a>
            </div>
        </div>
    </div>
</footer>
