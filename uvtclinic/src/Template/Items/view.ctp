<div class="page-content services-content services-detail-content">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            $rubric_row['caption'],
            ['controller' => 'ItemRubrics', 'action' => 'index', $rubric_row['slug']]
        );
        $this->Breadcrumbs->prepend(
            __('Услуги'),
            ['controller' => 'Pages', 'action' => 'services']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$rubric_row['caption']?>: <br><?=$row['caption']?></h1>
        <img class="main-img" src="/files/items/<?=$row['thumbnail_1170x480']?>" alt="">
        <div class="services_category-desc">
            <?=$row['content_full']?>
        </div>
    </div>
    <?php if (!empty($row['content_advantages'])) { ?>
    <div class="services_category-plus">
        <div class="face">
            <img class="face-img" src="/img/face.png" alt="">
        </div>
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-5 col-md-push-5 col-sm-6 col-sm-push-6 col-xs-12">
                    <div class="inner-wrapper">
                        <?=$row['content_advantages']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="wrapper">
        <?php if (!empty($cost)) { ?>
            <div class="services_category-price">
            <h2 class="title"><?=$rubric_row['caption_sub']?></h2>
            <table>
                <tr>
                    <th>Наименование услуги</th>
                    <th>Стоимость, руб.</th>
                </tr>
                <?php foreach ($cost as $cost_item) { ?>
                <tr>
                    <td>
                        <p class="main-text"><?=$cost_item['caption']?></p>
                    </td>
                    <td>
                        <p class="price"><?=$cost_item['cost']?></p>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php } ?>
    </div>
    <?php if (!empty($row['experts'])) { $experts = $row['experts']; ?>
    <div class="services_category-workerList">
        <div class="wrapper">
            <h2 class="title">Наши специалисты</h2>
            <div class="row list">
                <?php foreach ($experts as $expert) { ?>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="inner-container">
                        <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                            <img class="worker-img" src="/files/experts/<?=$expert['thumbnail_210x210']?>" alt="">
                            <div class="text-wrap">
                                <h6 class="worker-title"><?=$expert['caption']?></h6>
                                <p class="worker-desc">
                                    <?php foreach ($expert['specialities'] as  $specialities) {?>
                                        <?=$specialities['caption']?> <br>
                                    <?php } ?>
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (!empty($attachment_infos)) {?>
        <div class="wrapper">
            <div class="services_category-articles">
                <h3 class="title">Полезно знать</h3>
                <div class="row list">
                    <?php foreach ($attachment_infos as $attachment_info) { ?>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-container">
                                <a href="<?= $attachment_info['link'] ?>">
                                    <img class="articles-img" src="/files/attachment-infos/<?=$attachment_info['image']?>" alt="">
                                    <div class="text-wrap">
                                        <h5 class="articles-title"><?=$attachment_info['caption']?></h5>
                                        <p class="articles-desc"><?=$attachment_info['content']?></p>
                                        <p class="more-title">Подробнее</p>
                                        <div class="moreBtn more-button"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    <?php } ?>
</div>
