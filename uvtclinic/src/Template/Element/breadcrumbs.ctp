<?php
$this->Breadcrumbs->templates([
    'wrapper' => '<div{{attrs}} itemscope="" itemtype="http://schema.org/BreadcrumbList"><ul>{{content}}</ul></div>',
    'separator' => "\n" . '<li{{attrs}}><span class="sep">{{separator}}</span></li>' . "\n"
]);
$this->Breadcrumbs->prepend(__('Главная'), '/');
if (!empty($breadcrumbs)):
    $this->Breadcrumbs->add($breadcrumbs);
endif;
if (!empty($caption_for_layout)):
    $this->Breadcrumbs->add($caption_for_layout);
endif;
echo $this->Breadcrumbs->render(['class' => 'breadcrumbs'], ['separator' => '&gt;']);