<?php
$text = $this->Html->tag('p', "Здравствуйте! С сайта {$_SERVER['HTTP_HOST']} поступило новое сообщение! С формы расчета стоимости изготовления");
$fields = [
    'url' => 'С адреса',
    'name' => 'ФИО',
    'phone' => 'Телефон',
    'email' => 'Электронная почта',
    'msg' => 'Текст сообщения',
    'changes' => 'Фильтр',
    'zagotovka' => 'Вид заготовки',
    'persons' => 'Количество',
    'WhatsApp' => 'WhatsApp',
    'Viber' => 'Viber',
    'Tel' => 'Связь через телефон',
];
$text .= '<p>';
foreach ($fields as $field => $name):
    if (!empty($data[$field])):
        $separate = ($field != 'message' ? ': ' : $this->Html->tag('br'));

        $text .= $name . $separate . $this->Html->tag('b', $data[$field]) . $this->Html->tag('br');
    endif;
endforeach;
$text .= '</p>';
#
echo $text;
