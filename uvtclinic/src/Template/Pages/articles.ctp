<div class="page-content about-content about-articles-content">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <div class="border-top">
            <h1>Полезная информация</h1>
            <div class="articles-buttons">
                <a class="btn article-button article-active-button">Все статьи</a>
                <?php foreach ($articleRubrics as $articleRubric) { ?>
                    <a href="<?= $this->Url->build(["controller" => "ArticleRubrics", "action" => "index", $articleRubric['slug']]) ?>" class="btn article-button"><?=$articleRubric['caption']?></a>
                <?php } ?>
            </div>
            <div class="publ-list">
                <div class="row list">
                    <?php foreach ($articles as $article) { ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-container patient">
                            <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>">
                                <img class="publ-img" src="/files/articles/<?=$article['thumbnail_370x260']?>" alt="">
                            </a>
                            <div class="text-wrap">
                                <div class="btn sml-btn" style="background-color: #<?=$article['article_rubric']['color']?>"><?=$article['article_rubric']['caption']?></div>
                                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>">
                                    <h6 class="publ-title"><?=$article['caption']?></h6>
                                </a>
                                <p class="publ-date sml-text"><?=$article->published->format('d / m / Y')?></p>
                                <a class="moreBtn" href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>"></a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?= $this->element('pagination') ?>

        </div>
    </div>
</div>
