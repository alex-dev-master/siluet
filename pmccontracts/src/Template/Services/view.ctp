<div class="page-top sub">
    <div class="wrapper">
        <p class="subheading"><?=__('Услуги')?></p>
        <h1><?=$services_page['caption']?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                <div class="content__left-col">
                    <ul class="left-menu">
                        <?= $this->cell("Menu::services", [$langs, $settings]) ?>
                    </ul>
                    <div class="slogan"><p><?=__('Мы предлагаем')?></p></div>
                </div>

            </div>
            <div class="col-md-8">
                <h2><?=$services_page['caption_content']?></h2>
                <div class="text">
                    <?=$services_page['content']?>
                </div>
                <?php if (!empty($services[0]['objects'])) { ?>
               
                <div class="project-list">
                 <h3><?=__('Проекты данного направления')?></h3>
                    <div class="row list">
                        <?php foreach ($services[0]['objects'] as $object) {?>
                        <div class="col-md-6 col-sm-6 col-xs-12 project-col">
                            <div class="project-item">
                                <a href="<?= $this->Url->build(['controller' => 'Projects', 'action' => $object['slug']]); ?>">
                                    <div class="project-item-img-container">
                                        <img src="/files/objects/<?=$object['image']?>">
                                        <p class="status <?php if ($object['archive_status']){echo 'finish';}else{echo 'work';}?>"><?php if ($object['archive_status']) { echo __('Завершен'); } else { echo __('В работе'); } ?></p>
                                    </div>
                                    <div class="project-item__text">
                                        <p class="title"><?=$object['caption']?></p>
                                        <p class="count"><?=$object['city']?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
