<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemRubrics Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\HasMany $Items
 *
 * @method \App\Model\Entity\ItemRubric get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemRubric newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemRubric[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemRubric patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric findOrCreate($search, callable $callback = null, $options = [])
 */
class ArticleRubricsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('article_rubrics')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->setOrder(['ArticleRubrics.position' => 'DESC', 'ArticleRubrics.id' => 'DESC'])
            ->setFormSchema([
                'color' => ['label' => 'Цвет', 'class' => 'color-picker'],
            ]);
        $this->setTabSchema([

        ]);

        $this->addBehavior('Tree');
        $this->addBehavior('Timestamp');


        $this->hasMany('Articles', [
            'foreignKey' => 'article_rubric_id'
        ]);

    }

}
