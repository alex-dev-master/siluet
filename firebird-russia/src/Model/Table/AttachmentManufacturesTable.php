<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentImages Model
 *
 * @method \App\Model\Entity\AttachmentImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AttachmentManufacturesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachment_manufactures');
        $this->setDisplayField('caption')
            ->setDisplayImage('image')
            //->newPositionLast(true)
            ->setOrder(['AttachmentManufactures.position' => 'DESC', 'AttachmentManufactures.id' => 'DESC'])
            ->setNeighborhood([]);
        $this
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'max', 'x' => 150, 'y' => 150], 'type' => 'image'],
                'big_text_status' => ['label' => 'Включить расширенный текст'],
                'big_text_image' => ['label' => 'Изоображение', 'type' => 'image', 'params' => ['resize' => 'crop', 'x' => 1060, 'y' => 540]],
                'big_text_video' => ['label' => 'Видео'],
                'big_text' => ['label' => 'Текст после картинки', 'type' => 'wysiwyg'],
                'big_text_2' => ['label' => 'Текст после видео', 'type' => 'wysiwyg'],
            ]);
        $this->setPrimaryKey('id');

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'big_text', 'big_text_2']
        ]);

    }

}
