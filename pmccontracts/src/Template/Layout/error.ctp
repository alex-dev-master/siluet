<?php
/**
 * settings block
 */
if (isset($settings)) {
    foreach (['code_bh', 'code_ab', 'code_bb'] as $prop) {
        $this->start($prop);
        echo $settings->get($prop);
        $this->end();
    }
}
$this->Html->css('/css/slick/slick-theme', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/jquery-ui', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/daterangepicker', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/jquery.timepicker.min', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/slick/slick', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/owl.theme.default', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/owl.carousel', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/fancybox/jquery.fancybox', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/animate', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/foundation/selectric', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/style', ['type' => 'text/css', 'media' => 'all', 'block' => true]);

$this->append('js', $this->Html->script('jquery-3.2.1.min'));
$this->append('js', $this->Html->script('owl.carousel'));
$this->append('js', $this->Html->script('owl/owl.support'));
$this->append('js', $this->Html->script('owl/owl.autoplay'));
$this->append('js', $this->Html->script('autoplay'));
$this->append('js', $this->Html->script('bootstrap.min'));
$this->append('js', $this->Html->script('jquery.maskedinput'));
//$this->append('js', '/css/fancybox/jquery.fancybox');
$this->append('js', $this->Html->script('jquery.scrollbar'));
$this->append('js', $this->Html->script('jquery.selectric'));
$this->append('js', $this->Html->script('slick.min'));
$this->append('js', $this->Html->script('truncate.min'));
$this->append('js', $this->Html->script('jquery-ready'));
$this->append('js', $this->Html->script('common'));

?>

<!doctype html>
<html lang="ru">
<head>
    <title>Ошибка 404</title>
    <?php if (isset($description_for_layout) and !empty($description_for_layout)) {
        echo $this->Html->meta('description', $description_for_layout);
    } ?>
    <?php if (isset($keywords_for_layout) and !empty($keywords_for_layout)) {
        echo $this->Html->meta('keywords', $keywords_for_layout);
    } ?>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($href = $settings->get('favicon-uploaded')) { ?>
    <?= $this->Html->meta('icon', $href['url']) ?>
    <?php } ?>
    <?= $this->fetch('css') ?>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <?= $this->fetch('code_bh') ?>
</head>
<body>
    <?= $this->fetch('code_ab') ?>
    <?= $this->element('header', [$langs,$settings])?>
    <div class="content min-height">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12  col-sm-12">
                    <h1>Ошибка 404</h1>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('footer')?>
    <?= $this->fetch('js') ?>
    <script type="text/javascript" src="/css/fancybox/jquery.fancybox.js"></script>
    <script>
        $('.owl-carousel').owlCarousel({
        // animateOut: 'slideOutDown',
        // animateIn: 'flipInX',
        items: 1,
        touchDrag: false,
        pullDrag: false,
        nav: true,
        navSpeed: 3000,
        animateOut: 'My-zoomOutDown',
        animateIn: 'My-zoomInDown',
        // transitionStyle: "fade",
        smartSpeed: 4150
    });
</script>
<?= $this->fetch('code_bb') ?>
</body>
</html>
