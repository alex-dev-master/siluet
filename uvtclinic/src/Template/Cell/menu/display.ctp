<div class="header-btm">
    <div class="wrapper">
        <div class="header-btm__wrapper">
            <ul class="header-menu">
                <?php foreach ($menus as $menu) {?>
                    <?php if (!empty($menu['submenu'])) { ?>
                        <li class="drop-menu <?php if (!empty($menu['current'])) echo 'active-link'?>">
                            <a href="<?= $this->Url->build(["controller" => $menu['url']['controller'],"action" => $menu['url']['action']]) ?>"><?=$menu['caption']?></a>
                            <div class="submenu-block">
                                <ul class="submenu">
                                    <?php foreach ($menu['submenu'] as $submenu) {
                                        if (!empty($submenu['url']['slug'])) {
                                            $url = ["controller" => $submenu['url']['controller'],"action" => $submenu['url']['action'], $submenu['url']['slug']];
                                        } else {
                                            $url = ["controller" => $submenu['url']['controller'],"action" => $submenu['url']['action']];
                                        }
                                        ?>
                                    <li><a href="<?= $this->Url->build($url) ?>"><?=$submenu['caption']?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </li>
                    <?php } else { ?>
                        <li <?php if (!empty($menu['current'])) echo 'class="active-link"'?>><a href="<?= $this->Url->build(["controller" => $menu['url']['controller'],"action" => $menu['url']['action']]) ?>"><?=$menu['caption']?></a></li>
                    <?php } ?>
                <?php } ?>
            </ul>
            <a class="btn lightgrey-btn header-btmBtn" href="#appointment-modal" data-toggle="modal">Записаться на прием</a>
        </div>
    </div>
</div>
