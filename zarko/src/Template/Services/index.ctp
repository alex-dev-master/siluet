<div class="page-content service-page">
    <div class="container">
        <p class="page-title">Что мы делаем</p>
        <div class="service-page__text">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="h1"><?= $page['caption'] ?></div>
                </div>
                <div class="col-12 col-md-6 col-lg-8">
                    <div class="wysiwyg">
                        <?= $page['content'] ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- slider -->
        <div class="slider">
            <div class="slider-wrap">
                <?php foreach ($page->attachment_images as $slider_img) { ?>
                <div class="slider-item">
                    <img src="/files/attachment-images/<?= $slider_img['thumbnail_1160x530'] ?>" alt="">
                </div>
                <?php } ?>
            </div>
            <div class="slider-controls">
                <!-- prev -->
                <a href="" class="prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"/></svg></a>
                <!-- prev end -->

                <!-- next -->
                <a href="" class="next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></a>
                <!-- next end -->
            </div>
        </div>
        <!-- slider end -->

        <!-- services -->
        <div class="service-page__list icon-block">
            <?php if (isset($services)) { $j = 0;?>
                <?php foreach ($services as $i=>$service) { ?>
                   <?php if ($i%2 == 0) { echo '<div class="row">'; }?>
                    <div class="col-12 col-md-6">
                        <div class="service-page__item icon-block__item icon-block__left">
                            <div class="icon-block__img">
                                <?php if (isset($service['icon'])){ ?>
                                    <?php $icon = @file_get_contents('./'.$service['icon-uploaded']['url']); if (!empty($icon)) echo $icon;?>
                                <?php } ?>
                            </div>
                            <div class="icon-block__content">
                                <a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']]) ?>"><p class="title"><?=$service['caption']?></p></a>
                                <p class="description"><?=$service['short_description']?></p>
                                <a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']]) ?>" class="btn btn-default btn-with-icon">
                                    Подробнее
                                    <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"></path></svg></span>
                                </a>
                            </div>
                        </div>
                    </div>
                   <?php if ($i%2 != 0) { echo '</div>'; }?>
                <?php } ?>
            <?php } ?>
        </div>
        <!-- services end -->
    </div>


</div>

<div id="contact_us" class="page-content__bottom page-content__section withBg" style="background-image: url(/img/service_bg.png);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-8 col-xl-9">
                <div class="page-content__title">У вас есть интересный проект?</div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                <a href="#service-page__form" data-toggle="collapse" class="btn btn-primary btn-block btn-lg">Свяжитесь с нами!</a>
            </div>
        </div>
    </div>
</div>

<?= $this->Form->create(['schema' => []], ['id' => 'contacts']); ?>
<div id="service-page__form" class="service-page__form collapse">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Ваше имя <sup>*</sup></label>
                    <input type="text" name="name" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Электронная почта <sup>*</sup></label>
                    <input type="text" name="email" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Телефон</label>
                    <input type="text" name="phone" class="form-control">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="label">Текст сообщения <sup>*</sup></label>
                    <textarea rows="2" name="text" class="form-control"></textarea>
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="privacy-policy">
                        <span class="check"></span>
                        Я соглашаюсь с <a href="/privacy-policy">Политикой конфиденциальности</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 text-left text-md-right">
                <button class="btn btn-primary btn-with-icon">Отправить <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></button>
            </div>
        </div>
        <div class="success success-msg">
            <div class="container">
                <div class="success-msg__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><polygon points="37.1,44.6 34.2,47.3 47.8,61.9 79.7,29.9 76.9,27.1 47.8,56.2 "/><path d="M18.6,50c0,17.4,14.1,31.4,31.4,31.4S81.4,67.4,81.4,50h-4c0,15.1-12.3,27.4-27.4,27.4S22.6,65.1,22.6,50S34.9,22.6,50,22.6  v-4C32.7,18.6,18.6,32.7,18.6,50z"/></svg>
                </div>
                <div class="success-msg__title">Ваше сообщение отправлено!</div>
            </div>
        </div>
<!--        <h4 class="success" style="color: green;float: right; display: none ">Ваше сообщение отправлено!</h4>-->
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- page wrap end -->
