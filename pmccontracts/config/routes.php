<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

/*Router::scope('/admin', ['prefix' => 'admin'], function (RouteBuilder $routes) {
	$routes->connect('/:controller')->setPatterns(['controller' => '[a-z0-9_]+']);
});*/

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $defaultLocale = Configure::read('App.defaultLocale');
    foreach (Configure::read('Project.languages') as $lang => $options) {
        if ($lang == $defaultLocale or isset($options['locale']) and $options['locale'] == $defaultLocale) {
            $lang = null;
        }

    Router::scope('/' . $lang, ['lang' => $lang], function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'mainpage']);
    $routes->connect('/contacts', ['controller' => 'Contacts', 'action' => 'index']);
    $routes->connect('/politics', ['controller' => 'Pages', 'action' => 'politics']);
    $routes->scope('/services', function (RouteBuilder $routes) {
        $routes->connect('/:slug', ['controller' => 'Services', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
    });

    $routes->scope('/news', function (RouteBuilder $routes) {
        $routes->connect('/:slug', ['controller' => 'News', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
    });

    $routes->scope('/projects', function (RouteBuilder $routes) {
        $routes->connect('/:slug/*', ['controller' => 'Projects', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
        $routes->connect('/getObjects', ['controller' => 'Projects', 'action' => 'getObjects'])->setExtensions(['json']);
    });

    $routes->scope('/catalog', function (RouteBuilder $routes) {
        $routes->connect('/item/:slug', ['controller' => 'Items', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
        $routes->connect('/items/:slug/*', ['controller' => 'Items', 'action' => 'index'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);

        $routes->connect('/rubric/:slug', ['controller' => 'ItemRubrics', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
        $routes->connect('/', ['controller' => 'ItemRubrics', 'action' => 'index']);
    });

        $routes->connect('/sitemap', ['controller' => 'Misc', 'action' => 'sitemap']);
        $routes->connect('/sitemap.xml', ['controller' => 'Misc', 'action' => 'sitemap', 'xml' => true]);

        $routes->fallbacks(DashedRoute::class);
    });
    }



    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

Router::addUrlFilter(function ($params, $request) {
    if (!isset($params['lang'])) {
        $params['lang'] = null;
    }
    if (!$request instanceof \Cake\Http\ServerRequest) {
        return $params;
    }
    if ($request->getParam('lang') and $params['lang'] !== false) {
        $params['lang'] = $request->getParam('lang');
    }
    return $params;
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
