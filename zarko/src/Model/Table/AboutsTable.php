<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;


class AboutsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('abouts');

        $this
            ->setPrimaryKey('id')
            ->setDisplayField('caption');


        $this
//            ->addBehavior('Translate', ['fields' => ['caption', 'content', 'meta_title', 'meta_keywords', 'meta_description']])
            ->addBehavior('Timestamp');

        $this
            ->setFormSchema([
                'advantages_title_1' => ['label' => 'Заголовок' , 'tab' => 'advantages'],
                'advantages_img_1' => ['label' => 'Картинка', 'type' => 'image', 'tab' => 'advantages', 'params' => ['resize' => 'crop', 'x' => 36, 'y' => 36]],

                'advantages_title_2' => ['label' => 'Заголовок' , 'tab' => 'advantages'],
                'advantages_img_2' => ['label' => 'Картинка', 'type' => 'image', 'tab' => 'advantages', 'params' => ['resize' => 'crop', 'x' => 36, 'y' => 36]],

                'advantages_title_3' => ['label' => 'Заголовок' , 'tab' => 'advantages'],
                'advantages_img_3' => ['label' => 'Картинка', 'type' => 'image', 'tab' => 'advantages', 'params' => ['resize' => 'crop', 'x' => 36, 'y' => 36]],

                'advantages_title_4' => ['label' => 'Заголовок' , 'tab' => 'advantages'],
                'advantages_img_4' => ['label' => 'Картинка', 'type' => 'image', 'tab' => 'advantages', 'params' => ['resize' => 'crop', 'x' => 36, 'y' => 36]],

                'certificate_title' => ['label' => 'Заголовок', 'tab' => 'certificates'],
                'certificate_text' => ['label' => 'Текст', 'tab' => 'certificates', 'type' => 'wysiwyg']

            ])
            ->setTabSchema([
                'video' => array('label' => 'Видео', 'icon' => 'fa-video-camera'),
                'advantages' => ['label' => 'Преимущества'],
                'attachment_images' => ['label' => 'Галерея сертификатов', 'icon' => 'fa-photo'],
                'attachment_brends' => ['label' => 'Галерея брендов', 'icon' => 'fa-photo'],
                'certificates' => ['label' => 'Опыт и ответственность']
            ]);

            $this->hasMany('AttachmentImages', [
                 'dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Abouts']
             ]);

             $this->hasMany('AttachmentBrends', [
                  'dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentBrends.model' => 'Abouts']
             ]);


    }
}
