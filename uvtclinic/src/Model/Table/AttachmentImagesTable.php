<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentImages Model
 *
 * @method \App\Model\Entity\AttachmentImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AttachmentImagesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachment_images');
        $this->setDisplayField('caption')
                ->setDisplayImage('thumbnail')
                //->newPositionLast(true)
                ->setOrder(['AttachmentImages.position' => 'DESC', 'AttachmentImages.id' => 'DESC'])
                ->setNeighborhood(['model', 'foreign_key']);
        $this->setFormSchema([
            'image' => ['params' => ['copy' => '']],
            'thumbnail_270x270' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 270, 'y' => 270, 'source' => 'image']],
            'thumbnail_570x570' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 570, 'y' => 570, 'source' => 'image']],
            'big' => ['label' => 'Большое фото']
        ]);
    }
}
