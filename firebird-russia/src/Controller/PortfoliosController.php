<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Database\Query;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PortfoliosController extends AppController
{
    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $portfolio = TableRegistry::getTableLocator()->get('Portfolios')->find()->first();
        $this->Title->setMeta($portfolio, 'Portfolios');

//        $rubrics_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['ParamRubrics' => ['Params']])->toArray();

         $sub_caption_filter = [];
         $ids_params = [];

         if (!empty($rubric_id)) {
             $rubrics_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['ParamRubrics' => ['Params']])->where(['ItemRubrics.slug' => $rubric_id])->toArray();
             $this->Title->setMeta($rubrics_items[0], 'Portfolios');
             foreach ($rubrics_items[0]['param_rubrics'] as $k=>$param_rubrics) {
                 $sub_caption_filter[$k]['caption'] = $param_rubrics['caption'];
                 foreach ($param_rubrics['params'] as $i=>$param) {
                     $sub_caption_filter[$k]['params'][$i] = $param;
                     array_push($ids_params, $param['id']);
                 }
             }

             $items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()
                 ->where(['ItemRubrics.slug' => $rubric_id])
                 ->contain('Items.Params', function (Query $q) use ($ids_params){
                 return $q
                     ->where(['Params.id IN' => $ids_params]);
             })->toArray();
             $items = $items[0]['items'];
             $rubrics_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['ParamRubrics' => ['Params'], 'AttachmentBlanks'])->toArray();
         } else {
             $rubrics_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['ParamRubrics' => ['Params'], 'AttachmentBlanks'])->toArray();
             foreach ($rubrics_items[0]['param_rubrics'] as $k=>$param_rubrics) {
                 $sub_caption_filter[$k]['caption'] = $param_rubrics['caption'];
                 foreach ($param_rubrics['params'] as $i=>$param) {
                     $sub_caption_filter[$k]['params'][$i] = $param;
                     array_push($ids_params, $param['id']);
                 }
             }
             if (!empty($ids_params)) {
                 $items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain('Items.Params', function (Query $q) use ($ids_params) {
                     return $q
                         ->where(['Params.id IN' => $ids_params]);
                 })->toArray();
                 $items = $items[0]['items'];
             }
         }


        $this->set(compact('portfolio', 'rubrics_items', 'items', 'sub_caption_filter', 'rubric_id'));
    }

    public function view($id, $query = [], $merge_query = true)
    {
        $item = TableRegistry::getTableLocator()->get('Items')->find()->where(['Items.slug' => $id])->contain(['AttachmentImages', 'Params', 'ItemRubrics'])->first();
        $this->Title->setMeta($item, 'Portfolios');

        $slug_catergory = $item['item_rubric']['slug'];
        $rubrics_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->where(['ItemRubrics.slug' => $slug_catergory])->contain(['AttachmentBlanks'])->first();
        $works = TableRegistry::getTableLocator()->get('Works')->find()->toArray();
        $this->set(compact('item', 'rubrics_items', 'works'));
    }

    public function getItems() {
        $filters = $this->getRequest()->getQuery();
        if (!empty($filters['category_filter']) && !empty($filters['filter'])) {
            $filter_merge = array_combine($filters['category_filter'], $filters['filter']);
        }
        if (!empty($filters['category_filter'])) {
            $query = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain('Items.Params', function (Query $q) use ($filters){
                $q->where(['Params.slug IN' => $filters['category_filter']]);
                return $q;
            })->where(['ItemRubrics.slug' => $filters['tab']])->toArray();
            $rubric = $query[0];

            $item_resp = [];
            foreach ($query[0]['items'] as $item) {
                $param_captions = [];
                foreach ($item['params'] as $param){
                    $param_captions[] = $param['slug'];
                }
                if ($this->array_equal($filters['category_filter'], $param_captions)) {
                    $status_item_to_catalog = false;


                    foreach ($item['params'] as $param) {

                        $filter_param = $filter_merge[$param['slug']];
                        //$param['_data']
                        if (in_array($filter_param, $param['_data'])) {
                            $status_item_to_catalog = true;
                        } else {
                            $status_item_to_catalog =false;
                            break;
                        }

                    }
                    if ($status_item_to_catalog) {
                        $item_resp[] = $item;
                    }
                }
            }
            $query = $item_resp;

            $this->set(compact('query', 'rubric'));
            $this->render('/Element/getItems');

        } else {
            $query = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain('Items.Params')->where(['ItemRubrics.slug' => $filters['tab']])->toArray();
            $rubric = $query[0];
            $query = $query[0]['items'];
            $this->set(compact('query','rubric'));
            $this->render('/Element/getItems');
        }
    }

    public function getFilters() {
        $filters = $this->getRequest()->getQuery();
        $rubrics_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->where(['ItemRubrics.caption' => $filters['tab']])->contain(['ParamRubrics' => ['Params']])->toArray();
        $sub_caption_filter = $rubrics_items[0]['param_rubrics'];
        $this->set(compact('sub_caption_filter'));
        $this->render('/Element/getFilters');
    }

    public function array_equal($a, $b) {
        return (
            is_array($a)
            && is_array($b)
            && count($a) == count($b)
            && array_diff($a, $b) === array_diff($b, $a)
        );
    }

}
