<?php
if (isset($neighbors['prev']['_url'])) {
    $this->start('prev');
    echo $this->Html->link('Prev', $neighbors['prev']['_url']);
    $this->end();
}
if (isset($neighbors['next']['_url'])) {
    $this->start('next');
    echo $this->Html->link('Next', $neighbors['next']['_url']);
    $this->end();
}
?>
<h1><?= $row->caption ?></h1>
<?= $row->content ?>
<table width="75%" align="center">
    <tr>
        <td><?= $this->fetch('prev') ?></td>
        <td><?= $this->fetch('next') ?></td>
    </tr>
</table>