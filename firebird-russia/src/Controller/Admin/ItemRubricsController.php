<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 *
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemRubricsController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        if ($this->request->getParam('id')) {
            $this->set('pathnodes', $this->ItemRubrics->find('path', ['for' => $this->request->getParam('id')]));
        }
    }
    
}
