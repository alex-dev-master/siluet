<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ServicesController extends AppController
{
    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $page = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 4])->first()->toArray();
//        $services = TableRegistry::getTableLocator()->get('Services')->find()->contain(['Objects'])->toArray();
//        $services = $this->Facilities->find()->contain(['Objects'])->toArray();
        $services = TableRegistry::getTableLocator()->get('Services')->find();
        $ids = [];
        $services->each(function ($item) use (&$ids) {
            $ids[] = $item->get('id');
        });
        $objects = [];
        $object = TableRegistry::getTableLocator()->get('Objects')->find()
                ->join(['ObjectsServices' => ['table' => 'objects_services', 'type' => 'INNER', 'conditions' => 'ObjectsServices.object_id = Objects.id']])
                ->where(['ObjectsServices.service_id IN' => $ids])
                ->select(['Objects.id', 'ObjectsServices.object_id', 'ObjectsServices.service_id', 'Objects.caption', 'slug', 'archive_status'])
                ->each(function ($item) use (&$objects) {
                    $objects[$item['ObjectsServices']['service_id']][] = $item;
                });

        $services = $services->map(function ($item) use ($objects) {
            $rows = [];
            if (isset($objects[$item['id']])) {
                $rows = $objects[$item['id']];
            }
            $item->set('objects', $rows);
            return $item;
        });


        $this->Title->setMeta($page, 'Services');
        $this->set(compact('page', 'services'));
    }

    public function view($id, $query = [], $merge_query = true)
    {
        $services_page = TableRegistry::getTableLocator()->get('Services')
            ->find()
            ->where(['Services.slug' => $id])
            ->first();
//        $service_object = $this->ServiceObjects->find()->where(['slug' => $id])->contain(['Objects'])->first();
        $services = TableRegistry::getTableLocator()->get('Services')->find()->where(['Services.slug' => $id]);
        $ids = [];
        $services->each(function ($item) use (&$ids) {
            $ids[] = $item->get('id');
        });
        $objects = [];
        $object = TableRegistry::getTableLocator()->get('Objects')->find()
            ->join(['ObjectsServices' => ['table' => 'objects_services', 'type' => 'INNER', 'conditions' => 'ObjectsServices.object_id = Objects.id']])
            ->where(['ObjectsServices.service_id IN' => $ids])
            ->select(['Objects.id', 'ObjectsServices.object_id', 'ObjectsServices.service_id', 'Objects.caption', 'slug', 'image', 'archive_status'])
            ->order(['archive_status' => 'ASC', 'position' => 'ASC'])
            ->each(function ($item) use (&$objects) {
                $objects[$item['ObjectsServices']['service_id']][] = $item;
            });

        $services = $services->map(function ($item) use ($objects) {
            $rows = [];
            if (isset($objects[$item['id']])) {
                $rows = $objects[$item['id']];
            }
            $item->set('objects', $rows);
            return $item;
        })->toArray();


        $this->Title->setMeta($services_page, 'Services');
        $this->set(compact('services_page', 'services'));
    }

}
