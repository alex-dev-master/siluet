<?php
$sep = '<li class="sep">/</li>';
$this->Paginator->setTemplates([
    'prevActive' => '<a href="{{url}}" class="prev-page"><img src="/img/arrow-left.png"> </a>',
    'prevDisabled' => '',
    'number' => $sep .'<li><a href="{{url}}">{{text}}</a></li>',
    'current' => $sep .'<li class="active"><a href="{{url}}">{{text}}</a></li>',
    'first' => '<li class="active"><a href="{{url}}">{{text}}</a></li>',
    'nextActive' => '<a href="{{url}}" class="btn blog-btn">'.__('Показать ещё').'</a>',
    'nextDisabled' => '',
]);
?>

<?php if ($this->Paginator->hasPages()): ?>
<!--    <div class="pagination">
        <ul class="pages">
            <?= preg_replace("~^{$sep}~", '', $this->Paginator->numbers()); ?>
        </ul> -->

<?= $this->Paginator->next('');?>
    <!--       <?= $this->Paginator->prev('');?>-->
<!--    </div>-->
<?php endif; ?>




