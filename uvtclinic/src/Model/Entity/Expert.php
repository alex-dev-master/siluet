<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $caption
 * @property string $content
 * @property bool $disabled
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modiified
 */
class Expert extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'content' => true,
        'image' => true,
        'thumbnail_400x400' => true,
        'thumbnail_210x210' => true,
        'thumbnail_60x60' => true,
        'image_square' => true,
        'rank' => true,
        'specialization' => true,
        'education' => true,
        'experience' => true,
        'congresses' => true,
        'to_main_page' => true,
        'to_experts_page' => true,
        'to_abouts_page' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'disabled' => true,
        'position' => true,
        'created' => true,
        'modified' => true,
        'specialities' => true,
    ];
}
