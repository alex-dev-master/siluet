<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use \Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function mainpage() {

//        $mainpage = $this->Pages->find('first', array(
//            'conditions' => array('Pages.id' => 1)
//        ));
        $this->viewBuilder()->setLayout('default_main_page');
        $services = TableRegistry::get('Services')->find()->select(['caption', 'slug', 'icon'])->toArray();
        $this->set([
            'slider' => \Cake\ORM\TableRegistry::get('Sliders')->find()->contain(['services']),
            'services' => $services
        ]);
        parent::view(1);
    }


    public function politics($id) {
        $politics = TableRegistry::get('Pages')->find()->where(['id'=> 6])->first();
        $this->set(compact('politics'));
        parent::view(6);
    }

}
