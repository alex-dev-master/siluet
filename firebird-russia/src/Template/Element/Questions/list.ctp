<?php foreach ($questions as $question) { ?>
<div class="item">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <div class="text">
                        <p class="title question">В:</p>
                        <div class="inner-text">
                            <?=$question['caption_question']?>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2 col-md-10 col-sm-12">
                    <div class="text">
                        <div class="inner-text answer">
                            <?=$question['caption_response']?>
                        </div>
                        <p class="title">:О</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
