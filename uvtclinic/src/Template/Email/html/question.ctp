<?php
$text = $this->Html->tag('p', "Здравствуйте! С сайта {$_SERVER['HTTP_HOST']} поступило новое сообщение!");

$text .= '<p>';
foreach ($data as $field => $name):
        $field = str_replace('_', ' ', $field);
        $text .= $field . ':' . $this->Html->tag('b', $name) . $this->Html->tag('br');
endforeach;
$text .= '</p>';
#
echo $text;
