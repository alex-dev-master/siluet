<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ServicesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('services')
            ->setDisplayField('caption')
            ->setOrder(['Services.position' => 'desc', 'Services.id' => 'desc'])
            ->setDisplayImage('image')
            ->setPrimaryKey('id');

        $this
            ->setFormSchema([
                'content_small' => ['label' => 'Краткое описание'],
                'content_before_image' => ['label' => 'Текст перед галереей'],
                'content_after_image' => ['label' => 'Текст после галереи'],

                'image' => ['params' => ['copy' => '']],
                'thumbnail_960x410' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 960, 'y' => 410, 'source' => 'image']],
                'thumbnail_460x310' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 460, 'y' => 310, 'source' => 'image']],
            ])
            ->setTabSchema([
            ]);

        $this->hasMany('AttachmentImages', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Services']]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'content_small', 'content_before_image', 'content_after_image', 'meta_title', 'meta_description', 'meta_keywords']
        ]);

    }
}
