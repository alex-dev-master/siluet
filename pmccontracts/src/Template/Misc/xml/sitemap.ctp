<?= '<?xml version="1.0" encoding="utf-8"?>';?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>/</loc>
        <changefreq>monthly</changefreq>
        <priority>1</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Abouts', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach ($abouts as $k=>$about) { ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Abouts', 'action' => $k], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Services', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach($services as $service) { ?>
        <url>
            <loc><?php echo $this->Url->build(['controller' => 'Services', 'action' => $service['slug']], true); ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach($projects as $project) { ?>
        <url>
            <loc><?php echo $this->Url->build(['controller' => 'Projects', 'action' => $project['slug']], true); ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'News', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach($news as $new) { ?>
        <url>
            <loc><?php echo $this->Url->build(['controller' => 'News', 'action' => $new['slug']], true); ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Contacts', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
</urlset>
