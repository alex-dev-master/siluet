<?php

use Cake\Utility\Inflector;
use Cake\I18n\Number;

if (!isset($row)) {
    $row = ['params' => []];
}
$_params = [];
foreach ($row['params'] as $p) {
    $_params[$p['id']] = $p['_values'];
}
$i = 0;
$params
        ->filter(function ($item) {
            return !empty($item->params);
        })
        ->each(function ($item) use ($row, $_params, &$i) {
            echo $this->Form->label($item->caption);
            foreach ($item->params as $param) {
                if (empty($param->get('options'))) {
                    continue;
                }
                $options = $selected = [];
                $checked = false;
                if (isset($_params[$param->get('id')])) {
                    $checked = true;
                }
                foreach (array_map('trim', preg_split('~\r|\n~', trim($param->get('options')), -1, PREG_SPLIT_NO_EMPTY)) as $subject) {
                    if ((bool) preg_match('~^(\d+)\s(.+)\s?(\*)?$~U', $subject, $match) && !isset($options[(int) $match[1]])) {
                        $options[(int) $match[1]] = trim($match[2]);
                        if ($checked and in_array($match[1], $_params[$param->get('id')])) {
                            $selected[] = (int) $match[1];
                        } else if (!$checked and isset($match[3]) && ($match[3] === '*')) {
                            $selected[] = (int) $match[1];
                        }
                    }
                }
                echo $this->Form->control('params.' . $i . '.id', ['type' => 'checkbox', 'label' => $param->get('caption'), 'value' => $param->get('id'), 'checked' => $checked]);
                echo $this->Form->control('params.' . $i . '.values', ['type' => 'select', 'multiple' => 'checkbox', 'options' => $options, 'label' => false, 'val' => $selected]);
                $i ++;
            }
        });