<?php
use Cake\Core\Configure;
$elementPath = $this->request->getParam('controller') . '/Admin';
?>
<article class="col-sm-12 col-md-12 col-lg-12">
	<div class="jarviswidget">
		<div role="content">
			<div class="jarviswidget widget-body">
				<header role="heading">
					<span class="widget-icon"> <i class="<?php echo ((isset($h_icon) and !empty($h_icon)) ? $h_icon : 'fa fa-lg fa-file-o'); ?>"></i></span>
                    <h2>
                        <?php if (!isset($pathnodes)) {
                            $pathnodes = new \Cake\Collection\Collection([]);
                        }
                        $url = $UrlCollection->append(['controller' => 'ItemRubrics', 'action' => 'index']);
                        $pathnodes = $pathnodes->prepend([(object) ['id' => null, 'caption' => 'Каталог']]);
                        $cnt = $pathnodes->count();
                        $i = 1;
                        $pathnodes->each(function ($item) use ($url, $cnt, &$i) {
                            if ($i == $cnt) {
                                $url = $url->append(['controller' => 'Items']);
                            }
                            echo $this->Html->link($item->caption, $url->append(['id' => $item->id]), ['class' => 'txt-color-white']) . ' / ';
                            $i ++;
                        });
                        echo @$row[$display_field];
                        ?>
                    </h2>
					<span class="btn btn-ribbon">
                        <i class="fa fa-refresh" onclick="window.location.replace('<?php echo $this->request->getRequestTarget(); ?>');"></i>
					</span>
				</header>
	<?php
$action = $creating ? 'add' : 'edit';
$action_url = $UrlCollection->append(['action' => $action]);
if (isset($id)) {
    $action_url = $action_url->append(['id' => $id]);
} elseif (isset($rubric_id)) {
    $action_url = $action_url->append(['id' => $rubric_id]);
} elseif ($is_tree && isset($parent_id)) {
    $action_url = $action_url->append(['id' => $parent_id]);
}
//Form start
//$this->Url->build() because plugin vs prefix
echo $this->Form->create($entity, array('type' => 'file', 'id' => $action . '-form', 'url' => $this->Url->build($action_url), 'class' => 'smart-form'));
//
if ($this->elementExists($elementPath . '/view_buttons_' . $action)) {
	$buttons = $this->element($elementPath . '/view_buttons_' . $action);
} elseif ($this->elementExists('SiluetCms.' . $elementPath . '/view_buttons_' . $action)) {
	$buttons = $this->element('SiluetCms.' . $elementPath . '/view_buttons_' . $action);
} elseif ($this->elementExists('SiluetCms.Automate/view_buttons_' . $action)) {
	$buttons = $this->element('SiluetCms.Automate/view_buttons_' . $action);
}
echo $buttons;
        $TabSchemaCollection = new Cake\Collection\Collection($tab_schema);
        $tab_schema = $TabSchemaCollection->map(function ($description, $name) use ($creating) {
            if (isset($description['field_schema'])) {
                $collection = new \Cake\Collection\Collection($description['field_schema']);
                if (iterator_count($collection->filter(function ($value, $key) use ($creating) {
                    return $value['input']['type'] == 'skip' || ($creating ? $value['input']['type'] == 'static' : false);
                })) == count($description['field_schema'])) {
                    $description['type'] = 'skip';
                }
            }
            return $description;
        })->toArray();
?>
	<div class="ui-tabs">
		<ul class="nav nav-tabs">
		<?php
			if ($this->elementExists($elementPath . '/view_tab')) {
				$view_tab = $elementPath . '/view_tab';
			} elseif ($this->elementExists('SiluetCms.' . $elementPath . '/view_tab')) {
				$view_tab = 'SiluetCms.' . $elementPath . '/view_tab';
			} elseif ($this->elementExists('SiluetCms.Automate/view_tab')) {
				$view_tab = 'SiluetCms.Automate/view_tab';
			}
			$i = 0;
			foreach ($tab_schema as $tab_name => $tab_description) {
				if ($tab_description['type'] == 'skip') {
					continue;
				}
				?>
			<li<?php if (!$i) { ?> class="active"<?php } ?>><a href="#tab-frame-<?php echo $tab_name; ?>" data-toggle="tab"><i class="fa <?php echo $tab_description['icon']; ?>"></i>&nbsp;<?php echo $tab_description['label']; ?></a></li>
			<?php
				$i ++;
			}
		?>
		</ul>
		<div class="tab-content">
		<?php
		$i = 0;
		foreach ($tab_schema as $tab_name => $tab_description) {
			if ($tab_description['type'] == 'skip') {
				continue;
			}
            if ($tab_description['type'] == 'params') {
                $view_tab = 'params_tab';
            }
			?>
			<div id="tab-frame-<?php echo $tab_name; ?>" class="tab-pane ui-tabs-panel fade<?php if (!$i) { ?> in active<?php } ?>">
				<?php echo $this->elementCms($view_tab, compact('tab_name', 'tab_description')); ?>
			</div>
			<?php
			$i ++;
		}
		?>
		</div>
	</div>
	<?php
	echo $buttons;
	echo $this->Form->end();
	?>
				</div>
		</div>
	</div>
	<?php if ($type = Configure::read('Config.mapType')) { ?>
	<div style="display: none;" data-type="<?php echo $type; ?>" id="_map-type"></div>
	<?php } ?>
</article>
