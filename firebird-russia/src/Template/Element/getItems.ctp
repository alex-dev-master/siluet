
    <div class="wrapper">
        <div class="row list">
                <?php foreach ($query as $item) {
                    if (!empty($item)) {
                        ?>
                        <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                            <div class="item">

                                <div class="img-container">
                                    <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "view", $item['slug']]) ?>">
                                        <img src="/files/items/<?= $item['thumbnail_226x226'] ?>">
                                        <div <?= 'style="background: #'.$rubric['color'].';border-color: #'.$rubric['color'].'"';?> class="more-arrow"><img src="/img/arr-w.png"></div>
                                    </a>
                                    <a href="#order-modal" data-toggle="modal">
                                        <div <?= 'style="background: #'.$rubric['color'].';border-color: #'.$rubric['color'].'"';?> class="btn product__order-btn">Заказать</div>
                                    </a>
                                </div>
                                <div class="name">
                                    <p><?= $item['caption'] ?></p>
                                </div>

                            </div>
                        </div>
                        <?php
                    }
                    } ?>
                <?php
                    if (empty($query)) {
                        echo "<p>По Вашему запросу не найдено ни одной позиции</p>";
                    }
                ?>
        </div>
    </div>

