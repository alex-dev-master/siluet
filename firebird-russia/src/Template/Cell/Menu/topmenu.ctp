<div class="header-top">
    <div class="wrapper">
        <ul class="header-top__menu">
            <?php foreach ($menus as $menu) { ?>
                <li <?php if (!empty($menu['current'])) echo 'class="active-link"'?>><a href="<?= $this->Url->build($menu['url']) ?>"><?=$menu['caption']?></a></li>
            <?php } ?>
            <li><a target="_blank" href="https://firebird.agora.ru/">b2b портал</a></li>
        </ul>
         <div class="header-email-li">
                    <a class="header-email" href="mailto:<?=$settings['email_top']?>"><?=$settings['email_top']?></a>
                </div>
    </div>
</div>
