<div class="page-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$abouts['caption']?></h1>
        <div class="main-image">
            <img src="/files/abouts/<?=$abouts['image']?>">
        </div>
        <div class="styled-text text">
            <?=$abouts['content']?>
        </div>
    </div>
</div>
<div class="page-content about-page">
    <div class="wrapper">
        <div class="about-list">
            <div class="item">
                <div class="row list">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-push-6">
                        <div class="img-container">
                            <img src="/files/abouts/<?=$abouts['image_text_1']?>">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-pull-6 ">
                        <div class="parent">
                            <div class="text child">
                                <?=$abouts['text_block_1']?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row list">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="img-container">
                            <img src="/files/abouts/<?=$abouts['image_text_2']?>">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <div class="parent">
                            <div class="text child right-text">
                                <?=$abouts['text_block_2']?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row list">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-push-6">
                        <div class="img-container">
                            <img src="/files/abouts/<?=$abouts['image_text_3']?>">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-pull-6 ">
                        <div class="parent">
                            <div class="text child">
                                <?=$abouts['text_block_3']?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="about-license">
    <div class="wrapper">
        <h2 class="h2-sml"><?=__('Лицензии и сертификаты')?></h2>
        <div class="row list">
            <?php foreach ($abouts['attachment_images'] as $attachment_image) { ?>
            <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                <div class="certificate">
                    <a class="fancybox" href="/files/attachment-images/<?=$attachment_image['image']?>">
                        <div class="certificate__img-container">
                            <img src="/files/attachment-images/<?=$attachment_image['thumbnail_180x180']?>">
                        </div>
                    </a>
                    <a class="certificate__name"><?=$attachment_image['caption']?></a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="about-team">
    <div class="wrapper">
        <h2 class="h2-sml"><?=__('Наша команда')?></h2>
        <div class="team-list">
            <div class="row list">
                <?php foreach ($abouts['attachment_executives'] as $attachment_executive) { ?>
                <div class="col-md-6 col-xs-12 team-list__col">

                    <div class="team-list__item">
                        <div class="img-container">
                            <img src="/files/attachment-executives/<?=$attachment_executive['image']?>">
                        </div>
                        <div class="text">
                            <p class="title"><?=$attachment_executive['caption']?></p>
                            <p class="descr"><?=$attachment_executive['post']?></p>

                        </div>
                    </div>

                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>
