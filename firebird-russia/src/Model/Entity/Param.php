<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Param Entity
 *
 * @property int $id
 * @property int $param_rubric_id
 * @property string $caption
 * @property string $options
 * @property int $position
 * @property bool $disabled
 *
 * @property \App\Model\Entity\ParamRubric $param_rubric
 * @property \App\Model\Entity\Item[] $items
 */
class Param extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'param_rubric_id' => true,
        'slug' => true,
        'caption' => true,
        'options' => true,
        'position' => true,
        'to_filter' => true,
        'disabled' => true,
        'param_rubric' => true,
        'items' => true
    ];
}
