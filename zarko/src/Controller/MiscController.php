<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class MiscController extends AppController {
    public function initialize() {
        $this->modelClass = false;
        parent::initialize();
    }
    
    public function xls() {
        //$s = IOFactory::load(WWW_ROOT . 'files' . DS . 'test.xls');
        //var_dump($s->getActiveSheet()->getCell('A4')->getValue());die();
        $s = new Spreadsheet();
        $s->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'test hellow');
        $w = new Xlsx($s);
        $w->save('aaa.xlsx');
        die();
    }

    public function sitemap() {
        $tables = [
            //'Brands',
            'Services',
            'Experiences',
            'Articles',
            'Participants',
            'Peoples',
        ];
        if (!$this->request->getParam('xml')) {
            $this->htmlMap();
        } else {
            $this->xmlMap($tables);
        }
    }

    private function htmlMap() {
        $services = TableRegistry::get('Services')->find('all')->select(['caption', 'slug'])->toArray();
        $experiences = TableRegistry::get('Experiences')->find('all')->select(['caption','slug'])->toArray();
        $this->set(compact('services', 'experiences'));
    }

    private function xmlMap($tables = []) {
        $this->viewBuilder()->setClassName('Xml');
        $this->loadModel('Pages');
        $services = TableRegistry::get('Services')->find('all')->select(['slug'])->toArray();
        $experiences = TableRegistry::get('Experiences')->find('all')->select(['slug'])->toArray();
        $this->set(compact('services', 'experiences'));
    }


}
