<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public $paginate;

    public function mainpage() {
        $sliders = TableRegistry::getTableLocator()->get('Sliders')->find()->toArray();
        $about = TableRegistry::getTableLocator()->get('Pages')->find()->where(['id' => 4])->first();
        $services = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->toArray();
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()
            ->contain(['AttachmentTimetables' => ['Records', 'sort' => ['AttachmentTimetables.record_id' => 'ASC']], 'Specialities'])
            ->where(['Experts.to_main_page' => 1])
            ->toArray();

        $days = TableRegistry::getTableLocator()->get('Records')->find()->toArray();
        foreach ($experts as $k => $expert) {
            $record = [];
            foreach ($expert['attachment_timetables'] as $attachment_timetable) {
                $record[] = $attachment_timetable['record']['caption'];
            }
            $experts[$k]['days_work'] = $record;
        }



            $articles = TableRegistry::getTableLocator()->get('Articles')->find()->limit(3)->contain(['ArticleRubrics'])->toArray();

        $this->set(compact('sliders', 'about', 'services', 'experts', 'articles', 'days', 'record'));
        parent::view(1);
    }

    public function aboutMedicalActivities() {
        parent::view(5);
    }

    public function regulations() {
        parent::view(6);
    }

    public function orderRules() {
        parent::view(7);
    }

    public function rightsObligations() {
        parent::view(8);
    }

    public function jobs() {
        parent::view(9);
    }

    public function services() {
        $itemRubrics = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->toArray();
        $this->set(compact('itemRubrics'));
        parent::view(10);
    }

    public function experts() {
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()
            ->contain(['Specialities'])
            ->where(['to_experts_page' => true])->toArray();
        $this->set(compact('experts'));
        parent::view(11);
    }

    public function timetable() {
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()
            ->contain(['AttachmentTimetables' => ['Records', 'sort' => ['AttachmentTimetables.record_id' => 'ASC']], 'Specialities'])->toArray();
        $days = TableRegistry::getTableLocator()->get('Records')->find()->toArray();
        foreach ($experts as $k => $expert) {
            $record = [];
            foreach ($expert['attachment_timetables'] as $attachment_timetable) {
                $record[] = $attachment_timetable['record']['caption'];
            }
            $experts[$k]['days_work'] = $record;
        }

        $this->set(compact('experts', 'days', 'record'));
        parent::view(12);
    }

    public function costs() {
        $itemRubrics = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['AttachmentCosts'])->toArray();
        $this->set(compact('itemRubrics'));

        parent::view(13);
    }

    public function questionnaire() {
        parent::view(14);
    }

    public function articles() {
        $this->paginate = [
            'limit' => 9,
            'order' => [
                'Articles.published' => 'desc'
            ],
        ];

        $articleRubrics = TableRegistry::getTableLocator()->get('ArticleRubrics')->find()->toArray();
        $articles = $this->paginate(TableRegistry::getTableLocator()->get('Articles')->find()->contain(['ArticleRubrics']))->toArray();


        $this->set(compact('articleRubrics', 'articles'));

        parent::view(15);
    }

    public function reviews() {
        $this->paginate = [
            'limit' => 3,
            'order' => [
                'Articles.published' => 'desc'
            ],
        ];

        $reviews = $this->paginate(TableRegistry::getTableLocator()->get('Reviews')->find())->toArray();

        $this->set(compact('reviews'));
        parent::view(16);
    }

    public function gallery() {
        parent::view(17);
    }

    public function politics() {
        parent::view(18);
    }

}
