<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WorksTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('works')
            ->setDisplayField('caption_2')
            ->setOrder(['Works.position' => 'DESC', 'Works.id' => 'DESC'])
            ->setPrimaryKey('id');

        $this
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 400, 'y' => 400]],
                'image_icon' => ['label' => 'Иконка', 'type' => 'image', 'params' => ['resize' => 'max', 'x' => 100, 'y' => 100]],
                'caption_3' => ['label' => 'Заголовок на главной'],
                'caption_2' => ['label' => 'Заголовок к тексту'],

            ])
            ->setTabSchema([
            ]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'caption_3', 'caption_2']
        ]);


    }
}
