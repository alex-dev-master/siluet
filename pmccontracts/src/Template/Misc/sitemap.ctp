<div class="sitemap">
    <div class="wrapper">
        <div class="text">
            <h3>Карта сайта</h3>
            <ul class="sitemap-list">
                <li><a href="/"><?=__('Главная');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Abouts', 'action' => 'index']); ?>"><?=__('О компании');?></a>
                    <ul>
                        <?php foreach ($abouts as $k=>$about) { ?>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Abouts', 'action' => $k]); ?>"><?=$about?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Services', 'action' => 'index']); ?>"><?=__('Услуги');?></a>
                    <ul>
                        <?php foreach ($services as $k=>$service) { ?>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Services', 'action' => $service['slug']]); ?>"><?=$service['caption']?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'index']); ?>"><?=__('Проекты');?></a>
                    <ul>
                        <?php foreach ($projects as $k=>$project) { ?>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Projects', 'action' => $project['slug']]); ?>"><?=$project['caption']?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <li><a href="<?php echo $this->Url->build(['controller' => 'News', 'action' => 'index']); ?>"><?=__('Новости');?></a>
                    <ul>
                        <?php foreach ($news as $k=>$new) { ?>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'News', 'action' => $new['slug']]); ?>"><?=$new['caption']?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <li><a href="<?php echo $this->Url->build(['controller' => 'Contacts', 'action' => 'index']); ?>"><?=__('Контакты');?></a></li>
            </ul>
        </div>
    </div>
</div>

