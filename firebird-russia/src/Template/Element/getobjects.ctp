<?php foreach ($blogs as $blog) { ?>
    <a href="<?= $this->Url->build(["controller" => "Blogs","action" => "view", $blog['slug']]) ?>">
        <div class="item">
            <div class="img-container">
                <img src="/files/blogs/<?=$blog['image']?>">
            </div>
            <div class="text">
                <p class="date">
                    <?=$blog->published->format('d.m.Y')?>
                </p>
                <p class="title">
                    <?=$blog['caption']?>
                </p>
                <p class="descr"><?=$blog['preview']?></p>
            </div>
        </div>
    </a>
<?php } ?>
