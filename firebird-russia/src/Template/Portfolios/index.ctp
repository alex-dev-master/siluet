<div class="portfolio__products">
    <div class="top">
        <div class="wrapper">
            <?php
            if (!empty($rubric_id )){
                $this->Breadcrumbs->prepend(
                    $portfolio['caption'],
                    ['controller' => 'Portfolios', 'action' => 'index']
                );
            }
            ?>
            <?= $this->element('breadcrumbs') ?>
            <h1><?=$portfolio['caption']?></h1>
            <div class="tabs">
                <ul>
                    <?php foreach ($rubrics_items as $k=>$rubric) { ?>
                    <li <?php if ($rubric_id == $rubric['slug']) {echo 'class="active"'; $attachment_blanks = $rubric['attachment_blanks'];} if ($rubric_id ==null && $k == 0) {echo 'class="active"'; $attachment_blanks = $rubric['attachment_blanks']; }?>><a  <?php if ($rubric_id == $rubric['slug']) { $rubric_color = $rubric; echo 'style="background: #'.$rubric['color'].';border-color: #'.$rubric['color'].'"'; } if ($rubric_id ==null && $k == 0) {$rubric_color = $rubric; echo 'style="background: #'.$rubric['color'].';border-color: #'.$rubric['color'].'"';} ?> href="<?= $this->Url->build(["controller" => "Portfolios","action" => "index", $rubric['slug']]) ?>" data-slug="<?=$rubric['slug']?>"><?=$rubric['caption']?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="filter">
        <div class="wrapper">
            <div class="row">
                <?php foreach ($sub_caption_filter as $filter) { ?>
                    <?php foreach ($filter['params'] as $filter_param) { ?>
                        <?php if ($filter_param['to_filter']) { ?>
                            <div class="col-md-4 filter_catalog">
                             <label class="title_of_option" data-slug="<?=$filter_param['slug']?>"><?=$filter_param['caption']?></label>
                             <select>
                             <option disabled selected><?=__('Выберите')?></option>
                             <option data-category="Все"><?=__('Все')?></option>
                             <?php
                             $options = [];
                             foreach (array_map('trim', preg_split('~\r|\n~', trim($filter_param['options']), -1, PREG_SPLIT_NO_EMPTY)) as $subject) {
                                 if ((bool) preg_match('~^(\d+)\s(.+)\s?(\*)?$~U', $subject, $match) and ! isset($options[(int) $match[1]])) {
                                     $options[(int) $match[1]] = $match[2];
                                 }
                             }

                                foreach ($options as $param) {
                             ?>
                             <option data-category="<?=$filter_param['slug']?>"><?=$param?></option>
                             <?php } ?>
                             </select>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="filter-more">
                <h3><?=__('Заполните форму и получите расчет стоимости изготовления уже сегодня')?></h3>
                <?= $this->Form->create(['schema' => []], ['id' => 'filter-more', 'class' => '']); ?>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-5">
                        <label><?=__('Вид заготовки')?></label>
                        <div class="zagotovka__image">
                            <a href="#zagotovka-modal" data-toggle="modal">
                                <div class="top">
                                    <input class="name" type="text" name="zagotovka" id="zagotovka" placeholder="<?=__('Выберите')?>">
                                    <!--<span class="name">Выберите</span>-->
                                </div>
                            </a>
                            <div class="image" >
                                <div class="img-container">
                                    <img id="zagotovka-image" src="">
                                </div>
                                <a href="#" id="zagotovka-delete" class="delete-btn"><img src="/img/trash.png"> </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-7">
                        <label><?=__('Количество')?></label>
                        <div class="incr-input__div">
                            <input type="text" name="count" class="incr-input" id="persons" value="1">
                        </div>
                        <label for="msg"><?=__('Файл (не более 50Мб)')?></label>
                        <div class="files files-filter">
                            <div class="file-input">
                                <div class="file_upload">
                                    <div class="file-name"><?=__('Файл не выбран')?></div>
                                    <button><?=__('Обзор')?></button>
                                    <input type="file">
                                </div>
                                  <a class="delete-place"><img src="/img/delete.png"> </a>
                            </div>

                        </div>
                        <label for="msg"><?=__('Удобный способ связи')?></label>
                        <div class="call-type">
                            <label class="checkbox">
                                <input name="WhatsApp" type="checkbox" <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?>>
                                <div class="checkbox__text"><?=__('WhatsApp')?></div>
                            </label>
                            <label class="checkbox">
                                <input name="Viber" type="checkbox" <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?>>
                                <div class="checkbox__text"><?=__('Viber')?></div>
                            </label>
                            <label class="checkbox">
                                <input name="Tel" type="checkbox" <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?>>
                                <div class="checkbox__text"><?=__('Телефон')?></div>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-6  col-sm-12">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="name"><?=__('ФИО')?><span>*</span></label>
                                <input class="modal-input" name="name" id="name">
                                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="Email">E-mail<span>*</span></label>
                                        <input class="modal-input" name="email" id="Email">
                                        <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                                    </div>
                                    <div class="col-md-6">

                                        <label for="phone"><?=__('Телефон')?></label>
                                        <input class="modal-input" name="phone" id="phone">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="msg"><?=__('Комментарий')?><span>*</span></label>
                                <textarea name="msg" id="msg"></textarea>
                                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="btns">
                    <div class="row">
                        <input type="hidden" value="Форма над подвалом">
                        <div class="col-md-6 col-sm-6 col-lg-offset-6 col-lg-3">
                            <label class="checkbox">
                                <input name="privacy-policy" type="checkbox" <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?>>
                                <div class="checkbox__text"><?=__('Я принимаю')?>  <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?> </a></div>
                            </label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-3">
                            <div class="right">
                                <a href="#" class="btn cancel-btn"><?=__('Отменить')?></a>
                                <button data-tab="#success_filter-more" class="btn form__btn btn-form" <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?>><?=__('Отправить')?></button>
                            </div>
                        </div>
                    </div>
                    <h4 id="success_filter-more"  class="success" style="display: none;color: green;"><?=__('Заявка отправлена!')?></h4>
                </div>
                <?= $this->Form->end(); ?>
            </div>

            <a class="btn filter-btn" href="#" ><?=__('Заказать похожие')?></a>
        </div>
    </div>
    <div class="portfolio__products-list products matr-products">
        <div class="wrapper">
            <div class="row list">
                <?php if (!empty($items)) { ?>
                <?php foreach ($items as $item) {
                    if (!empty($item)) {
                    ?>
                    <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                        <div class="item">
                        <div class="img-container">
                            <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "view", $item['slug']]) ?>">
                                <img src="/files/items/<?=$item['thumbnail_226x226']?>">
                                <div class="more-arrow" <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?>><img src="/img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal" data-towar="<?=$item['caption']?>">
                                <div <?= 'style="background: #'.$rubric_color['color'].';border-color: #'.$rubric_color['color'].'"';?> class="btn product__order-btn"><?=__('Заказать')?></div>
                            </a>
                        </div>
                        <div class="name">
                            <p><?=$item['caption']?></p>
                        </div>

                    </div>
                    </div>
                <?php }
                } ?>
                <?php } else { ?>
                    <p>По Вашему запросу не найдено ни одной позиции</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<?=$this->element('makeOrder')?>

<div id="zagotovka-modal" class="modal wide-modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="/img/form-close.png"></button>
                <h4 class="modal-title"><?=__('Выбор заготовки')?></h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">


                <div class="zagotovka__items">
                    <div class="row list">
                        <?php foreach ($attachment_blanks as $blank) { ?>
                        <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                            <div class="item" data-zagotovka="<?=$blank['caption']?>" >
                                <div class="img-container">
                                    <img src="/files/attachment-blanks/<?=$blank['image']?>">
                                </div>
                                <div class="text">
                                    <p><?=$blank['caption']?></p>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
