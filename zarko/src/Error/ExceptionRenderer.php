<?php
namespace App\Error;

use Cake\Error\ExceptionRenderer as CakeExceptionRenderer;
use App\Controller\ErrorController;
use SiluetCms\Controller\ErrorController as CmsErrorController;
use Cake\Core\Configure;

class ExceptionRenderer extends CakeExceptionRenderer {
    protected function _getController() {
        if (Configure::read('admin') === true && !Configure::read('debug')) {
            return new CmsErrorController();
        }
        return new ErrorController();
    }
}