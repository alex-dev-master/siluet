<div class="page-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$service_page['caption']?></h1>
        <div class="styled-text text">
            <p><?=$service_page['content']?></p>
        </div>
    </div>
</div>
<div class="services">
    <div class="services__list">
        <div class="wrapper">
            <?php foreach ($services as $k => $service) { ?>
            <div class="item">
                <div class="row">
                    <div class="col-md-5">
                        <div class="img-container">
                            <a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']]) ?>"><img src="/files/services/<?=$service['thumbnail_460x310']?>"></a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="text">
                            <p class="title">
                                <a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']]) ?>"><?=$service['caption']?></a>
                            </p>
                            <div class="descr">
                                <?=$service['content_small']?>
                            </div>
                            <div class="btns">
                                <a href="#orderService-modal" data-name="<?=$service['caption']?>" data-toggle="modal" class="btn arr-btn"><?=__('Заказать услугу')?></a>
                                <br>
                                <a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']]) ?>" class="btn  arr-btn green"><?=__('Подробнее')?></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</div>
<div id="orderService-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(['schema' => []], ['id' => 'orderService-modal-form', 'class' => '']); ?>
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="/img/form-close.png"></button>
                <p class="modal-subtitle"><?=__('Заказ услуги')?></p>
                <h4 class="modal-title"><?=$service['caption']?></h4>
                <input type="hidden" value="<?=$service['caption']?>">
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <label for="name2"><?=__('ФИО')?><span>*</span></label>
                <input class="modal-input" name="name" id="name2">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Телефон')?><span>*</span></label>
                <input class="modal-input phone" name="phone" id="organization2">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail<span>*</span></label>
                <input class="modal-input" name="email" id="organization2">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Комментарий')?><span>*</span></label>
                <textarea name="msg"></textarea>
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                <label for="msg"><?=__('Файлы (не более 50Мб)')?></label>

                <div class="files">
                    <div class="file-input">
                        <div class="file_upload">
                            <div class="file-name"><?=__('Файл не выбран')?></div>
                            <button><?=__('Обзор')?></button>
                            <input type="file">
                        </div>
                        <a class="delete-place"><img src="/img/delete.png"> </a>
                    </div>
                    <a href="#" class="add-more__btn"><?=__('Добавить файл')?></a>
                </div>

            </div>
            <div class="modal-footer">
                <div class="btns">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="Форма заказа услуги">
                            <label class="checkbox">
                                <input name="privacy-policy" type="checkbox">
                                <div class="checkbox__text"><?=__('Я принимаю')?> <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?> </a></div>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <button data-tab="#success_orderService-modal-form" class="btn btn-modal"><?=__('Отправить')?> </button>
                        </div>
                    </div>
                </div>
                <img src="/img/doll.png" class="modal-doll">
                <h4 id="success_orderService-modal-form" class="success" style="display: none; color: green"><?=__('Ваша заявка отправлена')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>

