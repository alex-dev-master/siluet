<?php

use Cake\I18n\I18n;
use Cake\Utility\Inflector;
use Cake\I18n\Number;

switch ($tab_description['type']) {
    case 'attachment':
        //echo $this->Html->tag('iframe', '', array('src' => $this->Html->url(array('controller' => Inflector::tableize($tab_description['model']), 'action' => 'index', 'model' => $model, 'foreign_key' => $id, 'ext' => 'frame')), 'frameborder' => 'no', 'class' => 'frame col-sm-12 col-md-12 col-lg-12', 'height' => '400'));
        ?>
        <div class="attachments" data-src="<?php echo $this->Url->build($BgUrl = $UrlCollection->append(['controller' => $tab_description['controller'], 'action' => 'index', 'model' => $model, 'foreign_key' => $id, '_ext' => 'frame'])); ?>" data-href="<?php echo $this->Url->build($BgUrl->append(['background-save' => 1])); ?>"></div>
        <?php
        break;
    default:
        foreach ($tab_description['field_schema'] as $field_name => $field_description) {
            if ($field_description['input']['type'] == 'skip') {
                continue;
            }
            switch ($field_description['input']['type']) {
                case 'static':
                    if (isset($row[$field_name])) {
                        $value = $row[$field_name];
                        if (isset($field_description['input']['dateFormat'], $field_description['input']['timeFormat'])) {
                            $value = $row[$field_name]->i18nFormat([\IntlDateFormatter::FULL, \IntlDateFormatter::SHORT], null, Cake\I18n\I18n::getLocale());
                        }
                        echo '<section><label class="label">' . $field_description['input']['label'] . ': <span class="static">' . $value . '</span></label></section>';
                    }
                    break;
                case 'file':
                    if (isset($row[$field_name . '-uploaded'])) {
                        $file = $row[$field_name . '-uploaded'];
                        $dir = dirname($file['filename']);
                        ?>
                        <div class="col-md-5">
                            <div class="media">
                                <div class="media-body txt">
                                    <p>Файл: <?php echo $this->Html->link(basename($file['filename']), $file['url-time']); ?></p>
                                    <p>Размер файла: <?php echo Number::toReadableSize(filesize($file['filename'])); ?></p>
                                    <p>Изменено: <?php echo date('d.m.Y в H:i:s', $file['filetime']); ?></p>
                                </div>
                            </div>
                            <?php echo $this->Form->control('delete_' . $field_name, ['label' => __d('admin', 'delete_file_label'), 'type' => 'checkbox']); ?>
                        </div>
                        <?php
                    }
                    $field_description['input']['type'] = 'file';
                    if ($this->request->is('ajax')) {
                        $field_description['input']['data-ajax'] = 1;
                        $field_description['input']['data-name'] = 'upload_' . $field_name;
                    }
                    ?>
                    <div class="col-md-7">
                        <?php echo $this->Form->control('upload_' . $field_name, $field_description['input']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    break;
                case 'image':
                    if (isset($field_description['params']['source'])) {
                        continue;
                    }

                    if ($field_name == 'full_image') {
                        echo $this->element('Admin/Sliders/admin_interactive', compact('field_name', 'field_description'));
                        continue;
                    }

                    if (isset($row[$field_name . '-uploaded'])) {
                        $img = $row[$field_name . '-uploaded'];
                        $dir = dirname($img['filename']);
                        $thumbnail = dirname($img['url']) . '/_thumbs/' . $img['filetime'] . '_' . basename($img['filename']);
                        $image_info = getimagesize($img['filename']);
                        if (!is_file($thumb = $dir . DS . '_thumbs' . DS . $img['filetime'] . '_' . basename($img['filename']))) {
                            if ($image_info[0] > 100 and $image_info[1] > 100) {
                                if (!is_dir($dir . DS . '_thumbs')) {
                                    mkdir($dir . DS . '_thumbs', 0755, true);
                                }
                                $Image = new \SiluetCms\Utils\ImageResize();
                                $Image->thumb($img['filename'], $thumb);
                            } else {
                                $thumbnail = $img['url-time'];
                            }
                        }
                        ?>
                        <div class="col-md-5">
                            <div class="media">
                                <div class="photo">
                                    <?php echo $this->Html->link($this->Html->image($thumbnail, array('class' => 'pull-left')), $img['url-time'], array('class' => 'fancybox', 'escape' => false)); ?>
                                </div>
                                <div class="media-body txt">
                                    <p>Размер файла: <?php echo Number::toReadableSize(filesize($img['filename'])); ?></p>
                                    <p>Изменено: <?php echo date('d.m.Y в H:i:s', $img['filetime']); ?></p>
                                    <p>Размер изображения: <?php echo $image_info[0] . ' x ' . $image_info[1]; ?></p>

                                </div>
                            </div>

                            <?php echo $this->Form->control('delete_' . $field_name, array('label' => __d('admin', 'delete_image_label'), 'type' => 'checkbox')); ?>
                        </div>
                        <?php
                    }
                    $field_description['input']['type'] = 'file';
                    $attrs = $field_description['input'];
                    if ($this->request->is('ajax')) {
                        $attrs['data-ajax'] = 1;
                        $attrs['data-accept'] = 'image/jpeg,image/jpg,image/gif,image/png';
                        $attrs['data-name'] = 'upload_' . $field_name;
                    }
                    ?>
                    <div class="col-md-7">
                        <?php echo $this->Form->control('upload_' . $field_name, $attrs); ?>
                    </div>
                    <div class='clearfix'></div>
                    <?php
                    break;
                case 'wysiwyg':
                    $field_description['input'] = array_merge($field_description['input'], array('type' => 'textarea', 'class' => 'wysiwyg'));
                    echo $this->Form->control($field_name, $field_description['input']);
                    break;
                case 'btm':

                    break;
                case 'datetime':
                    $field_description['input']['type'] = 'text';
                default:
                    echo $this->Form->control($field_name, $field_description['input']);
            }
        }
}
