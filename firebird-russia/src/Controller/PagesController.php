<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Database\Query;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function mainpage() {
        $sliders = TableRegistry::getTableLocator()->get('Sliders')->find()->toArray();
        $works = TableRegistry::getTableLocator()->get('Works')->find()->toArray();
        $clients = TableRegistry::getTableLocator()->get('Clients')->find()->toArray();
        $AttachmentAdvantages = TableRegistry::getTableLocator()->get('AttachmentAdvantages')->find()->toArray();
        $page = TableRegistry::getTableLocator()->get('Pages')->find()->first();
        $this->Title->setMeta($page, 'Pages');

        $category_items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain('Items', function (Query $q) {
            $q->where(['Items.to_main' => true]);
            return $q;
        })->toArray();
        $this->set(compact('sliders', 'works', 'page', 'category_items', 'clients', 'AttachmentAdvantages'));
    }

    public function abouts() {
        $abouts = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 2])->first();
        $this->Title->setMeta($abouts, 'Pages');

        $this->set(compact('abouts'));
    }

    public function services() {
        $service_page = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 4])->first();
        $this->Title->setMeta($service_page, 'Pages');


        $services = TableRegistry::getTableLocator()->get('Services')->find()->contain(['AttachmentImages'])->toArray();

        $this->set(compact(['service_page', 'services']));
    }

    public function works() {
        $page = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 5])->first();
        $this->Title->setMeta($page, 'Pages');

        $works = TableRegistry::getTableLocator()->get('Works')->find()->toArray();
        $this->set(compact(['page', 'works']));
    }

    public function questions() {
        $page = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 6])->first();
        $this->Title->setMeta($page, 'Pages');

        $rubrics_questions = TableRegistry::getTableLocator()->get('QuestionRubrics')->find()->contain(['Questions'])->toArray();
        $this->set(compact(['rubrics_questions', 'page']));
    }

    public function getQuestions() {
        $slug = $this->getRequest()->getQuery(['category']);
        $questions_rubric = TableRegistry::getTableLocator()->get('QuestionRubrics')->find()->contain(['Questions'])->where(['QuestionRubrics.slug' => $slug])->first();
        $questions = $questions_rubric['questions'];
        $this->set(compact('questions'));
        $this->render('/Element/Questions/list');
    }

    public function politics() {
        $page = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 7])->first();
        $this->Title->setMeta($page, 'Pages');

        $this->set(compact('page'));
    }

}
