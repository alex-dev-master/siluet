<div class="page-content services-content services_category-content">
    <?php if (!empty($page['image_inner'])) { ?>
    <img class="content-bg" src="/files/item-rubrics/<?=$page['image_inner']?>" alt="">
    <?php } ?>
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('Услуги'),
            ['controller' => 'Pages', 'action' => 'services']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$page['caption']?></h1>
        <?php if (!empty($page['items'])) { ?>
        <div class="services_category-list">
            <?php foreach ($page['items'] as $item) { ?>
            <div class="services_category-item">
                <a href="<?= $this->Url->build(["controller" => "Items","action" => "view", $item['slug']]) ?>">
                    <div class="row list">
                        <div class="col-md-5 col-sm-6 col-xs-12">
                            <img class="list-img" src="/files/items/<?=$item['thumbnail_470x270']?>" alt="">
                        </div>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <div class="inner-text">
                                <h4 class="title"><?=$item['caption']?></h4>
                                <p class="desc"><?=$item['content']?></p>
                                <div class="btn serv-link">
                                    Подробнее <img src="/img/arrow.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <?php if (!empty($page['content_advantages'])) { ?>
    <div class="services_category-plus">
        <div class="hand">
            <img class="hand-img" src="/img/hand.png" alt="">
        </div>
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-5 col-md-push-5 col-sm-6 col-sm-push-6 col-xs-12">
                    <div class="inner-wrapper">
                        <?=$page['content_advantages']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="wrapper">
        <?php if (!empty($page['attachment_costs'])) { ?>
        <div class="services_category-price">
            <h2 class="title"><?=$page['caption_sub']?></h2>
            <table>
                <tr>
                    <th>Наименование услуги</th>
                    <th>Стоимость, руб.</th>
                </tr>
                <?php foreach ($page['attachment_costs'] as $cost) { ?>
                <tr>
                    <td>
                        <p class="main-text"><?=$cost['caption']?></p>
                    </td>
                    <td>
                        <p class="price"><?=$cost['cost']?></p>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
        <?php } ?>
        <?php if (!empty($page['content'])) { ?>
        <div class="services_category-desc">
            <?=$page['content']?>
        </div>
        <?php } ?>
    </div>
    <?php if (!empty($page['experts'])) { $experts = $page['experts']; ?>
    <div class="services_category-workerList">
        <div class="wrapper">
            <h2 class="title">Наши специалисты</h2>
            <div class="row list">
                <?php foreach ($experts as $expert) { ?>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="inner-container">
                            <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                                <img class="worker-img" src="/files/experts/<?=$expert['thumbnail_210x210']?>" alt="">
                                <div class="text-wrap">
                                    <h6 class="worker-title"><?=$expert['caption']?></h6>
                                    <p class="worker-desc">
                                        <?php foreach ($expert['specialities'] as  $specialities) {?>
                                            <?=$specialities['caption']?> <br>
                                        <?php } ?>
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (!empty($page['attachment_infos'])) {?>
    <div class="wrapper">
        <div class="services_category-articles">
            <h3 class="title">Полезно знать</h3>
            <div class="row list">
                <?php foreach ($page['attachment_infos'] as $attachment_info) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="inner-container">
                            <a href="<?= $attachment_info['link'] ?>">
                                <img class="articles-img" src="/files/attachment-infos/<?=$attachment_info['image']?>" alt="">
                                <div class="text-wrap">
                                    <h5 class="articles-title"><?=$attachment_info['caption']?></h5>
                                    <p class="articles-desc"><?=$attachment_info['content']?></p>
                                    <p class="more-title">Подробнее</p>
                                    <div class="moreBtn more-button"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
    <?php } ?>
</div>
