<div class="page-content timetable-content">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <div class="table-wrapper">
            <table>
                <tr>
                    <th>Специалист</th>
                    <?php foreach ($days as $day) { $have_days[] = $day['caption'];?>
                    <th><?=$day['caption']?></th>
                    <?php } ?>
                </tr>
                <?php foreach ($experts as $key => $expert) { ?>
                    <?php if (!empty($expert['attachment_timetables'])) { ?>
                        <tr>
                    <td>
                        <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                            <div class="worker-item">
                                <img class="worker-img" src="/files/experts/<?=$expert['thumbnail_60x60']?>" alt="">
                                <div class="worker-text">
                                    <h6 class="worker-title"><?=$expert['caption']?></h6>
                                    <p class="worker-desc">
                                        <?php
                                        $caption_specialities = [];
                                        foreach ($expert['specialities'] as $specialities) {
                                            $caption_specialities[] = $specialities['caption'];
                                        }
                                        $res = implode(', ', $caption_specialities);
                                        echo $res
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </td>
                    <?php $j = 0; foreach ($days as $day) {
                    if (in_array($day['caption'], $expert['days_work'])) {?>
                        <?php
                            $days_reduction = [
                                'Понедельник' =>'1',
                                'Вторник' => '2',
                                'Среда' => '3',
                                'Четверг' => '4',
                                'Пятница' => '5',
                                'Суббота' => '6',
                                'Воскресенье' => '0',
                            ];
                        ?>
                        <td>
                            <p class="time"><?=$expert['attachment_timetables'][$j]['time']?></p>
                            <a class="moreBtn links-order" data-id="<?=$key?>" data-date="<?=$days_reduction[$day['caption']]?>" data-expert="<?=$expert['caption']?>" data-time="<?=$expert['attachment_timetables'][$j]['time']?>" href="#timetable-modal" data-toggle="modal"><img src="/img/pencil-white.png" alt="">Записаться</a>
                        </td>
                        <?php $j++; } else { ?>
                        <td> </td>
                        <?php } ?>
                    <?php } ?>
                </tr>
                    <?php } ?>
                <?php } ?>
            </table>
        </div>

    </div>
</div>

<div id="timetable-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/img/modal-close.png"></button>
                <h3 class="modal-title">Запись на прием</h3>
            </div>
            <!-- Основное содержимое модального окна -->
            <?= $this->Form->create(['schema' => []], ['id' => 'sendTimetable', 'class' => 'form']); ?>
            <div class="modal-body">
                <label for="name2">Ваше имя<span>*</span></label>
                <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input required" name="phone" id="organization2" placeholder="Ваш номер телефона">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail</label>
                <input class="modal-input" name="email" id="organization2" placeholder="Электронная почта для связи">


                <label for="organization3">Специалист</label>
                <select name="select" id="organization3" class="disabled">
                    <?php foreach ($experts as $expert) { ?>
                    <option value="<?=$expert['caption']?>"><?=$expert['caption']?></option>
                    <?php } ?>
                </select>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <label for="organization2">Время</label>
                        <input class="modal-input input-time" name="time" id="organization2" type="text">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <label for="organization2">Дата</label>
                        <input class="modal-input input-date datepicker" name="date" id="organization2">
                    </div>
                </div>


                <label for="organization2">Комментарий</label>
                <textarea class="modal-input" id="organization2" name="msg" placeholder="Дополнительная информация"></textarea>

                <label class="checkbox">
                    <input type="checkbox" name="privacy-policy">
                    <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                            конфиденциальности</a></div>
                </label>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-mod"><img src="/img/mail.png">Отправить</button>
                </div>
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
