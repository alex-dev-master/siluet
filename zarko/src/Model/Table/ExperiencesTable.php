<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @method \App\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contact findOrCreate($search, callable $callback = null, $options = [])
 */
class ExperiencesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('experiences')
            ->setDisplayField('caption')
            ->setDisplayImage('image')
            ->setPrimaryKey('id');

        $this->setFormSchema([
            'image' => ['params' => ['copy' => '']],
            'thumbnail_560x320' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 560, 'y' => 320, 'source' => 'image']],

            'type_object' => ['label' => 'Тип объекта'],
            'address' => ['label' => 'Адрес', 'type' => 'text'],

            #
            'status_case' => ['label' => 'Отображать кейс', 'tab' => 'case'],
            'footer_menu' => ['label' => 'Отображать в футере', 'tab' => 'case'],
            'square' => ['label' => 'Площадь', 'tab' => 'case'],
            'year' => ['label' => 'Год', 'tab' => 'case'],
            'brief_case' => ['label' => 'Краткое описание объекта', 'tab' => 'case'],
            'block_short_info_object' => ['label' => 'Информация по объекту', 'tab' => 'case'],
            'block_short_info_time' => ['label' => 'Информация по срокам', 'tab' => 'case'],
            'block_short_info_task' => ['label' => 'Информация по задачам', 'tab' => 'case'],
            'about_project_title' => ['label' => 'Заголовок краткой информации о проекте', 'tab' => 'case'],
            'about_project_text' => ['label' => 'Тект описания проекта ', 'type' => 'wysiwyg', 'tab' => 'case'],
            'about_project_coord' => ['label' => 'Координаты нахождения на карте', 'tab' => 'case'],
            'video' => ['label' => 'Видео', 'tab' => 'case'],
            'city' => ['label' => 'Город', 'tab' => 'case'],
            'title_work' => ['label' => 'Заголовок (Работы на объекте)', 'tab' => 'case'],
            'title_partic_project' => ['label' => 'Заголовок (Участники о проекте)', 'tab' => 'case'],
            'image_participant' => ['label' => 'Изоображение (Участники о проекте)','params' => ['resize' => 'crop', 'x' => 620, 'y' => 780], 'type' => 'image', 'tab' => 'case'],

        ]);

        $this->setTabSchema([
            'case' => ['label' => 'Кейс'],
            'attachment_technologies' => ['label' => 'Технологии'],
            'attachment_participants' => ['label' => 'Участники'],
            'attachment_works' => ['label' => 'Работы  на объекте'],
        ]);

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Services');
//        $this->belongsToMany('Technologies');
//        $this->belongsToMany('Works');
//        $this->belongsToMany('Participants');
        $this->belongsTo('Types');
        $this->hasMany('AttachmentTechnologies', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentTechnologies.model' => 'Experiences']]);
        $this->hasMany('AttachmentParticipants', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentParticipants.model' => 'Experiences']]);
        $this->hasMany('AttachmentWorks', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentWorks.model' => 'Experiences']]);

    }
}
