<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $caption
 * @property string $map_caption
 * @property float $map_n
 * @property float $map_e
 * @property string $feedback_to
 * @property string $feedback_from
 * @property string $feedback_subject
 */
class Experience extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'image' => true,
        'address' => true,
        'type_object' => true,
        'type_id' => true,
        'square' => true,
        'year' => true,
        'slug' => true,
        'status_case' => true,
        'brief_case' => true,
        'block_short_info_object' => true,
        'block_short_info_time' => true,
        'block_short_info_task' => true,
        'about_project_title' => true,
        'about_project_text' => true,
        'map_n' => true,
        'map_e' => true,
        'video' => true,
        'city' => true,
        'title_work' => true,
        'title_partic_project' => true,
        'image_participant' => true,
        'footer_menu' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'disabled' => true,
        'position' => true,
        'created' => true,
        'modified' => true,
        'services' => true,
        'technologies' => true,
        'works' => true,
        'participants' => true,
        'types' => true,
        'attachment_participant' => true,

    ];
}
