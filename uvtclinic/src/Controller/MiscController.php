<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class MiscController extends AppController
{

    public function initialize()
    {
        $this->modelClass = false;
        parent::initialize();
    }

    public function sitemap() {
        if (!$this->request->getParam('xml')) {
            $this->htmlMap();
        } else {
            $this->xmlMap();
        }
    }

    private function htmlMap() {
        $this->Title->dataCaption = __('Карта сайта');
        $itemRubrics = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['Items'])->toArray();
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()->toArray();
        $articleRubrics = TableRegistry::getTableLocator()->get('ArticleRubrics')->find()->contain(['Articles'])->toArray();
        $this->set(compact('itemRubrics', 'experts', 'articleRubrics'));
    }

    private function xmlMap() {
        $this->viewBuilder()->setClassName('Xml');
        $this->Title->dataCaption = __('Карта сайта');
        $itemRubrics = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain(['Items'])->toArray();
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()->toArray();
        $articleRubrics = TableRegistry::getTableLocator()->get('ArticleRubrics')->find()->contain(['Articles'])->toArray();
        $this->set(compact('itemRubrics', 'experts', 'articleRubrics'));
    }
}
