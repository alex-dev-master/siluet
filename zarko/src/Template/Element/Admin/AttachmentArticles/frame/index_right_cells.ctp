<div class="pull-right">
	<?php
	if (isset($field_schema['price'])) { ?>
	<div class="smart-form live-edit">
        <div class="inpt-cont" data-url="<?php echo $this->Url->build($UrlCollection->append(['action' => 'saveField', 'field' => 'price', 'id' => $row[$primary_key]])); ?>"><span><?php echo $row['price']; ?></span></div>
	</div>
	<?php }
	if (isset($field_schema['disabled'], $row['disabled'])) {
		$toggle_disabled_url = $UrlCollection->append(['action' => 'toggle_disabled', 'id' => $row[$primary_key], '?' => ['render' => 0]]);
		if (isset($ctrl)) {
			$toggle_disabled_url = $UrlCollection->append(['controller' => $ctrl]);
		}
		?>
		<span class="onoffswitch">
			<input type="checkbox" name="disabled-<?php echo $model; ?>-<?php echo $row[$primary_key]; ?>" class="onoffswitch-checkbox" id="<?php echo $model; ?>-<?php echo $row[$primary_key]; ?>"<?php if (!$row['disabled']) { ?> checked="checked"<?php } ?> data-h="<?php echo $this->Url->build($toggle_disabled_url->toArray()); ?>">
			<label class="onoffswitch-label" for="<?php echo $model; ?>-<?php echo $row[$primary_key]; ?>"> 
				<span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span> 
				<span class="onoffswitch-switch"></span> 
			</label> 
		</span>
	<?php
	}
		
    
	$edit_url = $UrlCollection->append(['action' => 'edit', 'id' => $row[$primary_key]]);
	if (isset($ctrl)) {
		$edit_url = $UrlCollection->append(['controller' => $ctrl]);
	}
	echo $this->Html->link('<i class="fa fa-lg fa-pencil"></i>', $edit_url, ['title' => __d('admin', 'edit_action'), 'escape' => FALSE, 'modal' => true]);
	
	// cell_empty / cell_top / cell_up / cell_down / cell_bottom
	$_position = TRUE;
	if (isset($has_rubric) && $has_rubric && !isset($rubric_id)) {
		$_position = FALSE; // no positioning without rubric
	}
	if ($_position && isset($field_schema['position'])) {
		$up_url = $UrlCollection->append(['action' => 'up', 'id' => $row[$primary_key]]);
		$down_url = $UrlCollection->append(['action' => 'down', 'id' => $row[$primary_key]]);
		$top_url = $UrlCollection->append(['action' => 'top', 'id' => $row[$primary_key]]);
		$bottom_url = $UrlCollection->append(['action' => 'bottom', 'id' => $row[$primary_key]]);
		if ($i === 0 and (!isset($this->request->getParam('paging')[$model]['page']) or $this->request->getParam('paging')[$model]['page'] == 1)) {
			// cell_empty
			echo $this->Html->tag('i', '', ['class' => 'fa fa-lg fa-angle-double-up']);
			echo $this->Html->tag('i', '', ['class' => 'fa fa-lg fa-angle-up']);
		} else {
			// cell_top
			echo $this->Html->link('<i class="fa fa-lg fa-angle-double-up"></i>', $top_url, ['title' => __d('admin', 'top_action'), 'escape' => FALSE, 'class' => 'frame']);
			// cell_up
			echo $this->Html->link('<i class="fa fa-lg fa-angle-up"></i>', $up_url, ['title' => __d('admin', 'up_action'), 'escape' => FALSE, 'class' => 'frame']);
		}
		if ($i === $num_rows - 1 and (!isset($this->request->getParam('paging')[$model]['page']) or $this->request->getParam('paging')[$model]['page'] == $this->request->getParam('paging')[$model]['pageCount'])) {
			// cell_empty
			echo $this->Html->tag('i', '', ['class' => 'fa fa-lg fa-angle-down']);
			echo $this->Html->tag('i', '', ['class' => 'fa fa-lg fa-angle-double-down']);
		} else {
			// cell_down
			echo $this->Html->link('<i class="fa fa-lg fa-angle-down"></i>', $down_url, ['title' => __d('admin', 'down_action'), 'escape' => FALSE, 'class' => 'frame']);
			// cell_bottom
			echo $this->Html->link('<i class="fa fa-lg fa-angle-double-down"></i>', $bottom_url, ['title' => __d('admin', 'bottom_action'), 'escape' => FALSE, 'class' => 'frame']);
		}
        /**
         * TODO insert here position
         */
        /*if ($row['position']) echo $this->Html->link('<i class="glyphicon glyphicon-fullscreen"></i>',  $UrlCollection->append(['action' => 'insert_here', 'id' => $row[$primary_key]]), array('title' => __d('admin', 'bottom_action'), 'escape' => FALSE));*/
		//echo '<div class="cell cell_menu cell_top">' . $this->Html->link($this->Html->icon('arrow_in'), array('action' => 'insert_here', 'id' => $row[$model][$primary_key]), array('title' => __d('admin', 'insert_here_action'), 'escape' => FALSE)) . '</div>';
		/*$here_url = array('action' => 'insert_here', 'id' => $row[$model][$primary_key], 'autoPage' => TRUE, 'autoLang' => true);
		if (isset($ctrl)) {
			$here_url['controller'] = $ctrl;
		}
		echo $this->Html->link('<i class="fa fa-lg fa-arrows-alt"></i>', $here_url, array('title' => __d('admin', 'insert_here_action'), 'escape' => FALSE));*/
	}
	
	// cell_delete
	if (isset($field_schema['deleted'])) {
		$delete_url = $UrlCollection->append(['action' => 'erase', 'id' => $row[$primary_key]]);
	} else {
		$delete_url = $UrlCollection->append(['action' => 'delete', 'id' => $row[$primary_key]]);
	}
	if (isset($ctrl)) {
		$delete_url['controller'] = $ctrl;
	}
	echo $this->Html->link('<i class="fa fa-lg fa-trash-o"></i>', $delete_url, ['title' => __d('admin', 'delete_action'), 'onclick' => 'return confirm(\'' . addslashes(__d('admin', 'are_you_sure')) . '\');', 'escape' => FALSE, 'class' => 'frame']);
	?>
</div>
