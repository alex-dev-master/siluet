<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Params Model
 *
 * @property \App\Model\Table\ParamRubricsTable|\Cake\ORM\Association\BelongsTo $ParamRubrics
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\BelongsToMany $Items
 *
 * @method \App\Model\Entity\Param get($primaryKey, $options = [])
 * @method \App\Model\Entity\Param newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Param[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Param|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Param|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Param patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Param[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Param findOrCreate($search, callable $callback = null, $options = [])
 */
class ParamsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('params');
        $this->setDisplayField('caption');
        $this->setPrimaryKey('id');
        
        $this
                ->setFormSchema([
                    'to_filter' => ['label' => 'Отобразить фильтр']
                ])
                ->setOrder([
                    'Params.position' => 'DESC',
                    'Params.id' => 'DESC'
                ]);

        $this->belongsTo('ParamRubrics', [
            'foreignKey' => 'param_rubric_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'options']
        ]);
    }
    
    public function beforeFind(Event $event, Query $query, \ArrayObject $options)
    {
        return $query->mapReduce(function ($result, $key, $mapReduce) {
            if ($result instanceof \Cake\Datasource\EntityInterface) {
                $options = $values = [];
                foreach (array_map('trim', preg_split('~\r|\n~', trim($result->get('options')), -1, PREG_SPLIT_NO_EMPTY)) as $subject) {
                    if ((bool) preg_match('~^(\d+)\s(.+)\s?(\*)?$~U', $subject, $match) and ! isset($options[(int) $match[1]])) {
                        $options[(int) $match[1]] = $match[2];
                    }
                }
                if ($matching = $result->get('_matchingData')) {
                    $values = array_map('trim', explode('|', $matching['ItemsParams']['value']));
                }
                $result->set('_options', $options);
                $result->set('_values', $values);
            }
            $mapReduce->emit($result, $key);
        });
    }
    
    public function getParams()
    {
        $this->belongsToMany('Items');
        $params = $this->find()->contain(['ParamRubrics'])->toArray();
        dd($params);
    }
    
    public function getCondition($conditions, $params = [])
    {
        $i = 0;
        $q = \Cake\ORM\TableRegistry::getTableLocator()->get('ItemsParams')->find();
        foreach ($params as $pid => $values) {
            if (empty($values)) {
                continue;
            }
            $or = [];
            $index = ($i == 0 ? 'ItemsParams' : 'IP' .$i);
            foreach ($values as $b) {
                $or[] = [$index . '.value REGEXP ' => '[[:<:]]' . $b . '[[:>:]]'];
            }
            $q->where([
                'AND' => [
                    'OR' => $or,
                    $index . '.param_id' => $pid
                ]
            ]);
            if ($i) {
                $q->leftJoin([$index => 'items_params'], [
                    $index . '.item_id = ItemsParams.item_id'
                ]);
            }
            $i ++;
        }
        if ($i) {
            $conditions['Items.id'] = null;
            if ($ids = $q->select(['item_id'])->group(['ItemsParams.item_id'])->extract('item_id')->toArray()) {
                unset($conditions['Items.id']);
                $conditions['Items.id IN'] = $ids;
            }
        }
        return $conditions;
    }
}
