<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentImages Model
 *
 * @method \App\Model\Entity\AttachmentImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AttachmentArticlesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachment_articles');
        $this->setDisplayField('caption')
//            ->setDisplayImage('image')
            //->newPositionLast(true)
            ->setOrder(['AttachmentArticles.position' => 'DESC', 'AttachmentArticles.id' => 'DESC'])
            ->setNeighborhood([]);
        $this->setPrimaryKey('id');

        $this
            ->setFormSchema([
                'text' => ['type' => 'wysiwyg']
            ]);

//        $this->belongsToMany('Services');

    }

}
