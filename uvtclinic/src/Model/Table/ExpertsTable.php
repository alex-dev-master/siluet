<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ExpertsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('experts')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->setOrder(['Experts.position' => 'DESC', 'Experts.id' => 'DESC']);
        $this->setFormSchema([
            'rank' => ['label' => 'Звание (можно через запятую)'],
            'specialization' => ['label' => 'Специализация', 'type' => 'wysiwyg'],
            'education' => ['label' => 'Образование', 'type' => 'wysiwyg'],
            'experience' => ['label' => 'Опыт', 'type' => 'wysiwyg'],
            'congresses' => ['label' => 'Конгрессы', 'type' => 'wysiwyg'],
            'to_main_page' => ['label' => 'Отображать на главной'],
            'to_experts_page' => ['label' => 'Выводить на страницу "Наши специалисты"'],
            'to_abouts_page' => ['label' => 'Выводить на страницу "О клинике"'],
            'image' => ['params' => ['copy' => '']],
            'image_square' => ['label' => 'Квадратная картинка', 'type' => 'image', 'params' => ['resize' => 'crop', 'x' => 240, 'y' => 240]],
            'thumbnail_400x400' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 400, 'y' => 400, 'source' => 'image']],
            'thumbnail_210x210' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 210, 'y' => 210, 'source' => 'image']],
            'thumbnail_60x60' => ['type' => 'image', 'params' => ['resize' => 'max', 'x' => 60, 'y' => 60, 'source' => 'image']],
        ]);
        $this->setTabSchema([
            'attachment_timetables' => ['label' => 'Расписание работы']
        ]);

        $this->belongsToMany('Specialities');
        $this->hasMany('AttachmentFiles', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['model' => 'Experts']]);
        $this->hasMany('AttachmentTimetables', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['model' => 'Experts']]);
    }
}
