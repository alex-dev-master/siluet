<div class="page-content price-content">
    <img class="content-bg" src="/img/price-bg.png" alt="">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <?php foreach ($itemRubrics as $rubric) {?>
            <?php if (!empty($rubric['attachment_costs'])) { ?>
            <div class="block">
                <h2 class="title"><?=$rubric['caption']?></h2>
            <table>
                <tr>
                    <th>Наименование услуги</th>
                    <th>Стоимость, руб.</th>
                </tr>
                <?php foreach ($rubric['attachment_costs'] as $cost) { ?>
                <tr>
                    <td>
                        <p class="main-text"><?=$cost['caption']?></p>
                    </td>
                    <td>
                        <p class="price"><?=$cost['cost']?></p>
                    </td>
                </tr>
                <?php } ?>
            </table>
            </div>
            <?php } ?>

        <?php } ?>
        <?php if (!empty($row)) { ?>
        <div class="doc">
            <h3 class="title">Документы для скачивания</h3>
            <ul class="list-ul">
                <?php foreach ($row['attachment_files'] as $file) { ?>
                <li class="list-item">
                    <h5 class="doc-title"><?=$file['caption']?></h5>
                    <a class="doc-link" href="/files/attachment-files/<?=$file['file']?>" download><?=$file['file']?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>
