<div class="page-top">
    <div class="wrapper">
        <p class="subheading"><?=__('О компании');?></p>
        <h1><?=__('Эксперты компании');?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                <div class="content__left-col">
                    <ul class="left-menu">
                        <?= $this->cell("Menu::sidebar", [$langs, $settings]) ?>
                    </ul>
                    <div class="slogan"><p><?=__('Нам доверяют');?></p></div>
                </div>
            </div>
            <div class="col-md-8">
                <h2><?=$about['caption']?></h2>
                <?=$about['content']?>
                    <div class="team-list">
                        <?php foreach ($experts as $expert) { ?>
                            <div class="team-item">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5 col-ss-6 col-xs-12">
                                        <div class="team-item__img">
                                            <?php if (!empty($expert['image'])) { ?>
                                            <img src="/files/experts/<?=$expert['image']?>">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-ss-6 col-xs-12">
                                        <div class="team-item__text">
                                            <p class="team-item__name"><?=$expert['caption']?></p>
                                            <p class="team-item__spec"><?=$expert['post']?></p>
                                            <p class="team-item__descr"><?=$expert['text']?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
            </div>
        </div>
    </div>
</div>
