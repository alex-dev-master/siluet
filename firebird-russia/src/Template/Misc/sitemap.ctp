<div class="sitemap">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <div class="text">
            <h1>Карта сайта</h1>
            <ul class="sitemap-list">
                <li><a href="/"><?=__('Главная');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Abouts', 'action' => 'index']); ?>"><?=__('О нас');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Manufactures', 'action' => 'index']); ?>"><?=__('Производство');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Deliveries', 'action' => 'index']); ?>"><?=__('Доставка и оплата');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Shops', 'action' => 'index']); ?>"><?=__('Магазинам');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Portfolios', 'action' => 'index']); ?>"><?=__('Портфолио');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Portfolios', 'action' => 'index']); ?>"><?=__('Портфолио');?></a>
                    <ul>
                        <?php foreach ($items as $item) { ?>
                        <li><a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "index", $item['slug']]) ?>"><?=$item['caption'];?></a>
                            <ul>
                                <?php foreach ($item['items'] as $product) { ?>
                                <li>
                                    <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "view", $product['slug']]) ?>"><?=$product['caption'];?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Services', 'action' => 'index']); ?>"><?=__('Услуги');?></a>
                    <ul>
                        <?php foreach ($services as $service) { ?>
                        <li><a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $service['slug']]) ?>"><?=$service['caption']?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Works', 'action' => 'index']); ?>"><?=__('Как мы работаем');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Questions', 'action' => 'index']); ?>"><?=__('Вопросы и ответы');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Contacts', 'action' => 'index']); ?>"><?=__('Контакты');?></a></li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Jobs', 'action' => 'index']); ?>"><?=__('Вакансии');?></a>
                    <ul>
                        <?php foreach ($jobs as $job) { ?>
                            <li><a href="<?= $this->Url->build(["controller" => "Jobs","action" => "view", $job['slug']]) ?>"><?=$job['caption'];?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Blogs', 'action' => 'index']); ?>"><?=__('Блог');?></a>
                    <ul>
                        <?php foreach ($blogs as $blog) { ?>
                            <li><a href="<?= $this->Url->build(["controller" => "Blogs","action" => "view", $blog['slug']]) ?>"><?=$blog['caption'];?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo $this->Url->build(['controller' => 'Reviews', 'action' => 'index']); ?>"><?=__('Отзывы');?></a></li>


            </ul>
        </div>
    </div>
</div>

