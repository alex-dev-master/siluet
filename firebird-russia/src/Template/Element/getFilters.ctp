<div class="wrapper">
    <div class="row">
        <?php foreach ($sub_caption_filter as $filter) { ?>
            <?php foreach ($filter['params'] as $filter_param) { ?>
                <?php if ($filter_param['to_filter']) { ?>
                    <div class="col-md-4 filter_catalog">
                        <label class="title_of_option"><?=$filter_param['caption']?></label>
                        <select>
                            <option disabled selected>Выберите</option>
                            <?php
                            foreach ($filter_param['_options'] as $param) {
                                ?>
                                <option data-category="<?=$filter_param['caption']?>"><?=$param?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
    <?=$this->element('Portfolios/filterForm')?>
    <a class="btn filter-btn" href="#">Заказать похожие</a>
</div>
