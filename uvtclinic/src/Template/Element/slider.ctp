<div class="jumbotron">
    <div class="jumbotron__slider">
        <?php foreach ($sliders as $slider) { ?>
        <div class="slide">
            <img class="slide-img" src="files/sliders/<?=$slider['image']?>">
            <div class="wrapper parent">
                <div class="jumbotron__slider-text child">
                    <p class="title"><?=$slider['caption']?></p>
                    <p class="subtitle"><?=$slider['caption_2']?></p>
                    <?php if (!empty($slider['url'])) { ?>
                    <a class="btn jumbotron-btn" href="<?=$slider['url']?>">Посмотреть</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
