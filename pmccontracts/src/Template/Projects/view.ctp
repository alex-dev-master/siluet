<div class="page-top sub">
    <div class="wrapper">
        <p class="subheading"><?php if ($object['archive_status']) { echo 'Завершен'; } else { echo 'В работе'; } ?></p>
        <h1><?=$object['caption']?></h1>
    </div>
</div>
<div class="content pdg-0">
    <div class="project-one__info">
        <div class="wrapper">
            <div class="row">
                <div class="col-sm-8 col-md-8 col-sm-7">
                    <div class="project-one__gallery">
                        <div class="vertical-title">
                            <p>Фотогаллерея<span>01</span></p>
                        </div>
                        <div class="project-one__gallery-slider">
                            <?php foreach ($object['attachment_images'] as $image) { ?>
                            <div class="slide">
                                <img src="/files/attachment-images/<?=$image['thumbnail_700x400']?>">
                                <a href="/files/attachment-images/<?=$image['image']?>" rel="group" class="fancybox"><img src="/img/plus.png"></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-5">
                    <div class="project-one__info-text">
                        <div class="vertical-title">
                            <p>О проекте<span>02</span></p>
                        </div>
                        <ul class="project-one__info-list">
                            <?php if (!empty($object['customer'])) { ?>
                            <li>
                                <p class="title">Заказчик</p>
                                <p><?=$object['customer']?></p>
                            </li>
                            <?php } ?>
                            <?php if (!empty($object['implement_period'])) { ?>
                            <li>
                                <p class="title">Срок реализации</p>
                                <p><?=$object['implement_period']?></p>
                            </li>
                            <?php } ?>
                            <?php if (!empty($object['place'])) { ?>
                            <li>
                                <p class="title">Место</p>
                                <p><?=$object['place']?></p>
                            </li>
                            <?php } ?>
                            <?php if (!empty($object['investments'])) { ?>
                            <li>
                                <p class="title">Инвестиции</p>
                                <p><?=$object['investments']?></p>
                            </li>
                            <?php } ?>
                            <?php if (!empty($object['tasks'])) { ?>
                            <li>
                                <p class="title">Роли ООО "ПУСК"</p>
                                <?=$object['tasks']?>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="project-one__characteristics">
        <div class="wrapper">
            <h3>Характеристики проекта</h3>
            <?=$object['specifications']?>
        </div>
    </div>
    <div class="navigate-btns">
        <div class="wrapper">
            <?php
            $str = '?';
            if (!empty($category)) {
                $str = $str."category=$category&";
            }
            if (!empty($status_objects)) {
                $str = $str."status_objects=$status_objects&";
            }
            if (!empty($_GET['page'])) {
                $pageGet = $_GET['page'];
                $str = $str."page=$pageGet";
            }
            if ($str == '?') $str = '';
            ?>

            <?php if (!empty($slug_closest['before'])) {?>
            <a href="<?= $this->Url->build(['controller' => 'Projects', 'action' => $slug_closest['before']]); ?><?=$str?>" class="left-btn"><img src="/img/arrow-left.png"> </a>
            <?php } ?>
            <a href="/projects<?=$str?>" class="btn all-btn">К списку проектов</a>
            <?php if (!empty($slug_closest['after'])) {?>
            <a href="<?= $this->Url->build(['controller' => 'Projects', 'action' => $slug_closest['after']]); ?><?=$str?>" class="right-btn"><img src="/img/arrow-right.png"> </a>
            <?php } ?>
        </div>
    </div>
</div>

