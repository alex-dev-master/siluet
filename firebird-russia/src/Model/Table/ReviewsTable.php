<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReviewsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reviews')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->addBehavior('Timestamp')
            ->setOrder(['Reviews.published' => 'DESC']);
        $this->setDisplayImage('image');

        $this
            ->setFormSchema([
                'image' => ['params' => ['copy' => '']],
                'thumbnail_183x260' => ['type' => 'image', 'params' => ['resize' => 'fill', 'color' => 'ffffff', 'x' => 183, 'y' => 260, 'source' => 'image']],
            ])
            ->setTabSchema([
            ]);

//        $this->belongsToMany('Items');

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
        ]);
    }
}
