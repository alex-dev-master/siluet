<div class="page-content about-content about-articles_detail-content">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            $rubric_row['caption'],
            ['controller' => 'ArticleRubrics', 'action' => 'index', $rubric_row['slug']]
        );
        $this->Breadcrumbs->prepend(
            __('Полезная информация'),
            ['controller' => 'Pages', 'action' => 'articles']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <div class="border-top">
            <h1><?=$row['caption']?></h1>
            <h6 class="date"><?=$row->published->format('d / m / Y')?></h6>
            <div class="publ-main">
                <div class="row list">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="inner-container">
                            <img class="publ-mainImg" src="/files/articles/<?=$row['image_inner']?>" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="inner-container">
                            <?=$row['content_inner_small']?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="publ-desc text">
                <?php if (!empty($new_content)) {?>
                    <?=$new_content?>
                <?php } else { ?>
                <?=$row['content']?>
                <?php } ?>
            </div>
            <div class="articles-buttons">
                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "index"]) ?>" class="btn articles_detail-button">Все статьи</a>
                <a href="<?= $this->Url->build(["controller" => "ArticleRubrics", "action" => "index", $rubric_row['slug']]) ?>" class="btn articles_detail-button"><?=$rubric_row['caption']?></a>
            </div>
            <div class="pagination-pages">
                <?php if (!empty($slug_closest['before'])) { ?>
                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $slug_closest['before']['slug']]) ?>" class="prev-page">
                    <img src="/img/arrow-left.png">
                    <p><?=$slug_closest['before']['caption']?></p>
                </a>
                <?php } ?>
                <?php if (!empty($slug_closest['after'])) { ?>
                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $slug_closest['after']['slug']]) ?>" class="next-page">
                    <p><?=$slug_closest['after']['caption']?></p>
                    <img src="/img/arrow-right.png">
                </a>
                <?php } ?>
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>
