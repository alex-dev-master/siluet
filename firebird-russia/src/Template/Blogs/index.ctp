<div class="blog">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$caption_for_layout?></h1>
        <div class="row">
            <div class="col-md-8">
                <div class="blog__main-list">
                    <?php foreach ($blogs as $blog) { ?>
                    <?php if (!$blog['to_sidebar']) { ?>
                    <a href="<?= $this->Url->build(["controller" => "Blogs","action" => "view", $blog['slug']]) ?>">
                        <div class="item">
                            <div class="img-container">
                                <img src="/files/blogs/<?=$blog['image']?>">
                            </div>
                            <div class="text">
                                <p class="date">
                                    <?=$blog->published->format('d.m.Y')?>
                                </p>
                                <p class="title">
                                    <?=$blog['caption']?>
                                </p>
                                <p class="descr"><?=$blog['preview']?></p>
                            </div>
                        </div>
                    </a>
                        <?php }?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="blog__right-list">
                    <div class="row">
                        <?php foreach ($blogs_sidebar as $blog) { ?>
                            <?php if ($blog['to_sidebar'] && !empty($blog['image'])) { ?>
                                <div class="col-lg-12 col-md-12 col-sm-6">
                            <a href="<?= $this->Url->build(["controller" => "Blogs","action" => "view", $blog['slug']]) ?>">
                                <div class="item">
                                    <div class="img-container">
                                        <img src="/files/blogs/<?=$blog['image']?>">
                                    </div>
                                    <div class="text">
                                        <p class="date">
                                            <?=$blog->published->format('d.m.Y')?>
                                        </p>
                                        <p class="title">
                                            <?=$blog['caption']?>
                                        </p>
                                        <p class="descr"><?=$blog['preview']?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                             <?php }?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="center">
<!--            <a href="#" class="btn blog-btn">Показать еще</a>-->
            <?= $this->element('pagination') ?>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>
