<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;
use SiluetCms\Traits\TableTrait;
use Cake\Cache\Cache;

/**
 * Settings Model
 *
 * @method \App\Model\Entity\Setting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Setting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Setting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Setting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Setting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Setting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Setting findOrCreate($search, callable $callback = null, $options = [])
 */
class SettingsTable extends Table
{
    use TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settings')
                ->setDisplayField('project_title')
                ->setPrimaryKey('id')
                ->setFormSchema([
                    'favicon' => ['params' => ['path' => WWW_ROOT, 'name' => 'favicon.ico']],
                    'phone' => ['label' => 'Номер телефона на сайте'],
                    'email_top' => ['label' => 'Электронный адрес в шапке'],
                    'address' => ['label' => 'Адрес на сайте','type' => 'text'],

                    'contact_form_footer_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_footer'],
                    'contact_form_footer_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_footer'],
                    'contact_form_footer' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_footer'],

                    'contact_form_portfolio_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_portfolio'],
                    'contact_form_portfolio_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_portfolio'],
                    'contact_form_portfolio' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_portfolio'],

                    'contact_form_magazin_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_magazin'],
                    'contact_form_magazin_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_magazin'],
                    'contact_form_magazin' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_magazin'],

                    'contact_form_uslugi_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_uslugi'],
                    'contact_form_uslugi_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_uslugi'],
                    'contact_form_uslugi' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_uslugi'],

                    'contact_form_header_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_header'],
                    'contact_form_header_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_header'],
                    'contact_form_header' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_header'],

                    'contact_form_more_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_more'],
                    'contact_form_more_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_more'],
                    'contact_form_more' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_more'],

                    'contact_form_vacan_from' => ['label' => 'От кого','type' => 'text', 'tab' => 'contact_form_vacan'],
                    'contact_form_vacan_subject' => ['label' => 'Тема письма','type' => 'text', 'tab' => 'contact_form_vacan'],
                    'contact_form_vacan' => ['label' => 'Email оповещения','type' => 'text', 'tab' => 'contact_form_vacan'],

                    'insta_link' => ['label' => 'Ссылка на инстаграм','type' => 'text'],
                ])
                ->setTabSchema([
                    'contact_form_footer' => ['label' => 'Форма над подвалом'],
                    'contact_form_portfolio' => ['label' => 'Форма заказ в портфолио'],
                    'contact_form_magazin' => ['label' => 'Форма из раздела Магазинам'],
                    'contact_form_uslugi' => ['label' => 'Форма заказа услуги'],
                    'contact_form_header' => ['label' => 'Форма заказа звонка'],
                    'contact_form_more' => ['label' => 'Форма расчёта заготовки'],
                    'contact_form_vacan' => ['label' => 'Форма отклика на вакансию'],
                ]);
        $this->addBehavior('Translate', ['fields' =>
            ['address']
        ]);
    }
    
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options) {
        Cache::clear(false, 'settings');
    }
}
