<nav class="navbar">
    <div class="wrapper">
        <div class="header-top">
            <div class="">
                <div class="header-logo">
                    <a href="<?= $this->Url->build(['controller' => 'pages', 'action' => 'mainpage']) ?>"><img src="/img/logo-pages.png"></a>
                </div>
                <div class="header-menu">
                    <div class="hide-mob mobile-menu">
                        <ul class="header-menu-list">
<!--                            <li class="drop-menu">-->
<!--                                <a href="#">О компании</a>-->
<!--                                <div class="submenu-block">-->
<!--                                    <ul class="submenu">-->
<!--                                        <li><a href="/o-kompanii/otzyvy/">Отзывы</a></li>-->
<!--                                        <li><a href="/o-kompanii/menedzhery/">Менеджеры</a></li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </li>-->
                            <?php foreach ($menus as $menu) { ?>
                            <li class="<?php if (isset($menu['current'])) { echo 'active'; } if(!empty($menu['submenu'])) echo ' drop-menu';?> ">
                                <a href="<?= $this->Url->build($menu['url']) ?>"><?= $menu['caption'] ?></a>
                                <?php if(!empty($menu['submenu'])) { ?>
                                <div class="submenu-block">
                                    <ul class="submenu">
                                    <?php foreach ($menu['submenu'] as $submenu) { ?>
                                            <li><a href="<?= $this->Url->build($submenu['url']) ?>"><?=$submenu['caption']?></a></li>
                                    <?php }?>
                                    </ul>
                                </div>
                                <?php } ?>
                            </li>
                            <?php } ?>
                        </ul>


                        <ul class="header-right-btns">
                            <?php
                            if (!empty($settings['phone'])) {
                            $phones = explode(',', $settings['phone']);
                            foreach ($phones as $phone) {?>
                            <li>
                                <a class="header-phone" href="tel:<?=$phone?>"><?=$phone?></a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                    <li>
                        <?php if (!empty($langs)): ?>
                            <?php foreach ($langs as $lang): ?>
                                <?php if (!$lang['selected']) {?>
                                <!--<a class="header-lang" href="<?= $this->Url->build($lang['url']) ?>"><?= $lang['title'] ?></a>-->
                                <?php } ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </li>
                </ul>
                <a class="mobile-close"><img src="../img/cross.png"> </a>
            </div>
        </div>
        <div class="mobile-toggle">
            <div class="btn_mob">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</div>

</div>
</nav>
