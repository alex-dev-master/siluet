<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slider Entity
 *
 * @property int $id
 * @property string $caption
 * @property string $url
 * @property int $position
 * @property bool $disabled
 */
class Slider extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'sub_caption' => true,
        'content_details_button' => true,
        'link_details_button' => true,
        'url' => true,
        'image' => true,
        'position' => true,
        'disabled' => true,
        'project_title_first_block' => true,
        'project_desc_first_block' => true,
        'project_title_second_block' => true,
        'project_desc_second_block' => true,
        'project_title_third_block' => true,
        'project_desc_third_block' => true,
        'project_details_link' => true,
        'status_icon_1' => true,
        'image_icon_1' => true,
        'status_icon_2' => true,
        'image_icon_2' => true,
        'status_icon_3' => true,
        'image_icon_3' => true,
        'status_icon_4' => true,
        'image_icon_4' => true,
        'status_icon_5' => true,
        'image_icon_5' => true,
        'status_icon_6' => true,
        'image_icon_6' => true,
        'point_x' => true,
        'point_y' => true,
        'point_status' => true,
        'services' => true,
    ];
}
