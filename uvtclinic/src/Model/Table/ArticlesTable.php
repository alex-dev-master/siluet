<?php
namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 */
class ArticlesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    public $formSchema = array();
    public $tabSchema = array();

    private $__schema = array();
    private $__View = null;
    private $__contacts = array();
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->setDisplayImage('image')
            ->setOrder(['Articles.published' => 'desc'])
            ->setNeighborhood(['article_rubric_id']);

        $this->setFormSchema([
            'image' => ['params' => ['copy' => '']],
            'image_inner' => ['label' => 'Внутренняя картинка','type' => 'image','params' => ['resize' => 'crop', 'x' => 570, 'y' => 400]],
            'color' => ['label' => 'Цвет', 'class' => 'color-picker'],
            'thumbnail_370x260' => ['type' => 'image','params' => ['resize' => 'crop', 'x' => 370, 'y' => 260, 'source' => 'image']],
            'thumbnail_570x400' => ['type' => 'image','params' => ['resize' => 'crop', 'x' => 570, 'y' => 400, 'source' => 'image']],
            'thumbnail_220x220' => ['type' => 'image','params' => ['resize' => 'crop', 'x' => 220, 'y' => 220, 'source' => 'image']],
            'content_inner_small' => ['type' => 'wysiwyg', 'label' => 'Содержимое (краткое описание)']
        ]);

        $this->setTabSchema([
        ]);

        $this->addBehavior('Timestamp');

        $this->belongsTo('ArticleRubrics');
        $this->belongsToMany('Items');
        $this->belongsToMany('ItemRubrics');

    }

    public function beforeFind(Event $event, Query $query, \ArrayObject $options) {


    }

}
