<?php
$service = $this->Services->find()->where(['slug' => $slug]);
$objects = TableRegistry::getTableLocator()->get('Objects')->find()
    ->join(['ObjectsServices' => 'objects_services'], ['ObjectsServices.object_id = Object.id'])
    ->where(['ObjectsServices.service_id' => $service->get('id')])
    ->group('Object.id');


$services = TableRegistry::getTableLocator()->get('Services')->find();
$ids = [];
$services->each(function ($item) use (&$ids) {
    $ids[] = $item->get('id');
});
$objects = [];
TableRegistry::getTableLocator()->get('Objects')->find()
    ->join(['ObjectsServices' => 'object_services'], ['ObjectsServices.object_id = Object.id'])
    ->where(['service_id IN' => $ids])
    ->each(function ($item) use (&$objects) {
        $objects[$item->get('objects_services')->service_id][$item->get('id')] = $item;
    });
$services = $service->map(function ($item) use ($objects) {
    $rows = [];
    if (isset($objects[$item->get('id')])) {
        $rows = $objects[$item->get('id')];
    }
    $item->set('objects', $rows);
});
