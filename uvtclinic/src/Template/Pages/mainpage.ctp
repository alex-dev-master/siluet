<?= $this->element('slider', [])?>

<div class="main-page">
    <div class="main-buttons">
        <div class="wrapper">
            <div class="buttons-list">
                <div class="row list">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <a class="btn main-btn" href="#mail-modal" data-toggle="modal">
                            <img class="btn-img" src="/img/mail-big.png" alt="">
                            <div class="btn-text">
                                <h4 class="btn-title">Напишите нам</h4>
                                <p class="btn-desc">Открыть форму<img src="/img/arrow-black.png" alt=""></p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <a class="btn main-btn" href="/timetable">
                            <img class="btn-img" src="/img/calendar-big.png" alt="">
                            <div class="btn-text">
                                <h4 class="btn-title">Расписание приема</h4>
                                <p class="btn-desc">Посмотреть<img src="/img/arrow-black.png" alt=""></p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-md-push-0 col-sm-6 col-sm-push-3 col-xs-12">
                        <a class="btn main-btn" href="/contacts">
                            <img class="btn-img" src="/img/time-big.png" alt="">
                            <div class="btn-text">
                                <h4 class="btn-title">Режим работы</h4>
                                <p class="btn-desc">Посмотреть<img src="/img/arrow-black.png" alt=""></p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-serv">
        <div class="wrapper main-wrap">
            <p class="title-blue">Как мы можем помочь?</p>
            <h2 class="title">Наши услуги</h2>

            <div class="serv-list">
                <div class="row list">
                    <?php foreach ($services as $service) { ?>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="inner-container">
                            <a href="<?= $this->Url->build(["controller" => "ItemRubrics","action" => "index", $service['slug']]) ?>">
                                <img src="/files/item-rubrics/<?=$service['image']?>" alt="">
                                <?php if (!empty($service['image_2'])) {?>
                                <img class="hidden-img" src="/files/item-rubrics/<?=$service['image_2']?>" alt="">
                                <?php } ?>
                                <h6 class="serv-title"><?=$service['caption']?></h6>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-worker">
        <div class="wrapper main-wrap">
            <p class="title-blue">Кто у нас работает?</p>
            <h2 class="title">Наши специалисты</h2>
            <a class="btn long-btn" href="/experts">Все специалисты</a>
            <div class="worker-list">
                <div class="row list">
                    <?php foreach ($experts as $key=>$expert) { ?>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="inner-container">
                            <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                                <?php if (!empty($expert['image_square'])) {?>
                                <img class="worker-img" src="/files/experts/<?=$expert['image_square']?>" alt="">
                                <?php } ?>
                            </a>
                            <div class="text-wrap">
                                <a class="links" href="<?= $this->Url->build(["controller" => "Pages","action" => "timetable"]) ?>"><img src="/img/calendar.png" alt=""></a>

                                <?php $j = 0; $data_date_arr = []; foreach ($days as $day) {
                                    if (in_array($day['caption'], $expert['days_work'])) {
                                        $days_reduction = [
                                            'Понедельник' => '1',
                                            'Вторник' => '2',
                                            'Среда' => '3',
                                            'Четверг' => '4',
                                            'Пятница' => '5',
                                            'Суббота' => '6',
                                            'Воскресенье' => '0',
                                        ];
                                        $data_date_arr[] = $days_reduction[$day['caption']];
                                    }
                                }
                                if (!empty($data_date_arr)) {
                                    $data_date = implode(',', $data_date_arr);
                                } else {
                                    $data_date = '';
                                }
                                ?>
                                <a class="links" data-id="<?=$key?>" data-date="<?=$data_date?>"    data-expert="<?=$expert['caption']?>"  href="#timetable-modal" data-toggle="modal"><img src="/img/pencil.png" alt=""></a>

                                <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                                    <h5 class="worker-title"><?=$expert['caption']?></h5>
                                    <p class="worker-desc">
                                        <?php foreach ($expert['specialities'] as $speciality) { ?>
                                        <?=$speciality['caption']?> <br>
                                        <?php } ?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-publ">
        <div class="wrapper main-wrap">
            <p class="title-blue">Что нового?</p>
            <h2 class="title">Публикации</h2>

            <div class="publ-list">
                <div class="row list">
                    <?php foreach ($articles as $article) { ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-container patient">
                            <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>">
                                <img class="publ-img" src="/files/articles/<?=$article['thumbnail_370x260']?>" alt="">
                            </a>
                            <div class="text-wrap">
                                <div class="btn sml-btn" style="background-color: #<?=$article['article_rubric']['color']?>"><?=$article['article_rubric']['caption']?></div>
                                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>">
                                    <h6 class="publ-title"><?=$article['caption']?></h6>
                                </a>
                                <p class="publ-date sml-text"><?=$article->published->format('d / m / Y')?></p>
                                <a class="moreBtn" href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>"></a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <a class="btn long-btn" href="/articles">Все публикации</a>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="main-about">
        <div class="wrapper main-wrap">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 col-md-push-6">
                    <p class="title-blue">О клинике</p>
                    <h2 class="title"><?=$about['caption']?></h2>
                    <?=$about['content']?>
                    <a class="simple-link" href="/abouts">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="timetable-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/modal-close.png"></button>
                <h3 class="modal-title">Запись на прием</h3>
            </div>
            <!-- Основное содержимое модального окна -->
            <?= $this->Form->create(['schema' => []], ['id' => 'sendTimetable', 'class' => 'form']); ?>
            <div class="modal-body">
                <label for="name2">Ваше имя<span>*</span></label>
                <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input required" name="phone" id="organization2" placeholder="Ваш номер телефона">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail</label>
                <input class="modal-input" name="email" id="organization2" placeholder="Электронная почта для связи">


                <label for="organization3">Специалист</label>
                <select name="select" id="organization3" class="disabled">
                    <?php foreach ($experts as $expert) { ?>
                        <option value="<?=$expert['caption']?>"><?=$expert['caption']?></option>
                    <?php } ?>
                </select>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <label for="organization2">Время</label>
                        <input class="modal-input input-time" name="time" id="organization2" type="text">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <label for="organization2">Дата</label>
                        <input class="modal-input input-date datepicker" name="date" id="organization2">
                    </div>
                </div>


                <label for="organization2">Комментарий</label>
                <textarea class="modal-input" id="organization2" name="msg" placeholder="Дополнительная информация"></textarea>

                <label class="checkbox">
                    <input type="checkbox" name="privacy-policy">
                    <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                            конфиденциальности</a></div>
                </label>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-mod"><img src="/img/mail.png">Отправить</button>
                </div>
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

