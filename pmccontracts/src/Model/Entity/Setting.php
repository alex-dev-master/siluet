<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Setting Entity
 *
 * @property int $id
 * @property string $project_title
 * @property string $favicon
 * @property string $code_bh
 * @property string $code_ab
 * @property string $code_bb
 * @property string $copyright
 * @property string $bf587ceef5
 */
class Setting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_title' => true,
        'phone' => true,
        'favicon' => true,
        'code_bh' => true,
        'code_ab' => true,
        'code_bb' => true,
        'copyright' => true,
        'bf587ceef5' => true
    ];
}
