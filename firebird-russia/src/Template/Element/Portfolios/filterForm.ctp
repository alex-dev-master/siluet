<div class="filter-more">
    <h3>Заполните форму и получите расчет стоимости изготовления уже сегодня</h3>
    <?= $this->Form->create(['schema' => []], ['id' => 'filter-more', 'class' => '']); ?>
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-5">
                <label>Вид заготовки</label>
                <div class="zagotovka__image">
                    <a href="#zagotovka-modal" data-toggle="modal">
                        <div class="top">
                            <input class="name" type="text" name="zagotovka" id="zagotovka" placeholder="Выберите">
                            <!--<span class="name">Выберите</span>-->
                        </div>
                    </a>
                    <div class="image" >
                        <div class="img-container">
                            <img id="zagotovka-image" src="">
                        </div>
                        <a href="#" id="zagotovka-delete" class="delete-btn"><img src="/img/trash.png"> </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-7">
                <label>Количество</label>
                <div class="incr-input__div">
                    <input type="text" name="count" class="incr-input" id="persons" value="1">
                </div>
                <label for="msg">Файл (не более 50Мб)</label>
                <div class="files">
                    <div class="file-input">
                        <div class="file_upload">
                            <div>Файл не выбран</div>
                            <button>Обзор</button>
                            <input type="file">
                        </div>
                         
                    </div>

                </div>
                <label for="msg">Удобный способ связи</label>
                <div class="call-type">
                    <label class="checkbox">
                        <input name="WhatsApp" type="checkbox">
                        <div class="checkbox__text">WhatsApp</div>
                    </label>
                    <label class="checkbox">
                        <input name="Viber" type="checkbox">
                        <div class="checkbox__text">Viber</div>
                    </label>
                    <label class="checkbox">
                        <input name="Tel" type="checkbox">
                        <div class="checkbox__text">Телефон</div>
                    </label>
                </div>
            </div>
            <div class="col-md-5 col-lg-6  col-sm-12">
                <div class="row">
                    <div class="col-md-12">
                        <label for="name">Ваше имя<span>*</span></label>
                        <input class="modal-input" name="name" id="name">
                        <p class="error-message" style="display: none">Это поле обязательно для заполнения</p>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="Email">E-mail<span>*</span></label>
                                <input class="modal-input" name="email" id="Email">
                                <p class="error-message" style="display: none">Это поле обязательно для заполнения</p>
                            </div>
                            <div class="col-md-6">

                                <label for="phone">Телефон</label>
                                <input class="modal-input" name="phone" id="phone">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="msg">Комментарий<span>*</span></label>
                        <textarea name="msg" id="msg"></textarea>
                        <p class="error-message" style="display: none">Это поле обязательно для заполнения</p>
                    </div>

                </div>
            </div>
        </div>
        <div class="btns">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <label class="checkbox">
                        <input name="privacy-policy" type="checkbox">
                        <div class="checkbox__text"> Я принимаю <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>">Политику
                                конфиденциальности</a></div>
                    </label>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="right">
                        <a href="#" class="btn cancel-btn">Отменить</a>
                        <button class="btn form__btn btn-form">Отправить</button>
                    </div>
                </div>
            </div>
            <h4 class="success" style="display: none;color: green;">Заявка отправлена!</h4>
        </div>
    <?= $this->Form->end(); ?>
</div>
