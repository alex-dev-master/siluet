<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 */
class QuestionsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('questions');
        $this->setDisplayField('caption')
            ->setDisplayImage('image')
            ->setPrimaryKey('id')
            ->setOrder(['Questions.position' => 'desc', 'Questions.id' => 'desc'])
            ->setNeighborhood(['question_rubric_id'])
            ->paginateLimit(20);
        $this->setFormSchema([
            'caption_question' => ['label' => 'Вопрос', 'type' => 'wysiwyg'],
            'caption_response' => ['label' => 'Ответ', 'type' => 'wysiwyg'],
        ])
            ->setTabSchema([

            ]);
        $this->belongsTo('QuestionRubrics');

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'caption_question', 'caption_response']
        ]);
    }
}
