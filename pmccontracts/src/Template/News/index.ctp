<div class="page-top">
    <div class="wrapper">
        <h1 class="h1-big"><?=__('Новости');?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">

        <div class="news-list">
            <?php foreach ($news as $new) {?>
            <div class="new">
                <div class="row">
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <div class="new__img">
                            <a href="<?=$this->Url->build(['controller' => 'News', 'action' => $new['slug']])?>">
                                <?php if (!empty($new['thumbnail_470x290'])) { ?>
                                <img src="/files/news/<?=$new['thumbnail_470x290']?>">
                                <?php } else {?>
                                 <img src="/img/no-image.png">
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-8 col-xs-12">
                        <div class="new__text">
                            <a href="<?=$this->Url->build(['controller' => 'News', 'action' => $new['slug']])?>"><p class="new__title"><?=$new['caption']?></p></a>
                            <p class="new__description"><?=$new['preview']?></p>
                            <div class="new__btns">
                                <p class="new__date"><?=$new->published->format('d.m.Y')?></p>
                                <a href="<?=$this->Url->build(['controller' => 'News', 'action' => $new['slug']])?>" class="more-link"><?=__('Подробнее');?></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <?= $this->element('pagination') ?>
    </div>
</div>
