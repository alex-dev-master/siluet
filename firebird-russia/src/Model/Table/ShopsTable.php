<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ShopsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('shops')
            ->setDisplayField('caption')
            ->setPrimaryKey('id');

        $this
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 1160, 'y' => 610]],
                'image_manager' => ['label' => 'Изоображение', 'tab' => 'manager', 'type' => 'image','params' => ['resize' => 'crop', 'x' => 240, 'y' => 240]],
                'content_manager' => ['label' => 'Текст', 'tab' => 'manager'],
            ])
            ->setTabSchema([
                'manager' => ['label' => 'Менеджер']
            ]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'content_manager', 'meta_title', 'meta_description', 'meta_keywords']
        ]);

        $this->hasMany('AttachmentImages', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Shops']]);

    }
}
