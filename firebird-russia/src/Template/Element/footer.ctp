<?= $this->cell("Menu::footer", [$langs, $settings]) ?>

<div id="call-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <?= $this->Form->create(['schema' => []], ['id' => 'call-modal-form', 'class' => '']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="/img/form-close.png"></button>
                <h4 class="modal-title"><?=__('Заказать звонок')?></h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <label for="name2"><?=__('Ваше имя')?></label>
                <input class="modal-input" name="name" id="name2">
                <label for="organization2"><?=__('Телефон')?><span>*</span></label>
                <input class="modal-input phone" name="phone" id="organization2">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <input type="hidden" value="Форма заказа шапке">

            </div>
            <div class="modal-footer">
                <div class="btns">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="checkbox">
                                <input name="privacy-policy" type="checkbox">
                                <div class="checkbox__text"><?=__('Я принимаю')?><a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?></a></div>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <button href="#" class="btn btn-modal"><?=__('Отправить')?></button>
                        </div>
                    </div>
                </div>
                <img src="/img/doll.png" class="modal-doll">
                <h4 style="display: none; color: green;" class="success" ><?=__('Ваша заявка успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<div id="order-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(['schema' => []], ['id' => 'order-modal-form', 'class' => '']); ?>
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="/img/form-close.png"></button>
                <h4 class="modal-title"><?=__('Заявка на расчет')?></h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <input type="hidden" name="towar">
                <label for="name2"><?=__('Ваше имя')?><span>*</span></label>
                <input class="modal-input" id="name2" name="name">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Телефон')?><span>*</span></label>
                <input class="modal-input phone" id="organization2" name="phone">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail<span>*</span></label>
                <input class="modal-input" id="organization2" name="email">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Комментарий')?><span>*</span></label>
                <textarea name="msg"></textarea>
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>

                <label for="msg"><?=__('Файлы (не более 50Мб)')?></label>

                <div class="files">
                    <div class="file-input">
                        <div class="file_upload">
                            <div class="file-name"><?=__('Файл не выбран')?></div>
                            <button><?=__('Обзор')?></button>
                            <input type="file">
                        </div>
                        <a class="delete-place"><img src="/img/delete.png"> </a>
                    </div>
                    <a href="#" class="add-more__btn"><?=__('Добавить файл')?></a>
                </div>

            </div>
            <div class="modal-footer">
                <div class="btns">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="Заказ в портфолио">
                            <label class="checkbox">
                                <input type="checkbox" name="privacy-policy">
                                <div class="checkbox__text"><?=__('Я принимаю')?> <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?> </a></div>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <button data-tab="#success_order-modal-form" class="btn btn-modal"><?=__('Отправить')?></button>
                        </div>
                    </div>
                </div>
                <img src="/img/doll.png" class="modal-doll">
                <h4 id="success_order-modal-form"  style="display: none; color: green;" class="success" ><?=__('Ваша заявка успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

