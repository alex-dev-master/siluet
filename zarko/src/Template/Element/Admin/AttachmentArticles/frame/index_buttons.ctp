<?php
// list_button_add
if (isset($is_tree, $rubric_id) && $is_tree) {
    $add_url = $UrlCollection->append(['action' => 'add', $rubric_id]);
    $group_delete_url = $UrlCollection->append(['action' => 'group_delete', 'id' => $rubric_id]);
    $group_disable_url = $UrlCollection->append(['action' => 'group_disable', 'id' => $rubric_id]);
    $group_enable_url = $UrlCollection->append(['action' => 'group_enable', 'id' => $rubric_id]);
} elseif (isset($has_rubric, $rubric_id) && $has_rubric) {
    $add_url = $UrlCollection->append(['action' => 'add', $rubric_id]);
    $group_delete_url = $UrlCollection->append(['action' => 'group_delete', 'id' => $rubric_id]);
    $group_disable_url = $UrlCollection->append(['action' => 'group_disable', 'id' => $rubric_id]);
    $group_enable_url = $UrlCollection->append(['action' => 'group_enable', 'id' => $rubric_id]);
} else {
    $add_url = $UrlCollection->append(['action' => 'add']);
    $group_delete_url = $UrlCollection->append(['action' => 'group_delete']);
    $group_disable_url = $UrlCollection->append(['action' => 'group_disable']);
    $group_enable_url = $UrlCollection->append(['action' => 'group_enable']);
}
/*if ($this->request->ext) {
	$group_delete_url['ext'] = $this->request->ext;
	$group_disable_url['ext'] = $this->request->ext;
	$group_enable_url['ext'] = $this->request->ext;
}*/
/*if (isset($this->request->named['model'], $this->request->named['foreign_key'])) {
	$group_delete_url = array_merge($group_delete_url, ['model' => $this->request->named['model'], 'foreign_key' => $this->request->named['foreign_key']]);
	$group_disable_url = array_merge($group_disable_url, ['model' => $this->request->named['model'], 'foreign_key' => $this->request->named['foreign_key']]);
	$group_enable_url = array_merge($group_enable_url, ['model' => $this->request->named['model'], 'foreign_key' => $this->request->named['foreign_key']]);
}*/
if (!isset($add_deny)) {
    echo $this->Form->button(__d('admin', 'add_action'), ['url' => $add_url, 'icon' => ['fa fa-plus-circle'], 'class' => 'btn btn-primary', 'modal' => true]);
}
// list_buttons

// list_button_parent_back / list_button_rubric_back
if (isset($is_tree, $parent_or_rubric_id) && $is_tree) {
    $parent_back_url = $UrlCollection->append(['action' => 'back', 'id' => $parent_or_rubric_id, 'autoPage' => FALSE]);
    echo $this->Form->button(__d('admin', 'parent_index_action'), ['url' => $parent_back_url, 'class' => 'btn btn-primary', 'icon' => 'fa fa-arrow-circle-left']);
    $root_back_url = $UrlCollection->append(['action' => 'index', 'autoPage' => FALSE]);
    echo $this->Form->button(__d('admin', 'root_index_action'), ['url' => $root_back_url, 'class' => 'btn btn-primary', 'icon' => 'fa fa-arrow-circle-up']);
} elseif (isset($has_rubric, $rubric_id) && $has_rubric) {
    $rubric_back_url = $UrlCollection->append(['action' => 'rubricBack', 'id' => $rubric_id, 'autoPage' => FALSE]);
    echo $this->Form->button(__d('admin', 'rubric_index_action'), ['icon' => 'fa fa-arrow-up', 'url' => $rubric_back_url, 'class' => 'btn btn-primary']);
} elseif (isset($has_rubric) && $has_rubric && !isset($rubric_id)) {
    $rubric_back_url = $UrlCollection->append(['action' => 'rubricBack', 'autoPage' => FALSE]);
    echo $this->Form->button(__d('admin', 'rubric_index_action'), ['icon' => 'fa fa-arrow-circle-up', 'url' => $rubric_back_url, 'class' => 'btn btn-primary']);
}
//Multiple upload
/*if ($imgMultiUpload) {
	echo $this->Form->button('fa fa-plus-circle', __d('admin', 'add_multi_action'), false, array('class' => 'btn btn-primary img-multiupload', 'data-src' => $this->Html->url(array('background-upload' => true))));
	?>
	<div class="upload-image-status"></div>
	<div class="upload-image-block"></div>
	<?php
}*/



?>
<section class="btn_filter hide">
    <?php //echo __d('x_form', 'with_selected_label'); ?>
    <?php
    echo $this->Form->button(__d('admin', 'multi_disable_action'), ['icon' => 'fa fa-power-off', 'class' => 'btn btn-primary', 'data-url' => $this->Url->build($group_disable_url)]);
    echo $this->Form->button(__d('admin', 'multi_enable_action'), ['icon' => 'fa fa-check-circle', 'class' => 'btn btn-primary', 'data-url' => $this->Url->build($group_enable_url)]);
    echo $this->Form->button(__d('admin', 'multi_delete_action'), ['icon' => 'fa fa-minus-circle', 'class' => 'btn btn-primary', 'data-url' => $this->Url->build($group_delete_url), 'confirm' => 'Вы действительно хотите удалить отмеченные позиции?', 'escape' => false]);
    ?>

</section>
<?php

// list_button_filter
/*if (!$session->check('AdminFilter.' . $model)) {
	$button_text = $routine->icon('find') . ' ' . __d('admin', 'filter_action', TRUE);
	$button_attrs = array('onclick' => 'jQuery(\'.list_button_filter\').hide(); jQuery(\'.list_filter\').show();');
	echo '<div class="list_button list_button_filter">' . $html->tag('button', $button_text, $button_attrs) . '</div>';
}*/

// end of list_buttons


?>
