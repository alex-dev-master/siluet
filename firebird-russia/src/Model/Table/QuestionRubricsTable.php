<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemRubrics Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\HasMany $Items
 *
 * @method \App\Model\Entity\ItemRubric get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemRubric newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemRubric[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemRubric patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric findOrCreate($search, callable $callback = null, $options = [])
 */
class QuestionRubricsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('question_rubrics')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->setOrder(['QuestionRubrics.position' => 'DESC', 'QuestionRubrics.id' => 'DESC']);

        $this->addBehavior('Tree');

        $this->hasMany('Questions', [
            'foreignKey' => 'question_rubric_id'
        ]);
        $this->addBehavior('Translate', ['fields' =>
            ['caption']
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    /*public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 100)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->scalar('caption')
            ->maxLength('caption', 255)
            ->requirePresence('caption', 'create')
            ->notEmpty('caption');

        $validator
            ->integer('position')
            ->allowEmpty('position');

        $validator
            ->boolean('disabled')
            ->requirePresence('disabled', 'create')
            ->notEmpty('disabled');

        return $validator;
    }*/
}
