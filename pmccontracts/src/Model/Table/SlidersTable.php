<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;

/**
 * Sliders Model
 *
 * @method \App\Model\Entity\Slider get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slider newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slider|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slider[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slider findOrCreate($search, callable $callback = null, $options = [])
 */
class SlidersTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
                ->setTable('sliders')
                ->setDisplayField('caption')
                ->setDisplayImage('image')
                ->setPrimaryKey('id')
                ->setFormSchema([
                    'image' => ['params' => ['resize' => 'crop', 'x' => 1920, 'y' => 1080]],
                    'video' => ['label' => 'Видео  (mp4)', 'tab' => 'video', 'type' => 'file'],
                    'video_web' => ['label' => 'Видео  (webm)', 'tab' => 'video', 'type' => 'file'],
                    'status_video' => ['label' => 'Отображать видео', 'tab' => 'video'],
                    'text' => ['label' => 'Подзаголовок слайда', 'type' => 'wysiwyg'],
                    'main_text' => ['label' => 'Главный текст слайда', 'type' => 'wysiwyg'],
                    'map_slide' => ['label' => 'Отобразить как карта'],
                ])
                ->setTabSchema([
                    'video' => array('label' => 'Видео'),
                ])
                ->setOrder([
                    'Sliders.position' => 'DESC',
                    'Sliders.id' => 'DESC'
                ])->addBehavior('Translate', ['fields' =>
                    ['caption', 'main_text', 'text', 'meta_title', 'meta_description', 'meta_keywords']
                ]);
    }
}
