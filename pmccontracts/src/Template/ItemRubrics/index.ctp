
<?php
/**
 * PHP 7.1
 */
/*
    $cnt = 0;
    echo implode('', (new Cake\Collection\Collection($rows))->listNested()->printer(function ($item, $key, $iterator) use (&$cnt) {
    $li = '<li>' . $this->Html->link($item->caption, ['controller' => 'Items', 'action' => 'index', 'slug' => $item->slug]);
    if (!$iterator->hasChildren()) {
        $li .= '</li>';
    } else {
        $li .= '<ul>';
    }
    if ($cnt == 0) {
        $cnt = count($iterator->getChildren()->toArray()) - 1;
    }
    if ($key == $cnt) {
        $li .= '</ul>';
        $cnt = 0;
    }
    return $li;
}, null, '')->toList());*/
echo catalog($rows, $this->Html);
function catalog($rows, $Html, $html = '') {
    foreach ($rows as $row) {
        $html .= '<li>' . $Html->link($row->caption, ['controller' => 'Items', 'action' => 'index', 'slug' => $row->slug]);
        if ($row->children) {
            $html .= '<ul>';
            $html = catalog($row->children, $Html, $html) . '</ul>';
        }
        $html .= '</li>';
    }
    return $html;
}
?>
