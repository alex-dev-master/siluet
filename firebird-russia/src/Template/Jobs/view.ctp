<?php
$this->Breadcrumbs->prepend(
    __('Вакансии'),
    ['controller' => 'Jobs', 'action' => 'index']
);
?>

<div class="jobs_detail-top">
    <div class="wrapper ">
    <?= $this->element('breadcrumbs') ?>
    </div>
    <div class="wrapper pd-left">
        <a class="back" href="/jobs"><img src="/img/icon_back.png"></a>
        <h1><?=$job['caption']?></h1>
        <h3><?=$job['price']?></h3>
        <a class="apply" href="#job-modal" data-toggle="modal"><?=__('Откликнуться')?> <img src="/img/arr-w.png"></a>
        <?=$job['content']?>
        <div class="center">
            <a href="/jobs" class="btn jobs_detail-btn"><?=__('Вернуться к списку')?></a>
        </div>
    </div>
</div>





<?=$this->element('makeOrder')?>

<div id="job-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(['schema' => []], ['id' => 'job-modal-form', 'class' => '']); ?>
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="/img/form-close.png"></button>
                <p class="modal-subtitle"><?=__('Отклик на вакансию')?></p>
                <h4 class="modal-title"><?=$job['caption']?></h4>
                <input type="hidden" value="<?=$job['caption']?>">
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <label for="name2"><?=__('ФИО')?><span>*</span></label>
                <input class="modal-input" name="name" id="name2">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Телефон')?><span>*</span></label>
                <input class="modal-input phone" name="phone" id="organization2">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail<span>*</span></label>
                <input class="modal-input" name="email" id="organization2">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Комментарий')?><span>*</span></label>
                <textarea name="msg"></textarea>
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                <label for="msg"><?=__('Файлы (не более 50Мб)')?></label>

                <div class="files">
                    <div class="file-input">
                        <div class="file_upload">
                            <div class="file-name"><?=__('Файл не выбран')?></div>
                            <button><?=__('Обзор')?></button>
                            <input type="file">
                        </div>
                        <a class="delete-place"><img src="/img/delete.png"> </a>
                    </div>
                    <a href="#" class="add-more__btn"><?=__('Добавить файл')?></a>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" value="Вакансии">
                <div class="btns">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="checkbox">
                                <input name="privacy-policy" type="checkbox">
                                <div class="checkbox__text"><?=__('Я принимаю')?> <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?></a></div>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <button data-tab="#success_job-modal-form" class="btn btn-modal"><?=__('Отправить')?></button>
                        </div>
                    </div>
                </div>
                   <h4 id="success_job-modal-form" class="success" style="display: none; color: green"><?=__('Ваша заявка отправлена')?></h4>
                <img src="/img/doll.png" class="modal-doll">
            </div>

            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
