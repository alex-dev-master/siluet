<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;
use SiluetCms\Traits\TableTrait;
use Cake\Cache\Cache;

/**
 * Settings Model
 *
 * @method \App\Model\Entity\Setting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Setting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Setting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Setting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Setting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Setting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Setting findOrCreate($search, callable $callback = null, $options = [])
 */
class SettingsTable extends Table
{
    use TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settings')
                ->setDisplayField('project_title')
                ->setPrimaryKey('id')
                ->setFormSchema([
                    'favicon' => ['params' => ['path' => WWW_ROOT, 'name' => 'favicon.ico']],
                    'address' => ['label' => 'Адрес', 'type' => 'text'],
                    'link_vk' => ['label' => 'Ссылка на VK', 'type' => 'text'],
                    'link_facebook' => ['label' => 'Ссылка на Facebook', 'type' => 'text'],
                    'link_inst' => ['label' => 'Ссылка на Instagram', 'type' => 'text'],
                    'weekends' => ['label' => 'Выходные дни'],

                    'reviews_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'reviews'],
                    'reviews_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'reviews'],
                    'reviews_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'reviews'],

                    'call_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'call'],
                    'call_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'call'],
                    'call_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'call'],

                    'appoint_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'appoint'],
                    'appoint_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'appoint'],
                    'appoint_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'appoint'],

                    'time_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'time'],
                    'time_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'time'],
                    'time_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'time'],

                    'mail_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'mail'],
                    'mail_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'mail'],
                    'mail_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'mail'],

                    'job_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'job'],
                    'job_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'job'],
                    'job_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'job'],

                    'question_feedback_to' => ['label' => 'Кому', 'type' => 'text', 'tab' => 'question'],
                    'question_feedback_from' => ['label' => 'От кого', 'type' => 'text', 'tab' => 'question'],
                    'question_feedback_subject' => ['label' => 'Тема', 'type' => 'text', 'tab' => 'question'],
                ]);
        $this->setTabSchema([
            'reviews' => ['label' => 'Шаблон письма отзывов'],
            'call' => ['label' => 'Шаблон письма заказа звонка'],
            'appoint' => ['label' => 'Шаблон письма запись на приём (в шапке)'],
            'time' => ['label' => 'Шаблон письма запись на приём (в таблице)'],
            'mail' => ['label' => 'Шаблон письма с формы на главной'],
            'job' => ['label' => 'Шаблон письма с формы в вакансиях'],
            'question' => ['label' => 'Шаблон письма с формы оценки качества'],
        ]);
    }

    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options) {
        Cache::clear(false, 'settings');
    }
}
