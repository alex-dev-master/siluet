<?php
$this->Breadcrumbs->prepend(
    __('Услуги'),
    ['controller' => 'Services', 'action' => 'index']
    );
    ?>

    <div class="services_detail-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
    </div>
    <div class="wrapper pd-left">
        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "services"]) ?>"><img src="/img/icon_back.png"></a>
        <h1><?=$service['caption']?></h1>
        <div class="main-image">
            <img src="/files/services/<?=$service['thumbnail_960x410']?>">
        </div>
    </div>
</div>
<div class="services_detail-text">
    <div class="wrapper">
        <?=$service['content_before_image']?>
    </div>
</div>
<?php if (!empty($service['attachment_images'])) { ?>
<div class="services_detail-example">
    <div class="wrapper">
        <h3><?=__('Примеры наших работ')?></h3>
        <div class="services_detail__slider-block">
            <div class="services_detail__slider">
                <?php foreach ($service['attachment_images'] as $image) { ?>
                <div class="slide">
                    <div class="img-container">
                        <img class="slide-img" src="/files/attachment-images/<?=$image['image']?>">
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="services_detail__slider__nav">
                <a href="#" class="slide-nav-arrow left"><img src="/img/slide__arr-l.png"> </a>
                <a href="#" class="slide-nav-arrow right"><img src="/img/slide__arr-r.png"> </a>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="services_detail-description">
    <div class="wrapper">
        <?=$service['content_after_image']?>
        <div class="center">
            <a class="btn services_detail-btn" href="#orderService-modal" data-toggle="modal"><?=__('Заказать услугу')?></a>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>
<div id="orderService-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(['schema' => []], ['id' => 'orderService-modal-form', 'class' => '']); ?>
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                    src="/img/form-close.png"></button>
                    <p class="modal-subtitle"><?=__('Заказ услуги')?></p>
                    <h4 class="modal-title"><?=$service['caption']?></h4>
                    <input type="hidden" value="<?=$service['caption']?>">
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <label for="name2"><?=__('ФИО')?><span>*</span></label>
                    <input class="modal-input" name="name" id="name2">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                    <label for="organization2"><?=__('Телефон')?><span>*</span></label>
                    <input class="modal-input phone" name="phone" id="organization2">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                    <label for="organization2">E-mail<span>*</span></label>
                    <input class="modal-input phone" name="phone" id="organization2">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                    <label for="organization2"><?=__('Комментарий')?><span>*</span></label>
                    <textarea name="msg"></textarea>
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                    <label for="msg"><?=__('Файлы (не более 50Мб)')?></label>

                    <div class="files">
                        <div class="file-input">
                            <div class="file_upload">
                                <div><?=__('Файл не выбран')?></div>
                                <button><?=__('Обзор')?></button>
                                <input type="file">
                            </div>
                            <a class="delete-place"><img src="/img/delete.png"> </a>
                        </div>
                        <a href="#" class="add-more__btn"><?=__('Добавить файл')?></a>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btns">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" value="Форма заказа услуги">
                                <label class="checkbox">
                                    <input name="privacy-policy" type="checkbox">
                                    <div class="checkbox__text"><?=__('Я принимаю')?> <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?> </a></div>
                                </label>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-modal"><?=__('Отправить')?></button>
                            </div>
                        </div>
                    </div>
                    <img src="/img/doll.png" class="modal-doll">
                    <h4 class="success" style="display: none; color: green"><?=__('Ваша заявка отправлена')?></h4>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
