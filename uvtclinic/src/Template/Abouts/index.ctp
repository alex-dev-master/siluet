<div class="page-content about-main-content">
    <img class="content-bg" src="/files/abouts/<?=$page['image']?>" alt="">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$page['caption']?></h1>
        <div class="text about-text">
            <?=$page['content']?>
        </div>
    </div>
    <div class="slider-quot">
        <div class="row list">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="img-wrap">
                    <img class="photo speaker-main-photo" src="/files/attachment-peoples/<?=$page['attachment_peoples'][0]['image']?>" alt="">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php $z=1; foreach ($page['attachment_peoples'] as $attachment_peoples) { ?>
                <div class="speaker-text text-wrap <?php if ($z == 1) echo 'active';?>" data-tab="speaker-<?=$z?>" >
                    <p class="quot"><?=$attachment_peoples['quotos']?></p>
                    <h5 class="name"><?=$attachment_peoples['caption']?></h5>
                    <p class="prof sml-text"><?=$attachment_peoples['post']?></p>
                </div>
                <?php $z++; } ?>
            </div>
           <!--  <div class="col-md-6 col-sm-6 col-xs-12 speaker-2">
                <div class="img-wrap">
                    <img class="photo" src="/img/about-1.png" alt="">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 speaker-2">

            </div> -->
        </div>
        <div class="quot-buttons">
            <?php $j=1; foreach ($page['attachment_peoples'] as $k=>$attachment_peoples) {?>
            <div class="speaker-btn <?php if ($j == 0) echo 'speaker-btn-active';?>" data-tab="speaker-<?=$j?>" data-img="/files/attachment-peoples/<?=$attachment_peoples['image']?>">
                <img src="/files/attachment-peoples/<?=$attachment_peoples['icon']?>" alt="" >
            </div>
            <?php $j++; } ?>

        </div>
    </div>
    <div class="wrapper">
        <div class="middle-info">
            <div class="row list">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="text-wrap">
                        <h3 class="title">Запись на прием</h3>
                        <ul class="list-ul">
                            <li class="list-item">
                                <img class="icon" src="/img/phone-blue.png" alt="">
                                <p class="text"><?=$page['info_phone']?></p>
                            </li>
                            <li class="list-item">
                                <img class="icon" src="/img/time-22.png" alt="">
                                <p class="text"><?=$page['info_time']?></p>
                                <a class="link" href="/contacts">Посмотреть график работы</a>
                            </li>
                            <li class="list-item">
                                <img class="icon" src="/img/pencil.png" alt="">
                                <p class="text"><?=$page['info_write']?></p>
                                <a class="link" href="#appointment-modal" data-toggle="modal">Записаться на прием</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="sl-wrap">
                        <div class="about__admin-slider">
                            <?php foreach ($page['attachment_admins'] as $admins) { ?>
                            <div class="slide">
                                <img class="photo" src="/files/attachment-admins/<?=$admins['image']?>" alt="">
                                <h5 class="name"><?=$admins['caption']?></h5>
                                <p class="prof sml-text"><?=$admins['post']?></p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="about__admin-slider-navigate">
                            <a href="#" class="slide-nav-arrow left"><img src="/img/arr-l.png"></a>
                            <div class="pagingInfo"><span class="active-slide__number"></span> / <span
                                class="slide__count"></span>
                            </div>
                            <a href="#" class="slide-nav-arrow right"><img src="/img/arr-r.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-plus">
        <div class="bear">
            <img class="bear-img" src="/img/bear.png" alt="">
        </div>
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">
                    <div class="inner-wrapper">
                        <h3 class="title"><?=$page['caption_block_care']?></h3>
                        <?=$page['content_block_care']?>
                        <h5  class="subtitle">К вашим услугам</h5>
                        <ul class="list-ul">
                            <?php
                            $services = explode(',', $page['services_block_care']);
                            ?>
                            <?php foreach ($services as $service) { ?>
                            <li class="list-item"><?=$service?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="worker-list">
            <h3 class="title">Наша команда</h3>
            <div class="worker-buttons">
                <div class="btn worker-button worker__tab worker-active-button" data-tab="all">Все</div>
                <?php foreach ($specialities as $speciality) { ?>
                <?php if (!empty($speciality['experts'])) { ?>
                <div class="btn worker-button worker__tab" data-tab="<?=$speciality['slug']?>"><?=$speciality['caption_plural']?></div>
                <?php } ?>
                <?php } ?>
            </div>
            <div class="row list">
                <?php foreach ($experts as $expert) { ?>
                <?php
                $arrSpec= [];
                foreach ($expert['specialities'] as $specialityExpert) {
                $arrSpec[] = $specialityExpert['slug'];
            }
            $specialityExpertSlug = implode(", ", $arrSpec);
            ?>
            <div class="inner-container worker__tab_content" data-tab="<?=$specialityExpertSlug?>">
                <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                    <img class="worker-img" src="/files/experts/<?=$expert['thumbnail_210x210']?>" alt="">
                    <h6 class="worker-title"><?=$expert['caption']?></h6>
                    <?php
                    $arrSpec= [];
                    foreach ($expert['specialities'] as $specialityExpert) {
                    $arrSpec[] = $specialityExpert['caption'];
                }
                $specialityExpert = implode(", ", $arrSpec);
                ?>
                <p class="worker-desc sml-text"><?=$specialityExpert?></p>
            </a>
        </div>
        <?php } ?>
    </div>
</div>
</div>
<div class="info-data">
    <div class="wrapper">
        <h3 class="title">Сведения об организации</h3>
        <div class="text">
            <?=$page['content_requisites']?>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="look-more">
        <h3 class="title">Смотрите также</h3>
        <div class="links-list">
            <div class="row list">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "aboutMedicalActivities"]) ?>" class="link">О медицинской деятельности</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "rightsObligations"]) ?>" class="link">О правах и обязанностях граждан в сфере охраны здоровья</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "regulations"]) ?>" class="link">Нормативные документы</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "questionnaire"]) ?>" class="link">Оценка качества обслуживания</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "orderRules"]) ?>" class="link">Правила внутреннего распорядка</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "jobs"]) ?>" class="link">Вакансии</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "reviews"]) ?>" class="link">Отзывы</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-text">
                        <a href="<?= $this->Url->build(["controller" => "Pages","action" => "gallery"]) ?>" class="link">Галерея</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
