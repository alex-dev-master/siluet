<div class="page-content team-detail-content">

    <div class="main">
        <div class="wrapper">
            <?php
            $this->Breadcrumbs->prepend(
                $expert['caption']
            );
            $this->Breadcrumbs->prepend(
                __('Наши специалисты'),
                ['controller' => 'Pages', 'action' => 'experts']
            );
            ?>
            <?= $this->element('breadcrumbs') ?>
            <div class="border-top">
                <div class="row list">
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <div class="inner-container">
                            <img class="worker-img" src="/files/experts/<?=$expert['thumbnail_400x400']?>" alt="">
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-8 col-xs-12">
                        <div class="inner-container">
                            <h1 class="name"><?=$expert['caption']?></h1>
                            <?php
                            foreach ($expert['specialities'] as $specialities) {
                                $caption_specialities[] = $specialities['caption'];
                            }
                            $specialities = implode(", ", $caption_specialities);
                            ?>
                            <h5 class="spec">Специальность: <?=$specialities?></h5>
                                   <div class="text">

                            <?=$expert['content']?>
                            </div>
                            <?php if (!empty($expert['attachment_timetables'])) { ?>
                            <a class="btn common-btn timetable-link" href="/timetable">Расписание приема</a>
                            <?php } ?>
                            <?php if (!empty($expert['rank'])) { ?>
                            <h5 class="list-title">Звание</h5>
                            <?php
                            $rank_arr = explode(",", $expert['rank']);
                            ?>
                            <ul class="list-ul">
                                <?php foreach ($rank_arr as $rank) { ?>
                                <li class="list-item"><?php echo trim($rank) ?></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                            <?php if (!empty($expert['specialization'])) { ?>
                            <h5 class="list-title">Специализация</h5>
                            <?=$expert['specialization']?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!empty($expert['attachment_files'])) { ?>
    <div class="documents">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-5 col-sm-3 col-xs-12">
                    <div class="inner-container">
                        <h3 class="title">Документы</h3>
                    </div>
                </div>
                <div class="col-md-7 col-sm-9 col-xs-12">
                    <div class="inner-container">
                        <ul class="list-ul">
                            <?php foreach ($expert['attachment_files'] as $file) {?>
                            <li class="list-item">
                                <h5 class="doc-title"><?=$file['caption']?></h5>
                                <a class="doc-link sml-text" href="/files/attachment-files/<?=$file['file']?>" download><?=$file['file']?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (!empty($expert['education'])) { ?>
    <div class="education">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-5 col-sm-3 col-xs-12">
                    <div class="inner-container">
                        <h3 class="title">Образование</h3>
                    </div>
                </div>
                <div class="col-md-7 col-sm-9 col-xs-12">
                    <div class="inner-container">
                        <?=$expert['education']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (!empty($expert['experience'])) { ?>
    <div class="experience">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-5 col-sm-3 col-xs-12">
                    <div class="inner-container">
                        <h3 class="title">Опыт</h3>
                    </div>
                </div>
                <div class="col-md-7 col-sm-9 col-xs-12">
                    <div class="inner-container">
                        <?=$expert['experience']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (!empty($expert['congresses'])) { ?>
    <div class="сongresses">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-5 col-sm-3 col-xs-12">
                    <div class="inner-container">
                        <h3 class="title">Конгрессы</h3>
                    </div>
                </div>
                <div class="col-md-7 col-sm-9 col-xs-12">
                    <div class="inner-container">
                        <?=$expert['congresses']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
