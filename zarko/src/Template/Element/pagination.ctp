<?php
$this->Paginator->setTemplates([
    'prevActive' => '<a href="{{url}}" class="btn-prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"/></svg></a>',
    'prevDisabled' => '<a href="{{url}}" class="btn-prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"/></svg></a>',
    'number' => '<span class="align-self-start"><a href="{{url}}">{{text}}</a></span>',
    'current' => '<span class="align-self-start"><a href="{{url}}">{{text}}</a></span>',
    'last' => '<span class="align-self-start"><a href="{{url}}">{{text}}</a></span>',
    'nextActive' => '<a href="{{url}}" class="btn-next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></a>',
    'nextDisabled' => '<a href="{{url}}" class="btn-next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></a>',
]);
?>

<?php if ($this->Paginator->hasPages()): ?>
<div class="pagination">
    <!-- prev -->
    <?= $this->Paginator->prev('');?>
    <!-- prev end -->

    <!-- count -->
    <div class="count">
        <div class="count-wrap row no-gutters justify-content-between">
            <?php echo $this->Paginator->counter(
                '<span class="align-self-start">{{page}}</span><span class="align-self-end">{{pages}}</span>'
            );?>

        </div>
    </div>
    <!-- count end -->
    <!-- next -->
    <?= $this->Paginator->next('');?>
    <!-- next end -->
</div>
<?php endif; ?>
