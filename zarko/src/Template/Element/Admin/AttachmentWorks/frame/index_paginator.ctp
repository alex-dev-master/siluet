<?php if ($this->Paginator->hasPages()) {
    $url = ['autoLang' => true];
    if (isset($action)) {
        $url['action'] = $action;
    }
    if ($ext = $this->request->getParam('_ext')) {
        $url['_ext'] = $ext;
    }
    if ($m = $this->request->getQuery('model') and $f = $this->request->getQuery('foreign_key')) {
        $url += ['model' => $m, 'foreign_key' => $f];
    }
    if ($url) {
        $this->Paginator->options(['url' => $url]);
    }
    ?>
<ul class="pagination pagination-sm pagination-alt">
	<?php echo $this->Paginator->numbers(['modulus' => 3, 'first' => 1, 'last' => 1]); ?>
</ul>
<?php } ?>