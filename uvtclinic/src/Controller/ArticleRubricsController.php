<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ArticleRubricsController extends AppController
{
    public $paginate;

    public function index($rubric_id = null, $callback = null)
    {
        parent::index($rubric_id, function ($callback) use ($rubric_id) {
//            $callback->where(['slug' => $rubric_id]);
        }); // TODO: Change the autogenerated stub

        $this->paginate = [
            'limit' => 9,
            'order' => [
                'Articles.published' => 'desc'
            ],
        ];

        $articles = $this->paginate(TableRegistry::getTableLocator()->get('Articles')
            ->find()
            ->where(['ArticleRubrics.slug' => $rubric_id])
            ->contain(['ArticleRubrics']))
            ->toArray();
        $this->set(compact('articles'));
    }

}
