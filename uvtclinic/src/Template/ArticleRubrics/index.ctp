<div class="page-content about-content about-articles-content">
    <div class="wrapper">
        <?php

        $this->Breadcrumbs->prepend(
            __('Полезная информация'),
            ['controller' => 'Pages', 'action' => 'articles']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <div class="border-top">
            <h1>Полезная информация</h1>
            <div class="articles-buttons">
                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "index"]) ?>" class="btn article-button <?php if (empty($rubric_id)) echo 'article-active-button'; ?>">Все статьи</a>
                <?php foreach ($rows as $articleRubric) { ?>
                    <a href="<?= $this->Url->build(["controller" => "ArticleRubrics", "action" => "index", $articleRubric['slug']]) ?>" class="btn article-button <?php if (!empty($rubric_id) && $rubric_id == $articleRubric['slug']) echo 'article-active-button'; ?>"><?=$articleRubric['caption']?></a>
                <?php } ?>
            </div>
            <div class="publ-list">
                <div class="row list">
                    <?php foreach ($articles as $article) { ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="inner-container patient">
                                <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>">
                                    <img class="publ-img" src="/files/articles/<?=$article['thumbnail_370x260']?>" alt="">
                                </a>
                                <div class="text-wrap">
                                    <div class="btn sml-btn" style="background-color: #<?=$article['article_rubric']['color']?>"><?=$article['article_rubric']['caption']?></div>
                                    <a href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>">
                                        <h6 class="publ-title"><?=$article['caption']?></h6>
                                    </a>
                                    <p class="publ-date"><?=$article->published->format('d / m / Y')?></p>
                                    <a class="moreBtn" href="<?= $this->Url->build(["controller" => "Articles", "action" => "view", $article['slug']]) ?>"></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?= $this->element('pagination') ?>

        </div>
    </div>
</div>
