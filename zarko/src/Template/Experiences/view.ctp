<!-- header -->
<?php $transp_status = ['tansp' => 'true'];?>
<?= $this->element('header', $transp_status)?>
<!-- header end -->

<div class="page-content project-item">
    <div class="page-content__header page-content__section withBg" style="background-image: url(/files/experiences/<?=$object['image']?>);">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 col-lg-8 col-xl-7">
                    <div class="project-item__header">
                        <h1 class="project-item__title"><?=$object['caption']?></h1>
                        <span class="project-item__subtitle"><?=strip_tags ($object['brief_case'])?></span>
                        <p class="project-item__type"><?=$object['type']['caption']?></p>
                    </div>
                    <div class="project-item__params">
                        <div class="item">
                            <div class="title"><?=$object['square']?> м<sup>2</sup></div>
                            <div class="description">Площадь</div>
                        </div>
                        <div class="item">
                            <div class="title"><?=$object['year']?></div>
                            <div class="description">Год</div>
                        </div>
                        <?php if (!empty($object['city'])) { ?>
                        <div class="item">
                            <div class="title"><?=$object['city']?></div>
                            <div class="description">Город</div>
                        </div>
                        <?php } ?>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <!-- project item short description -->
    <?php if (!empty($object['block_short_info_object']) && !empty($object['block_short_info_time']) && !empty($object['block_short_info_task'])) { ?>
    <div class="section project-item__description">
        <div class="container">
            <div class="row">
                <?php if (!empty($object['block_short_info_object'])) { ?>
                <div class="col-12 col-md-4">
                    <h5>Объект</h5>
                    <p><?=$object['block_short_info_object']?></p>
                </div>
                <?php } ?>
                <?php if (!empty($object['block_short_info_time'])) {?>
                <div class="col-12 col-md-4">
                    <h5>Сроки</h5>
                    <p><?=$object['block_short_info_time']?></p>
                </div>
                <?php } ?>
                <?php if (!empty($object['block_short_info_task'])) {?>
                <div class="col-12 col-md-4">
                    <h5>Задача</h5>
                    <p><?=$object['block_short_info_task']?></p>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- project item short description end -->

    <!-- project item location -->
    <?php if (!empty($object['about_project_title']) && !empty($object['about_project_text'])) { ?>
    <div class="section project-item__location">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-md-6 col-lg-7 order-md-last">
                    <p class="page-title">О проекте</p>
                    <div class="wysiwyg">
                        <h3><?=$object['about_project_title']?></h3>
                        <?=$object['about_project_text']?>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <input type="hidden" id="coords_input" value="<?= $object['map_n'].', '.$object['map_e'] ?>">
                    <div id="map_project_item" style="width: 360px;height: 360px">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- project item location end -->

    <!-- project item slider -->
    <?php if (!empty($object['attachment_technologies'])) { ?>
    <div class="section section-push section-push__right project-item__slider">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-push__line-left"></div>
                </div>
            </div>
        </div>


        <!-- slider wrap -->
        <div class="project-item__slider-wrap">

            <?php foreach ($object['attachment_technologies'] as $techno) {?>
            <!-- slider item -->
                <?php if (!empty($techno)) { ?>
            <div class="project-item__slider-item">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                            <div class="section-push__content">
                                <div class="page-title">Технологии <br>и решения</div>
                                <p class="section__title"><?=$techno['caption']?></p>
                                <p class="description"><?=$techno['text']?></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-8 col-lg-7 col-xl-8">
                            <div class="section-push__item">
                                <div class="project-item__slider-img" style="background-image:url(/img/project_item-img.png)">
                                    <img src="/files/attachment-technologies/<?=$techno['thumbnail_560x320']?>" alt="">
                                </div>
                                <div class="image-description">
                                    <?=$techno['image_text']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    <?php } ?>
            <!-- slider item -->
            <?php } ?>
        </div>
        <!-- slider wrap end -->

        <!-- slider controls -->
        <div class="project-item__slider-controls">
            <div class="container">
                <div class="row align-items-end">
                    <div class="col-6 col-md-4 col-lg-5 col-xl-4">

                        <!-- count -->
                        <div class="count">
                            <div class="count-wrap row no-gutters justify-content-between">
                                <span class="align-self-start">1</span>
                                <span class="align-self-end">5</span>
                            </div>
                        </div>
                        <!-- count end -->
                    </div>
                    <div class="col-6 col-md-6 col-lg-7 col-xl-8">
                        <div class="nav">
                            <!-- prev -->
                            <a href="" class="prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"/></svg></a>
                            <!-- prev end -->

                            <!-- next -->
                            <a href="" class="next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></a>
                            <!-- next end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider controls end -->
    </div>
    <?php } ?>
    <!-- project item slider end -->

    <!-- project item list -->
    <?php if (!empty($object['attachment_works'])) { ?>
    <div class="project-item__list">
        <div class="container">
            <div class="page-title">Работы на объекте</div>
            <h3 class="title-underline"><?=$object['title_work']?></h3>
            <ul class="list">
                <li class="list-header">
                    <div class="row">
                        <div class="col-12 col-sm-7 col-md-8 col-lg-9 col-xl-10">Наименование</div>
                        <div class="col-12 col-sm-5 col-md-4 col-lg-3 col-xl-2">Виды работ</div>
                    </div>
                </li>

                <?php $works = $object['attachment_works']; foreach ($works as $o=>$work) { ?>
                    <?php if ($o>=3) {$count_list = true; continue;} ?>
                    <li>
                    <div class="row">
                        <div class="col-12 col-sm-7 col-md-8 col-lg-9 col-xl-10"><?=$work['caption']?></div>
                        <div class="col-12 col-sm-5 col-md-4 col-lg-3 col-xl-2">
                            <?php foreach ($work['services'] as $service) {?>
                            <div class="ic tooltip" data-tooltip="<?=$service['caption']?>">
                                <?php if (isset($service['icon'])){ ?>
                                    <?php $icon = @file_get_contents('./'.$service['icon-uploaded']['url']); if (!empty($icon)) echo $icon;?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ul>
            <ul class="list list-hidden collapse">
                <?php if (isset($count_list)) { ?>
                    <?php foreach ($works as $z=>$work) {?>
                        <?php if ($z>=3) { ?>
                            <li>
                                <div class="row">
                                    <div class="col-12 col-sm-7 col-md-8 col-lg-9 col-xl-10"><?=$work['caption']?></div>
                                    <div class="col-12 col-sm-5 col-md-4 col-lg-3 col-xl-2">
                                        <?php foreach ($work->services as $service) {?>
                                            <div class="ic tooltip" data-tooltip="<?=$service['caption_full']?>">
                                                <?php if (isset($service['icon'])){ ?>
                                                    <?php $icon = @file_get_contents('./'.$service['icon-uploaded']['url']); if (!empty($icon)) echo $icon;?>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
            <?php if (isset($count_list) && $z>=3) { ?>
            <button class="btn btn-default btn-link btn-list-collapse" type="button" data-toggle="collapse" data-target=".list-hidden" aria-expanded="false" data-open="Показать все" data-close="Cкрыть все">
                <span class="btn-title">Показать все</span>
                <span class="ic"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 11.3 11.3" xml:space="preserve"><path d="M10.3,4.7H6.7V1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1v3.7H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h3.7v3.7c0,0.6,0.4,1,1,1 c0.6,0,1-0.4,1-1V6.7h3.7c0.6,0,1-0.4,1-1S10.9,4.7,10.3,4.7z"></path></svg></span>
            </button>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
    <!-- project item list end -->

    <!-- project item video -->
    <?php if (!empty($object['video'])) { ?>
    <div class="container">
        <div class="embed-responsive embed-responsive-16by9">
            <?=$object['video']?>
        </div>
    </div>
    <?php } ?>
    <!-- project item video end -->

    <!-- project item participants -->
    <?php if (!empty($object['attachment_participants'])) { ?>
    <div class="section section-push section-push__left project-item__participants">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-lg-7">
                    <div class="section-push__content">
                        <div class="page-title">Участники о проекте</div>
                        <p class="section__title"><?=$object['title_partic_project']?></p>
                        <div class="participants-list">
                            <?php foreach ($object['attachment_participants'] as $participant) { ?>
                            <!-- item -->
                            <div class="participants-list__item">
                                <div class="participants-list__img"><img src="/files/attachment-participants/<?=$participant['thumbnail_60x60']?>" alt=""></div>
                                <div class="participants-list__content">
                                    <p class="title"><?=$participant['caption']?></p>
                                    <p class="subtitle"><?=$participant['post']?></p>
                                    <div class="content">
                                        <?=$participant['text']?>
                                    </div>
                                </div>
                            </div>
                            <!-- item end -->
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 order-lg-first">
                    <div class="section-push__item">
                        <img src="/files/experiences/<?=$object['image_participant']?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- project item participants end -->

    <!-- tags -->
    <div class="container">
        <div class="tags">
            <div class="row">
                <div class="col-12 col-md-3 text-center text-md-left">
                    <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "index"]) ?>" class="btn btn-light btn-with-icon"><span class="icon icon-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"></path></svg></span> Список проектов</a>
                </div>
                <div class="col-12 col-md-9 text-center text-md-right">
                    <?php $type_object = $object['type']['caption'];?>
                    <?php if (!empty($type_object)) { ?>
                    <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "index", "?town=".$type_object]) ?>" class="btn btn-primary"><?=$object['type']['caption']?></a>
                    <?php } ?>
                    <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "cases"]) ?>" class="btn btn-primary">Кейсы</a>
                </div>
            </div>
        </div>
    </div>
    <!-- tags end -->

</div>

<div class="page-content__bottom page-content__section withBg" id="contact_us" style="background-image: url(/img/service_bg.png);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-8 col-xl-9">
                <div class="page-content__title">У вас есть интересный проект?</div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                <a href="#service-page__form" data-toggle="collapse" class="btn btn-primary btn-block btn-lg">Свяжитесь с нами!</a>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->create(['schema' => []], ['id' => 'contacts']); ?>
<div class="service-page__form collapse" id="service-page__form">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Ваше имя <sup>*</sup></label>
                    <input type="text" name="name" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Электронная почта <sup>*</sup></label>
                    <input type="text" name="email" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Телефон</label>
                    <input type="text" name="phone" class="form-control">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="label">Текст сообщения <sup>*</sup></label>
                    <textarea rows="2" name="text" class="form-control"></textarea>
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="privacy-policy">
                        <span class="check"></span>
                        Я соглашаюсь с <a href="/privacy-policy">Политикой конфиденциальности</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 text-left text-md-right">
                <button class="btn btn-primary btn-with-icon">Отправить <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></button>
            </div>
        </div>
        <div class="success success-msg">
            <div class="container">
                <div class="success-msg__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><polygon points="37.1,44.6 34.2,47.3 47.8,61.9 79.7,29.9 76.9,27.1 47.8,56.2 "/><path d="M18.6,50c0,17.4,14.1,31.4,31.4,31.4S81.4,67.4,81.4,50h-4c0,15.1-12.3,27.4-27.4,27.4S22.6,65.1,22.6,50S34.9,22.6,50,22.6  v-4C32.7,18.6,18.6,32.7,18.6,50z"/></svg>
                </div>
                <div class="success-msg__title">Ваше сообщение отправлено!</div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<?= $this->element('footer')?>
