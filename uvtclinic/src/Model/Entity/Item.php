<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $caption
 * @property string $content
 * @property string $image
 * @property int $position
 * @property bool $disabled
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'item_rubric_id' => true,
        'slug' => true,
        'caption' => true,
        'content' => true,
        'content_full' => true,
        'content_not_full' => true,
        'content_advantages' => true,
        'image' => true,
        'thumbnail_1170x480' => true,
        'thumbnail_470x270' => true,
        'meta_title' => true,
        'meta_description' => true,
        'position' => true,
        'disabled' => true,
        'tags' => true,
        'params' => true,
        'linked_items' => true,
        'articles' => true,
        'experts' => true
    ];
}
