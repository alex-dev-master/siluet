<div class="jumbotron">
    <div class="jumbotron__slider">
        <?php foreach ($sliders as $slider) { ?>
            <div class="slide">
                <?php if (!empty($slider['url'])) { ?>
                <a href="<?=$slider['url']?>">
                <?php } ?>
                <div class="wrapper parent">
                    <div class="row child">
                        <div class="col-lg-7 col-md-5">
                            <div class="jumbotron__slider-text ">
                                <p class="subtitle"><?=$slider['caption']?></p>
                                <p class="title"><?=$slider['title_text_1']?></p>
                                <p class="special"><?=$slider['title_text_2']?></p>
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-5 pos-inh">
                            <div class="img-container">
                                <img class="slide-img" src="/files/sliders/<?=$slider['image']?>">
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($slider['url'])) { ?>
                </a>
                <?php } ?>
            </div>
        <?php } ?>

    </div>
    <div class="jumbotron__nav">
        <a href="#" class="jumbotron__slider-left"><img src="img/jumb-arr-l.png"> </a>
        <a href="#" class="jumbotron__slider-right"><img src="img/jumb-arr-r.png"> </a>
    </div>
    <a href="#order-modal" data-toggle="modal" class="btn jumbotron-btn"><?=__('Сделать заказ')?></a>
</div>
