<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sliders Model
 *
 * @method \App\Model\Entity\Slider get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slider newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slider|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slider[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slider findOrCreate($search, callable $callback = null, $options = [])
 */
class SlidersTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
                ->setTable('sliders')
                ->setDisplayField('title_text_1')
                ->setDisplayImage('image')
                ->setPrimaryKey('id')
                ->setFormSchema([
                    'title_text_1' => ['label' => 'Выделенный заголовок'],
                    'title_text_2' => ['label' => 'Текст слайда']
                ])
                ->setOrder([
                    'Sliders.position' => 'DESC',
                    'Sliders.id' => 'DESC'
                ]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'title_text_1', 'title_text_2', 'meta_keywords']
        ]);
    }
}
