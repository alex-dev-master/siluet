<div class="page-content team-content">
    <img class="content-bg" src="/img/team-bg.jpg" alt="">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <div class="worker-list">
            <div class="row list">
                <?php foreach ($experts as $expert) { ?>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="inner-container">
                        <a href="<?= $this->Url->build(["controller" => "Experts","action" => "view", $expert['slug']]) ?>">
                            <img class="worker-img" src="/files/experts/<?=$expert['thumbnail_210x210']?>" alt="">
                            <h6 class="worker-title"><?=$expert['caption']?></h6>
                            <?php
                                $specialitiesExpert = [];
                                foreach ($expert['specialities'] as $speciality) {
                                    $specialitiesExpert[] = $speciality['caption'];
                                }
                            ?>
                            <p class="worker-desc sml-text"><?=implode(", ", $specialitiesExpert);?></p>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>




    </div>
</div>
