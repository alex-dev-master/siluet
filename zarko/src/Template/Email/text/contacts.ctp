<?php
echo "Здравствуйте! С сайта {$_SERVER['HTTP_HOST']} поступило новое сообщение!\n";
$fields = [
    'name' => 'ФИО',
    'phone' => 'Телефон',
    'email' => 'Электронная почта',
    'text' => 'Текст сообщения'
];
foreach ($fields as $field => $name):
    if (!empty($data[$field])):
        $separate = ($field != 'message' ? ': ' : "\n");
        echo $name . $separate . $data[$field] . "\n";
    endif;
endforeach;
