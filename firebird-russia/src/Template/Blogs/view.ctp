<?php
$this->Breadcrumbs->prepend(
    __('Блог'),
    ['controller' => 'Blogs', 'action' => 'index']
);
?>

<div class="blog_detail-top">
<div class="wrapper">
<?= $this->element('breadcrumbs') ?>
</div>
    <div class="wrapper pd-left">
        <a href="/blogs"><img src="/img/icon_back.png"></a>
        <p class="date"><?=$page->published->format('d.m.Y')?></p>
        <h1><?=$page['caption']?></h1>
        <div class="main-image">
            <img src="/files/blogs/<?=$page['thumbnail_960x510']?>">
        </div>
    </div>
</div>
<div class="blog_detail-text">
    <div class="wrapper">
        <?=$page['content']?>
        <div class="center">
            <a href="/blogs" class="btn blog_detail-btn"><?=__('Вернуться к списку')?></a>
        </div>
    </div>
</div>
<?php if (!empty($page['items'])) { ?>
<div class="blog_detail-example">
    <div class="wrapper">
        <h3><?=$page['to_items']?></h3>
        <div class="blog_detail__slider-block">
            <div class="blog_detail__slider">
                <?php foreach ($page['items'] as $item) { ?>
                <div class="slide">
                    <div class="img-container">
                        <img class="slide-img" src="/files/items/<?=$item['thumbnail_226x226']?>">
                        <a href="#order-modal" data-toggle="modal" data-towar="<?=$item['caption']?>" class="blog-order-btn"><?=__('Заказать')?></a>
                        <a href="<?= $this->Url->build(["controller" => "Portfolios","action" => "view", $item['slug']]) ?>" data-toggle="modal" data-towar="<?=$item['caption']?>" class="more-arrow"><img src="/img/arr-w.png"></a>
                    </div>
                    <div class="text">
                        <p><?=$item['caption']?></p>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="blog_detail__slider__nav">
                <a href="#" class="slide-nav-arrow left"><img src="/img/arr-black-l.png"> </a>
                <a href="#" class="slide-nav-arrow right"><img src="/img/arr-black-r.png"> </a>
            </div>
        </div>
    </div>
</div>
<?php } ?>





<?=$this->element('makeOrder')?>
