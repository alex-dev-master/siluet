<div class="header__spez_panel">
            <div class="wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="spez__left">
                        <div class="spez__wrap">
                                    <div class="spez__color_text">
                                        Цветовая схема сайта:
                                    </div>
                                    <div class="spez__colors">
                                        <a href="javascript:;" class="spez__color m--current" data-color="white">A</a><a href="javascript:;" class="spez__color" data-color="black">A</a>                            </div>
                                    </div>
                            <div class="spez__wrap">
                                <div class="spez__scale_text">
                                    Размер шрифта:
                                </div>
                                <div class="spez__scales">
                                    <a href="javascript:;" class="spez__scale m--current" data-scale="1">A</a><a href="javascript:;" class="spez__scale" data-scale="2">A</a><a href="javascript:;" class="spez__scale" data-scale="3">A</a>                            </div>
                                </div>
                                
                                </div>
                                <a href="javascript:;" class="spez__back">Обычная версия сайта <img src="/img/arrow-b.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
<nav class="navbar">
    <div class="header-top">
        <div class="wrapper header-top-wrapper">
            <a class="logo" href="<?= $this->Url->build(["controller" => "Pages","action" => "mainpage"]) ?>">
                <img src="/img/logo.png">
                <div class="logo-text">
                    <h3>Клиника ЮВТ</h3>
                    <p>Офтальмологический центр</p>
                </div>
            </a>
            <div class="header-info">
                <div class="address">
                    <div class="moreBtn address-btn"></div>
                    <div class="address-text">
                        <p class="address-title"><?=$settings['address']?></p>
                        <p class="address-desc">Выходные —<span class="address-span"> <?=$settings['weekends']?></span></p>
                    </div>
                </div>
                <div class="tel">
                    <div class="moreBtn tel-btn"></div>
                    <div class="tel-text">
                        <a class="tel-phone" href="tel:+7 (812) 409-40-92"><?=ltrim($settings['phone'], '8 ')?></a>
                        <a class="tel-orderCall" href="#call-modal" data-toggle="modal">Заказать звонок</a>
                    </div>
                </div>
            </div>
            <a class="eye js-spec" href="#"><img src="/img/eye.png">Версия для слабовидящих</a>
            <div class="mobile-toggle">
                <div class="btn_mob">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>

    <?= $this->cell('Menu', [$settings]);?>
</nav>
