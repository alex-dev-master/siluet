<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ParamRubrics Model
 *
 * @property \App\Model\Table\ParamsTable|\Cake\ORM\Association\HasMany $Params
 *
 * @method \App\Model\Entity\ParamRubric get($primaryKey, $options = [])
 * @method \App\Model\Entity\ParamRubric newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ParamRubric[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ParamRubric|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ParamRubric saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ParamRubric patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ParamRubric[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ParamRubric findOrCreate($search, callable $callback = null, $options = [])
 */
class ParamRubricsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('param_rubrics');
        $this->setDisplayField('caption');
        $this->setPrimaryKey('id');
        $this->setOrder([
            'ParamRubrics.position' => 'DESC'
        ]);

        $this->hasMany('Params', [
            'foreignKey' => 'param_rubric_id'
        ]);
    }
}
