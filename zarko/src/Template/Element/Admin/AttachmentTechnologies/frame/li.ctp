<li class="media dd-item" data-id="<?php echo $row[$primary_key]; ?>" data-p="<?php echo $row['position']; ?>">
    <div class="dd-handle dd3-handle"></div>
    <div class="dd3-content">
        <div class="pull-left">
            <label class="checkbox left-cell-checkbox">
                <input type="checkbox" name="group-checkbox[]" value="<?php echo $row[$primary_key]; ?>">
                <i></i>
            </label>
            <div class="ctrl">
                <?php
                echo $this->Html->link('<i class="fa fa-lg fa-' . ((bool) $row['disabled'] ? '' : 'un') . 'lock"></i>', $UrlCollection->append(['action' => 'toggle_disabled', 'id' => $row[$primary_key]]), ['title' => __d('admin', 'toggle_disabled_action'), 'escape' => FALSE]);
                ?>
                <br>
                <?php
                echo $this->Html->link('<i class="fa fa-lg fa-trash-o"></i>', $UrlCollection->append(['action' => 'delete', 'id' => $row[$primary_key]]), ['title' => __d('admin', 'delete_action'), 'class' => '_remove', 'data-title' => addslashes(__d('admin', 'are_you_sure')), 'escape' => FALSE]);
                ?>
            </div>
        </div>
        <div class="img">
            <a href="<?php echo $this->Url->build($Url = $UrlCollection->append(['action' => 'edit', 'id' => $row[$primary_key], '_ext' => 'frame'])); ?>" class="pull-left" data-toggle="modal" data-target="#modal-dialog" data-src="<?php echo $this->Url->build($Url->append(['background-upload' => 1])); ?>">
                <?php
                if (isset($row[$display_image . '-uploaded'])) {
                    $image_info = getimagesize($row[$display_image . '-uploaded']['filename']);
                    ?>
                    <img src="<?php echo $row[$display_image . '-uploaded']['url-time']; ?>"<?php if ($image_info[0] > 72) { ?> style="width: 72px;"<?php } ?>>
                <?php } elseif (isset($row['file-uploaded'])) { ?>
                    <p><?php echo $row['file-uploaded']['basename']; ?></p>
                <?php } else { ?>
                    <i class="fa fa-pencil-square-o"></i>
                <?php } ?>
            </a>	
        </div>

        <div class="media-body">
            <?php
            foreach ($tab_schema as $tab_name => $tab_description) {
                foreach ($tab_description['field_schema'] as $field_name => $field_description) {
                    if ($field_description['input']['type'] != 'text') {
                        continue;
                    }
                    ?>
                    <div class="inpt-cont" data-url="<?php echo $this->Url->build($UrlCollection->append(['action' => 'saveField', 'field' => $field_name, 'id' => $row[$primary_key]])); ?>">
                        <p><?php echo $field_description['input']['label']; ?>: <span>
                                <?php if (empty($row[$field_name])) { ?>
                                </span></p>
                            <div class="row_wrap">
                                <div class="row">
                                    <div class="col-xs-12 col-md-8 col-lg-9">
                                        <label class="input">
                                            <input type="text" name="field-<?php echo rand(); ?>" value="" placeholder="<?php echo $field_description['input']['label']; ?>">
                                        </label>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-3">
                                        <div class="buttons">
                                            <button type="button" class="btn btn-primary">OK</button>
                                            <button type="button" class="btn">Отменить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <?php echo $row[$field_name]; ?></span></p>
                        <?php } ?>
                    </div>
                <?php }
            }
            ?>
        </div>
    </div>
</li>