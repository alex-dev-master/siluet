<div class="col-6 col-md-8">
    <div class="navigation-wrap">
        <div class="navigation-wrapper">
            <ul class="navigation">
                <?php foreach ($menus as $menu) {?>
                <li class="navigation-item"><a href="<?= $this->Url->build($menu['url']) ?>" class="navigation-item__link <?php if (isset($menu['current'])) { echo 'active'; } ?>"><?= $menu['caption'] ?></a></li>
                <?php } ?>
                <?php if (!empty($langs)): ?>
                        <?php foreach ($langs as $lang): ?>
                            <?php if (!$lang['selected']) {?>
                            <!--<li class="navigation-item"><a href="<?= $this->Url->build($lang['url']) ?>" class="navigation-item__link lang"><?= $lang['title'] ?></a></li> -->
                            <?php } ?>
                        <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <span class="hamburger hamburger--collapse navbar__item">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</span>
</div>
