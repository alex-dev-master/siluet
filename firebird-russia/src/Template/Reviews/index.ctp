<div class="testimonials-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?__('Отзывы')?></h1>
    </div>
</div>
<div class="testimonials-list">
    <?php foreach ($reviews as $review) {  ?>
    <div class="item">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-md-push-8 col-sm-push-6">
                    <?php if (!empty($review['image']) && !empty($review['thumbnail_183x260'])) { ?>
                    <a class="fancybox" href="/files/reviews/<?=$review['image']?>">
                        <div class="img-container">
                            <img src="/files/reviews/<?=$review['thumbnail_183x260']?>">
                        </div>
                    </a>
                    <?php } ?>
                </div>
                <div class="col-md-8 col-sm-6 col-md-pull-4 col-sm-pull-6">
                    <div class="parent">
                        <div class="text">
                            <h4><?=$review['caption']?></h4>
                            <?=$review['content']?>
                            <p class="date"><?=$review->published->format('d.m.Y')?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<?= $this->element('pagination2') ?>

<!-- Пагинация -->




<?=$this->element('makeOrder')?>
