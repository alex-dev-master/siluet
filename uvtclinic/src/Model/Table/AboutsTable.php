<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sliders Model
 *
 * @method \App\Model\Entity\Slider get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slider newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slider|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slider[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slider findOrCreate($search, callable $callback = null, $options = [])
 */
class AboutsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('abouts')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 1440, 'y' => 500]],
                'post_1' => ['label' => 'Должность', 'tab' => 'quotes'],
                'caption_name_1' => ['label' => 'ФИО', 'tab' => 'quotes'],
                'content_quotes_1' => ['label' => 'Цитата', 'tab' => 'quotes', 'type' => 'textarea'],
                'post_2' => ['label' => 'Должность', 'tab' => 'quotes'],
                'caption_name_2' => ['label' => 'ФИО', 'tab' => 'quotes'],
                'content_quotes_2' => ['label' => 'Цитата', 'tab' => 'quotes', 'type' => 'textarea'],
                'content_requisites' => ['label' => 'Сведения об организации', 'type' => 'wysiwyg'],

                'info_phone' => ['label' => 'Телефон', 'tab' => 'infos', 'type' => 'text'],
                'info_time' => ['label' => 'График', 'tab' => 'infos', 'type' => 'text'],
                'info_write' => ['label' => 'Запись', 'tab' => 'infos', 'type' => 'text'],

                'caption_block_care' => ['label' => 'Заголовок', 'tab' => 'block_care', 'type'],
                'content_block_care' => ['label' => 'Содержимое', 'tab' => 'block_care'],
                'services_block_care' => ['label' => 'Список услуг (через запятую)', 'tab' => 'block_care'],
            ])
            ->setTabSchema([
                'quotes' => ['label' => 'Цитаты'],
                'infos' => ['label' => 'Информация о записе на прием'],
                'block_care' => ['label' => 'Блок с заботой'],
                'attachment_admins' => ['label' => 'Администраторы'],
                'attachment_peoples' => ['label' => 'Цитаты сотрудиков'],
            ]);
        $this->hasMany('AttachmentAdmins', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentAdmins.model' => 'Abouts']]);
        $this->hasMany('AttachmentPeoples', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentPeoples.model' => 'Abouts']]);

    }
}
