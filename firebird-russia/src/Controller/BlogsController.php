<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class BlogsController extends AppController
{
    public $paginate;

    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $this->paginate = [
            'limit' => 3,
            'order' => [
//                'Objects.archive_status' => 'asc'
            ],
        ];

        $meta = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 8]) ->first();
        $this->Title->setMeta($meta, 'Blogs');

        $blogs = $this->paginate(TableRegistry::getTableLocator()->get('Blogs')->find()->where(['Blogs.to_sidebar' => false])->order(['Blogs.published' => 'DESC']))->toArray();
        $blogs_sidebar = TableRegistry::getTableLocator()->get('Blogs')->find()->where(['Blogs.to_sidebar' => true])->order(['Blogs.published' => 'DESC'])->toArray();
        $this->set(compact('blogs', 'blogs_sidebar'));
    }

    public function view($id, $query = [], $merge_query = true)
    {
        $page = TableRegistry::getTableLocator()->get('Blogs')->find()->where(['Blogs.slug' => $id])->contain(['Items'])->first();
        $this->Title->setMeta($page, 'Blogs');
        $this->set(compact('page'));

    }

    public function getObjects() {
        $this->paginate = [
            'limit' => 3,
        ];
        $blogs = $this->paginate(TableRegistry::getTableLocator()->get('Blogs')->find()->order(['Blogs.published' => 'DESC']))->toArray();

        $this->set(compact('blogs'));
        $this->render('/Element/getobjects');
    }

}
