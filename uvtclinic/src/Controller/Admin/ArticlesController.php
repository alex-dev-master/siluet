<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 *
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function edit($id = null, callable $callback = null)
    {
        $this->setRequest($this->getRequest()->withData('items._ids', []));
        $this->setRequest($this->getRequest()->withData('item_rubrics._ids', []));
        parent::edit($id, $callback);
        $View = $this->createView();
        $tab_schema = $View->get('tab_schema');
        unset($tab_schema['habtm']['field_schema']['items._ids']);
        unset($tab_schema['habtm']['field_schema']['item_rubrics._ids']);

        $this->set('tab_schema', $tab_schema);
    }

}
