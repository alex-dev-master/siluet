<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use \Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ExperiencesController extends AppController
{
    public $paginate;

    public function initialize()
    {
        parent::initialize();
    }

    public function index($rubric_id = null, $query = array(), $merge_query = true) {
        if ($rubric_id == 'cases') {
            if ($this->request->is('get')) {
                $data = $this->request->getQuery('town');
                $this->getObjectsCases($data);
            } else {
                $this->getObjectsCases();
            }
        } else {
            if ($this->request->is('get')) {
                $data = $this->request->getQuery('town');
                $this->getAllObjects($data);

            } else {
                $this->getAllObjects();
            }
        }
        $type_object = $this->Experiences->find()->contain(['Types'])->toArray();
        $arr_type = [];
        foreach ($type_object as $type){
            if (!empty($type['type']['caption'])){
            array_push($arr_type, $type['type']['caption']);
            }
        }

        $arr_type = array_unique($arr_type);

        $meta = ['meta_title' => 'Наш опыт', 'meta_description' => 'Наш опыт', 'meta_keywords' => 'Наш опыт'];
        $this->Title->setMeta($meta);
        $this->set(compact('arr_type'));

    }

    private function getAllObjects($data = null) {
        $this->paginate = [
            'order' => [
                'Experiences.id' => 'asc'
            ],
            'contain' => ['Services']
        ];

        if (!empty($data)) {
//            $allObjects = $this->paginate($this->Experiences->find('all', ['conditions' => ['type_object' => $data]]), ['limit' => 3]);
            $allObjects = $this->paginate($this->Experiences->find()->contain(['Types'])->where(['Types.caption' => $data])->limit(3));
        } else {
            $allObjects = $this->paginate($this->Experiences->find()->contain(['Types']), ['limit' => 3]);
        }
        $this->set(compact('allObjects'));
    }

    private function getObjectsCases($data = null) {


        $this->paginate = [
            'limit' => 3,
            'order' => [
                'Experiences.id' => 'asc',
            ],
        ];

        if (!empty($data)) {
//            $objectsCases = $this->paginate($this->Experiences->find('all', ['conditions' => ['type_object' => $data, 'status_case' => 1]]), ['limit' => 3]);
            $objectsCases = $this->paginate($this->Experiences->find()->contain(['Types'])->where(['Types.caption' => $data, 'status_case' => 1])->limit(3));
        } else {
            $objectsCases = $this->paginate($this->Experiences->find('all')->where(['status_case' => 1]), ['limit' => 3]);

        }

//        $objectsCases = $this->paginate($this->Experiences->find('all')->where(['status_case' => 1]));
        $this->set(compact('objectsCases'));
    }

    public function view($id, $query = [], $merge_query = true)
    {
        $this->viewBuilder()->setLayout('default_main_page');
        $object= $this->Experiences->findBySlug($id)->contain(['Services', 'Types', 'AttachmentTechnologies', 'AttachmentParticipants', 'AttachmentWorks'  => ['Services']])->first();
        $this->Title->setMeta($object);
        $this->set(compact('object'));
    }

}
