
<div class="faq-theme">
 <div class="wrapper">
<?= $this->element('breadcrumbs') ?>
</div>
    <div class="wrapper">

        <h1><?=$page['caption']?></h1>
        <ul class="tabs-list">
            <?php foreach ($rubrics_questions as $k=>$rubric) { ?>
            <li class="<?php if ($k == 0) echo 'active'; ?>"><a data-name="<?=$rubric['slug']?>" href=""><?=$rubric['caption']?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="faq-list">
        <?=$this->element('/Questions/list', ['questions' => $rubrics_questions[0]['questions']])?>
</div>


<?=$this->element('makeOrder')?>
