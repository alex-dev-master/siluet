<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Params Model
 *
 * @property \App\Model\Table\ParamRubricsTable|\Cake\ORM\Association\BelongsTo $ParamRubrics
 *
 * @method \App\Model\Entity\Param get($primaryKey, $options = [])
 * @method \App\Model\Entity\Param newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Param[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Param|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Param saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Param patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Param[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Param findOrCreate($search, callable $callback = null, $options = [])
 */
class ParamsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('params');
        $this->setDisplayField('caption');
        $this->setPrimaryKey('id');
        $this->setOrder([
            'Params.position' => 'DESC'
        ]);
        $this->setNeighborhood(['Params.param_rubric_id']);

        $this->belongsTo('ParamRubrics', [
            'foreignKey' => 'param_rubric_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['param_rubric_id'], 'ParamRubrics'));

        return $rules;
    }
}
