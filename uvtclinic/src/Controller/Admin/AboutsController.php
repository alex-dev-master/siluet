<?php
namespace App\Controller\Admin;

class AboutsController extends AppController {
    public function edit($id = null, callable $callback = null)
    {
        parent::edit($id, $callback);
        $View = $this->createView();
        $tab_schema = $View->get('tab_schema');
        unset($tab_schema['quotes']);
        $this->set('tab_schema', $tab_schema);
    }
}
