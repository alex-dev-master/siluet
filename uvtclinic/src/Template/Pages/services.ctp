<div class="page-content services-content">
    <img class="content-bg" src="/files/pages/<?=$row['thumbnail_1440x540']?>" alt="">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <div class="serv-list">
            <div class="row list">
                <?php foreach ($itemRubrics as $rubric) { ?>
                <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="inner-container">
                    <a href="/services/rubric/<?=$rubric['slug']?>">
                        <img src="/files/item-rubrics/<?=$rubric['image']?>" alt="">
                        <?php if (!empty($rubric['image_2'])) { ?>
                        <img class="hidden-img" src="/files/item-rubrics/<?=$rubric['image_2']?>" alt="">
                        <?php } ?>
                        <h6 class="serv-title"><?=$rubric['caption']?></h6>
                    </a>
                </div>
                </div>
                <?php } ?>
            </div>
        </div>
              <div class="text">
        <?=$row['content']?>
        </div>
    </div>
</div>
