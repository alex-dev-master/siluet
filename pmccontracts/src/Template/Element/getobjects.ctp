<?php //$this->layout = null ; ?>
<div class="catalog-objects">
    <div class="project-list">
        <div class="row list">
            <?php foreach ($objects as $object) { ?>
                <div class="col-md-4 col-sm-6 col-ss-6 col-xs-12 project-col">
                    <div class="project-item">
                        <?php
                        $str = '?';
                        if (!empty($category_slug)) {
                            $str = $str."category=$category_slug&";
                        }
                        if (!empty($status_object)) {
                            $str = $str."status_objects=$status_object&";
                        }
                        if (!empty($_GET['page'])) {
                            $pageGet = $_GET['page'];
                            $str = $str."page=$pageGet";
                        }
                        if ($str == '?') $str = '';
                        ?>
                        <a href="<?= $this->Url->build(['controller' => 'Projects', 'action' => $object['slug']]); ?>/<?=$str?>">
                            <div class="project-item-img-container">
                                <img src="/files/objects/<?=$object['image']?>">
                                <p class="status <?php if ($object['archive_status']){echo 'finish';}else{echo 'work';}?>"><?php if ($object['archive_status']) { echo __('Завершен'); } else { echo __('В работе'); } ?></p>
                            </div>
                            <div class="project-item__text">
                                <p class="title"><?=$object['caption']?></p>
                                <p class="count"><?=$object['place']?></p>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?= $this->element('pagination') ?>
</div>
