<div class="page-top">
    <div class="wrapper">
        <h1 class="h1-big"><?=__('Услуги')?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                <div class="content__left-col">
                    <ul class="left-menu">
                        <?= $this->cell("Menu::services", [$langs, $settings]) ?>
                    </ul>
                    <div class="slogan"><p><?=__('Мы предлагаем')?></p></div>
                </div>

            </div>
            <div class="col-md-8">
                <h2><?=$page['caption']?></h2>
                <?=$page['content']?>
                <div class="service-list">
                    <div class="row list">
                        <?php foreach ($services as $service) { ?>
                        <div class="col-md-6 col-sm-6 col-ss-6 col-xs-12 service-col">
                            <div class="service-item">
                                <a href="<?= $this->Url->build(['controller' => 'Services', 'action' => $service['slug']]); ?>">
                                    <div class="service-item-img-container">
                                        <img src="/files/services/<?=$service['image']?>">
                                    </div>
                                    <div class="service-item__text">
                                        <?php
                                        $int = preg_split('//',count($service['objects']));
                                        array_pop($int); array_shift($int);
                                        $last = end($int);
                                        if ($last == 0) {
                                            $word = 'проектов';
                                        }
                                        if ($last == 1) {
                                            $word = 'проект';
                                        }
                                        if ($last >= 2 && $last < 5) {
                                            $word = 'проекта';
                                        }
                                        if ($last >= 5 && $last <= 10) {
                                            $word = 'проектов';
                                        }
                                        ?>
                                        <p class="title"><?=$service['caption']?></p>
                                        <p class="count"><?= count($service['objects']) ?> <?=__($word)?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
