<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ItemRubricsController extends AppController {
    public function index($rubric_id = null, $query = array(), $merge_query = true) {
        $page = TableRegistry::getTableLocator()->get('ItemRubrics')->find()
            ->contain(['Items', 'AttachmentCosts', 'AttachmentInfos', 'Experts' => ['Specialities']])
            ->where(['ItemRubrics.slug' => $rubric_id])->first();
        $this->set(compact('page'));
        $this->Title->setMeta($page);
    }
}
