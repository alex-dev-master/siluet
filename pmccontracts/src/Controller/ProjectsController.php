<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Database\Expression\QueryExpression;
use Cake\Database\Query;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ProjectsController extends AppController
{
    public $paginate;

    public function initialize()
    {
        parent::initialize();
    }

    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $project_page = TableRegistry::getTableLocator()->get('Projects')->find()->first();
//        $services = TableRegistry::getTableLocator()->get('Services')->find();
        $services = $this->getServices();

        $getQueryParams = $this->getRequest()->getQueryParams();
        if (!empty($getQueryParams['category'])) {
            $category = $getQueryParams['category'];
            $this->set(compact('category'));
        }
        if (!empty($getQueryParams['status_objects'])) {
            $status_objects = $getQueryParams['status_objects'];
            $this->set(compact('status_objects'));
        }

        if (!empty($this->request->getQuery('category'))) {
            $category_slug = $this->request->getQuery('category');
            $status_objects = $this->request->getQuery('status_objects');
            $this->getAllObjectsCategory($category_slug, $status_objects);
        } else {
            $this->getAllObjects('undefined');
        }
        $this->Title->setMeta($project_page, 'Projects');
        $this->set(compact('project_page', 'services', 'objects'));
    }

    private function getServices() {
        $services = TableRegistry::getTableLocator()->get('Services')->find();
        $ids = [];
        $services->each(function ($item) use (&$ids) {
            $ids[] = $item->get('id');
        });
        $objects = [];
        $object = TableRegistry::getTableLocator()->get('Objects')->find()
            ->join(['ObjectsServices' => ['table' => 'objects_services', 'type' => 'INNER', 'conditions' => 'ObjectsServices.object_id = Objects.id']])
            ->where(['ObjectsServices.service_id IN' => $ids])
            ->select(['Objects.id', 'ObjectsServices.object_id', 'ObjectsServices.service_id', 'Objects.caption', 'slug'])
            ->each(function ($item) use (&$objects) {
                $objects[$item['ObjectsServices']['service_id']][] = $item;
            });

        $services = $services->map(function ($item) use ($objects) {
            $rows = [];
            if (isset($objects[$item['id']])) {
                $rows = $objects[$item['id']];
            }
            $item->set('objects', $rows);
            return $item;
        });
        return $services;
    }

    public function getObjects() {
//        $this->layout = null ;
        $category_slug = $this->request->getQuery('category');
        $status_object = $this->request->getQuery('status_objects');
//        $category_slug = 'Управляющий проектом';
        if ($category_slug != 'undefined') {
            $this->getAllObjectsCategory($category_slug, $status_object);
        } else {
            $this->getAllObjects($status_object);
        }
        $this->set(compact('category_slug', 'status_object'));
        $this->render('/Element/getobjects');
    }

    private function getAllObjects($status_object) {
        $this->paginate = [
            'limit' => 6,
            'order' => [
                'Objects.archive_status' => 'asc',
                'Objects.position' => 'asc',
            ],
        ];
        if ($status_object == 'undefined') {
            $objects = $this->paginate(TableRegistry::getTableLocator()->get('Objects')->find());
        }
        elseif ($status_object == 'notarchive') {
            $objects = $this->paginate(TableRegistry::getTableLocator()->get('Objects')->find()->where(['Objects.archive_status' => 0]));
        }
        elseif ($status_object == 'archive') {
            $objects = $this->paginate(TableRegistry::getTableLocator()->get('Objects')->find()->where(['Objects.archive_status' => 1]));
        }
        else {
            $objects = $this->paginate(TableRegistry::getTableLocator()->get('Objects')->find());
        }


        $this->set(compact('objects'));
    }

    private function getAllObjectsCategory($category_slug, $status_object) {
        $this->paginate = [
            'limit' => 6,
            'order' => [
                'Objects.archive_status' => 'asc',
                'Objects.position' => 'asc',
            ],

        ];
        $query_object_services = TableRegistry::getTableLocator()->get('Objects')->find()
            ->contain(['Services' => function ($q) use ($category_slug) {
                return $q->where(['Services.slug' => $category_slug]);
            }]);
        $arr_id = [];
        foreach ($query_object_services as $service) {
            if (!empty($service['services'])) {
                array_push($arr_id, $service['id']);
            }
        }
        if ($status_object == 'undefined') {
            $query_object = TableRegistry::getTableLocator()->get('Objects')->find()
                ->where(function (QueryExpression $exp, Query $q) use ($arr_id) {
                    if (!empty($arr_id)) {
                        return $exp->in('Objects.id', $arr_id);
                    } else {
                        return $exp;
                    }
                });
        }
        elseif ($status_object == 'notarchive') {
            $query_object = TableRegistry::getTableLocator()->get('Objects')->find()
                ->where(function (QueryExpression $exp, Query $q) use ($arr_id) {
                    if (!empty($arr_id)) {
                        return $exp->in('Objects.id', $arr_id);
                    } else {
                        return $exp;
                    }
                })->where(['Objects.archive_status' => 0]);
        }

        elseif ($status_object == 'archive') {
            $query_object = TableRegistry::getTableLocator()->get('Objects')->find()
                ->where(function (QueryExpression $exp, Query $q) use ($arr_id) {
                    if (!empty($arr_id)) {
                        return $exp->in('Objects.id', $arr_id);
                    } else {
                        return $exp;
                    }
                })->where(['Objects.archive_status' => 1]);
        }

        if (!isset($query_object)) {
            $query_object = TableRegistry::getTableLocator()->get('Objects')->find()
                ->where(function (QueryExpression $exp, Query $q) use ($arr_id) {
                    if (!empty($arr_id)) {
                        return $exp->in('Objects.id', $arr_id);
                    } else {
                        return $exp;
                    }
                });
        }


        $objects = $this->paginate($query_object);

        $this->set(compact('objects'));
    }

    public function view($id, $query = [], $merge_query = true)
    {
        $object = TableRegistry::getTableLocator()->get('Objects')->find()->contain(['AttachmentImages'])->where(['Objects.slug' => $id])->first();

        if (!empty($this->getRequest()->getQueryParams())) {
            $getQueryParams = $this->getRequest()->getQueryParams();
            $category = $getQueryParams['category'];
            $status_objects = $getQueryParams['status_objects'];
            if ($category != 'undefined' && $status_objects == 'undefined') {
                $id_slugs = TableRegistry::getTableLocator()->get('Objects')->find()
                    ->contain('Services', function (Query $q) use ($category) {
                        return $q
                            ->where(['Services.slug' => $category]);
                    })
                    ->order(['Objects.archive_status' => 'asc','Objects.position' => 'asc'])
                    ->toArray();
                $new_id_slug = [];
                foreach ($id_slugs as $k => $id_slug) {
                    if (!empty($id_slug['services'])) {
                        $new_id_slug[$k] = $id_slug;
                    }
                }
                $id_slugs = $new_id_slug;
            } elseif ($category == 'undefined' && $status_objects != 'undefined') {
                $status = 0;
                if ($status_objects == 'archive') {
                    $status = 1;
                }
                $id_slugs = TableRegistry::getTableLocator()->get('Objects')->find()
                    ->where(['Objects.archive_status' => $status])
                    ->order(['Objects.archive_status' => 'asc','Objects.position' => 'asc'])
                    ->select(['id', 'slug'])->toArray();
            } elseif ($category != 'undefined' && $status_objects != 'undefined') {
                $status = 0;
                if ($status_objects == 'archive') {
                    $status = 1;
                }
                $id_slugs = TableRegistry::getTableLocator()->get('Objects')->find()
                    ->contain('Services', function (Query $q) use ($category) {
                        return $q
                            ->where(['Services.slug' => $category]);
                    })
                    ->where(['Objects.archive_status' => $status])
                    ->order(['Objects.archive_status' => 'asc','Objects.position' => 'asc'])
                    ->select(['id', 'slug'])->toArray();

                $new_id_slug = [];
                foreach ($id_slugs as $k => $id_slug) {
                    if (!empty($id_slug['services'])) {
                        $new_id_slug[$k] = $id_slug;
                    }
                }
                $id_slugs = $new_id_slug;
            }
            $this->set(compact('category', 'status_objects'));
        } else {
            $id_slugs = TableRegistry::getTableLocator()->get('Objects')->find()
                ->select(['id', 'slug'])
                ->order(['Objects.archive_status' => 'asc','Objects.position' => 'asc'])
                ->toArray();
        }
        if (empty($id_slugs)) {
            $id_slugs = TableRegistry::getTableLocator()->get('Objects')->find()
                ->order(['Objects.archive_status' => 'asc','Objects.position' => 'asc'])
                ->select(['id', 'slug'])->toArray();
        }
        $arr_ids = [];
        foreach ($id_slugs as $id_keys) {
            array_push($arr_ids, $id_keys['id']);
        }
        $resp_arr = $this->closestNum($arr_ids, $object['id']);

        foreach ($id_slugs as $id_slug) {
            if (!empty($resp_arr['before']) && $id_slug['id'] == $resp_arr['before']) {
                $slug_closest['before'] = $id_slug['slug'];
            }

            if (!empty($resp_arr['after']) && $id_slug['id'] == $resp_arr['after']) {
                $slug_closest['after'] = $id_slug['slug'];
            }
        }

        $this->Title->setMeta($object, 'Projects');
        $this->set(compact('object', 'slug_closest'));
    }

    private function closestNum($arrs,$num){
        $result = [];
        foreach ($arrs as $k => $arr) {
            $key = $k;
            $keyAfeter = $key + 1;
            $keyBefore = $key - 1;
            if ($num  == $arr) {
                if (!empty($arrs[$keyBefore])) {
                    $result['before'] = $arrs[$keyBefore];
                }
                if (!empty($arrs[$keyAfeter])) {
                    $result['after'] = $arrs[$keyAfeter];
                }
                return $result;
            }
        }
    }
//    private function closestNum($arr,$num){
//        $res = false;
//        sort($arr);
//        $less = array_filter($arr, function($a) use ($num){return $a < $num;});
//        $greater = array_filter($arr, function($a) use ($num){return $a > $num;});
//        $min = end($less);
//        $max = reset($greater);
//
//        $avg = (($min + $max) / 2);
//        $res = 'Число '.$num.' - ';
//        $res .= $num == $avg ? 'ровно между '.$min.' и '.$max : 'ближе к '.($num > $avg ? $max : $min);
//
//        $resp_arr = [$min, $max];
//        return $resp_arr;
//    }

}
