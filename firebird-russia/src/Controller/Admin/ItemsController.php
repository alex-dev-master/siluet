<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 *
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemsController extends AppController
{
    public function beforeRender(\Cake\Event\Event $event) {
        if (in_array($this->request->getParam('action'), array('add', 'edit', 'index'))) {
            $id = $this->request->getParam('id');
            if ($this->request->getParam('action') == 'edit' and isset($this->viewVars['row']['item_rubric_id'])) {
                $id = $this->viewVars['row']['item_rubric_id'];
            }
            if ($id) {
                $this->set('pathnodes', $this->Items->ItemRubrics->find('path', ['for' => $id]));
            }
        }
        parent::beforeRender($event);
    }



    public function edit($id = null)
    {
        if ($this->getRequest()->is(['post', 'put'])) {
            $data = [];
            foreach ($this->getRequest()->getData('params', []) as $params) {
                if (!isset($params['id']) or empty($params['values'])) {
                    continue;
                }
                $data[] = ['id' => $params['id'], '_joinData' => ['value' => implode('|', $params['values'])]];
            }
            $this->request = $this->getRequest()->withData('params', $data);
        }
        parent::edit($id);
        $View = $this->createView();
        $params = $this->Items->ItemRubrics->ParamRubrics
            ->find()
            ->join([
                't1' => [
                    'table' => 'item_rubrics_param_rubrics',
                    'conditions' => 't1.param_rubric_id = ParamRubrics.id'
                ]
            ])
            ->select(['id', 'caption'])
            ->where(['t1.item_rubric_id' => $View->get('row')['item_rubric_id']])
            ->contain(['Params']);
        $this->set('params', $params);
        $tab_schema = $View->get('tab_schema');
        $tab_schema['params'] = [
            'type' => 'params',
            'label' => 'Параметры',
            'icon' => 'fa-cogs',
            'field_schema' => [
                'params._ids' => $tab_schema['habtm']['field_schema']['params._ids']
            ]
        ];
        unset($tab_schema['habtm']['field_schema']['params._ids']);
        $this->set('tab_schema', $tab_schema);
    }
    
}
