<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * Menu cell
 */
class MenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    private $urls = [
        'about' => ['controller' => 'Abouts', 'action' => 'index'],
        'contacts' => ['controller' => 'Contacts', 'action' => 'index'],
        'experience' => ['controller' => 'Experiences', 'action' => 'index'],
        'services' => ['controller' => 'Services', 'action' => 'index'],
        'sitemap' => ['controller' => 'Misc', 'action' => 'sitemap']
    ];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    private function addMenu(&$menus, $caption, $url = false, $submenu = [], $key = false)
    {
        $menu = ['caption' => $caption];

        if (!empty($url)) {
            $menu['url'] = $url;

            if (!empty($url['controller']) && $this->request->getParam('controller')  == $url['controller']) {
                if (!empty($url['action'])) {
                    if ($this->request->action == 'view' && $url['action'] == 'index') {
                        $menu['current'] = true;
                    } else {
                        $menu['current'] = ($this->request->getParam('action')  == $url['action']);
                    }
                }
                if ($menu['current'] && isset($url['id']) || isset($url['slug'])) {
                    $menu['current'] = ((isset($url['id']) && $this->request->id == $url['id']) || (isset($url['slug']) && $this->request->slug == $url['slug']));
                }
            }
        }

        if (!empty($submenu)) {
            $menu['submenu'] = $submenu;
            if (is_array($submenu) && empty($menu['current'])) {
                foreach ($submenu as $sub) {
                    if (!empty($sub['current'])) {
                        $menu['current'] = true;
                        break;
                    }
                }
            }
        }

        if ($key) {
            $menus[$key] = $menu;
        } else {
            array_push($menus, $menu);
        }
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($langs)
    {
        $menus = [];
        $this->addMenu($menus, __('Кто мы?'), $this->urls['about']);
        $this->addMenu($menus, __('Что мы делаем'), $this->urls['services']);
        $this->addMenu($menus, __('Наш опыт'), $this->urls['experience']);
        $this->addMenu($menus, __('Контакты'), $this->urls['contacts']);
        $this->set(compact('menus', 'langs'));
    }

    public function footer($langs, $settings)
    {
        $menus = [];
        $serv = [];
        $projects = [];

        $this->addMenu($menus, __('О нас'), $this->urls['about']);
        $this->addMenu($menus, __('Наш опыт'), $this->urls['experience']);
        $this->addMenu($menus, __('Контакты'), $this->urls['contacts']);
        $this->addMenu($menus, __('Карта сайта'), $this->urls['sitemap']);

        $services = TableRegistry::get('Services')->find()->select(['caption', 'slug'])->toArray();
        foreach ($services as $service) {
            $this->addMenu($serv, $service->get('caption'), '/services/view/'.$service->get('slug'));
        }
        $experiences = TableRegistry::get('Experiences')->find()->select(['caption', 'slug'])->where(['footer_menu' => true])->toArray();
        foreach ($experiences as $experience) {
            $this->addMenu($projects, $experience->get('caption'), '/experiences/view/'.$experience->get('slug'));
        }
        $this->set(compact('menus', 'langs', 'serv', 'projects', 'settings'));
    }
}
