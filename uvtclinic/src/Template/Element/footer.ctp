<footer class="footer">
    <div class="footer-top">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-6 col-sm-4 col-xs-12 ">
                    <h3>Мы на связи</h3>
                    <a class="phone" href="tel:+7 (812) 409-40-92"><?=ltrim($settings['phone'], '8 ')?></a>
                    <ul class="contact-menu">
                        <li class="address">
                            <p><?=$settings['address']?></p>
                        </li>
                        <li class="email">
                            <a href="mailto:info@uvt-clinic.ru "><?=$settings['email']?></a>
                        </li>
                    </ul>
                    <div class="social-list">
                        <a href="<?=$settings['link_vk']?>" target="_blank"><img src="/img/vk.png" alt=""></a>
                        <a href="<?=$settings['link_facebook']?>" target="_blank"><img src="/img/fb.png" alt=""></a>
                        <a href="<?=$settings['link_inst']?>" target="_blank"><img src="/img/inst.png" alt=""></a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-8 col-xs-12">
                    <div class="row">
                        <?= $this->cell('Menu::footer', [$settings]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-btm">
        <div class="wrapper">
            <ul class="footer-info">
                <li><p class="copy-text"><span class="copy">&copy;</span><?=$settings['copyright']?></p></li>
                <li><a href="/politics">Политика конфиденциальности</a></li>
                <li><a href="/sitemap">Карта сайта</a></li>
                <li class="siluet-li">
                    <p class="siluet">
                        <a target="_blank" href="http://siluetstudio.com"><?=$settings['bf587ceef5']?></a>
                    </p>
                </li>
            </ul>
        </div>
    </div>
</footer>

<div id="job-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/img/modal-close.png"></button>
                <p class="modal-subtitle">Отклик на вакансию</p>
                <h3 class="modal-title"></h3>
            </div>
            <!-- Основное содержимое модального окна -->
            <?= $this->Form->create(['schema' => []], ['id' => 'sendJob', 'class' => 'form']); ?>
            <div class="modal-body">
                <input type="hidden" value="" name="spec">
                <label for="name2">ФИО<span>*</span></label>
                <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input required" name="phone" id="organization2" placeholder="Ваш номер телефона">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail</label>
                <input class="modal-input" name="email" id="organization2" placeholder="Электронная почта для связи">
                <label for="organization2">Комментарий</label>
                <textarea class="modal-input" name="msg" id="organization2" placeholder="Дополнительные сведения"></textarea>

                <label for="msg">Файл (не более 50Мб)</label>
                <div class="files">
                    <div class="file-input">
                        <div class="file_upload">
                            <div>Файл не выбран</div>
                            <button>Обзор</button>
                            <input name="file" type="file">
                        </div>
                    </div>
                </div>
                <label class="checkbox">
                    <input name="privacy-policy" type="checkbox">
                    <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                            конфиденциальности</a></div>
                </label>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-mod"><img src="/img/mail.png">Отправить</button>
                </div>
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<div id="appointment-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/img/modal-close.png"></button>
                <h3 class="modal-title">Запись на прием</h3>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <?= $this->Form->create(['schema' => []], ['id' => 'sendAppointment', 'class' => 'form']); ?>
                <?php
                $url      = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $validURL = str_replace("&", "&amp", $url);
                ?>
                <input type="hidden" name="page" value="<?=$validURL?>">
                <label for="name2">Ваше имя<span>*</span></label>
                <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input required" name="phone" id="organization2" placeholder="Ваш номер телефона">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail</label>
                <input class="modal-input" name="email" id="organization2" placeholder="Электронная почта для связи">
                <label for="organization2">Комментарий</label>
                <textarea class="modal-input" name="msg" id="organization2" placeholder="Дополнительная информация"></textarea>

                <label class="checkbox">
                    <input type="checkbox" name="privacy-policy">
                    <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                            конфиденциальности</a></div>
                </label>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-mod"><img src="/img/mail.png">Отправить</button>
                </div>
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<div id="mail-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/modal-close.png"></button>
                <h3 class="modal-title">Сообщение для клиники ЮВТ</h3>
            </div>
            <!-- Основное содержимое модального окна -->
            <?= $this->Form->create(['schema' => []], ['id' => 'sendMailModal', 'class' => 'form']); ?>
            <div class="modal-body">
                <label for="name2">Ваше имя<span>*</span></label>
                <input class="modal-input required" id="name2" name="name" placeholder="Как к вам обратиться?">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input required" id="organization2" name="phone" placeholder="Ваш номер телефона">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail</label>
                <input class="modal-input" id="organization2" name="email" placeholder="Электронная почта для связи">
                <label for="organization2">Текст сообщения</label>
                <textarea class="modal-input" id="organization2" name="msg" placeholder="Что вы хотели сказать?"></textarea>

                <label class="checkbox">
                    <input type="checkbox" name="privacy-policy">
                    <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                            конфиденциальности</a></div>
                </label>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-mod"><img src="/img/mail.png">Отправить</button>
                </div>
                <h4 id="success_msg" class="success"><?=__('Форма успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<div id="call-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="img/modal-close.png"></button>
                <h3 class="modal-title">Заказ звонка</h3>
            </div>
            <!-- Основное содержимое модального окна -->
            <?= $this->Form->create(['schema' => []], ['id' => 'sendCallModal', 'class' => 'form']); ?>
            <div class="modal-body">
                <?php
                $url      = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $validURL = str_replace("&", "&amp", $url);
                ?>
                <input type="hidden" name="page" value="<?=$validURL?>">
                <label for="name2">Ваше имя<span>*</span></label>
                <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">Телефон<span>*</span></label>
                <input class="modal-input required" name="phone" id="organization2" placeholder="Ваш номер телефона">
                <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                <label class="checkbox">
                    <input type="checkbox" name="privacy-policy">
                    <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                            конфиденциальности</a></div>
                </label>
                <div class="btns">
                    <button data-tab="#success_msg" class="btn btn-mod"><img src="img/mail.png">Заказать звонок</button>
                </div>
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
