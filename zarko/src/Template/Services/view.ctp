<div class="page-content service-page">
    <div class="page-content__header page-content__section withBg" style="background-image: url(/files/services/<?= $servicesItem['thumbnail_1600x669'] ?>);">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 col-lg-8 col-xl-7">
                    <div class="service-page__subtitle">
                        Наши услуги
                        <div class="sep"></div>
                        <div class="icon">
                            <?php $icon = @file_get_contents('./'.$servicesItem['icon-uploaded']['url']); if (!empty($icon)) echo $icon;?>
                        </div>
                    </div>
                    <h1 class="service-page__title"><?= $servicesItem['caption_full'] ?></h1>
                    <p class="service-page__description"><?= $servicesItem['explanation'] ?></p>
                </div>
                <div class="col-12 col-md-3 col-lg-4 col-xl-5">
                    <div class="service-page__categories">
                        <?php foreach ($services_icon as $serv_img) { ?>
                            <a href="<?= $this->Url->build(["controller" => "Services","action" => "view", $serv_img['slug']]) ?>" class="categories-item <?php if ($serv_img['slug'] == $servicesItem['slug']) {echo ' current'; } ?>">
                                <?php $icon = @file_get_contents('./files/services/'.$serv_img['icon']); if (!empty($icon)) echo $icon;?>
                            </a>
                        <?php } ?>

                        <div class="btn-scroll">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 10 20.4" xml:space="preserve"><path d="M4.3,19.7L5,20.4l0.7-0.7c0,0,0,0,0,0l4-4c0.4-0.4,0.4-1,0-1.4s-1-0.4-1.4,0L6,16.6V1c0-0.6-0.4-1-1-1S4,0.4,4,1v15.6 l-2.3-2.3c-0.4-0.4-1-0.4-1.4,0C0.1,14.5,0,14.7,0,15s0.1,0.5,0.3,0.7L4.3,19.7C4.3,19.7,4.3,19.7,4.3,19.7z"/></svg>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container">

        <!-- num -->
        <?php if (!empty($servicesItem['text_showing_1']) && !empty($servicesItem['text_showing_2']) && !empty($servicesItem['text_showing_3']) && !empty($servicesItem['text_showing_4'])) { ?>
        <div class="num">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="num-item">
                        <?php if (!empty($servicesItem['num_showing_1'])) { ?>
                        <div class="num-item__left"><?= $servicesItem['num_showing_1'] ?></div>
                        <?php } ?>
                        <div class="num-item__title"><?= $servicesItem['text_showing_1'] ?></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="num-item">
                        <?php if (!empty($servicesItem['num_showing_2'])) { ?>
                        <div class="num-item__left"><?= $servicesItem['num_showing_2'] ?></div>
                        <?php } ?>
                        <div class="num-item__title"><?= $servicesItem['text_showing_2'] ?></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="num-item">
                        <?php if (!empty($servicesItem['num_showing_3'])) { ?>
                        <div class="num-item__left"><?= $servicesItem['num_showing_3'] ?></div>
                        <?php } ?>
                        <div class="num-item__title"><?= $servicesItem['text_showing_3'] ?></div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="num-item">
                        <?php if (!empty($servicesItem['num_showing_4'])) { ?>
                        <div class="num-item__left"><?= $servicesItem['num_showing_4'] ?></div>
                        <?php } ?>
                        <div class="num-item__title"><?= $servicesItem['text_showing_4'] ?></div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- num end -->
    </div>
    <?php if (!empty($servicesItem['service_desc_block_title']) && !empty($servicesItem['service_desc_block_img'])) { ?>
    <div class="section section-push section-push__right">
        <div class="container">
            <div class="row align-items-md-center">
                <div class="col-12 col-sm-6 col-md-5 col-lg-5 col-xl-4">
                    <div class="section-push__content">
                        <div class="section__subtitle">01</div>
                        <p class="section__title"><?= $servicesItem['service_desc_block_title'] ?></p>
                        <?= $servicesItem['service_desc_block_decs'] ?>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-7 col-lg-7 col-xl-8">
                    <div class="section-push__item">
                        <?php if (!empty($servicesItem['service_desc_block_img'])) { ?>
                        <img src="/files/services/<?= $servicesItem['service_desc_block_img'] ?>" alt="">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <div class="container">
        <!-- icons -->
        <?php if (isset($advantages) && !empty($advantages)) {?>
        <div class="icon-block">
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$advantages['advantages_img_1']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$advantages['advantages_title_1']?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$advantages['advantages_img_2']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$advantages['advantages_title_2']?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$advantages['advantages_img_3']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$advantages['advantages_title_3']?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$advantages['advantages_img_4']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$advantages['advantages_title_4']?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- icons end -->

        <!-- video -->
        <?php if (!empty($servicesItem['video'])) { ?>
        <div class="section-video">
            <div class="embed-responsive embed-responsive-16by9">
                <?php if (isset($servicesItem['video'])) {?>
                    <?= $servicesItem['video'] ?>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <!-- video end -->
    </div>

    <!-- collapse section -->
    <?php if (!empty($servicesItem['attachment_articles'])) { ?>
    <div class="section section-gray withImg service-page__competencies">
        <img class="sectionImg" src="/img/badge.png" alt="">

        <div class="container">
            <div class="section__subtitle">02</div>
            <div class="section__title">Наши компетенции</div>

            <!-- collapse -->
            <div class="collapse-list collapse-list__service">

                <p class="collapse-title">Наименование</p>
                <?php foreach ($servicesItem['attachment_articles'] as $h=>$service_article) { ?>
                <!-- collapse item -->
                <div class="collapse-item">
                    <a href="#competencies-<?= $h ?>" class="collapse-item__link collapsed" data-toggle="collapse"  aria-expanded="false">
                        <?=$service_article['caption']?>
                        <span class="ic">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 11.3 11.3" xml:space="preserve"><path d="M10.3,4.7H6.7V1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1v3.7H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h3.7v3.7c0,0.6,0.4,1,1,1 c0.6,0,1-0.4,1-1V6.7h3.7c0.6,0,1-0.4,1-1S10.9,4.7,10.3,4.7z"/></svg>
						</span>
                    </a>

                    <div id="competencies-<?= $h ?>" class="collapse-item__collapse collapse">
                        <div class="collapse-item__content">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="wysiwyg">
                                        <?=$service_article['text']?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- collapse item end -->
                <?php } ?>

            </div>
            <button class="btn btn-default btn-link btn-details btn-collapse-showall" type="button" data-open="Показать все" data-close="Cкрыть все">
                <span class="btn-title">Показать все</span>
                <span class="ic"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 11.3 11.3" xml:space="preserve"><path d="M10.3,4.7H6.7V1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1v3.7H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h3.7v3.7c0,0.6,0.4,1,1,1 c0.6,0,1-0.4,1-1V6.7h3.7c0.6,0,1-0.4,1-1S10.9,4.7,10.3,4.7z"/></svg></span>
            </button>
        </div>
    </div>
    <?php } ?>
    <!-- collapse section end -->


    <!-- objects slider -->
    <?php if (!empty($experiences)) { ?>
    <div class="objects-slider">
        <div class="container">
            <div class="section__subtitle">03</div>
            <div class="section__title">Объекты в управлении</div>

            <div class="objects-slider__wrap">
                <?php foreach ($experiences as $experience) { ?>
                <!-- item -->
                <div class="objects-slider__item">
                    <div class="objects-slider__img">
                        <?php if ($experience['status_case']) { ?>
                        <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "view", $experience['slug']])?>"><img src="/files/experiences/<?=$experience['thumbnail_560x320']?>" alt=""></a>
                        <?php } else { ?>
                            <img src="/files/experiences/<?=$experience['thumbnail_560x320']?>" alt="">
                        <?php } ?>
                    </div>
                    <div class="objects-slider__footer">
                        <div class="title"><?=$experience['caption']?></div>
                        <div class="description">Общая площадь — <?=$experience['square']?> м<sup>2</sup></div>
                    </div>
                </div>
                <!-- item end -->
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <?php if (count($experiences) > 3) {?>
                    <div class="controls">
                        <div class="prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"/></svg></div>
                        <div class="pag"></div>
                        <div class="next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></div>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-12 col-md-6 text-left text-md-right">
                    <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "index"]) ?>" class="btn btn-primary btn-with-icon">
                        Все объекты в управлении
                        <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- objects slider end -->
</div>

<div id="contact_us" class="page-content__bottom page-content__section withBg" style="background-image: url(/img/service_bg.png);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-8 col-xl-9">
                <div class="page-content__title">Ваш дом нуждается в управлении?</div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                <a href="#service-page__form" data-toggle="collapse" class="btn btn-primary btn-block btn-lg">Свяжитесь с нами!</a>
            </div>
        </div>
    </div>
</div>

<?= $this->Form->create(['schema' => []], ['id' => 'contacts']); ?>
<div id="service-page__form" class="service-page__form collapse">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Ваше имя <sup>*</sup></label>
                    <input type="text" name="name" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Электронная почта <sup>*</sup></label>
                    <input type="text" name="email" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Телефон</label>
                    <input type="text" name="phone" class="form-control">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="label">Текст сообщения <sup>*</sup></label>
                    <textarea rows="2" name="text" class="form-control"></textarea>
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="privacy-policy">
                        <span class="check"></span>
                        Я соглашаюсь с <a href="/privacy-policy">Политикой конфиденциальности</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 text-left text-md-right">
                <button class="btn btn-primary btn-with-icon">Отправить <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></button>
            </div>
        </div>
        <div class="success success-msg">
            <div class="container">
                <div class="success-msg__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><polygon points="37.1,44.6 34.2,47.3 47.8,61.9 79.7,29.9 76.9,27.1 47.8,56.2 "/><path d="M18.6,50c0,17.4,14.1,31.4,31.4,31.4S81.4,67.4,81.4,50h-4c0,15.1-12.3,27.4-27.4,27.4S22.6,65.1,22.6,50S34.9,22.6,50,22.6  v-4C32.7,18.6,18.6,32.7,18.6,50z"/></svg>
                </div>
                <div class="success-msg__title">Ваше сообщение отправлено!</div>
            </div>
        </div>
<!--        <h4 class="success" style="color: green;float: right; display: none ">Ваше сообщение отправлено!</h4>-->
    </div>
</div>
<?= $this->Form->end(); ?>
