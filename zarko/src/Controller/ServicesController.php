<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use \Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ServicesController extends AppController
{
    public function index($rubric_id = null, $query = array(), $merge_query = true) {
        $page = TableRegistry::get('Pages')->find('first', ['conditions' => ['id' => 4], 'contain' => ['AttachmentImages']]);
        $services = TableRegistry::get('Services')->find();
        $this->Title->setMeta($page);
        $this->set(compact('page', 'services'));
    }

    public function view($id, $query = [], $merge_query = true)
    {
        $servicesItem= $this->Services->findBySlug($id)->contain('AttachmentArticles')->first();
        $advantages = TableRegistry::get('Abouts')->find('first', ['fields' =>
                                                                    ['advantages_title_1', 'advantages_img_1',
                                                                        'advantages_title_2', 'advantages_img_2',
                                                                        'advantages_title_3', 'advantages_img_3',
                                                                        'advantages_title_4', 'advantages_img_4',]]);
        $services_icon = TableRegistry::get('Services')->find('all', ['fields' => ['icon', 'slug']]);
        $experiences = TableRegistry::get('Experiences')->find('all', ['fields' => ['id', 'caption', 'square', 'image', 'thumbnail_560x320', 'slug', 'status_case']])->matching('Services', function ($q) use ($id) {
            return $q->where(['Services.slug =' => $id]);
        })->toArray();
        $this->Title->setMeta($servicesItem);
        $this->set(compact('servicesItem', 'advantages', 'services_icon', 'experiences'));
    }


}
