<div class="page-content projects-page">
    <div class="container">
        <p class="page-title">Наш опыт</p>

        <div class="row">
            <div class="col-12">
                <div class="projects-page__title">Список проектов</div>
                <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "index"]) ?>" class="btn <?php if (isset($allObjects)) { echo 'btn-dark'; $obj = $allObjects;} else {echo 'btn-light'; $obj = $objectsCases;}?> ">Все проекты</a>
                <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "index", 'cases']) ?>" class="btn <?php if (!isset($allObjects)) { echo 'btn-dark';} else {echo 'btn-light';}?>">Только с кейсами</a>
                <div class="projects-page__type">
                    <form method="get" id="id_town">
                    <p class="label-control">Тип объекта</p>
                    <select name="town" class="select"  onchange="this.form.submit()">
                        <option value="" selected>Все объекты</option>
                        <?php foreach ($arr_type  as $type) {?>
                        <option value="<?=$type?>" <?php if (isset($_GET['town']) && $type==$_GET['town']) echo 'selected';?> ><?=$type?></option>
                        <?php } ?>
                    </select>
                        <input type="submit" style="display: none">
                    </form>
                </div>
            </div>

        </div>
        <?php if (!empty($allObjects)) { ?>
        <!-- projects list -->
        <div class="projects-page__list">

            <!-- collapse -->
            <div class="collapse-list">

                <p class="collapse-title">Наименование</p>

                <!-- collapse item -->
                <?php foreach ($allObjects as $k=>$allObject) { ?>
                <div class="collapse-item">
                    <a href="#project-<?=$k?>" class="collapse-item__link collapsed" data-toggle="collapse"  aria-expanded="false">
                        <?= $allObject['caption'] ?>
                        <span class="ic">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 11.3 11.3" xml:space="preserve"><path d="M10.3,4.7H6.7V1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1v3.7H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h3.7v3.7c0,0.6,0.4,1,1,1 c0.6,0,1-0.4,1-1V6.7h3.7c0.6,0,1-0.4,1-1S10.9,4.7,10.3,4.7z"/></svg>
								</span>
                    </a>

                    <div id="project-<?=$k?>" class="collapse-item__collapse collapse">
                        <div class="collapse-item__content">
                            <div class="row align-items-center">
                                <!-- text -->
                                <div class="col-12 col-md-6">
                                    <div class="projects-page__content">

                                        <div class="projects-page__field">
                                            <?php if (!empty($allObject['address'])) { ?>
                                            <div class="row">
                                                <div class="col-5 col-md-6 col-lg-4 col-xl-3 projects-page__field-title">Адрес</div>
                                                <div class="col-7 col-md-6 col-lg-8 col-xl-9 projects-page__field-description"><?= $allObject['address'] ?> </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="projects-page__field">
                                            <?php if (!empty($allObject['type']['caption'])) { ?>
                                            <div class="row">
                                                <div class="col-5 col-md-6 col-lg-4 col-xl-3 projects-page__field-title">Тип объекта</div>
                                                <div class="col-7 col-md-6 col-lg-8 col-xl-9 projects-page__field-description"><?= $allObject['type']['caption'] ?></div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="projects-page__field">
                                            <?php if (!empty($allObject['square'])) { ?>
                                            <div class="row">
                                                <div class="col-5 col-md-6 col-lg-4 col-xl-3 projects-page__field-title">Площадь</div>
                                                <div class="col-7 col-md-6 col-lg-8 col-xl-9 projects-page__field-description"><?= $allObject['square'] ?> м<sup>2</sup></div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="projects-page__field">
                                            <?php if (!empty($allObject['year'])) { ?>
                                            <div class="row">
                                                <div class="col-5 col-md-6 col-lg-4 col-xl-3 projects-page__field-title">Год</div>
                                                <div class="col-7 col-md-6 col-lg-8 col-xl-9 projects-page__field-description"><?= $allObject['year'] ?></div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <?php if (!empty($allObject['services'])) { ?>
                                        <div class="projects-page__field">
                                            <div class="row align-items-center">
                                                <div class="col-5 col-md-6 col-lg-4 col-xl-3 projects-page__field-title">Виды работ</div>
                                                <div class="col-7 col-md-6 col-lg-8 col-xl-9 projects-page__field-description">
                                                    <?php foreach($allObject->services as $service) { ?>
                                                        <div class="ic tooltip" data-tooltip="<?= $service['caption'] ?>">
                                                            <?php if (isset($service['icon'])){ ?>
                                                                <?php $icon = @file_get_contents('./'.$service['icon-uploaded']['url']); if (!empty($icon)) echo $icon;?>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if ($allObject['status_case']){ ?>
                                        <div class="projects-page__field">
                                            <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "view", $allObject['slug']]) ?>" class="btn btn-default btn-with-icon">Посмотреть кейс <span class="ic"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></a>
                                        </div>
                                        <?php } ?>


                                    </div>
                                </div>
                                <!-- text end -->

                                <!-- img -->
                                <div class="col-12 col-md-6 order-md-first">
                                    <div class="projects-page__img">

                                            <img src="/files/experiences/<?= $allObject['thumbnail_560x320'] ?>" alt="">

                                    </div>
                                </div>
                                <!-- img end -->
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- collapse item end -->
            </div>
            <!-- collapse end -->

            <!-- pagination -->
            <?= $this->element('pagination') ?>
            <!-- pagination end -->

        </div>
        <!-- projects list end -->
        <?php } ?>

        <?php if (!empty($objectsCases)) { ?>
        <!-- projects list -->
        <div class="projects-page__list">
            <?php foreach ($objectsCases as $l=>$objectItem) {?>
                <!-- item -->
                <div class="projects-page__item">
                    <div class="row align-items-md-center">
                        <!-- text -->
                        <div class="col-12 col-sm-6">
                            <div class="projects-page__content">
                                <p class="title"><?= $objectItem['caption'] ?></p>
                                <p class="description"><?= $objectItem['brief_case'] ?></p>
                                <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "view", $objectItem['slug']]) ?>" class="details">Посмотреть кейс</a>
                            </div>
                        </div>
                        <!-- text end -->

                        <!-- img -->
                        <div class="col-12 col-sm-6 <?php if($l%2 == 0) echo 'order-sm-first';?>">
                            <div class="projects-page__img">
                                <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "view", $objectItem['slug']]) ?>">
                                    <img src="/files/experiences/<?=$objectItem['thumbnail_560x320'];?>" alt="">
                                    <span class="ic"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span>
                                </a>
                            </div>
                        </div>
                        <!-- img end -->
                    </div>
                </div>
                <!-- item end -->
            <?php } ?>

            <!-- pagination -->
            <?= $this->element('pagination') ?>
            <!-- pagination end -->

        </div>
        <!-- projects list end -->
        <?php } ?>

    </div>
</div>

<div class="page-content__bottom page-content__section withBg" id="contact_us" style="background-image: url(/img/projects_bg.png);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-8 col-xl-9">
                <div class="page-content__title">У вас есть интересный проект?</div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                <a href="#service-page__form" data-toggle="collapse" class="btn btn-primary btn-block btn-lg">Свяжитесь с нами!</a>
            </div>
        </div>
    </div>
</div>

<?= $this->Form->create(['schema' => []], ['id' => 'contacts']); ?>
<div class="service-page__form collapse" id="service-page__form">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Ваше имя <sup>*</sup></label>
                    <input type="text" name="name" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Электронная почта <sup>*</sup></label>
                    <input type="text" name="email" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Телефон</label>
                    <input type="text" name="phone" class="form-control">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="label">Текст сообщения <sup>*</sup></label>
                    <textarea rows="2" name="text" class="form-control"></textarea>
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="privacy-policy">
                        <span class="check"></span>
                        Я соглашаюсь с <a href="/privacy-policy">Политикой конфиденциальности</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 text-left text-md-right">
                <button class="btn btn-primary btn-with-icon">Отправить <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></button>
            </div>
        </div>
        <div class="success success-msg">
            <div class="container">
                <div class="success-msg__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><polygon points="37.1,44.6 34.2,47.3 47.8,61.9 79.7,29.9 76.9,27.1 47.8,56.2 "/><path d="M18.6,50c0,17.4,14.1,31.4,31.4,31.4S81.4,67.4,81.4,50h-4c0,15.1-12.3,27.4-27.4,27.4S22.6,65.1,22.6,50S34.9,22.6,50,22.6  v-4C32.7,18.6,18.6,32.7,18.6,50z"/></svg>
                </div>
                <div class="success-msg__title">Ваше сообщение отправлено!</div>
            </div>
        </div>
<!--        <h4 class="success" style="color: green;float: right; display: none ">Ваше сообщение отправлено!</h4>-->
    </div>
</div>
<?= $this->Form->end(); ?>
