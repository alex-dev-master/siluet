<?php
/**
 * settings block
 */
if (isset($settings)) {
    foreach (['code_bh', 'code_ab', 'code_bb'] as $prop) {
        $this->start($prop);
        echo $settings->get($prop);
        $this->end();
    }
}
$this->Html->css('slick/slick-theme', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/jquery-ui', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/daterangepicker', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/jquery.timepicker.min', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('slick/slick', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/owl.theme.default', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/owl.carousel', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('fancybox/jquery.fancybox', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/animate', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/selectric', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('style', ['type' => 'text/css', 'media' => 'all', 'block' => true]);

$this->append('js', $this->Html->script('jquery-3.2.1.min'));
$this->append('js', $this->Html->script('owl.carousel'));
$this->append('js', $this->Html->script('owl/owl.support'));
$this->append('js', $this->Html->script('owl/owl.autoplay'));
$this->append('js', $this->Html->script('autoplay'));
$this->append('js', $this->Html->script('bootstrap.min'));
$this->append('js', $this->Html->script('jquery.maskedinput'));
//$this->append('js', '/css/fancybox/jquery.fancybox');
$this->append('js', $this->Html->script('jquery.scrollbar'));
$this->append('js', $this->Html->script('jquery.spincrement.min'));
$this->append('js', $this->Html->script('detect.min'));
$this->append('js', $this->Html->script('viewportchecker'));
$this->append('js', $this->Html->script('jquery.selectric'));
$this->append('js', $this->Html->script('slick.min'));
$this->append('js', $this->Html->script('truncate.min'));
$this->append('js', $this->Html->script('jquery-ready'));
$this->append('js', $this->Html->script('jquery.query-object'));
$this->append('js', $this->Html->script('history/scripts/bundled/html5/jquery.history'));
$this->append('js', $this->Html->script('common'));

?>

<!doctype html>
<html lang="ru">
<head>
    <title><?= $title_for_layout; ?></title>
    <?php if (isset($description_for_layout) and !empty($description_for_layout)) {
        echo $this->Html->meta('description', $description_for_layout);
    } ?>
    <?php if (isset($keywords_for_layout) and !empty($keywords_for_layout)) {
        echo $this->Html->meta('keywords', $keywords_for_layout);
    } ?>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($href = $settings->get('favicon-uploaded')) { ?>
        <?= $this->Html->meta('icon', $href['url']) ?>
    <?php } ?>
    <?= $this->fetch('css') ?>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <?= $this->fetch('code_bh') ?>
    <!--[if IE]>
<style>
    #owl-demo .item video{
         height: auto;
    }
</style>
<![endif]-->
</head>
<body>
<?= $this->fetch('code_ab') ?>
<?= $this->element('header', [$langs,$settings])?>
<?= $this->fetch('content') ?>
<?= $this->element('footer')?>
<?= $this->fetch('js') ?>
<script type="text/javascript" src="/css/fancybox/jquery.fancybox.js"></script>
<script>
    $('.owl-carousel').owlCarousel({
        // animateOut: 'slideOutDown',
        // animateIn: 'flipInX',
        items: 1,
        touchDrag: false,
        pullDrag: false,
        nav: true,
        navSpeed: 3000,
        animateOut: 'My-zoomOutDown',
        animateIn: 'My-zoomInDown',
        // transitionStyle: "fade",
        smartSpeed: 4150
    });
</script>
<?= $this->fetch('code_bb') ?>
</body>
</html>
