<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AboutsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('abouts')
            ->setDisplayField('caption')
            ->setPrimaryKey('id');

        $this
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 1160, 'y' => 610]],
                'image_text_1' => ['label' => 'Картинка 1', 'params' => ['resize' => 'crop', 'x' => 560, 'y' => 380], 'tab' => 'text_block', 'type' => 'image'],
                'text_block_1' => ['label' => 'Текст', 'tab' => 'text_block', 'type' => 'wysiwyg'],
                'image_text_2' => ['label' => 'Картинка 2','params' => ['resize' => 'crop', 'x' => 560, 'y' => 380], 'tab' => 'text_block', 'type' => 'image'],
                'text_block_2' => ['label' => 'Текст', 'tab' => 'text_block', 'type' => 'wysiwyg'],
                'image_text_3' => ['label' => 'Картинка 3','params' => ['resize' => 'crop', 'x' => 560, 'y' => 380], 'tab' => 'text_block', 'type' => 'image'],
                'text_block_3' => ['label' => 'Текст', 'tab' => 'text_block', 'type' => 'wysiwyg'],
            ])
            ->setTabSchema([
                'text_block' => ['label' => 'Текстовый блок'],
                'attachment_executives' => ['label' => 'Руководство'],
            ]);

        $this->hasMany('AttachmentImages', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Abouts']]);
        $this->hasMany('AttachmentExecutives', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentExecutives.model' => 'Abouts']]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'text_block_1', 'text_block_2', 'text_block_3', 'meta_title', 'meta_description', 'meta_keywords']
        ]);

    }
}
