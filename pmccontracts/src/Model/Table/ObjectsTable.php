<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ObjectsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('objects')
            ->setDisplayField('caption')
            ->setDisplayImage('image')
            ->setPrimaryKey('id')
            ->setOrder([
                'Objects.position' => 'ASC',
                'Objects.id' => 'ASC'
            ])
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 370, 'y' => 230]],
                'completed' => ['label' => 'Завершенных проектов'],
                'archive_status' => ['label' => 'Добавить в архив'],
                'customer' => ['label' => 'Заказчик'],
                'implement_period' => ['label' => 'Срок реализации'],
                'place' => ['label' => 'Месторасположение'],
                'investments' => ['label' => 'Инвестирование'],
                'tasks' => ['label' => 'Задачи', 'type' => 'wysiwyg'],
                'specifications' => ['label' => 'Характеристики проекта', 'type' => 'wysiwyg'],
            ])
            ->addBehavior('Translate', ['fields' =>
                ['caption', 'customer', 'implement_period', 'place', 'investments', 'tasks', 'specifications', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
        $this->hasMany('AttachmentImages', [
            'dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Objects']
        ]);
        $this->belongsToMany('Services');
    }

}
