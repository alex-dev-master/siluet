<div class="page-top">
    <div class="wrapper">
        <h1 class="h1-big"><?=__('О компании');?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                <div class="content__left-col">
                    <ul class="left-menu">
                <?= $this->cell("Menu::sidebar", [$langs, $settings]) ?>
                    </ul>
                    <div class="slogan"><p><?=__('Надежный партнер');?></p></div>
                </div>
            </div>
            <div class="col-md-8">
             <h2><?=$about['caption']?></h2>
                <div class="text">
                    <?=$about['content']?>
                </div>
            </div>
        </div>
    </div>
</div>
