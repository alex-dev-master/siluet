<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $caption
 * @property string $content
 * @property bool $disabled
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modiified
 */
class Service extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'slug' => true,
        'icon' => true,
        'short_description' => true,
        'caption_full' => true,
        'image' => true,
        'explanation' => true,
        'num_showing_1' => true,
        'text_showing_1' => true,
        'num_showing_2' => true,
        'text_showing_2' => true,
        'num_showing_3' => true,
        'text_showing_3' => true,
        'num_showing_4' => true,
        'text_showing_4' => true,
        'service_desc_block_title' => true,
        'service_desc_block_decs' => true,
        'service_desc_block_img' => true,
        'video' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'disabled' => true,
        'position' => true,
        'created' => true,
        'modified' => true,
        'articles' => true,
        'peoples' => true,
    ];
}
