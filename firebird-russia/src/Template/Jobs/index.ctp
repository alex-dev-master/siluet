<div class="page-top page-top__jobs">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$page['caption']?></h1>
        <div class="styled-text text">
            <?=$page['content']?>
        </div>
    </div>
</div>
<div class="jobs-list">
    <div class="wrapper">
        <div class="row">
            <?php foreach ($jobs as $job) { ?>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="parent">
                    <div class="text">
                        <h4><?=$job['caption']?></h4>
                        <?=$job['preview']?>
                        <a href="<?= $this->Url->build(["controller" => "Jobs","action" => "view", $job['slug']]) ?>"><?=__('Подробнее')?> <img src="/img/arr-w.png"></a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

</div>

<!--<div class="pagination">-->
<!--    <div class="wrapper">-->
<!--        <a href="#" class="prev-page"><img src="/img/jumb-arr-l.png"> </a>-->
<!--        <ul class="pages">-->
<!--            <li><a href="#">1</a></li>-->
<!--            <li><a href="#">2</a></li>-->
<!--            <li><a href="#">3</a></li>-->
<!--            <li class="active"><a href="#">4</a></li>-->
<!--            <li><a href="#">5</a></li>-->
<!--            <li><a href="#">6</a></li>-->
<!--            <li><a href="#">7</a></li>-->
<!--        </ul>-->
<!--        <a href="#" class="next-page"><img src="/img/jumb-arr-r.png"> </a>-->
<!--    </div>-->
<!--</div>-->

<?= $this->element('pagination2') ?>

<!-- Пагинация -->


<?=$this->element('makeOrder')?>
