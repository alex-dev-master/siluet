<?php
namespace App\Traits;

use Cake\Controller\Controller;
use SiluetCms\Controller\AppController;

trait ControllerTrait {
    use \SiluetCms\Traits\ControllerTrait;
}