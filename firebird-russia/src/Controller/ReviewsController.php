<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ReviewsController extends AppController
{
    public $paginate;

    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $this->paginate = [
            'limit' => 3,
        ];

        $meta = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id' => 10]) ->first();
        $this->Title->setMeta($meta, 'Reviews');

//        $reviews = $this->paginate(TableRegistry::getTableLocator()->get('Reviews')->find()->where(['Reviews.image !=' => ''])->order(['Reviews.published' => 'DESC']))->toArray();
        $reviews = $this->paginate(TableRegistry::getTableLocator()->get('Reviews')->find()->order(['Reviews.published' => 'DESC']))->toArray();
        $this->set(compact('reviews'));

    }




}
