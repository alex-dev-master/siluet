<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $caption
 * @property string $map_caption
 * @property float $map_n
 * @property float $map_e
 * @property string $feedback_to
 * @property string $feedback_from
 * @property string $feedback_subject
 */
class Delivery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'meta_title' => true,
        'content_delivery' => true,
        'content_payment' => true,
        'meta_description' => true,
        'meta_keywords' => true,
    ];
}
