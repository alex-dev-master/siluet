<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\ORM\TableRegistry;
$settings1 = TableRegistry::getTableLocator()->get('Settings')->find()->first();
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?php
    $this->Html->css('slick/slick-theme', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
    $this->Html->css('foundation/jquery-ui', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
    $this->Html->css('foundation/daterangepicker', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
    $this->Html->css('slick/slick', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
    $this->Html->css('foundation/selectric', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
    $this->Html->css('fancybox/jquery.fancybox', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
    $this->Html->css('style', ['type' => 'text/css', 'media' => 'all', 'block' => true]);

    $this->append('js', $this->Html->script('jquery-3.2.1.min'));
    $this->append('js', $this->Html->script('jquery-ui'));
    $this->append('js', $this->Html->script('jquery.justified.min'));
    $this->append('js', $this->Html->script('bootstrap.min'));
    $this->append('js', $this->Html->script('jquery.maskedinput'));
    $this->append('js', $this->Html->script('jquery.scrollbar'));
    $this->append('js', $this->Html->script('jquery.justifiedGallery'));
    $this->append('js', $this->Html->script('justifiedGallery'));
    $this->append('js', $this->Html->script('moment.min'));
    $this->append('js', $this->Html->script('masonry.pkgd.min'));
    $this->append('js', $this->Html->script('truncate.min'));
    $this->append('js', $this->Html->script('jquery.selectric'));
    $this->append('js', $this->Html->script('slick.min'));
    $this->append('js', $this->Html->script('jquery.timepicker.min'));
    $this->append('js', $this->Html->script('daterangepicker'));
    $this->append('js', $this->Html->script('jquery-ready'));
    $this->append('js', $this->Html->script('common'));
    ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
<div class="nobg">
    <?= $this->element('header', ['settings' => $settings1])?>
    <?= $this->fetch('content') ?>
    <?= $this->element('footer', ['settings' => $settings1])?>
    <?= $this->fetch('js') ?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="/js/main-map.js"></script>
    <script type="text/javascript" src="/css/fancybox/jquery.fancybox.js"></script>

</div>
</body>
</html>
