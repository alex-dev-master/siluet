<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OptimizationsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('optimizations')
            ->setPrimaryKey('id')
            ->setFormSchema([
                'meta_title' => ['label' => 'Тег title (заголовок)', 'tab' => 'general'],
                'meta_description' => ['label' => 'Мета-тег description (описание)', 'tab' => 'general'],
                'meta_keywords' => ['label' => 'Мета-тег keywords (ключевые слова)', 'tab' => 'general'],
            ])
            ->setTabSchema([
                'general' => array('label' => 'SEO'),
            ])
            ->addBehavior('Translate', ['fields' =>
                ['meta_title', 'meta_description', 'meta_keywords']
            ]);
    }


}
