<?php
namespace App\Controller;

use App\Controller\AppController;

class ItemRubricsController extends AppController {
    public function index($rubric_id = null, $query = array(), $merge_query = true) {
        parent::index($rubric_id, $query, $merge_query);
        $this->loadModel('Pages');
        $this->set('page', $page = $this->Pages->find('byid', ['value' => 3]));
        $this->Title->setMeta($page, 'Pages');
    }
}
