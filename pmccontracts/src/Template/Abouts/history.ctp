<div class="page-top">
    <div class="wrapper">
        <p class="subheading"><?=__('О компании');?></p>
        <h1><?=__('История компании');?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                <div class="content__left-col">
                    <ul class="left-menu">
                <?= $this->cell("Menu::sidebar", [$langs, $settings]) ?>
                    </ul>
                    <div class="slogan"><p><?=__('Нам доверяют');?></p></div>
                </div>
            </div>
            <div class="col-md-8">
             <h2><?=$about['caption']?></h2>
                <div class="text">
                   
                    <?=$about['content']?>
                </div>
            </div>
        </div>
    </div>
</div>
