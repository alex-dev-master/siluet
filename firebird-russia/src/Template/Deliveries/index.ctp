<div class="delivery-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$caption_for_layout?></h1>
        <ul>
            <?php foreach ($deliveries as $k=>$delivery) { ?>
            <li><button class="delivery-btn <?php if ($k==0) echo 'activeBtn';?> " onclick="currentSlide(<?=++$k?>)"><?=$delivery['caption']?></button></li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="delivery-container">
    <?php foreach ($deliveries as $k=>$delivery) { ?>
        <div class="delivery-main">

            <div class="wrapper">
             <img src="/files/deliveryitems/<?=$delivery['image']?>" class="deliv-img">
                <?=$delivery['content']?>
            </div>
        </div>
    <?php } ?>
</div>

<?=$this->element('makeOrder')?>
