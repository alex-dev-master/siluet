<div class="pull-left cnt">
	<?php /*if (isset($row[$model]['position']) and $num_rows > 1 and $level == 1) { ?>
	<div class="cell checkbox col-md-1">
		<input type="checkbox" name="select-items[]" value="<?php echo $row[$model][$primary_key]; ?>"<?php echo ((isset($elementsInCookie[$model]) and in_array($row[$model][$primary_key], $elementsInCookie[$model])) ? ' checked="checked"' : ''); ?> />
	</div>
	<?php }*/ ?>
	
	<div class="smart-form pull-left">
		
		<label class="checkbox left-cell-checkbox">
			<input type="checkbox" name="group-checkbox[]" id="lc-<?php echo $model; ?>-<?php echo $row[$primary_key]; ?>" value="<?php echo $row[$primary_key]; ?>"<?php if (isset($row['position'])) { ?> data-c="1"<?php } ?>>
			<i></i>
		</label>
		
	</div>
	<?php
	
	
	// cell_display_field (cell_children_index)
	
	// cell_edit
	$edit_url = $UrlCollection->append(['action' => 'edit', 'id' => $row[$primary_key]]);
	if (isset($ctrl)) {
		$edit_url = $UrlCollection->append(['controller' => $ctrl]);
	}
	?>
	<?php if (!empty($display_image)) {
        if (!empty($display_image) and isset($row[$display_image . '-uploaded'])) {
			$img = $row[$display_image . '-uploaded'];
            $image_info = getimagesize($img['filename']);
            $thumbnail = $img['filename'];
			$dir = dirname($thumbnail);
            if ($image_info[0] > 100 or $image_info[1] > 100) {
                $thumbnail = dirname($img['url']) . '/_thumbs/' . $img['filetime'] . '_' . basename($img['filename']);
                $image_info = getimagesize($img['filename']);
				if (!is_file($thumb = $dir . DS . '_thumbs' . DS . $img['filetime'] . '_' . basename($img['filename']))) {
					if ($image_info[0] > 100 and $image_info[1] > 100) {
						if (!is_dir($dir . DS . '_thumbs')) {
							mkdir($dir . DS . '_thumbs', 0755, true);
						}
                        $Image = new \SiluetCms\Utils\ImageResize();
                        $Image->thumb($img['filename'], $thumb);
					} else {
						$thumbnail = $img['url-time'];
					}
				}
            } else {
                $thumbnail = $img['url-time'];
            }
			echo $this->Html->image($thumbnail, ['alt' => '', 'class' => 'pull-left']);
		} 
	?>
	<div class="field_txt">
	<?php } else {
        if (isset($is_tree) && $is_tree) {
            $children_index_url = $UrlCollection->append(['controller' => $model, 'action' => 'index', 'id' => $row[$primary_key], 'autoPage' => FALSE]);
            echo $this->Html->link('<i class="fa fa-lg fa-folder"></i>', $children_index_url, ['title' => __d('admin', 'children_index_action'), 'escape' => FALSE, 'modal' => true]);
        } else {
            echo $this->Html->link('<i class="fa fa-lg fa-file' . ((isset($row['disabled']) and $row['disabled']) ? '' : '-o') . '"></i>', $edit_url, ['title' => __d('admin', 'children_index_action'), 'escape' => FALSE, 'modal' => true]);
        }
    }
	$pencil_action = $edit_url;
	if (isset($has_items, $item_model) && $has_items) {
		$edit_url = $UrlCollection->append(['controller' => $item_model, 'action' => 'index', $row[$primary_key]]);
	}
	if (isset($is_tree) && $is_tree) {
		$children_index_url = $UrlCollection->append(['action' => 'index', 'id' => $row[$primary_key]]);
		if (isset($has_items, $item_model) && $has_items and isset($row['children']) and empty($row['children'])) {
			$children_index_url = $edit_url;
		}
		echo $this->Html->link($row[$display_field], $children_index_url, ['title' => __d('admin', 'edit_action'), 'modal' => true]);
	} else {
		if (!isset($row[$display_field])) {
			$display_field = $primary_key;
		}
		echo $this->Html->link($row[$display_field], $edit_url, ['title' => __d('admin', 'edit_action'), 'modal' => true]);
	}
	echo $this->Html->link('<i class="fa fa-lg fa-pencil"></i>', $pencil_action, ['title' => __d('admin', 'edit_action'), 'escape' => FALSE, 'modal' => true]);
	?>
	<?php if (!empty($display_image)) { ?>
	</div>
	<?php } ?>

	
	<?php
	//echo $this->Html->link('<i class="fa fa-lg fa-pencil"></i>', $edit_url, array('title' => __d('admin', 'edit_action'), 'escape' => FALSE));
	
	// cell_items_index
	/**
	 * 
	 */
	/*if (isset($has_items, $item_model) && $has_items) {
		if (isset($row[0]['num_items']) && ((int)$row[0]['num_items'] === 0)) {
			$items_index_icon = 'arrow_right_red';
		} else {
			$items_index_icon = 'fa-arrow-circle-right';
		}
		$items_index_url = array('controller' => Inflector::tableize($item_model), 'action' => 'index', $row[$model][$primary_key], 'autoPage' => FALSE);
		echo $this->html->link('<i class="fa fa-lg ' . $items_index_icon . '"></i>', $items_index_url, array('class' => 'arr', 'title' => __d('admin', 'items_index_action'), 'escape' => FALSE));
		if (isset($row[0]['num_items'])) {
			echo '<div class="cell cell_num_items">' . $row[0]['num_items'] . '</div>';
		}
	}*/
	/**
	 * 
	 */
	?>
</div>
