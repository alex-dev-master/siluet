<div class="page-top">
    <div class="wrapper">
        <p class="subheading"><?=__('О компании');?></p>
        <h1><?=__('Отзывы');?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                <div class="content__left-col">
                    <ul class="left-menu">
                        <?= $this->cell("Menu::sidebar", [$langs, $settings]) ?>
                    </ul>
                    <div class="slogan"><p><?=__('Нам доверяют');?></p></div>
                </div>
            </div>
            <div class="col-md-8">
                    <h2><?=$about['caption']?></h2>
                    <?=$about['content']?>
                    <div class="testimonial-list">
                        <div class="row list">
                            <?php foreach ($reviews as $review) { ?>
                            <div class="item-col col-md-4 col-sm-4 col-ss-6 col-xs-12">
                                <div class="testimonial-list__item">
                                    <a href="/files/reviews/<?=$review['image']?>" class="fancybox testimonial-link">
                                        <div class="testimonial-list__img-container">
                                            <?php if (!empty($review['thumbnail_190x170'])) { ?>
                                            <img src="/files/reviews/<?=$review['image']?>">
                                            <?php } ?>
                                        </div>
                                        <?php if (!empty($review['image_logo'])) { ?>
                                        <img class="testimonial-brand" src="/files/reviews/<?=$review['image_logo']?>">
                                        <?php } ?>
                                    </a>
                                    <div class="testimonial-list__item-text">

                                        <p class="testimonial-list__item-title">
                                            <?=$review['caption']?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
