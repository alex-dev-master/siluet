
<div class="page-content contacts-content">
    <img class="content-bg" src="/img/contacts-bg.png" alt="">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1>Контакты</h1>
        <div class="main-info">
            <div class="row list">
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="inner-wrap">
                        <img class="icon" src="/img/cont-tel.png" alt="">
                        <div class="text-wrap">
                            <a class="title" href="tel:<?=$settings['phone']?>"><?=$settings['phone']?></a>
                            <p class="desc  sml-text">Ежедневно с 9.00 до 17.00</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="inner-wrap">
                        <img class="icon" src="/img/cont-place.png" alt="">
                        <div class="text-wrap">
                            <h4 class="title"><?=$settings['address']?></h4>
                            <p class="desc  sml-text"><?=$settings['city']?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-push-0 col-sm-6 col-sm-push-3 col-xs-12">
                    <div class="inner-wrap">
                        <img class="icon" src="/img/cont-mail.png" alt="">
                        <div class="text-wrap">
                            <a class="title" href="mailto:<?=$settings['email']?>"><?=$settings['email']?></a>
                            <p class="desc  sml-text">Запись и общие вопросы</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="map-timetable">
            <div class="row list">
                <div class="col-md-7 col-sm-6 col-xs-12 ">
                    <div class="contacts-map">
                        <div id="map"></div>
                    </div>
                </div>
                <div class="col-md-4 col-md-push-1 col-sm-6 col-xs-12 ">
                    <div class="inner-wrap">
                        <h4 class="title">График работы</h4>
                        <?=$row['schedule']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form">
        <input type="hidden" name="map_n" value="<?=$row['map_n']?>">
        <input type="hidden" name="map_e" value="<?=$row['map_e']?>">
        <?= $this->Form->create(['schema' => []], ['id' => 'sendContactForm', 'class' => 'form']); ?>
        <div class="wrapper">
            <h3 class="title">Напишите нам!</h3>
               <h4 id="success_msg" class="success" style="display: none;color: #44b9f0"><?=__('Форма успешно отправлена!')?></h4>
            <div class="row list">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <label for="name2">ФИО<span>*</span></label>
                    <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 ">
                    <label for="organization2">Телефон<span>*</span></label>
                    <input class="modal-input required" name="phone" id="organization2" placeholder="Ваш номер телефона">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>
                <div class="col-md-3 col-md-push-0 col-sm-6 col-sm-push-3 col-xs-12 ">
                    <label for="organization2">E-mail<span>*</span></label>
                    <input class="modal-input required" name="email" id="organization2" placeholder="Ваша электронная почта">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <label for="organization2">Текст сообщения</label>
                    <textarea class="modal-input" name="msg" id="organization2" placeholder="Поделитесь вашими мыслями"></textarea>
                </div>
            </div>
            <div class="row list">
                <div class="col-md-9 col-sm-9 col-xs-12 ">
                    <label class="checkbox">
                        <input type="checkbox" name="privacy-policy">
                        <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                                конфиденциальности</a></div>
                    </label>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 ">
                    <div class="btns">
                        <button data-tab="#success_msg" class="btn btn-form"><img src="/img/mail.png">Отправить</button>
                    </div>

                </div>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

