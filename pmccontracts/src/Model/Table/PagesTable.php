<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PagesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pages')
                ->setDisplayField('caption')
                ->setPrimaryKey('id')
                ->addBehavior('Timestamp')
            ->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
        $this->hasMany('AttachmentImages', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['model' => 'Pages']]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
//    public function validationDefault(Validator $validator)
//    {
//        $validator
//            ->integer('id')
//            ->allowEmpty('id', 'create');
//
//        $validator
//            ->scalar('slug')
//            ->maxLength('slug', 150)
//            ->allowEmpty('slug');
//
//        $validator
//            ->scalar('caption')
//            ->maxLength('caption', 255)
//                ->minLength('caption', 5, __d('errors', 'Мало символов'))
//            ->requirePresence('caption', 'create')
//            ->notEmpty('caption', __d('errors', 'admin_empty_field', [__d('admin', 'caption_field')]));
//
//        $validator
//            ->scalar('content')
//            ->requirePresence('content', 'create')
//            ->notEmpty('content', __d('errors', 'admin.empty_field', [__d('admin', 'content_field')]));
//
//        return $validator;
//    }
}
