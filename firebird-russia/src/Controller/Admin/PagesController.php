<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function edit($id = null) {
        parent::edit($id);
        if ($id == 5) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['attachment_images']);
            unset($tab_schema['image']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id == 6) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['attachment_images']);
            unset($tab_schema['image']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id == 1) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            $tab_schema['general']['field_schema']['content']['input']['label'] = 'О компании';
//            unset($tab_schema['image']);
            $tab_schema['image']['label'] = 'Изоображение о компании';
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id == 7) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['image']);
            unset($tab_schema['attachment_images']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id == 8) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['image']);
            unset($tab_schema['attachment_images']);
            unset($tab_schema['general']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id == 9) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['image']);
            unset($tab_schema['attachment_images']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id == 10) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['image']);
            unset($tab_schema['general']);
            unset($tab_schema['attachment_images']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }

        if ($id != 1) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['attachment_advantages']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }
    }
}
