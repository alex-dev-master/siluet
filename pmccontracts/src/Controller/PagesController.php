<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function mainpage() {
        $this->viewBuilder()->setLayout('default_main_page');
        $this->set([
            'sliders' => \Cake\ORM\TableRegistry::get('Sliders')->find()->toArray()
        ]);
//        parent::view(1);
    }

    public function abouts() {
        $this->Pages->addBehavior('Translate', ['fields' =>
            ['caption', 'content']
        ]);
        parent::view(2);
    }

    public function politics() {
        $politics = TableRegistry::getTableLocator()->get('Pages')->find()->where(['Pages.id'=>6])->first();
        $this->Title->setMeta($politics, 'Pages');
        $this->set(compact('politics'));
    }

}
