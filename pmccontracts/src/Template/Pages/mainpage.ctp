<?= $this->element('MainPage/header', [$lang])?>
<!--[if IE]>
<style>
    #owl-demo .item video{
         height: auto;
    }
</style>
<![endif]-->
<div id="qunit"></div>
<div id="qunit-fixture">

    <div id="owl-demo" class="owl-carousel owl-theme">
        <?php foreach ($sliders as $slider) { ?>
        <div class="item <?php if ($slider['map_slide']) echo 'map'; ?>">
            <?php
                if ($slider['status_video']) {
                    if (!empty($slider['video']) || !empty($slider['video_web'])){
                        ?>
                        <div class="trailer is_overlay">
                        <video id="video" width="100%" height="auto" autoplay="autoplay" muted="muted" loop="loop" preload="auto">
                            <?php if (!empty($slider['video_web'])) { ?>
                                <source src="/files/sliders/<?=$slider['video_web']?>" type="video/webm">
                            <?php }?>
                            <?php if (!empty($slider['video'])) { ?>
                                <source src="/files/sliders/<?=$slider['video']?>" type="video/mp4">
                            <?php } ?>
                        </video>
                        </div>
                        <?php
                    }
                } else {
            ?>
            <img src="/files/sliders/<?=$slider['image']?>">
            <?php } ?>
            <div class="wrapper">
                <div class="item-text">
                    <?=$slider['main_text']?>
                    <?=$slider['text']?>
                    <?php if (!empty($slider['url'])) { ?>
                    <a href="<?=$slider['url']?>" class="main__more-btn"><?= __('Узнать больше'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>

</div>
<?= $this->element('MainPage/footer')?>
