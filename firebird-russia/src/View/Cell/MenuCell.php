<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
/**
 * Menu cell
 */
class MenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    private $urls = [
        'portfolios' => ['controller' => 'Portfolios', 'action' => 'index'],
        'services' => ['controller' => 'Pages', 'action' => 'services'],
        'working' => ['controller' => 'Pages', 'action' => 'works'],
        'questions' => ['controller' => 'Pages', 'action' => 'questions'],
        'contacts' => ['controller' => 'Contacts', 'action' => 'index'],
    ];

    private $urls_top = [
      'abouts' => ['controller' => 'Abouts', 'action' => 'index'],
      'manufacture' => ['controller' => 'Manufactures', 'action' => 'index'],
      'delivery' => ['controller' => 'Deliveries', 'action' => 'index'],
      'shops' => ['controller' => 'Shops', 'action' => 'index'],
      'jobs' => ['controller' => 'Jobs', 'action' => 'index'],
    ];

    private $urls_footer_first = [
        'О нас' => ['controller' => 'Abouts', 'action' => 'index'],
        'Услуги' => ['controller' => 'Pages', 'action' => 'services'],
        'Производство' => ['controller' => 'Manufactures', 'action' => 'index'],
        'Вакансии' => ['controller' => 'Jobs', 'action' => 'index'],
        'Блог' => ['controller' => 'Blogs', 'action' => 'index'],
    ];

    private $urls_footer_second = [
        'Как мы работаем' => ['controller' => 'Pages', 'action' => 'works'],
        'Доставка и оплата' => ['controller' => 'Deliveries', 'action' => 'index'],
        'Вопросы и ответы' => ['controller' => 'Pages', 'action' => 'questions'],
        'Магазинам' => ['controller' => 'Shops', 'action' => 'index'],
        'Отзывы' => ['controller' => 'Reviews', 'action' => 'index'],
    ];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    private function addMenu(&$menus, $caption, $url = false, $submenu = [], $key = false)
    {
        $menu = ['caption' => $caption];

        if (!empty($url)) {
            $menu['url'] = $url;
            if (!empty($url['controller']) && $this->request->getParam('controller')  == $url['controller']) {
                if (!empty($url['action'])) {
                    if ($this->request->action == 'view' && $url['action'] == 'index') {
                        $menu['current'] = true;
                    } else {
                        $menu['current'] = ($this->request->getParam('action')  == $url['action']);
                    }
                }
                if ($menu['current'] && isset($url['id']) || isset($url['slug'])) {
                    $menu['current'] = ((isset($url['id']) && $this->request->id == $url['id']) || (isset($url['slug']) && $this->request->slug == $url['slug']));
                }
            }
        }

        if (!empty($submenu)) {
            $menu['submenu'] = $submenu;
            if (is_array($submenu) && empty($menu['current'])) {
                foreach ($submenu as $sub) {
                    if (!empty($sub['current'])) {
                        $menu['current'] = true;
                        break;
                    }
                }
            }
        }

        if ($key) {
            $menus[$key] = $menu;
        } else {
            array_push($menus, $menu);
        }
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($langs, $settings)
    {
        $menus = [];

        $this->addMenu($menus, __('Портфолио'), $this->urls['portfolios']);
        $this->addMenu($menus, __('Услуги'), $this->urls['services']);
        $this->addMenu($menus, __('Как мы работаем'), $this->urls['working']);
        $this->addMenu($menus, __('Вопросы и ответы'), $this->urls['questions']);
        $this->addMenu($menus, __('Контакты'), $this->urls['contacts']);

        $this->set(compact('menus', 'langs', 'settings'));
    }

    public function topmenu($langs, $settings)
    {
        $menus = [];

        $this->addMenu($menus, __('О нас'), $this->urls_top['abouts']);
        $this->addMenu($menus, __('Производство'), $this->urls_top['manufacture']);
        $this->addMenu($menus, __('Доставка и оплата'), $this->urls_top['delivery']);
        $this->addMenu($menus, __('Магазинам'), $this->urls_top['shops']);
        $this->addMenu($menus, __('Вакансии'), $this->urls_top['jobs']);

        $this->set(compact('menus', 'langs', 'settings'));
    }

    public function footer($langs, $settings)
    {
        $menus = [];
        $submenu = [];
        $submenu_second = [];
        $submenu_third = [];

        $categories = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->select(['caption', 'slug'])->toArray();

        foreach ($this->urls_footer_first as $k=>$url_sub) {
            $caption = $this->captionRaplace($k);
            $this->addMenu($submenu, $caption, $url_sub);
        }
        $this->addMenu($menus, __('Firebird'), [], $submenu);

        foreach ($this->urls_footer_second as $k=>$url_sub) {
            $caption = $this->captionRaplace($k);
            $this->addMenu($submenu_second, $caption, $url_sub);
        }
        $this->addMenu($menus, __('Помощь'), [], $submenu_second);



        foreach ($categories as $all_sub_service) {
            $all_sub_service->url = ['controller' => 'Portfolios', 'action' => $all_sub_service['slug']];
            $new_service_item[] = $all_sub_service;
        }
        foreach ($new_service_item as $url_sub) {
            $this->addMenu($submenu_third, $url_sub['caption'], $url_sub['url']);
        }
        $this->addMenu($menus, __('Портфолио'), $this->urls['services'], $submenu_third);

        $this->set(compact('menus', 'langs', 'settings'));
    }

    private function captionRaplace($caption) {
        $str = __($caption);
        return $str;
    }
}
