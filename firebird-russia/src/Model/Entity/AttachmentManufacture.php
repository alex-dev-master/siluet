<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AttachmentImage Entity
 *
 * @property int $id
 * @property int $foreign_key
 * @property string $model
 * @property string $caption
 * @property string $image
 * @property string $thumbnail
 * @property int $position
 * @property bool $disabled
 */
class AttachmentManufacture extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'foreign_key' => true,
        'model' => true,
        'caption' => true,
        'image' => true,
        'content' => true,
        'big_text_status' => true,
        'big_text_image' => true,
        'big_text_video' => true,
        'big_text' => true,
        'big_text_2' => true,
        'position' => true,
        'disabled' => true
    ];
}
