<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;

/**
 * Sliders Model
 *
 * @method \App\Model\Entity\Slider get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slider newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slider|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slider[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slider findOrCreate($search, callable $callback = null, $options = [])
 */
class SlidersTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
                ->setTable('sliders')
                ->setDisplayField('caption')
                ->setDisplayImage('image')
                ->setPrimaryKey('id')
                ->setFormSchema([
                    'image' => ['params' => ['resize' => 'crop', 'x' => 1600, 'y' => 900]],
                    'sub_caption' => ['label' => 'Подзаголовок'],
                    'content_details_button' => ['label' => 'Текст кнопки подробнее'],
                    'link_details_button' => ['label' => 'Ссылка кнопки подробнее'],
                    'project_title_first_block' => ['label' => 'Заголовок первого блока'],
                    'project_desc_first_block' => ['label' => 'Описание первого блока', 'type' => 'wysiwyg'],

                    'project_title_second_block' => ['label' => 'Заголовок второго блока'],
                    'project_desc_second_block' => ['label' => 'Описание второго блока', 'type' => 'wysiwyg'],

                    'project_title_third_block' => ['label' => 'Заголовок третьего блока'],
                    'project_desc_third_block' => ['label' => 'Описание третьего блока', 'type' => 'wysiwyg'],

                    'project_details_link' => ['label' => 'Детали проекта'],
                    #
//                    'status_icon_1' => ['label' => 'Проектирование', 'tab' => 'project_icons_services'],
//                    'image_icon_1' => ['label' => '', 'type' => 'file', 'tab' => 'project_icons_services'],
//                    'status_icon_2' => ['label' => 'Инженерные системы', 'tab' => 'project_icons_services'],
//                    'image_icon_2' => ['label' => '', 'type' => 'file', 'tab' => 'project_icons_services'],
//                    'status_icon_3' => ['label' => 'Отделочные работы', 'tab' => 'project_icons_services'],
//                    'image_icon_3' => ['label' => '', 'type' => 'file', 'tab' => 'project_icons_services'],
//                    'status_icon_4' => ['label' => 'Строительство ', 'tab' => 'project_icons_services'],
//                    'image_icon_4' => ['label' => '', 'type' => 'file', 'tab' => 'project_icons_services'],
//                    'status_icon_5' => ['label' => 'Эксплуатация ', 'tab' => 'project_icons_services'],
//                    'image_icon_5' => ['label' => '', 'type' => 'file', 'tab' => 'project_icons_services'],
//                    'status_icon_6' => ['label' => 'Управление  ', 'tab' => 'project_icons_services'],
//                    'image_icon_6' => ['label' => '', 'type' => 'file', 'tab' => 'project_icons_services'],
                    #
                    'full_image' => array('params' => 'resize=x&x=970', 'tab' => 'interactive', 'type' => 'image', 'label' => 'Изображение'),
                    'point_status' => array('tab' => 'interactive', 'label' => 'Отображать точку'),
                    'point_x' => array('tab' => 'interactive', 'label' => 'Пикселей по горизонтали'),
                    'point_y' => array('tab' => 'interactive', 'label' => 'Пикселей по вертикали'),
                ])
                ->setTabSchema([
                    'project_icons_services' => array('label' => 'Параметры', 'icon' => 'fa-archive'),
                    'interactive' => array('label' => 'Интерактивное изображение', 'icon' => 'fa-plus-circle')
                ])
                ->setOrder([
                    'Sliders.position' => 'DESC',
                    'Sliders.id' => 'DESC'
                ])
                ->addBehavior('Translate', ['fields' =>
                                                     ['caption', 'sub_caption', 'content_details_button', 'project_title_first_block', 'project_desc_first_block', 'project_title_second_block', 'project_desc_second_block', 'project_title_third_block', 'project_desc_third_block', 'project_details_link']
                                                ]);

                $this->belongsToMany('Services');


    }
}
