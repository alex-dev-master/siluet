<div class="header-btm">
    <div class="wrapper">
        <a class="logo" href="<?= $this->Url->build(['controller' => 'pages', 'action' => 'mainpage']) ?>"><img src="/img/logo.png"></a>
        <div class="mobile-menu">
            <ul class="header-menu">
                <!--<li class="drop-menu">-->
                <?php foreach ($menus as $menu) { ?>
                <li <?php if (!empty($menu['current'])) echo 'class="active-link"'?>><a href="<?= $this->Url->build($menu['url']) ?>"><?=$menu['caption']?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="right-block">
            <ul class="header-right-btns">
                <li>
                    <a class="header-btn" href="#call-modal" data-toggle="modal"><?=__('Заказать звонок')?></a>
                </li>
                <li class="header-phone-li">
                    <a class="header-phone" href="tel:<?=$settings['phone']?>"><?=$settings['phone']?></a>
                </li>
               <!--  <li class="header-email-li">
                    <a class="header-email" href="mailto:<?=$settings['email_top']?>"><?=$settings['email_top']?></a>
                </li> -->
                <!--<li>
                    <?php if (!empty($langs)): ?>
                        <?php foreach ($langs as $lang): ?>
                            <?php if (!$lang['selected']) {?>
                                <a class="header-lang" href="<?= $this->Url->build($lang['url']) ?>"><?= $lang['title'] ?></a>
                            <?php } ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </li> -->
            </ul>
        </div>
        <div class="mobile-toggle">
            <div class="btn_mob">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</div>
