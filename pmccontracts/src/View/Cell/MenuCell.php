<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
/**
 * Menu cell
 */
class MenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    private $urls = [
        'about' => ['controller' => 'Abouts', 'action' => 'index'],
        'services' => ['controller' => 'Services', 'action' => 'index'],
        'projects' => ['controller' => 'Projects', 'action' => 'index'],
        'news' => ['controller' => 'News', 'action' => 'index'],
        'contacts' => ['controller' => 'Contacts', 'action' => 'index'],
    ];
    private $urls_sub = [
        'about_about' =>
            [
                'caption' => 'О нас',
                'url' => ['controller' => 'Abouts', 'action' => 'index']
            ],
        'about_history' =>
            [
            'caption' => 'История компании',
            'url' => ['controller' => 'Abouts', 'action' => 'history']
            ],
        'about_team' =>
            [
                'caption' => 'Руководство компании',
                'url' => ['controller' => 'Abouts', 'action' => 'team']
            ],
        'about_experts' =>
            [
                'caption' => 'Эксперты компании',
                'url' => ['controller' => 'Abouts', 'action' => 'experts']
            ],
        'about_reviews' =>
            [
                'caption' => 'Отзывы',
                'url' => ['controller' => 'Abouts', 'action' => 'reviews']
            ],
    ];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    private function addMenu(&$menus, $caption, $url = false, $submenu = [], $key = false)
    {
        $menu = ['caption' => $caption];

        if (!empty($url)) {
            $menu['url'] = $url;

            if (!empty($url['controller']) && $this->request->getParam('controller')  == $url['controller']) {
                if (!empty($url['action'])) {
                    if ($this->request->action == 'view' && $url['action'] == 'index') {
                        $menu['current'] = true;
                    } else {
                        $menu['current'] = ($this->request->getParam('action')  == $url['action']);
                    }
                }
                if ($menu['current'] && isset($url['id']) || isset($url['slug'])) {
                    $menu['current'] = ((isset($url['id']) && $this->request->id == $url['id']) || (isset($url['slug']) && $this->request->slug == $url['slug']));
                }
            }
        }

        if (!empty($submenu)) {
            $menu['submenu'] = $submenu;
            if (is_array($submenu) && empty($menu['current'])) {
                foreach ($submenu as $sub) {
                    if (!empty($sub['current'])) {
                        $menu['current'] = true;
                        break;
                    }
                }
            }
        }

        if ($key) {
            $menus[$key] = $menu;
        } else {
            array_push($menus, $menu);
        }
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($langs, $settings)
    {
        $menus = [];
        $submenu = [];

        foreach ($this->urls_sub as $url_sub) {
            $this->addMenu($submenu, __($url_sub['caption']), $url_sub['url']);
        }


        $submenu_services = [
            [
                'caption' => 'Все услуги',
                'url' => ['controller' => 'Services', 'action' => 'index']
            ]
        ];
        $new_service_item = [];


        $all_sub_services = TableRegistry::getTableLocator()->get('Services')->find()->toArray();
        foreach ($all_sub_services as $all_sub_service) {
            $all_sub_service->url = ['controller' => 'Services', 'action' => $all_sub_service['slug']];
            $new_service_item[] = $all_sub_service;
        }
        foreach ($new_service_item as $url_sub) {
            $this->addMenu($submenu_services, $url_sub['caption'], $url_sub['url']);
        }


        $this->addMenu($menus, __('О компании'), $this->urls['about'], $submenu);
        $this->addMenu($menus, __('Услуги'), $this->urls['services'], $submenu_services);
        $this->addMenu($menus, __('Проекты'), $this->urls['projects']);
        $this->addMenu($menus, __('Новости'), $this->urls['news']);
        $this->addMenu($menus, __('Контакты'), $this->urls['contacts']);
        $this->set(compact('menus', 'langs', 'settings'));
    }

    public function sidebar($langs, $settings)
    {
        $this->display($langs, $settings);
    }

    public function services($langs, $settings)
    {
        $menus = [];
        $submenu = [
            [
                'caption' => 'Все услуги',
                'url' => ['controller' => 'Services', 'action' => 'index']
            ]
        ];
        $new_service_item = [];

        $all_sub_services = TableRegistry::getTableLocator()->get('Services')->find()->toArray();
        foreach ($all_sub_services as $all_sub_service) {
            $all_sub_service->url = ['controller' => 'Services', 'action' => $all_sub_service['slug']];
            $new_service_item[] = $all_sub_service;
        }
        foreach ($new_service_item as $url_sub) {
            $this->addMenu($submenu, $url_sub['caption'], $url_sub['url']);
        }

        $this->addMenu($menus, __('Услуги'), $this->urls['services'], $submenu);
        $this->set(compact('menus', 'langs', 'settings'));
    }

    public function footer($langs, $settings)
    {
//        $menus = [];
//        $serv = [];
//        $projects = [];
//
//        $this->addMenu($menus, __('О нас'), $this->urls['about']);
//        $this->addMenu($menus, __('Наш опыт'), $this->urls['experience']);
//        $this->addMenu($menus, __('Контакты'), $this->urls['contacts']);
//        $this->addMenu($menus, __('Карта сайта'), $this->urls['sitemap']);
//
//        $services = TableRegistry::get('Services')->find()->select(['caption', 'slug'])->toArray();
//        foreach ($services as $service) {
//            $this->addMenu($serv, $service->get('caption'), '/services/view/'.$service->get('slug'));
//        }
//        $experiences = TableRegistry::get('Experiences')->find()->select(['caption', 'slug'])->where(['footer_menu' => true])->toArray();
//        foreach ($experiences as $experience) {
//            $this->addMenu($projects, $experience->get('caption'), '/experiences/view/'.$experience->get('slug'));
//        }
//        $this->set(compact('menus', 'langs', 'serv', 'projects', 'settings'));
    }
}
