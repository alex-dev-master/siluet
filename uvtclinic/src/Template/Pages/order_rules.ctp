<div class="page-content about-content about-doc-content">
    <img class="content-bg" src="/files/pages/<?=$row['thumbnail_1440x540']?>" alt="">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('О клинике'),
            ['controller' => 'Abouts', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <div class="text">
        <?=$row['content']?>
        </div>
    </div>
</div>
