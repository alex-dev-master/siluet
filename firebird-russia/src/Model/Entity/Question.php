<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $caption
 * @property string $content
 * @property string $image
 * @property int $position
 * @property bool $disabled
 */
class Question extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'question_rubric_id' => true,
        'slug' => true,
        'caption' => true,
        'caption_question' => true,
        'caption_response' => true,
        'content' => true,
        'image' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'position' => true,
        'disabled' => true,
    ];
}
