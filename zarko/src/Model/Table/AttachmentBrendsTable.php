<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentImages Model
 *
 * @method \App\Model\Entity\AttachmentImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AttachmentBrendsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachment_brends');
        $this->setDisplayField('caption')
            ->setDisplayImage('thumbnail')
            //->newPositionLast(true)
            ->setOrder(['AttachmentBrends.position' => 'DESC', 'AttachmentBrends.id' => 'DESC'])
            ->setNeighborhood([]);
        $this->setPrimaryKey('id');
        $this
            ->setFormSchema([
                'image' => ['params' => ['copy' => '']],
                'thumbnail' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 370, 'y' => 370, 'source' => 'image']],
            ]);

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('foreign_key')
            ->requirePresence('foreign_key', 'create')
            ->notEmpty('foreign_key');

        $validator
            ->scalar('model')
            ->maxLength('model', 50)
            ->requirePresence('model', 'create')
            ->notEmpty('model');

        /*$validator
            ->scalar('caption')
            ->maxLength('caption', 255)
            ->requirePresence('caption', 'create')
            ->notEmpty('caption');*/

        $validator
            ->scalar('image')
            ->maxLength('image', 25)
            ->allowEmpty('image');

        $validator
            ->scalar('thumbnail')
            ->maxLength('thumbnail', 25)
            ->allowEmpty('thumbnail');

        $validator
            ->integer('position')
            ->allowEmpty('position');

        $validator
            ->boolean('disabled')
            ->requirePresence('disabled', 'create')
            ->notEmpty('disabled');

        return $validator;
    }
}
