<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentImages Model
 *
 * @method \App\Model\Entity\AttachmentImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AttachmentPeoplesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachment_peoples');
        $this->setDisplayField('caption')
            ->setDisplayImage('icon')
            //->newPositionLast(true)
            ->setOrder(['AttachmentPeoples.position' => 'DESC', 'AttachmentPeoples.id' => 'DESC'])
            ->setNeighborhood(['model', 'foreign_key']);
        $this->setFormSchema([
            'image' => ['params' => ['resize' => 'crop', 'x' => 635, 'y' => 390]],
            'icon' => ['label' => 'Миниатюра','type' => 'image','params' => ['resize' => 'fill', 'color' => 'ffffff', 'x' => 50, 'y' => 50]],
            'quotos' => ['label' => 'Цитата', 'type' => 'textarea'],
            'post' => ['label' => 'Должность'],
        ]);
    }
}
