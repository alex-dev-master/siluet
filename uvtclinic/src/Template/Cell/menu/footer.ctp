
<div class="col-md-6 col-sm-6">
    <h3 class="footer-subtitle">Клиника ЮВТ</h3>
    <ul class="footer-menu">
        <?php foreach ($menus as $menu) {
            if (!empty($menu['url']['slug'])) {
                $url = ["controller" => $menu['url']['controller'],"action" => $menu['url']['action'], $menu['url']['slug']];
            } else {
                $url = ["controller" => $menu['url']['controller'],"action" => $menu['url']['action']];
            }
            ?>

        <li>
            <a href="<?= $this->Url->build($url) ?>"><?= $menu['caption'] ?></a>
        </li>
        <?php } ?>
    </ul>
</div>
<div class="col-md-6 col-sm-6">
    <h3 class="footer-subtitle">Услуги</h3>
    <ul class="footer-menu">
        <?php foreach ($services as $service) { ?>
        <li>
            <a href="<?= $this->Url->build(["controller" => $service['url']['controller'],"action" => $service['url']['action'], $service['url']['slug']]) ?>"><?=$service['caption']?></a>
        </li>
        <?php } ?>
    </ul>
</div>
