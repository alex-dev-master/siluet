<footer class="footer">
    <div class="footer-top">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-6 col-sm-5 col-xs-12">
                    <div class="row">
                        <?php foreach ($menus as $menu) { ?>
                            <div class="col-md-4   footer-col">
                                <p class="footer-subtitle"><?=$menu['caption']?></p>
                                <ul class="footer-menu">
                                    <?php foreach ($menu['submenu'] as $sub_menu) { ?>
                                        <li <?php if (!empty($sub_menu['current'])) echo 'class="active-link"'?> >
                                            <a href="<?= $this->Url->build($sub_menu['url']) ?>"><?=$sub_menu['caption']?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>


                </div>
                <div class="footer-col col-md-4 col-md-offset-1 col-sm-6 col-xs-12 ">
                    <p class="footer-subtitle"><?=__('Контакты')?></p>
                    <ul class=" contact-menu">
                        <li class="phone">
                            <a href="tel:<?=$settings['phone']?>"><?=$settings['phone']?></a>
                        </li>
                        <li class="address">
                            <p>
                                <?=$settings['address']?>
                            </p>
                            <a href="/contacts"><?=__('На карте')?></a>
                        </li>

                        <li class="write">
                            <a href="/contacts"><?=__('Написать нам письмо')?></a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-12 footer-col soc-col">
                    <ul class="soc-list">
                        <li>
                            <a href="<?=$settings['insta_link']?>">
                                <img src="/img/icon_inst.png">
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>

    </div>
    <div class="footer-btm">
        <div class="wrapper">
            <ul class="footer-info">
                <li><p class="copy-text"><span class="copy">&copy;</span><?=$settings['bf587ceef5']?></p></li>
                <li><a href="<?= $this->Url->build(["controller" => "Misc","action" => "sitemap"])?>"><?=__('Карта сайта')?></a></li>
                <li><a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политика конфиденциальности')?></a></li>
                <li class="siluet-li">
                    <p class="siluet">
                        <a target="_blank" href="http://siluetstudio.com"><?=$settings['copyright']?></a>
                    </p>
                </li>
            </ul>
        </div>
    </div>
</footer>
