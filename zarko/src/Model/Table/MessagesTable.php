<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;

/**
 * Sliders Model
 *
 * @method \App\Model\Entity\Slider get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slider newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slider|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slider[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slider findOrCreate($search, callable $callback = null, $options = [])
 */
class MessagesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('messages')
            ->setDisplayField('name')
            ->setPrimaryKey('id')
            ->paginateLimit(20)
            ->setOrder(['Messages.created' => 'DESC', 'Messages.id' => 'DESC'])
            ->setFormSchema([
                'name' => ['label' => 'Имя отправителя'],
                'email' => ['label' => 'Email'],
                'phone' => ['label' => 'Телефон'],
                'text' => ['label' => 'Текст сообщения'],
            ])
            ->setTabSchema([

            ]);

        $this->addBehavior('Timestamp');

//            ->addBehavior('Translate', ['fields' =>
//                ['caption', 'sub_caption', 'content_details_button', 'project_title_first_block', 'project_desc_first_block', 'project_title_second_block', 'project_desc_second_block', 'project_title_third_block', 'project_desc_third_block', 'project_details_link']
//            ]);


    }
}
