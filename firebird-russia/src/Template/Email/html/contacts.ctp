<?php
$text = $this->Html->tag('p', "Здравствуйте! С сайта {$_SERVER['HTTP_HOST']} поступило новое сообщение!");
$fields = [
    'categoryForm' => 'С формы',
    'url' => 'С адреса',
    'towar' => 'Товар',
    'service' => 'Услуга',
    'vacancy' => 'Вакансия',
    'name' => 'ФИО',
    'phone' => 'Телефон',
    'email' => 'Электронная почта',
    'msg' => 'Текст сообщения',
];
$text .= '<p>';
foreach ($fields as $field => $name):
    if (!empty($data[$field])):
        $separate = ($field != 'message' ? ': ' : $this->Html->tag('br'));
        $text .= $name . $separate . $this->Html->tag('b', $data[$field]) . $this->Html->tag('br');
    endif;
endforeach;
$text .= '</p>';
#
echo $text;
