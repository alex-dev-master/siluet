<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $caption
 * @property string $content
 * @property bool $disabled
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modiified
 */
class About extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'content_requisites' => true,
        'content' => true,
        'image' => true,
        'post_1' => true,
        'caption_name_1' => true,
        'content_quotes_1' => true,
        'post_2' => true,
        'caption_name_2' => true,
        'content_quotes_2' => true,
        'info_phone' => true,
        'info_time' => true,
        'info_write' => true,
        'caption_block_care' => true,
        'content_block_care' => true,
        'services_block_care' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,

    ];
}
