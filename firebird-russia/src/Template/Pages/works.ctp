<div class="page-top processes__top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$page['caption']?></h1>
        <div class="text">
            <?=$page['content']?>
        </div>
    </div>
</div>
<div class="processes">
    <div class="processes__steps">
        <div class="wrapper">
            <div class="processes__steps-slider">
                <?php $i = 1; foreach ($works as $k=>$work) { ?>
                <div class="slide">
                    <a href="javascript:void();" data-n="<?=$k?>">
                        <div class="number">
                            <span><?=$i?></span>
                        </div>
                        <div class="name">
                            <?=$work['caption']?>
                        </div>
                    </a>
                </div>
                <?php  $i++; } ?>
            </div>
        </div>
    </div>
    <div class="processes__slider-block">
        <div class="wrapper">
            <div class="processes__slider">
                <?php $i = 1; foreach ($works as $k=>$work) { ?>
                <div class="slide_one">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="img-container">
                                <img src="/files/works/<?=$work['image']?>"/>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="slide_text">
                                <p class="title"><img src="/files/works/<?=$work['image_icon']?>"/> <?=$work['caption_2']?></p>
                                <div class="text">
                                    <?=$work['content']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php  $i++; } ?>
            </div>
            <div class="processes__slider__nav">
                <a href="#" class="slide-nav-arrow left"><img src="/img/left-arr.png"> </a>
                <div class="pagingInfo">
                    <span class="active-slide__number"></span> / <span
                        class="slide__count"></span>
                </div>
                <a href="#" class="slide-nav-arrow right"><img src="/img/right-arr.png"> </a>
            </div>
        </div>

    </div>
</div>
<?=$this->element('makeOrder')?>
