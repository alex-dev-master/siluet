<?php
namespace App\Controller\Admin;

class AboutsController extends AppController {
    public function edit($id = null)
    {
        parent::edit($id);
        if ($id == 1) {
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            $tab_schema['attachment_images']['label'] = 'Сертификаты';
            $schema = $this->Abouts->getFormSchema();
            $this->Abouts->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'text_block_1', 'text_block_2', 'text_block_3', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Abouts->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }
    }
}
