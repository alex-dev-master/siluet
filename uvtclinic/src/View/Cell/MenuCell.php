<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
/**
 * Menu cell
 */
class MenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];



    private $urls_top = [
      'abouts' => ['controller' => 'Abouts', 'action' => 'index'],
      'services' => ['controller' => 'Pages', 'action' => 'services'],
      'experts' => ['controller' => 'Pages', 'action' => 'experts'],
      'timetable' => ['controller' => 'Pages', 'action' => 'timetable'],
      'costs' => ['controller' => 'Pages', 'action' => 'costs'],
      'contacts' => ['controller' => 'Contacts', 'action' => 'index'],
    ];

    private $urls_top_sub = [
        'about-medical-activities' => ['controller' => 'Abouts', 'action' => 'aboutMedicalActivities'],
        'regulations' => ['controller' => 'Abouts', 'action' => 'regulations'],
        'order-rules' => ['controller' => 'Abouts', 'action' => 'orderRules'],
        'rights-obligations' => ['controller' => 'Abouts', 'action' => 'rightsObligations'],
        'jobs' => ['controller' => 'Abouts', 'action' => 'jobs'],
        'reviews' => ['controller' => 'Pages', 'action' => 'reviews'],
        'gallery' => ['controller' => 'Pages', 'action' => 'gallery'],
        'questionnaire' => ['controller' => 'Pages', 'action' => 'questionnaire'],
    ];

    private $urls_footer = [
        'abouts' => ['controller' => 'Abouts', 'action' => 'index'],
        'experts' => ['controller' => 'Pages', 'action' => 'experts'],
        'timetable' => ['controller' => 'Pages', 'action' => 'timetable'],
        'costs' => ['controller' => 'Pages', 'action' => 'costs'],
        'jobs' => ['controller' => 'Abouts', 'action' => 'jobs'],
        'services' => ['controller' => 'Pages', 'action' => 'services'],
        'questionnaire' => ['controller' => 'Pages', 'action' => 'questionnaire'],
        'articles' => ['controller' => 'Pages', 'action' => 'articles'],
        'reviews' => ['controller' => 'Pages', 'action' => 'reviews'],
        'contacts' => ['controller' => 'Contacts', 'action' => 'index'],
    ];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    private function addMenu(&$menus, $caption, $url = false, $submenu = [], $key = false)
    {
        $menu = ['caption' => $caption];

        if (!empty($url)) {
            $menu['url'] = $url;
            if (!empty($url['controller']) && $this->request->getParam('controller')  == $url['controller']) {
                if (!empty($url['action'])) {
                    if ($this->request->action == 'view' && $url['action'] == 'index') {
                        $menu['current'] = true;
                    } else {
                        $menu['current'] = ($this->request->getParam('action')  == $url['action']);
                    }
                }
                if ($menu['current'] && isset($url['id']) || isset($url['slug'])) {
                    $menu['current'] = ((isset($url['id']) && $this->request->id == $url['id']) || (isset($url['slug']) && $this->request->slug == $url['slug']));
                }
            }
        }

        if (!empty($submenu)) {
            $menu['submenu'] = $submenu;
            if (is_array($submenu) && empty($menu['current'])) {
                foreach ($submenu as $sub) {
                    if (!empty($sub['current'])) {
                        $menu['current'] = true;
                        break;
                    }
                }
            }
        }

        if ($key) {
            $menus[$key] = $menu;
        } else {
            array_push($menus, $menu);
        }
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($settings)
    {
        $menus = [];
        $menus_urls_top_sub = [];
        $submenu_second = [];

        $this->addMenu($menus_urls_top_sub, __('О медицинской деятельности'), $this->urls_top_sub['about-medical-activities']);
        $this->addMenu($menus_urls_top_sub, __('Нормативные документы'), $this->urls_top_sub['regulations']);
        $this->addMenu($menus_urls_top_sub, __('Правила внутреннего распорядка'), $this->urls_top_sub['order-rules']);
        $this->addMenu($menus_urls_top_sub, __('О правах и обязанностях граждан в сфере охраны здоровья'), $this->urls_top_sub['rights-obligations']);
        $this->addMenu($menus_urls_top_sub, __('Вакансии'), $this->urls_top_sub['jobs']);
        $this->addMenu($menus_urls_top_sub, __('Отзывы'), $this->urls_top_sub['reviews']);
        $this->addMenu($menus_urls_top_sub, __('Галерея'), $this->urls_top_sub['gallery']);
        $this->addMenu($menus_urls_top_sub, __('Оценка качества обслуживания'), $this->urls_top_sub['questionnaire']);

        $categories = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->toArray();
        foreach ($categories as $all_sub_service) {
            $all_sub_service->url = ['controller' => 'ItemRubrics', 'action' => 'index', 'slug' => $all_sub_service['slug']];
            $new_service_item[] = $all_sub_service;
        }
        foreach ($new_service_item as $url_sub) {
            $this->addMenu($submenu_second, $url_sub['caption'], $url_sub['url']);
        }

        $this->addMenu($menus, __('О клинике'), $this->urls_top['abouts'], $menus_urls_top_sub);
        $this->addMenu($menus, __('Услуги'), $this->urls_top['services'], $submenu_second);
        $this->addMenu($menus, __('Наши специалисты'), $this->urls_top['experts']);
        $this->addMenu($menus, __('Расписание приема'), $this->urls_top['timetable']);
        $this->addMenu($menus, __('Стоимость'), $this->urls_top['costs']);
        $this->addMenu($menus, __('Контакты'), $this->urls_top['contacts']);

        $this->set(compact('menus', 'langs', 'settings'));
    }

    public function footer($settings)
    {
        $menus = [];
        $services = [];

        $this->addMenu($menus, __('О клинике'), $this->urls_footer['abouts']);
        $this->addMenu($menus, __('Наши специалисты'), $this->urls_footer['experts']);
        $this->addMenu($menus, __('Расписание приема'), $this->urls_footer['timetable']);
        $this->addMenu($menus, __('Стоимость'), $this->urls_footer['costs']);
        $this->addMenu($menus, __('Вакансии'), $this->urls_footer['jobs']);
        $this->addMenu($menus, __('Оценка качества обслуживания'), $this->urls_footer['questionnaire']);
        $this->addMenu($menus, __('Полезная информация'), $this->urls_footer['articles']);
        $this->addMenu($menus, __('Отзывы'), $this->urls_footer['reviews']);
        $this->addMenu($menus, __('Контакты'), $this->urls_footer['contacts']);

        $categories = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->toArray();
        foreach ($categories as $all_sub_service) {
            $all_sub_service->url = ['controller' => 'ItemRubrics', 'action' => 'index', 'slug' => $all_sub_service['slug']];
            $new_service_item[] = $all_sub_service;
        }
        foreach ($new_service_item as $url_sub) {
            $this->addMenu($services, $url_sub['caption'], $url_sub['url']);
        }

        $this->set(compact('menus', 'services'));

    }

}
