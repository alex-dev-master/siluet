<div class="page-content contact-page">

    <!-- location -->
    <div class="contact-location">
        <div class="container">
            <p class="page-title">Контакты</p>
            <div class="row">
                <div class="col-12 col-md-5">
                    <p class="h1"><?= $contact_arr['caption'] ?></p>
                    <div class="contact-location__field">
                        <div class="contact-location__icon">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 20" xml:space="preserve"><path d="M8,0C3.6,0,0,3.5,0,7.7c0,1.7,0.6,3.2,1.6,4.6L8,20l6.4-7.8c1-1.3,1.6-2.9,1.6-4.6C16,3.5,12.5,0,8,0z M12.9,11L8,16.9 L3.2,11C2.4,10,2,8.9,2,7.7C2,4.5,4.7,2,8,2s6,2.5,6,5.7C14,8.9,13.6,10,12.9,11z"/><path d="M8,4C5.8,4,4,5.8,4,8s1.8,4,4,4s4-1.8,4-4S10.2,4,8,4z M8,10c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S9.1,10,8,10z"/></svg>
                        </div>
                        <p class="contact-location__title">Адрес</p>
                        <p class="contact-location__description"><?= $contact_arr['map_caption'] ?></p>
                    </div>
                    <div class="contact-location__field">
                        <div class="contact-location__icon">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20.1" xml:space="preserve"><path d="M4,2C4,2,4,2,4,2L4,2l0.1,0.1c0.3,0.2,0.5,0.5,0.8,0.8C5,3,5.2,3.2,5.3,3.3l1.2,1.2c0,0,0.1,0.1,0.1,0.1c0,0,0,0.1-0.1,0.1 C6.4,4.9,6.3,5,6.2,5.1c-0.3,0.3-0.7,0.7-1,1l-1.1,1l0.6,1.3C5,9.2,5.5,10,6.2,10.9c1.3,1.6,2.7,2.9,4.2,3.9 c0.2,0.1,0.4,0.3,0.6,0.4c0.1,0.1,0.2,0.1,0.3,0.2l1.3,0.8l1.1-1.1l1.5-1.5c0,0,0,0,0.1-0.1c0,0,0,0,0.1,0.1l2.5,2.5 c0,0,0.1,0.1,0.1,0.1c0,0,0,0.1-0.1,0.1c-0.2,0.2-0.4,0.4-0.5,0.5l-0.1,0.1c-0.3,0.3-0.6,0.6-0.9,0.9c-0.3,0.3-0.6,0.4-1,0.4 c-0.1,0-0.1,0-0.2,0c-1.2-0.1-2.3-0.5-3.2-1c-2.5-1.2-4.6-2.9-6.4-5c-1.5-1.8-2.5-3.4-3.1-5.2C2.1,5.9,2,5.1,2,4.4 c0-0.3,0.1-0.6,0.4-0.8L4,2C3.9,2.1,4,2,4,2 M4,0C3.5,0,3,0.2,2.5,0.6L1,2.2c-0.6,0.6-0.9,1.3-1,2c-0.1,1,0.1,2.1,0.6,3.3 c0.7,2,1.8,3.8,3.5,5.8c2,2.4,4.4,4.2,7.1,5.5c1,0.5,2.4,1.1,4,1.2c0.1,0,0.2,0,0.3,0c1,0,1.9-0.4,2.5-1.1c0,0,0,0,0,0 c0.2-0.3,0.5-0.6,0.8-0.8c0.2-0.2,0.4-0.4,0.6-0.6c0.9-0.9,0.9-2.1,0-3l-2.5-2.5c-0.4-0.4-0.9-0.7-1.5-0.7c-0.5,0-1,0.2-1.5,0.7 l-1.5,1.5c-0.1-0.1-0.3-0.2-0.4-0.2c-0.2-0.1-0.3-0.2-0.5-0.2c-1.4-0.9-2.6-2-3.8-3.4c-0.6-0.8-1-1.4-1.3-2c0.4-0.4,0.8-0.7,1.1-1.1 C7.7,6.4,7.9,6.3,8,6.1c0.4-0.4,0.7-1,0.7-1.5c0-0.5-0.2-1-0.7-1.5L6.7,1.9C6.6,1.8,6.5,1.6,6.3,1.5C6,1.2,5.8,0.9,5.5,0.6 C5,0.2,4.5,0,4,0"/></svg>
                        </div>
                        <p class="contact-location__title">Телефон</p>
                        <p class="contact-location__description"><a href="tel:+7(812)6776767"><?= $settings['phone'] ?></a></p>
                    </div>
                    <div class="contact-location__field">
                        <div class="contact-location__icon">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 15" xml:space="preserve"><path d="M17,0H3C1.3,0,0,1.3,0,3v9c0,1.7,1.3,3,3,3h14c1.7,0,3-1.3,3-3V3C20,1.3,18.7,0,17,0z M15.7,9.3c-0.4-0.4-1-0.4-1.4,0 s-0.4,1,0,1.4l2.3,2.3H3.4l2.3-2.3c0.4-0.4,0.4-1,0-1.4s-1-0.4-1.4,0L2,11.6V3.3l8,7l8-7v8.3L15.7,9.3z M3.5,2h13L10,7.7L3.5,2z"/></svg>
                        </div>
                        <p class="contact-location__title">E-mail</p>
                        <p class="contact-location__description"><a href="mailto:test@test.ru"><?= $settings['email'] ?></a></p>
                    </div>
                </div>
                <div class="col-12 col-md-7">
                    <input id="coord_map2_shir" type="hidden" value="<?= $contact_arr['map_n'] ?>">
                    <input id="coord_map2_dolg" type="hidden" value="<?= $contact_arr['map_e'] ?>">
                    <input type="hidden">
                    <div id="map2" style="width: 100%; height: 400px" class="contact-location__map">
<!--                        <a href="" class="btn btn-primary btn-map">Открыть Google.Maps</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- location end -->

    <!-- form -->
    <?= $this->Form->create(['schema' => []], ['id' => 'contacts']); ?>
        <div class="section section-gray contact-form">
        <div class="container">
            <div class="section__title">Напишите нам</div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label class="label">Ваше имя <sup>*</sup></label>
                        <input type="text" name="name" class="form-control">
                        <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label class="label">Электронная почта <sup>*</sup></label>
                        <input type="text" name="email" class="form-control">
                        <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label class="label">Телефон</label>
                        <input type="text" name="phone" class="form-control">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="label">Текст сообщения <sup>*</sup></label>
                        <textarea rows="2" name="text" class="form-control"></textarea>
                        <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="privacy-policy">
                            <span class="check"></span>
                            Я соглашаюсь с <a href="/privacy-policy">Политикой конфиденциальности</a>
                        </label>
                    </div>
                </div>
                <div class="col-12 col-md-6 text-left text-md-right">
                    <button class="btn btn-primary btn-with-icon">Отправить <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></button>
                </div>
            </div>
            <div class="success success-msg">
                <div class="container">
                    <div class="success-msg__icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><polygon points="37.1,44.6 34.2,47.3 47.8,61.9 79.7,29.9 76.9,27.1 47.8,56.2 "/><path d="M18.6,50c0,17.4,14.1,31.4,31.4,31.4S81.4,67.4,81.4,50h-4c0,15.1-12.3,27.4-27.4,27.4S22.6,65.1,22.6,50S34.9,22.6,50,22.6  v-4C32.7,18.6,18.6,32.7,18.6,50z"/></svg>
                    </div>
                    <div class="success-msg__title">Ваше сообщение отправлено!</div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end(); ?>
    <!-- form end -->
    <!-- wysiwyg -->
    <div class="container contact-text">

        <div class="wysiwyg">
            <h2>Реквизиты</h2>
            <?= $contact_arr['requisites_text'] ?>
            <a href="/files/contacts/<?= $contact_arr['requisites_file'] ?>" class="btn btn-primary btn-with-icon" download>
                Скачать файл Арко Реквизиты.docx
                <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span>
            </a>
        </div>
    </div>
    <!-- wysiwyg end -->


</div>
