<?= $this->Form->create(null, ['id' => 'index-actions']); ?>
<?php
$this->set('UrlCollection', $UrlCollection->append(['_ext' => 'frame', '?' => ['model' => $this->request->getQuery('model'), 'foreign_key' => $this->request->getQuery('foreign_key'), 'sub' => 1]]));
?>
<div class="buttons btn_list">
    <?php echo $this->elementCms('index_buttons', array('top_element' => 1)); ?>
    <?php echo $this->elementCms('index_paginator'); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div<?php //if ($is_tree) {  ?> class="dd nestable"<?php if (!$is_tree) { ?> data-depth="1"<?php } ?><?php if (isset($field_schema['position'])) { ?> id="nestable-list-<?php echo $model; ?>" data-src="<?php echo $this->Url->build($UrlCollection->append(['action' => 'index', 'tree' => 1, 'id' => $this->request->getParam('id'), '_ext' => 'json'])); ?>"<?php if (isset($jsontree)) { ?> data-tree="<?php echo urlencode($jsontree); ?>"<?php }
    } ?>>
            <ol class="dd-list sec">
                <?php
                echo $this->General->listRecords($rows, $is_tree, $primary_key); //, $display_image);
                ?>
            </ol>
        </div>
    </div>
</div>
    <?php if (is_array($rows) and ! empty($rows)) { ?>
    <div class="buttons">
        <?php echo $this->elementCms('index_paginator'); ?>
    <?php echo $this->elementCms('index_buttons'); ?>
    </div>
<?php } ?>
<?= $this->Form->end(); ?>