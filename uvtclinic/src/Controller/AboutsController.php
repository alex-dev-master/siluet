<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AboutsController extends AppController
{
    public function index($rubric_id = null, $callback = null)
    {
        $page = TableRegistry::getTableLocator()->get('Abouts')->find()->contain(['AttachmentAdmins', 'AttachmentPeoples'])->first();
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()
            ->contain(['Specialities'])
            ->where(['Experts.to_abouts_page' => 1])
            ->toArray();
        $specialities = TableRegistry::getTableLocator()->get('Specialities')->find()->contain(['Experts'])->toArray();
        $articles = TableRegistry::getTableLocator()->get('Articles')->find()->contain(['ArticleRubrics'])->toArray();
        $this->Title->setMeta($page);
        $this->set(compact('page','articles', 'experts', 'specialities'));
    }
}
