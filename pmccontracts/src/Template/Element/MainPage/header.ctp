<?php

use Cake\ORM\TableRegistry;
$urls_sub_abouts = [
    'about_about' =>
        [
            'caption' => 'О нас',
            'url' => ['controller' => 'Abouts', 'action' => 'index']
        ],
    'about_history' =>
        [
            'caption' => 'История компании',
            'url' => ['controller' => 'Abouts', 'action' => 'history']
        ],
    'about_team' =>
        [
            'caption' => 'Руководство компании',
            'url' => ['controller' => 'Abouts', 'action' => 'team']
        ],
    'about_experts' =>
        [
            'caption' => 'Эксперты компании',
            'url' => ['controller' => 'Abouts', 'action' => 'experts']
        ],
    'about_reviews' =>
        [
            'caption' => 'Отзывы',
            'url' => ['controller' => 'Abouts', 'action' => 'reviews']
        ],
];

?>
<div class="overlay"></div>
<!--a id="back-top" href="#top"></a-->

<nav class="navbar">
    <div class="wrapper">
        <div class="header-top">
            <div class="row">
                <div class="header-logo">
                    <a href="/"><img src="/img/logo.png"></a>
                </div>
                <div class="header-menu">
                    <div class="hide-mob mobile-menu">
                        <ul class="header-menu-list">
                            <li class="drop-menu"><a href="<?= $this->Url->build(['controller' => 'Abouts', 'action' => 'index']); ?>"><?=__('О компании')?></a>
                            <div class="submenu-block">
                                <ul class="submenu">
                            <?php
                            foreach ($urls_sub_abouts as $submenu2) { ?>
                            <li><a href="<?= $this->Url->build($submenu2['url']) ?>"><?=$submenu2['caption']?></a></li>
                            <?php } ?>
                                </ul>
                            </div>
                            </li>
                            <li class="drop-menu"><a href="<?= $this->Url->build(['controller' => 'Services', 'action' => 'index']); ?>"><?=__('Услуги')?></a>
                                <div class="submenu-block">
                                    <ul class="submenu">
                                        <li><a href="<?= $this->Url->build( [
                                                'controller' => 'Services', 'action' => 'index'
                                            ]) ?>"><?=__('Все услуги')?></a></li>
                                        <?php
                                        $all_sub_services = TableRegistry::getTableLocator()->get('Services')->find()->toArray();
                                        foreach ($all_sub_services as $submenu) {
                                            $submenu->url = ['controller' => 'Services', 'action' => $submenu['slug']];
                                            ?>
                                            <li><a href="<?= $this->Url->build($submenu['url']) ?>"><?=$submenu['caption']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="<?= $this->Url->build(['controller' => 'Projects', 'action' => 'index']); ?>"><?=__('Проекты')?></a></li>
                            <li><a href="<?= $this->Url->build(['controller' => 'News', 'action' => 'index']); ?>"><?=__('Новости')?></a></li>
                            <li><a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'index']); ?>"><?=__('Контакты')?></a></li>
                        </ul>


                        <ul class="header-right-btns">
                            <?php
                                if (!empty($settings['phone'])) {
                                    $phones = explode(',', $settings['phone']);
                                    foreach ($phones as $phone) {?>
                                     <li>
                                        <a class="header-phone" href="tel:<?=$phone?>"><?=$phone?></a>
                                     </li>
                            <?php
                                    }
                                }
                            ?>
                            <li>
                                <?php if (!empty($langs)): ?>
                                    <?php foreach ($langs as $lang): ?>
                                        <?php if (!$lang['selected']) {?>
                                <!--<a class="header-lang" href="<?= $this->Url->build($lang['url']) ?>"><?= $lang['title'] ?></a>-->
                                        <?php } ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                        <a class="mobile-close"><img src="../img/cross.png"> </a>
                    </div>
                </div>
                <div class="mobile-toggle">
                    <div class="btn_mob">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>
