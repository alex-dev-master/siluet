<div class="page-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$shop['caption']?></h1>
        <div class="main-image">
            <img src="/files/shops/<?=$shop['image']?>">
        </div>
        <div class="styled-text text">
            <?=$shop['content']?>
        </div>
    </div>
</div>

<div class="page-content toShops-page">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <div class="text">
                    <div class="img-container">
                        <img src="/files/shops/<?=$shop['image_manager']?>">
                    </div>
                    <?=$shop['content_manager']?>
                </div>
            </div>
            <div class="col-md-7 col-sm-7">
                <?= $this->Form->create(['schema' => []], ['id' => 'question-modal-form', 'class' => 'form']); ?>
                    <label for="toShops-name"><?=__('ФИО')?><span>*</span></label>
                    <input name="name" class="modal-input" id="toShops-name">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                    <label for="toShops-Email">E-mail<span>*</span></label>
                    <input name="email" class="modal-input" id="toShops-Email">
                     <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                    <label for="toShops-msg"><?=__('Текст сообщения')?></label>
                    <textarea name="msg" id="toShops-msg" class="toShops__textarea"></textarea>
                    <input type="hidden" value="Форма из раздела Магазинам">
                    <label class="checkbox">
                        <input name="privacy-policy" type="checkbox">
                        <div class="checkbox__text"><?=__('Я принимаю')?>  <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?></a></div>
                    </label>
                    <button data-tab="#success_question-modal-form" class="btn form__btn btn-form"><?=__('Отправить')?></button>
                <h4 id="success_question-modal-form" class="success" style="display: none;color: green"><?=__('Форма успешно отправлена!')?></h4>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="toShops-goods">
    <div class="wrapper">
        <h2 class="h2-sml"><?=__('Наша продукция в торговых точках')?></h2>
        <div class="shop__slider-block">
            <div class="shop__slider">
                <?php foreach ($shop['attachment_images'] as $attachment_image) {?>
                <div class="slide">
                    <img class="slide-img" src="/files/attachment-images/<?=$attachment_image['thumbnail_360x360']?>">
                </div>
                <?php } ?>
            </div>
            <div class="shop__slider__nav">
                <a href="#" class="slide-nav-arrow left"><img src="/img/jumb-arr-l.png"> </a>
                <a href="#" class="slide-nav-arrow right"><img src="/img/jumb-arr-r.png"> </a>
            </div>
        </div>
    </div>
</div>



<?=$this->element('makeOrder')?>
