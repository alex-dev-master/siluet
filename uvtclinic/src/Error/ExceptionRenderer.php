<?php
namespace App\Error;

use Cake\Error\ExceptionRenderer as CakeExceptionRenderer;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Http\Response;
use Cake\Http\ServerRequestFactory;
use App\Controller\ErrorController;
use SiluetCms\Controller\ErrorController as CmsErrorController;

class ExceptionRenderer extends CakeExceptionRenderer {
    protected function _getController() {
        if (!$request = Router::getRequest(true)) {
            $request = ServerRequestFactory::fromGlobals();
        }
        $response = new Response();
        if (Configure::read('admin') === true) {
            return new CmsErrorController($request, $response);
        }
        return new ErrorController($request, $response);
    }
}