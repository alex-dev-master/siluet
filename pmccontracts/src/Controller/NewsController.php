<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class NewsController extends AppController
{
    public $paginate;

    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $this->loadModel('Pages');
        $this->paginate = [
            'limit' => 4,
            'order' => [
//                'Objects.archive_status' => 'asc'
            ],
        ];
        $page = $this->Pages->find()->where(['Pages.id'=>5])->first();
        $this->Title->setMeta($page, 'News');

        $news = $this->paginate(TableRegistry::getTableLocator()->get('News')->find()->order(['News.published' => 'DESC']))->toArray();

        $this->set(compact('news'));

    }

    public function view($id, $query = [], $merge_query = true)
    {
        $news = TableRegistry::getTableLocator()->get('News')->find()->where(['News.slug' => $id])->contain(['AttachmentImages'])->first()->toArray();
        $this->Title->setMeta($news, 'News');
        $this->set(compact('news'));
    }
}
