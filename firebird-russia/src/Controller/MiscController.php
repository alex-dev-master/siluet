<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Query;
use Cake\ORM\TableRegistry;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class MiscController extends AppController {
    public function initialize() {
        $this->modelClass = false;
        parent::initialize();
    }

    protected function setCaption($caption)
    {
        $this->Title->dataCaption = $caption;
        $this->Title->pageCaption = $caption;
    }

    public function xls() {
        //$s = IOFactory::load(WWW_ROOT . 'files' . DS . 'test.xls');
        //var_dump($s->getActiveSheet()->getCell('A4')->getValue());die();
        $s = new Spreadsheet();
        $s->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'test hellow');
        $w = new Xlsx($s);
        $w->save('aaa.xlsx');
        die();
    }

    public function sitemap() {
        if (!$this->request->getParam('xml')) {
            $this->htmlMap();
        } else {
            $this->xmlMap();
        }
    }

    private function htmlMap() {

        $items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain('Items.Params')->toArray();
        $services = TableRegistry::getTableLocator()->get('Services')->find()->toArray();
        $jobs = TableRegistry::getTableLocator()->get('Jobs')->find()->toArray();
        $blogs = TableRegistry::getTableLocator()->get('Blogs')->find()->toArray();
        $this->setCaption(__('Карта сайта'));
//        $this->Title->dataCaption = __('Карта сайта');
        $this->set(compact('items','services', 'jobs', 'blogs'));
    }

    private function xmlMap() {
        $this->viewBuilder()->setClassName('Xml');
        $items = TableRegistry::getTableLocator()->get('ItemRubrics')->find()->contain('Items.Params')->toArray();
        $services = TableRegistry::getTableLocator()->get('Services')->find()->toArray();
        $jobs = TableRegistry::getTableLocator()->get('Jobs')->find()->toArray();
        $blogs = TableRegistry::getTableLocator()->get('Blogs')->find()->toArray();
        $this->setCaption(__('Карта сайта'));
//        $this->Title->dataCaption = __('Карта сайта');
        $this->set(compact('items','services', 'jobs', 'blogs'));
    }
}
