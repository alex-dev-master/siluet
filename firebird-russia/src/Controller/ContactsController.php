<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ContactsController extends AppController
{
    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $contact = TableRegistry::getTableLocator()->get('Contacts')->find()->first();
        $this->Title->setMeta($contact, 'Contacts');

        $this->set(compact('contact'));
    }

    public function makeOrder() {
            $data = $_POST;
            $data['Files'] = $_FILES;
            $settings = TableRegistry::getTableLocator()->get('Settings')->find()->toArray();
            $settings = $settings[0];
            $contacts_forms = [
                'Форма над подвалом' => [
                    $settings['contact_form_footer_from'],
                    $settings['contact_form_footer_subject'],
                    $settings['contact_form_footer'],
                ],
                'Заказ в портфолио' => [
                    $settings['contact_form_portfolio_from'],
                    $settings['contact_form_portfolio_subject'],
                    $settings['contact_form_portfolio'],
                ],
                'Форма из раздела Магазинам' => [
                    $settings['contact_form_magazin_from'],
                    $settings['contact_form_magazin_subject'],
                    $settings['contact_form_magazin'],
                ],
                'Форма заказа услуги' => [
                    $settings['contact_form_uslugi_from'],
                    $settings['contact_form_uslugi_subject'],
                    $settings['contact_form_uslugi'],
                ],
                'Форма заказа шапке' => [
                    $settings['contact_form_header_from'],
                    $settings['contact_form_header_subject'],
                    $settings['contact_form_header'],
                ],
                'Вакансии' => [
                    $settings['contact_form_vacan_from'],
                    $settings['contact_form_vacan_subject'],
                    $settings['contact_form_vacan'],
                ]
            ];
            $send_info = TableRegistry::getTableLocator()->get('Contacts')->find('first', ['fields' => ['feedback_to', 'feedback_from', 'feedback_subject']]);
            foreach ($contacts_forms as $k => $form) {
                if (!empty($data['categoryForm']) && $k == $data['categoryForm']) {
                    $feedback_to = $form[2];
                    $feedback_subject = $form[1];
                    $feedback_from = $form[0];
                }
            }
            if (empty($feedback_to)) {
                $feedback_to = $send_info['feedback_to'];
            }
            if (empty($feedback_subject)) {
                $feedback_subject = $send_info['feedback_subject'];
            }
            if (empty($feedback_from)) {
                $feedback_from = $send_info['feedback_from'];
            }


            $this->sendCakeEmail($feedback_to, $feedback_subject, 'contacts', $data, $feedback_from);
            $this->set('success', 'true');
            $this->set('_serialize', ['success']);
    }

    public function makeContact() {
        $data = $_POST;
        $data['Files'] = $_FILES;
        $send_info = TableRegistry::getTableLocator()->get('Contacts')->find('first', ['fields' => ['email', 'feedback_from', 'feedback_subject']]);
        $this->sendCakeEmail($send_info['email'], 'Обратной связи из раздела «Контакты»', 'contactsPage', $data, $send_info['feedback_from']);
        $this->set('success', 'true');
        $this->set('_serialize', ['success']);
    }

    public function sendBlank() {
        $data = $_POST;
        if ($data['WhatsApp'] == 'false') {
            $data['WhatsApp'] = 'Нет';
        } else {
            $data['WhatsApp'] = 'Да';
        }
        if ($data['Viber'] == 'false') {
            $data['Viber'] = 'Нет';
        } else {
            $data['Viber'] = 'Да';
        }
        if ($data['Tel'] == 'false') {
            $data['Tel'] = 'Нет';
        } else {
            $data['Tel'] = 'Да';
        }

        $settings = TableRegistry::getTableLocator()->get('Settings')->find()->toArray();
        $settings = $settings[0];

        $contacts_forms = [
                $settings['contact_form_more_from'],
                $settings['contact_form_more_subject'],
                $settings['contact_form_more'],
        ];

        $feedback_to = $contacts_forms[2];
        $feedback_subject = $contacts_forms[1];
        $feedback_from = $contacts_forms[0];

        $data['Files'] = $_FILES;
        $send_info = TableRegistry::getTableLocator()->get('Contacts')->find('first', ['fields' => ['feedback_to', 'feedback_from', 'feedback_subject']]);

        if (empty($feedback_to)) {
            $feedback_to = $send_info['feedback_to'];
        }
        if (empty($feedback_subject)) {
            $feedback_subject = $send_info['feedback_subject'];
        }
        if (empty($feedback_from)) {
            $feedback_from = $send_info['feedback_from'];
        }

        $this->sendCakeEmail($feedback_to, $feedback_subject, 'sendBlank', $data, $feedback_from);
        $this->set('success', 'true');
        $this->set('_serialize', ['success']);
    }


    protected function sendCakeEmail($emails, $subject, $template, $data, $from)
    {
        $Email = new \Cake\Mailer\Email();
        $Email
            ->setTo($emails)
//            ->setFrom('no_reply@' . env('SERVER_NAME'))
            ->setFrom($from)
            ->setSubject($subject . ' на сайте ' . env('SERVER_NAME'))
            ->setTemplate($template)
            ->emailFormat('both')
            ->setViewVars(compact('data'));
        if (!empty($data['Files'])) {
            foreach ($data['Files'] as $file) {
                $arr[$file['name']] = [
                    'file' => $file['tmp_name'],
                    'mimetype' => $file['type'],
                    ];
            }
            $Email->setAttachments($arr);
        }
        $Email->send();
    }



}
