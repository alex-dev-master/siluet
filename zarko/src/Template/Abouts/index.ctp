
<div class="page-content about-page">
    <div class="container">
        <div class="about-page__text">
            <p class="page-title">Кто мы</p>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="h1"><?= $about_query['caption']; ?></div>
                </div>
                <div class="col-12 col-md-6 col-lg-8">
                    <div class="wysiwyg">
                        <?= $about_query['content']; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($about_query['video'])) { ?>
        <!-- video -->
        <div class="embed-responsive embed-responsive-16by9">
            <?= $about_query['video']; ?>
        </div>
        <!-- video end -->
        <?php } ?>
        <?php if (!empty($about_query['advantages_title_1']) || !empty($about_query['advantages_title_2']) || !empty($about_query['advantages_title_3']) || !empty($about_query['advantages_title_4'])) { ?>
        <!-- icons -->
        <div class="icon-block">
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$about_query['advantages_img_1']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$about_query['advantages_title_1'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$about_query['advantages_img_2']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$about_query['advantages_title_2'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$about_query['advantages_img_3']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$about_query['advantages_title_3'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="icon-block__item">
                        <div class="icon-block__img">
                            <img src="/files/abouts/<?=$about_query['advantages_img_4']?>" alt="">
                        </div>
                        <div class="icon-block__title">
                            <?=$about_query['advantages_title_4'] ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- icons end -->
        <?php } ?>

        <hr>

        <!-- team -->
        <div class="about-page__team">
            <p class="page-title">Работы на объекте</p>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="h1">Наша команда</div>
                </div>
                <div class="col-12 col-md-6 text-left text-md-right">
                    <!-- tabs controls -->
                    <ul class="nav nav-tabs nav-tabs__icons" role="tablist">
                        <?php foreach ($peoples as $k=>$service) { ?>
                            <?php if (!empty($service['peoples'])) { ?>
                            <li class="nav-item">
                                <a class="nav-link tooltip <?php if ($k==0){echo 'active';}?>" data-tooltip="<?=$service['caption']?>" data-toggle="tab" href="#<?=$service['slug']?>" role="tab" aria-selected="<?php if ($k==0){echo 'true';}else{echo 'false';}?>">
                                    <?php if (isset($service['icon'])){ ?>
                                        <?php $icon = @file_get_contents('./'.$service['icon-uploaded']['url']); if (!empty($icon)) echo $icon;?>
                                    <?php } ?>
                                </a>
                            </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <!-- tabs controls end -->
                </div>
            </div>

            <!-- tab content -->
            <div class="tab-content">
                <?php foreach ($peoples as $z=>$service) { ?>
                <div class="tab-pane show <?php if ($z==0){echo 'active';}?>" id="<?=$service['slug']?>" role="tabpanel">
                    <!-- slider -->
                    <div class="team-slider">
                        <div class="team-slider__wrap">
                            <?php foreach ($service['peoples'] as $people) { ?>
                            <div class="team-slider__item">
                                <div class="img"><img src="/files/peoples/<?=$people['image'];?>" alt=""></div>
                                <div class="team-slider__footer">
                                    <div class="title"><?=$people['caption']?></div>
                                    <div class="description"><?=$people['post']?></div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="controls">
                                    <div class="prev"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M0.7,4.3L0,5l0.7,0.7c0,0,0,0,0,0l4,4c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,6h15.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l2.3-2.3 c0.4-0.4,0.4-1,0-1.4C5.9,0.1,5.7,0,5.4,0S4.9,0.1,4.7,0.3L0.7,4.3C0.7,4.3,0.7,4.3,0.7,4.3z"/></svg></div>
                                    <div class="pag"></div>
                                    <div class="next"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 text-left text-md-right">
                                <a href="/services" class="btn btn-primary btn-with-icon">
                                    Что мы можем
                                    <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- slider end -->
                </div>
                <?php } ?>
            </div>
            <!-- tab content end -->
        </div>
        <!-- team end -->

        <hr class="m-xl">
        <?php if (!empty($about_query['certificate_title']) || !empty($about_query['certificate_text'])) { ?>
        <div class="row">
            <div class="col-12 col-md-6">
                <p class="h1"><?= $about_query['certificate_title'] ?></p>
                <div class="wysiwyg">
                    <?= $about_query['certificate_text'] ?>
                        <img src="/img/iso-img.png" alt="">
                        &nbsp;
                        <img src="/img/simens-img.png" alt="">
                    </p>
                    <a href="" class="btn btn-primary btn-with-icon btn-gallery" onclick="$('a.fancybox').eq(0).trigger('click'); return false;">
                        Посмотреть все сертификаты
                        <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span>
                    </a>
                    <p>&nbsp;</p>

                    <div class="about-page__gallery">
                        <?php foreach ($about_query['attachment_images'] as  $gallery) { ?>
                        <a class="fancybox" data-fancybox="gallery" href="<?='/files/attachment-images/'.$gallery['image']?>"><img src="<?='/files/attachment-images/'.$gallery['image']?>" alt=""></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="wysiwyg">
                    <img src="/img/about-img.png" alt="">
                </div>
            </div>
        </div>

        <hr class="m-xl">
        <?php } ?>
        <!-- map -->
        <div class="about-page__map">
            <p class="h1">География проектов</p>
            <div id="map3" style="width: 100%; height: 560px">
<!--                <img src="assets/img/map.png" alt="" style="width: 100%">-->
            </div>
            <div class="about-page__map-footer">
                <a href="<?= $this->Url->build(["controller" => "Experiences","action" => "index"]) ?>" class="btn btn-primary btn-with-icon">К проектам <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></a>
            </div>
        </div>
        <!-- map end -->

        <?php
            $status_clients = true;
        foreach ($about_query->attachment_brends as $brend_img) {
            if (empty($brend_img)) {
        $status_clients = false;
             }
        }
        ?>
        <?php if ($status_clients) {?>
        <!-- clients -->
        <div class="about-page__clients">
            <div class="row">
                <div class="col-12 col-md-4">
                    <p class="title-sm">Наши клиенты</p>
                </div>
                <div class="col-12 col-md-8">
                    <div class="row">
                        <?php foreach ($about_query->attachment_brends as $brend_img) {?>
                        <div class="col-6 col-md-4 col-lg-3">
                            <div class="img-gallery">
                                <a href="<?= $brend_img['slug']; ?>" class="img-gallery__link" target="_blank">
											<span class="img-gallery__img">
												<img src="/files/attachment-brends/<?= $brend_img['image']; ?>" alt="">
											</span>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- clients end -->
        <?php } ?>
    </div>


</div>

<div id="contact_us" class="page-content__bottom page-content__section withBg" style="background-image: url(/img/about_bg.png);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-8 col-xl-9">
                <div class="page-content__title">У вас есть интересный проект?</div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                <a href="#service-page__form" data-toggle="collapse" class="btn btn-primary btn-block btn-lg">Свяжитесь с нами!</a>
            </div>
        </div>
    </div>
</div>

<!-- form -->
<?= $this->Form->create(['schema' => []], ['id' => 'contacts']); ?>
<div id="service-page__form" class="service-page__form collapse">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Ваше имя <sup>*</sup></label>
                    <input type="text" name="name" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Электронная почта <sup>*</sup></label>
                    <input type="text" name="email" class="form-control">
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label class="label">Телефон</label>
                    <input type="text" name="phone" class="form-control">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="label">Текст сообщения <sup>*</sup></label>
                    <textarea rows="2" name="text" class="form-control"></textarea>
                    <p style="color: red; display: none">Это поле обязательно для заполнения</p>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="privacy-policy">
                        <span class="check"></span>
                        Я соглашаюсь с <a href="/privacy-policy">Политикой конфиденциальности</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 text-left text-md-right">
                <button class="btn btn-primary btn-with-icon">Отправить <span class="icon"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20.4 10" xml:space="preserve"><path d="M19.7,5.7L20.4,5l-0.7-0.7c0,0,0,0,0,0l-4-4c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L16.6,4H1C0.4,4,0,4.4,0,5s0.4,1,1,1h15.6 l-2.3,2.3c-0.4,0.4-0.4,1,0,1.4C14.5,9.9,14.7,10,15,10s0.5-0.1,0.7-0.3L19.7,5.7C19.7,5.7,19.7,5.7,19.7,5.7z"/></svg></span></button>
            </div>
        </div>
        <div class="success success-msg">
            <div class="container">
                <div class="success-msg__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><polygon points="37.1,44.6 34.2,47.3 47.8,61.9 79.7,29.9 76.9,27.1 47.8,56.2 "/><path d="M18.6,50c0,17.4,14.1,31.4,31.4,31.4S81.4,67.4,81.4,50h-4c0,15.1-12.3,27.4-27.4,27.4S22.6,65.1,22.6,50S34.9,22.6,50,22.6  v-4C32.7,18.6,18.6,32.7,18.6,50z"/></svg>
                </div>
                <div class="success-msg__title">Ваше сообщение отправлено!</div>
            </div>
        </div>
<!--        <h4 class="success" style="color: green;float: right; display: none ">Ваше сообщение отправлено!</h4>-->
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- form end -->
