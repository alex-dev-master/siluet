<div class="page-top">
    <div class="wrapper">
        <h1 class="h1-big"><?=__('Контакты');?></h1>
    </div>
</div>
<div class="contacts">
    <div id="coords">
    <?php foreach ($addresses as $coords) {?>
        <input type="hidden" value="<?=$coords['map_n'].','.$coords['map_e']?>">
    <?php } ?>
    </div>
    <div class="wrapper">
        <?php foreach ($addresses as $k=>$address) {?>
        <div class="contact">
            <div class="row">
                <div class="col-md-5  col-sm-6">
                    <p class="contacts__city"><?=$address['city']?></p>
                    <ul class="contacts__list">
                        <li class="contacts__list-phone"><a href="tel:<?=$address['phone']?>"><?=$address['phone']?></a></li>
                        <li class="contacts__list-address"><p><?=$address['address']?></p></li>
                        <li class="contacts__list-mail"><a href="mailto:<?=$address['email']?>"><?=$address['email']?></a></li>
                    </ul>
                </div>
                <div class="col-md-7 col-sm-6">
                    <div class="contacts__map-col">
                        <div class="map" id="<?='map'.$k?>"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
