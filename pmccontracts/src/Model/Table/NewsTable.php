<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NewsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setEntityClass('App\Model\Entity\News');
        $this->setTable('news')
            ->setDisplayField('caption')
            ->setDisplayImage('image')
            ->setPrimaryKey('id')
            ->setOrder(['News.published' => 'DESC'])
            ->setFormSchema([
                'image' => ['params' => ['copy' => '']],
                'thumbnail_470x290' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 470, 'y' => 290, 'source' => 'image']],
                'image_inside' => ['label' => 'Внутреннее фото', 'type' => 'image', 'params' => ['resize' => 'crop', 'x' => 970, 'y' => 598]],
                'preview' => ['label' => 'Анонс новости', 'type' => 'wysiwyg'],
            ])
            ->addBehavior('Translate', ['fields' =>
                ['caption', 'preview', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ])
            ->addBehavior('Timestamp');

        $this->hasMany('AttachmentImages', [
            'dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'News']
        ]);


    }

}
