<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ContactsController extends AppController
{
    public function index($rubric_id = null, $callback = null)
    {
        $row = TableRegistry::getTableLocator()->get('Contacts')->find()->first();
        $this->Title->setMeta($row);
        $this->set(compact('row'));
    }

    public function sendQuestions() {
        $this->set('success', 'true');
        $this->set('_serialize', ['success']);
    }

    public function sendCallModal() {
        $request = $this->request->getData();
        $send_info = TableRegistry::getTableLocator()->get('Settings')->find('first', ['fields' => ['call_feedback_to', 'call_feedback_from', 'call_feedback_subject']]);
        $this->sendCakeEmail($send_info['call_feedback_to'], $send_info['call_feedback_subject'], 'call', $request, $send_info['call_feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }

    public function sendAppointment() {
        $request = $this->request->getData();
        $send_info = TableRegistry::getTableLocator()->get('Settings')->find('first', ['fields' => ['appoint_feedback_to', 'appoint_feedback_from', 'appoint_feedback_subject']]);
        $this->sendCakeEmail($send_info['appoint_feedback_to'], $send_info['appoint_feedback_subject'], 'appoint', $request, $send_info['appoint_feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }

    public function sendTimetable() {
        $request = $this->request->getData();
        $send_info = TableRegistry::getTableLocator()->get('Settings')->find('first', ['fields' => ['time_feedback_to', 'time_feedback_from', 'time_feedback_subject']]);

        $registerTable = TableRegistry::getTableLocator()->get('Registers');
        $review = $registerTable->newEntity();
        if (!empty($request['name'])) {
            $review->caption = $request['name'];
        }
        if (!empty($request['phone'])) {
            $review->phone = $request['phone'];
        }
        if (!empty($request['email'])) {
            $review->email = $request['email'];
        }
        if (!empty($request['msg'])) {
            $review->comment = $request['msg'];
        }
        if (!empty($request['select'])) {
            $review->expert = $request['select'];
        }
        if (!empty($request['time'])) {
            $review->time = $request['time'];
        }
        if (!empty($request['date'])) {
            $review->date = $request['date'];
        }
        $registerTable->save($review);

        $id_last = $registerTable->find()->order(['Registers.id' => 'DESC'])->select(['id'])->first();
        $cms_tables_groups = TableRegistry::getTableLocator()->get('cms_tables_groups');
        for ($i = 1; $i<=4; $i++) {
            $table_groups = $cms_tables_groups->newEntity();
            $table_groups->model = 'Registers';
            $table_groups->foreign_key = $id_last['id'];
            $table_groups->group_id = $i;
            $cms_tables_groups->save($table_groups);
        }

        $this->sendCakeEmail($send_info['time_feedback_to'], $send_info['time_feedback_subject'], 'timetable', $request, $send_info['time_feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }

    public function sendMailModal() {
        $request = $this->request->getData();
        $send_info = TableRegistry::getTableLocator()->get('Settings')->find('first', ['fields' => ['mail_feedback_to', 'mail_feedback_from', 'mail_feedback_subject']]);
        $this->sendCakeEmail($send_info['mail_feedback_to'], $send_info['mail_feedback_subject'], 'mail', $request, $send_info['mail_feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }

    public function sendJob() {
        $request = $this->getRequest()->getData();
        $data = $request;
        $data['Files'] = $_FILES;
        $send_info = TableRegistry::getTableLocator()->get('Settings')->find('first', ['fields' => ['job_feedback_to', 'job_feedback_from', 'job_feedback_subject']]);
        $this->sendCakeEmailFiles($send_info['job_feedback_to'], $send_info['job_feedback_subject'], 'job', $data, $send_info['job_feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }

    public function sendQuestion() {
        $request = $this->request->getData();
        $send_info = TableRegistry::getTableLocator()->get('Settings')->find('first', ['fields' => ['question_feedback_to', 'question_feedback_from', 'question_feedback_subject']]);
        $this->sendCakeEmail($send_info['question_feedback_to'], $send_info['question_feedback_subject'], 'question', $request, $send_info['question_feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }

    public function sendContactForm() {
        $request = $this->request->getData();
        $send_info = TableRegistry::getTableLocator()->get('Contacts')->find('first', ['fields' => ['feedback_to', 'feedback_from', 'feedback_subject']]);
        $this->sendCakeEmail($send_info['feedback_to'], $send_info['feedback_subject'], 'mail', $request, $send_info['feedback_from']);
        return $this->responseJson(json_encode(['success' => 'true']));
    }



}
