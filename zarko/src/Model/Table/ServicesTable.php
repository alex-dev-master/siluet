<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;

/**
 * Contacts Model
 *
 * @method \App\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contact findOrCreate($search, callable $callback = null, $options = [])
 */
class ServicesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('services')
            ->setDisplayField('caption')
            ->setPrimaryKey('id');

        $this->setFormSchema([
            'caption' => ['label' => 'Название услуги'],
            'slug' => ['label' => 'Slug (слово для URL)'],
            'icon' => ['label' => 'Иконка (в svg)', 'params' => ['resize' => 'crop', 'x' => 21, 'y' => 21], 'type' => 'file'],
            'short_description' => ['label' => 'Краткое описание'],
            'disabled' => ['label' => 'Услуга отключена'],

            #
            'caption_full' => ['label' => 'Полный заголовок', 'tab' => 'inside'],
            'explanation' => ['label' => 'Краткое пояснение под заголовком', 'tab' => 'inside'],
            'image' => ['label' => 'Основное изоображение', 'type' => 'image', 'tab' => 'inside'],
            'thumbnail_1600x669' => ['type' => 'image', 'params' => ['resize' => 'crop', 'x' => 1600, 'y' => 669, 'source' => 'image']],
            'video' => ['label' => 'Видео', 'tab' => 'inside', 'type' => 'video'],

            #
            'num_showing_1' => ['label' => 'Число', 'tab' => 'num_showing'],
            'text_showing_1' => ['label' => 'Подпись', 'tab' => 'num_showing'],
            'num_showing_2' => ['label' => 'Число', 'tab' => 'num_showing'],
            'text_showing_2' => ['label' => 'Подпись', 'tab' => 'num_showing'],
            'num_showing_3' => ['label' => 'Число', 'tab' => 'num_showing'],
            'text_showing_3' => ['label' => 'Подпись', 'tab' => 'num_showing'],
            'num_showing_4' => ['label' => 'Число', 'tab' => 'num_showing'],
            'text_showing_4' => ['label' => 'Подпись', 'tab' => 'num_showing'],

            #
            'service_desc_block_title' => ['label' => 'Заголовок', 'tab' => 'desc_service'],
            'service_desc_block_decs' => ['label' => 'Текст', 'tab' => 'desc_service', 'type' => 'wysiwyg'],
            'service_desc_block_img' => ['label' => 'Изоображение', 'tab' => 'desc_service', 'type' => 'image', 'params' => ['resize' => 'crop', 'x' => 980, 'y' => 530]],
        ]);

        $this->setTabSchema([
            'inside' => ['label' => 'Дополнительно'],
            'num_showing' => ['label' => 'Блок с числовыми показателями '],
            'desc_service' => ['label' => 'Блок с описанием услуги'],
            'attachment_articles' => ['label' => 'Компетенции'],
        ]);

        $this->setOrder(['Services.position' => 'DESC', 'Services.id' => 'DESC']);


        $this->addBehavior('Timestamp');

//        $this->belongsToMany('Articles');
        $this->belongsToMany('Peoples');
        $this->hasMany('AttachmentArticles', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentArticles.model' => 'Services']]);

    }
}
