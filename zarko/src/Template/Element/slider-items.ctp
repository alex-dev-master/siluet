<div class="wellcome-slider__item">
    <div class="wellcome-slider__img" style="background-image:url(<?= $slider['image-uploaded']['url']; ?>);">
<!--        <div class="dot-start" style="top: 74.1%;left: 55.7%;"></div>-->
            <input type="hidden" class="point_y" id="point_y" value="<?= $slider['point_y'] ?>">
            <input type="hidden" class="point_x" id="point_x" value="<?= $slider['point_x'] ?>">
        <div class="dot-start" <?php if (!$slider['point_status']) { echo 'style="display:none;"';};?>></div>
    </div>
    <div class="svg-curve__line" <?php if (!$slider['point_status']) { echo 'style="display:none;"'; };?>>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <path fill="none" stroke="#ffffff" stroke-width="1px" />
        </svg>
    </div>
    <div class="wellcome-slider__content">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-md-7 col-lg-7">
                    <h1 class="wellcome-slider__title"><?= $slider['caption']?></h1>
                    <p class="description"><?= $slider['sub_caption']?></p>
                    <a href="<?=$slider['link_details_button'];?>" class="details">
										<span class="icon">
											<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 26.4 8" xml:space="preserve"><path fill="#231F20" d="M22.7,0.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L22.6,3H1C0.4,3,0,3.4,0,4s0.4,1,1,1h21.6l-1.3,1.3 c-0.4,0.4-0.4,1,0,1.4C21.5,7.9,21.7,8,22,8s0.5-0.1,0.7-0.3L26.4,4L22.7,0.3z"/></svg>
										</span>
                        <?= $slider['content_details_button']?>
                    </a>
                </div>
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="wellcome-slider__description">
                        <ul class="list-block">
                            <li class="list-block__item">
                                <div class="dot"></div>
                                <div class="list-block__data">
                                    <div class="list-block__title"><?= $slider['project_title_first_block']?></div>
                                    <div class="list-block__content"><?= $slider['project_desc_first_block']?></div>
                                </div>
                            </li>
                            <li class="list-block__item">

                                <div class="list-block__data">
                                    <div class="list-block__title"><?= $slider['project_title_second_block']?></div>
                                    <div class="list-block__content"><?= $slider['project_desc_second_block']?></div>
                                </div>
                            </li>
                            <li class="list-block__item">
                                <div class="list-block__data">
                                    <div class="list-block__title"><?= $slider['project_title_third_block']?></div>
                                    <div class="list-block__content"><?= $slider['project_desc_third_block']?></div>
                                </div>
                            </li>
                            <li class="list-block__item">
                                <div class="list-block__icons">
                                    <?php foreach ($services as $service) { ?>
                                        <?php
                                            $status_icon = false;
                                            foreach ($slider['services'] as $slider_serv) {
                                                if ($service['slug'] == $slider_serv['slug']){
                                                    $status_icon = true;
                                                }

                                            }
                                        ?>
                                    <div class="icon <?php if ($status_icon) echo 'selected';?>">
                                        <?php
                                        $svg_icon = @file_get_contents('./files/services/'.$service['icon']);
                                        if (isset($svg_icon)){echo $svg_icon;}
                                        ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </li>
                            <li class="list-block__item">
                                <a href="<?= $slider['project_details_link'] ?>" class="list-block__link">Детали проекта</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
