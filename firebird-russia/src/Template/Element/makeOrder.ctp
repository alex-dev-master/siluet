<div class="order-form">
    <div class="wrapper">
        <h2><?=__('Сделать заказ')?></h2>
        <?= $this->Form->create(['schema' => []], ['id' => 'makeOrder', 'class' => 'form']); ?>
        <h4 id="success_makeOrder" class="success" style="display: none;color: green"><?=__('Форма успешно отправлена!')?></h4>
        <div class="row">
                <div class="col-md-6 col-sm-6 form__left-col">
                    <label for="name"><?=__('Ваше имя')?><span>*</span></label>
                    <input class="modal-input" name="name" id="name">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>

                    <label for="phone"><?=__('Телефон')?></label>
                    <input class="modal-input phone" name="phone" id="phone">

                    <label for="Email">E-mail<span>*</span></label>
                    <input class="modal-input" name="email" id="Email">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>

                <div class="col-md-6  col-sm-6">
                    <label for="msg"><?=__('Комментарий')?><span>*</span></label>
                    <textarea name="msg" id="msg"></textarea>
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                    <input type="hidden" value="Форма над подвалом">
                    <label for="msg"><?=__('Файлы (не более 50Мб)')?></label>

                    <div class="files">
                        <div class="file-input">
                            <div class="file_upload">
                                <div class="file-name"><?=__('Файл не выбран')?></div>
                                <button><?=__('Обзор')?></button>
                                <input name="file" type="file">
                            </div>
                            <a class="delete-place"><img src="/img/delete.png"> </a>
                        </div>
                        <a href="#" class="add-more__btn"><?=__('Добавить файл')?></a>
                    </div>
                </div>

            </div>
            <div class="btns">
                <div class="row">
                    <div class="col-md-4 col-md-offset-6 col-sm-6 col-ss-6">
                        <label class="checkbox">
                            <input name="privacy-policy" type="checkbox">
                            <div class="checkbox__text"> <?=__('Я принимаю')?> <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"> <?=__('Политику конфиденциальности')?></a></div>
                        </label>
                    </div>
                    <div class="col-md-2 col-sm-6 col-ss-6">
                        <button data-tab="#success_makeOrder" class="btn form__btn btn-form"><?=__('Отправить')?></button>
                    </div>
                </div>
            </div>
        <?= $this->Form->end(); ?>
    </div>
</div>
