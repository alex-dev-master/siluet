<div class="contacts">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$contact['caption']?></h1>
        <div class="row">
            <div class="col-md-4 col-sm-5">
                <ul class="contacts__list">
                    <li>
                        <p class="title"><?=__('Адрес')?></p>
                        <p><?=$contact['address']?></p>
                    </li>
                    <li>
                    <li>
                        <p class="title">E-mail</p>
                        <a href="mailto:info@swissam.ru"><?=$contact['email']?> — администрация</a>
                    </li>
                    <li>
                        <p class="title"><?=__('Телефоны')?></p>
                        <?php
                            $phones = explode(',', $contact['phones']);
                            foreach ($phones as $phone) {
                        ?>
                        <a href="#"><?=$phone?></a>
                        <?php } ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-8 col-sm-7">
                <div class="contacts__map-col">
                    <input type="hidden" value="<?=$contact['map_n']?>,<?=$contact['map_e']?>">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->element('contacts')?>
