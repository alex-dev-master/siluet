<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @method \App\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contact findOrCreate($search, callable $callback = null, $options = [])
 */
class ServicesTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
            ->setTable('services')
            ->setDisplayImage('image')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->setOrder([
                'Services.position' => 'DESC',
                'Services.id' => 'DESC'
            ])
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 370, 'y' => 230]],
                'caption_content' => ['label' => 'Заголовок текста']
            ])
            ->addBehavior('Translate', ['fields' =>
                ['caption', 'caption_content', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
//        $this->belongsToMany('Objects', ['joinTable' => 'objects_services',]);

    }
}
