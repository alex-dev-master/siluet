<div class="portfolio__products">
    <div class="top">
        <div class="wrapper">
            <h1>Наша продукция</h1>
            <div class="tabs">
                <ul>
                    <li class="active"><a href="#"> Матрешки</a></li>
                    <li><a href="#"> Шкатулки</a></li>
                    <li><a href="#"> Шары</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="filter">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-4">
                    <label>Вид матрешки</label>
                    <select>
                        <option>Выберите</option>
                        <option>Синяя</option>
                        <option>Круглая</option>
                        <option>Мягкая</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Размер</label>
                    <select>
                        <option>Выберите</option>
                        <option>Маленькая</option>
                        <option>Большая</option>
                        <option>Огромная как слон</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Количество мест</label>
                    <select>
                        <option>Выберите</option>
                        <option>5</option>
                        <option>7</option>
                        <option>9</option>
                    </select>
                </div>
            </div>
            <div class="filter-more">
                <h3>Заполните форму и получите расчет стоимости изготовления уже сегодня</h3>
                <form class="form">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-5">
                            <label>Вид заготовки</label>
                            <div class="zagotovka__image">
                                <a href="#zagotovka-modal" data-toggle="modal">
                                    <div class="top">
                                        <input class="name" type="text" name="zagotovka" id="zagotovka" placeholder="Выберите">
                                        <!--<span class="name">Выберите</span>-->
                                    </div>
                                </a>
                                <div class="image" >
                                    <div class="img-container">
                                        <img id="zagotovka-image" src="">
                                    </div>
                                    <a href="#" id="zagotovka-delete" class="delete-btn"><img src="/img/trash.png"> </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-7">
                            <label>Количество</label>
                            <div class="incr-input__div">
                                <input type="text" name="count" class="incr-input" id="persons" value="1">
                            </div>
                            <label for="msg">Файл (не более 50Мб)</label>
                            <div class="files">
                                <div class="file-input">
                                    <div class="file_upload">
                                        <div>Файл не выбран</div>
                                        <button>Обзор</button>
                                        <input type="file">
                                    </div>
                                </div>

                            </div>
                            <label for="msg">Удобный способ связи</label>
                            <div class="call-type">
                                <label class="checkbox">
                                    <input type="checkbox">
                                    <div class="checkbox__text">WhatsApp</div>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox">
                                    <div class="checkbox__text">Viber</div>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox">
                                    <div class="checkbox__text">Телефон</div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-5 col-lg-6  col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="name">ФИО<span>*</span></label>
                                    <input class="modal-input" id="name">
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="Email">E-mail<span>*</span></label>
                                            <input class="modal-input" id="Email">
                                        </div>
                                        <div class="col-md-6">

                                            <label for="phone">Телефон</label>
                                            <input class="modal-input" id="phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="msg">Комментарий<span>*</span></label>
                                    <textarea id="msg"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="btns">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label class="checkbox">
                                    <input type="checkbox">
                                    <div class="checkbox__text"> Я принимаю <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>">Политику
                                            конфиденциальности</a></div>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="right">
                                    <a href="#" class="btn cancel-btn">Отменить</a>
                                    <a href="#" class="btn form__btn btn-form">Отправить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <a class="btn filter-btn" href="#">Заказать похожие</a>
        </div>
    </div>
    <div class="portfolio__products-list products matr-products">
        <div class="wrapper">
            <div class="row list">
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                    <div class="item">

                        <div class="img-container">
                            <a href="#">
                                <img src="img/matr-1.jpg">
                                <div class="more-arrow"><img src="img/arr-w.png"></div>
                            </a>
                            <a href="#order-modal" data-toggle="modal">
                                <div class="btn product__order-btn">Заказать</div>
                            </a>
                        </div>
                        <div class="name">
                            <p>M - 008</p>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<?=$this->element('makeOrder')?>
