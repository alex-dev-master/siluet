<?php
namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('caption')
                ->setDisplayImage('image')
                ->setPrimaryKey('id')
                ->setOrder(['Items.position' => 'desc', 'Items.id' => 'desc'])
                ->setNeighborhood(['item_rubric_id'])
                ->paginateLimit(20);
        $this->setFormSchema([
                'image' => ['params' => ['copy' => '']],
                'thumbnail_226x226' => ['type' => 'image', 'params' => ['resize' => 'max', 'x' => 226, 'y' => 226, 'source' => 'image']],
                'image_main_case' => ['label' => 'Баннерное изоображение внутри продукта', 'type' => 'image', 'params' => ['resize' => 'max', 'x' => 1160, 'y' => 640]],
                'standard_solutions' => ['label' => 'Стандратные решения', 'type' => 'wysiwyg'],
                'to_main' => ['label' => 'Вывести на главную'],
            ])
            ->setTabSchema([
            ])
            ->setNeighborhood(['item_rubric_id'])
            ->paginateLimit(12);
        $this->belongsTo('ItemRubrics');
        $this->belongsToMany('Params');
        $this->hasMany('AttachmentImages', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Items']]);

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'content', 'standard_solutions', 'image_main_case', 'meta_title', 'meta_description', 'meta_keywords']
        ]);

    }

    public function beforeFind(Event $event, Query $query, \ArrayObject $options) {
        $is_admin = \Cake\Core\Configure::read('admin');
        return $query->mapReduce(function ($result, $key, $mapReduce) use ($options, $is_admin) {
            if ($result instanceof \Cake\Datasource\EntityInterface) {
                if (!$is_admin) {
                    $category = Router::getRequest()->getParam('category');
                    if ($c = $result->get('item_rubric') and isset($c['slug'])) {
                        $category = $c['slug'];
                    }
                    if ($result->has('slug')) {
                        $url = ['controller' => 'Items', 'action' => 'view', 'category' => $category, 'slug' => $result->get('slug')];
                        if (isset($options['brand'])) {
                            $url['brand'] = $options['brand'];
                        }
                        $result->set('_url', Router::url($url));
                    }
                }
                if ($result->get('params')) {
                    $selected = [];
                    foreach ($result->get('params') as $param) {
                        $joinData = array_map('trim', explode('|', $param->get('_joinData')['value']));
                        $values = [];
                        foreach (array_map('trim', preg_split('~\r|\n~', trim($param->get('options')), -1, PREG_SPLIT_NO_EMPTY)) as $subject) {
                            if ((bool) preg_match('~^(\d+)\s(.+)\s?(\*)?$~U', $subject, $match) and in_array($match[1], $joinData)) {
                                $values[$match[1]] = $match[2];
                            }
                        }
                        $param->set('_values', $joinData);
                        $param->set('_data', $values);
                    }
                }
            }
            $mapReduce->emit($result, $key);
        });
    }

}
