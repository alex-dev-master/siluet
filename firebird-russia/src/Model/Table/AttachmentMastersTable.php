<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentImages Model
 *
 * @method \App\Model\Entity\AttachmentImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AttachmentMastersTable extends Table
{
    use \SiluetCms\Traits\TableTrait;


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachment_masters');
        $this->setDisplayField('caption')
            ->setDisplayImage('image')
            //->newPositionLast(true)
            ->setOrder(['AttachmentMasters.position' => 'DESC', 'AttachmentMasters.id' => 'DESC'])
            ->setNeighborhood([]);
        $this
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'crop', 'x' => 240, 'y' => 240]],
                'post' => ['label' => 'Должность'],
            ]);
        $this->setPrimaryKey('id');

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'post', 'content']
        ]);
    }

}
