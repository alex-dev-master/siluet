<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use \SiluetCms\Controller\AutomateTrait;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 *
 * @property \SiluetCms\Controller\Component\ClientComponent $Client
 */
class AppController extends Controller
{
    use AutomateTrait;

    public $components = [
        'SiluetCms.Client'
    ];

    public function initialize()
    {
        parent::initialize();
        $this->run();
    }

    /**
     * Common method to getting list of records
     *
     * <code>
     * parent::index($rubric_id, function ($query) {<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;$query->select(['id', 'slug', 'caption'])->contain(['Tags']);<br>
     * });
     * </code>
     *
     * You can use callback function in first argument
     *
     * <code>
     * parent::index(function ($query) {<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;$query->select(['id', 'slug', 'caption']);<br>
     * });
     * </code>
     *
     * @param string|callable|null $rubric_id ID or Slug for rubric model if Table has RubricTable
     * @param callable|null $callback A callback function to customize Query object
     * @return null
     * @throws \Cake\Http\Exception\NotFoundException
     */
    public function index($rubric_id = null, $callback = null)
    {
        $this->Client->index($rubric_id, $callback);
    }

    /**
     * Common method for get record by its ID or slug
     *
     * <code>
     * parent::view($slug);
     * </code>
     *
     * or you can use callback to customize <i>\Cake\ORM\Query</i> object before execute
     *
     * <code>
     * parent::view($slug, function ($query) {<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;$query->where(['active' => 1])->clearContain();<br>
     * });
     * </code>
     *
     * You can use callback function in first argument. Keep in mind that <i>$query</i> will be reset to default state.
     *
     * <code>
     * parent::view(function () {<br>
     * &nbsp;&nbsp;&nbsp;&nbsp;$query->where(['id' => 1])->select(['id', 'caption']);<br>
     * });
     * </code>
     *
     * @param string|callable|null $slug ID or Slug of record
     * @param callable|null $callback A callback function to customize the Query object
     * @return null
     * @throws \Cake\Http\Exception\NotFoundException
     */
    public function view($slug = null, $callback = null)
    {
        $this->Client->view($slug, $callback);
    }

    public function responseJson($data) {
        $response = $this->response;
        $response = $response->withType('application/json')
            ->withStringBody($data);
        return $response;
    }

    public function sendCakeEmail($emails, $subject, $template, $data, $from)
    {
        $Email = new \Cake\Mailer\Email();
        $Email
            ->setTo($emails)
            ->setFrom($from)
            ->setSubject($subject . ' на сайте ' . env('SERVER_NAME'))
            ->setTemplate($template)
            ->setEmailFormat('both')
//            ->setTransport('default')
            ->setViewVars(compact('data'))
            ->send();
    }

    public function sendCakeEmailFiles($emails, $subject, $template, $data, $from)
    {
        $Email = new \Cake\Mailer\Email();
        $Email
            ->setTo($emails)
            ->setFrom($from)
            ->setSubject($subject . ' на сайте ' . env('SERVER_NAME'))
            ->setTemplate($template)
            ->setEmailFormat('both')
            ->setViewVars(compact('data'));
        if (!empty($data['Files'])) {
            foreach ($data['Files'] as $file) {
                $arr[$file['name']] = [
                    'file' => $file['tmp_name'],
                    'mimetype' => $file['type'],
                ];
            }
            $Email->setAttachments($arr);
        }
        $Email->send();
    }

}
