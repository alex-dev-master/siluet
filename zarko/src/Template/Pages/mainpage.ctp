<?php use Cake\Collection\Collection;?>
<!-- header -->
<?= $this->element('MainPage/header')?>
<!-- header end -->

<!-- slider -->
<div class="wellcome">
    <div class="wellcome-slider">
        <!-- slider item -->
        <?php
        (new Collection($slider))
            ->filter(function ($slider) {
                return $slider->get('image-uploaded');
            })
            ->each(function ($slider) {
//                echo $this->Html->div('slick-one', $this->Html->image($slider->get('image-uploaded')['url-time']), ['escape' => false]);
                    echo $this->element('slider-items', compact('slider'));

        });
        ?>
        <!-- slider item end -->
    </div>
    <div class="wellcome-slider__controls">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-12 col-md-3">
                    <div class="prev">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 26.4 8" xml:space="preserve"><path fill="#231F20" d="M3.7,7.7c0.4,0.4,1,0.4,1.4,0s0.4-1,0-1.4L3.8,5h21.6c0.6,0,1-0.4,1-1s-0.4-1-1-1H3.8l1.3-1.3 c0.4-0.4,0.4-1,0-1.4C4.9,0.1,4.7,0,4.4,0S3.9,0.1,3.7,0.3L0,4L3.7,7.7z"/></svg>
                    </div>
                    <div class="count">2/5</div>
                    <div class="next">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 26.4 8" xml:space="preserve"><path fill="#231F20" d="M22.7,0.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L22.6,3H1C0.4,3,0,3.4,0,4s0.4,1,1,1h21.6l-1.3,1.3 c-0.4,0.4-0.4,1,0,1.4C21.5,7.9,21.7,8,22,8s0.5-0.1,0.7-0.3L26.4,4L22.7,0.3z"/></svg>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- slider end -->

<!-- footer -->
<?= $this->element('MainPage/footer')?>
<!-- footer end -->
