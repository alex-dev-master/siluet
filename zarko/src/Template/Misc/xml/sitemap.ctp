<?= '<?xml version="1.0" encoding="utf-8"?>';?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'mainpage'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>1</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Abouts', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Services', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach($services as $service) { ?>
        <url>
            <loc><?php echo $this->Url->build(['controller' => 'Services', 'action' => 'view', $service['slug']], true); ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Experiences', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <?php foreach($experiences as $experience) { ?>
        <url>
            <loc><?php echo $this->Url->build(['controller' => 'Experiences', 'action' => 'view', $experience['slug']], true); ?></loc>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>
    <?php } ?>
    <url>
        <loc><?php echo $this->Url->build(['controller' => 'Contacts', 'action' => 'index'], true); ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
</urlset>
