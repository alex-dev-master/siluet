<div class="page-top">
<div class="wrapper">
    <!--<p class="subheading">О компании</p>-->
    <h1 class="h1-big">Новости</h1>
</div>
</div>
<div class="new-one content">
    <div class="wrapper">
        <p class="news-one__date"><?=$news['published']->format('d.m.Y')?></p>
        <div class="news-one__text  text">
            <h3 class="title"><?=$news['caption']?></h3>
            <p class="accent"><?=$news['preview']?></p>
            <?=$news['content']?>
        </div>
    </div>

</div>
<?php if (!empty($news['attachment_images'])) { ?>
<div class="news-slider-block">
    <div class="wrapper">
        <div class="news-slider">
         <?php foreach ($news['attachment_images'] as  $slide) { ?>
        <div class="slide">
                <a rel="group" href="/files/attachment-images/<?=$slide['image']?>" class="fancybox">
                    <img src="/files/attachment-images/<?=$slide['thumbnail_170x170']?>">
                </a>
            </div>
         <?php } ?>
        </div>
        <div class="news-slider__nav">
            <a href="#" class="news-slider-left"><img src="/img/slide-left.png"></a>
            <a href="#" class="news-slider-right"><img src="/img/slide-right.png"></a>
        </div>
    </div>
</div>
<?php } ?>
<div class="news-btn">
    <div class="wrapper">
        <a href="<?=$this->Url->build(['controller' => 'News', 'action' => 'index'])?>" class="news__all-btn"><?=__('Все новости');?></a>
    </div>
</div>
