<div class="page-top">
    <div class="wrapper">
        <h1 class="h1-big"><?=__($project_page['caption'])?></h1>
    </div>
</div>
<div class="content">
    <div class="wrapper">
        <div class="project-top">
            <div class="row">
                <div class="col-md-5 col-sm-5 col-lg-4">
                    <div class="project-numbers">
                        <div class="project-numbers__item">
                            <p class="count project-spincrement"><?=__($project_page['completed'])?></p>
                            <p class="title"><?=$project_page['content_numbers1']?></p>
                        </div>
                        <div class="project-numbers__item">
                            <p class="count project-spincrement"><?=$project_page['in_work']?></p>
                            <p class="title"><?=$project_page['content_numbers2']?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7 col-lg-8">
                    <div class="project-text">
                        <?=$project_page['content']?>
                    </div>
                </div>
            </div>
        </div>
        <div class="project-filter">
            <div class="left-col">
               <!-- <p> <?=__('Показаны')?>:
                    <span>
                        <?php
                        $all_projects =false;
                        foreach ($services as $service) {
                        if (isset($_GET['category']) && $service['slug']==$_GET['category']) {
                            echo $service['caption'];
                            $all_projects =true;
                        }
                        }
                        if (!$all_projects) {
                            echo __('Все проекты');
                        }?>
                    </span>
                </p> -->
                <form method="get" id="status_form">
                    <select name="status">
                        <option value="undefined" disabled selected><?=__('Стадия реализации')?></option>
                        <option value="undefined"><?=__('Все проекты')?></option>
                        <option value="notarchive" <?php if (isset($_GET['status_objects']) && 'notarchive'==$_GET['status_objects']) echo 'selected';?>><?=__('В работе')?></option>
                        <option value="archive" <?php if (isset($_GET['status_objects']) && 'archive'==$_GET['status_objects']) echo 'selected';?>><?=__('Завершен')?></option>
                    </select>
                </form>
            </div>
            <div class="right-col">
                <form method="get" id="category_form">
                <select name="category">
                    <option value="undefined" disabled selected><?=__('Выбрать категорию')?></option>
                    <option value="undefined"><?=__('Все категории')?></option>
                    <?php foreach ($services as $service) { ?>
                        <?php if (!empty($service['objects'])) { ?>
                    <option value="<?=$service['slug']?>" <?php if (isset($_GET['category']) && $service['slug']==$_GET['category']) echo 'selected';?> ><?=$service['caption']?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
                </form>
            </div>
        </div>
        <div class="catalog-objects">
        <div class="project-list">
            <div class="row list">
                <?php
                $str = '?';
                if (!empty($category)) {
                    $str = $str."category=$category&";
                }
                if (!empty($status_objects)) {
                    $str = $str."status_objects=$status_objects&";
                }
                if (!empty($_GET['page'])) {
                    $pageGet = $_GET['page'];
                    $str = $str."page=$pageGet";
                }
                if ($str == '?') $str = '';
                ?>
                <?php foreach ($objects as $object) { ?>
                <div class="col-md-4 col-sm-6 col-ss-6 col-xs-12 project-col">
                    <div class="project-item">
                        <a href="<?= $this->Url->build(['controller' => 'Projects', 'action' => $object['slug']]); ?><?=$str?>">
                            <div class="project-item-img-container">
                                <img src="/files/objects/<?=$object['image']?>">
                                <p class="status <?php if ($object['archive_status']){echo 'finish';}else{echo 'work';}?>"><?php if ($object['archive_status']) { echo __('Завершен'); } else { echo __('В работе'); } ?></p>
                            </div>
                            <div class="project-item__text">
                                <p class="title"><?=$object['caption']?></p>
                                <p class="count"><?=$object['place']?></p>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?= $this->element('pagination') ?>
        </div>
    </div>
</div>
