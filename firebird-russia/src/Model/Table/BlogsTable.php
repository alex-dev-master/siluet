<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use function PHPSTORM_META\type;

/**
 * Pages Model
 *
 * @method \App\Model\Entity\Page get($primaryKey, $options = [])
 * @method \App\Model\Entity\Page newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Page[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Page|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Page patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Page[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Page findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BlogsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('blogs')
            ->setDisplayField('caption')
            ->setPrimaryKey('id')
            ->addBehavior('Timestamp')
            ->setOrder(['Blogs.published' => 'DESC']);
        $this->setDisplayImage('image');

        $this
            ->setFormSchema([
                'image' => ['params' => ['resize' => 'resize', 'x' => 720, 'y' => 370]],
                'thumbnail_960x510' => ['tab' => 'image', 'label' => 'Внутреннее изоображение', 'type' => 'image', 'params' => ['resize' => 'resize', 'x' => 960, 'y' => 510]],
                'preview' => ['label' => 'Краткое описание', 'type' => 'wysiwyg'],
                'to_sidebar' => ['label' => 'Выделить справа'],
                'to_items' => ['label' => 'Заголовок к товарам','tab' => 'habtm'],

            ])
            ->setTabSchema([
            ]);

        $this->belongsToMany('Items');

        $this->addBehavior('Translate', ['fields' =>
            ['caption', 'preview', 'content', 'to_items', 'meta_title', 'meta_description', 'meta_keywords']
        ]);
    }
}
