<div class="page-content about-content about-jobs-content">
    <img class="content-bg" src="/files/pages/<?=$row['thumbnail_1440x540']?>" alt="">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('О клинике'),
            ['controller' => 'Abouts', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$row['caption']?></h1>
        <?php foreach ($row['attachment_jobs'] as $job) { ?>
        <div class="job-item">
            <h5 class="job-title"><?=$job['caption']?></h5>
            <a class="btn job-btn" data-spec="<?=$job['caption']?>" href="#job-modal" data-toggle="modal">Откликнуться</a>
            <ul class="list-ul">
                <li><span class="salary"><?=$job['cost']?></span></li>
                <?=$job['content']?>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>
