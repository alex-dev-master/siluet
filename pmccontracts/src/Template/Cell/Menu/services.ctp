
<?php foreach ($menus as $menu) {
    $actionName = $this->request->getParam('slug');
    ?>
    <?php if (isset($menu['submenu'])) { ?>
        <?php foreach ($menu['submenu'] as $sidebar) { ?>
            <li class="<?php if ($sidebar['url']['action'] == $actionName) { echo 'active'; } ?>"><a href="<?= $this->Url->build($sidebar['url']); ?>"><?= $sidebar['caption'] ?></a></li>
        <?php } ?>
    <?php } ?>
<?php } ?>

