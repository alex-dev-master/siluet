<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @method \App\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contact findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
                ->setTable('contacts')
                ->setDisplayField('caption')
                ->setPrimaryKey('id');

        $this->setFormSchema([
                'map_caption' => ['label' => 'Адрес', 'tab' => 'map'],
                'requisites_text' => ['label' => 'Текст', 'type' => 'wysiwyg', 'tab' => 'requisites'],
                'requisites_file' => ['label' => 'Файл реквизитов для скачивания', 'type' => 'file', 'tab' => 'requisites']
            ]);

        $this->setTabSchema([
                'requisites' => ['label' => 'Реквизиты']
            ]);
    }
}
