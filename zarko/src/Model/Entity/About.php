<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


class About extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'caption' => true,
        'content' => true,
        'content_bottom' => true,
        'image' => true,
        'video' => true,
        'advantages_title_1' => true,
        'advantages_title_2' => true,
        'advantages_title_3' => true,
        'advantages_title_4' => true,
        'certificate_title' => true,
        'certificate_text' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'modified' => true
    ];
}
