<?php
/**
 * settings block
 */
if (isset($settings)) {
    foreach (['code_bh', 'code_ab', 'code_bb'] as $prop) {
        $this->start($prop);
        echo $settings->get($prop);
        $this->end();
    }
}
$this->Html->css('slick/slick-theme', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/jquery-ui', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/daterangepicker', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('slick/slick', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('foundation/selectric', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('style', ['type' => 'text/css', 'media' => 'all', 'block' => true]);

$this->append('js', $this->Html->script('jquery-3.2.1.min'));
$this->append('js', $this->Html->script('jquery-ui'));
$this->append('js', $this->Html->script('jquery.justified.min'));
$this->append('js', $this->Html->script('bootstrap.min'));
$this->append('js', $this->Html->script('jquery.maskedinput'));
$this->append('js', $this->Html->script('jquery.scrollbar'));
$this->append('js', $this->Html->script('jquery.justifiedGallery'));
$this->append('js', $this->Html->script('justifiedGallery'));
$this->append('js', $this->Html->script('moment.min'));
$this->append('js', $this->Html->script('truncate'));
$this->append('js', $this->Html->script('jquery.selectric'));
$this->append('js', $this->Html->script('slick.min'));
$this->append('js', $this->Html->script('jquery.timepicker.min'));
$this->append('js', $this->Html->script('daterangepicker'));
$this->append('js', $this->Html->script('truncate.min'));
$this->append('js', $this->Html->script('jquery-ready'));
$this->append('js', $this->Html->script('common'));

?>
<!DOCTYPE html>
<html>
<head>
    <title>Error 404</title>
    <?php if (isset($description_for_layout) and !empty($description_for_layout)) {
        echo $this->Html->meta('description', $description_for_layout);
    } ?>
    <?php if (isset($keywords_for_layout) and !empty($keywords_for_layout)) {
        echo $this->Html->meta('keywords', $keywords_for_layout);
    } ?>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($href = $settings->get('favicon-uploaded')) { ?>
        <?= $this->Html->meta('icon', $href['url']) ?>
    <?php } ?>

    <?= $this->fetch('css') ?>
    <link href="/css/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>

    <?= $this->fetch('code_bh') ?>
</head>
<body>
<?= $this->fetch('code_ab') ?>
<div class="overlay"></div>
<!--a id="back-top" href="#top"></a-->
<?= $this->element('header', [$lang, $langs, $settings])?>

<?= $this->fetch('content') ?>
<?= $this->element('footer', [$lang, $langs, $settings])?>
<?= $this->fetch('js') ?>
<script type="text/javascript" src="/css/fancybox/jquery.fancybox.js"></script>
<?= $this->fetch('code_bb') ?>
</body>
</html>
