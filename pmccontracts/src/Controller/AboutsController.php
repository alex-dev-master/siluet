<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AboutsController extends AppController
{
    public function index($rubric_id = null, $query = [], $merge_query = true)
    {
        $about = TableRegistry::getTableLocator()->get('Abouts')->find()->first();
        $this->Title->setMeta($about, 'Abouts');
        $this->set(compact('about'));
    }

    public function history()
    {
        $about = TableRegistry::getTableLocator()->get('Abouts')->find()->where(['Abouts.id'=> 2])->first();
        $this->Title->setMeta($about, 'Abouts');
        $this->set(compact('about'));
    }

    public function team()
    {
        $about = TableRegistry::getTableLocator()->get('Abouts')->find()->where(['Abouts.id'=> 3])->first();
        $executives = TableRegistry::getTableLocator()->get('Executives')->find()->toArray();
        $this->Title->setMeta($about, 'Abouts');
        $this->set(compact('about', 'executives'));
    }

    public function experts()
    {
        $about = TableRegistry::getTableLocator()->get('Abouts')->find()->where(['Abouts.id'=> 4])->first();
        $experts = TableRegistry::getTableLocator()->get('Experts')->find()->toArray();
        $this->Title->setMeta($about, 'Abouts');
        $this->set(compact('about', 'experts'));
    }

    public function reviews()
    {
        $about = TableRegistry::getTableLocator()->get('Abouts')->find()->where(['Abouts.id'=> 5])->first();
        $reviews = TableRegistry::getTableLocator()->get('Reviews')->find()->toArray();
        $this->Title->setMeta($about, 'Abouts');
        $this->set(compact('about', 'reviews'));
    }

}
