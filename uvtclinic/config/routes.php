<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\{
    Plugin,
    Configure
};
use Cake\Routing\ {
    RouteBuilder,
    Router,
    Route\DashedRoute
};

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

/*Router::scope('/admin', ['prefix' => 'admin'], function (RouteBuilder $routes) {
	$routes->connect('/:controller')->setPatterns(['controller' => '[a-z0-9_]+']);
});*/
if (!$languages = Configure::read('Project.languages')) {
    $defaultLocale = Configure::read('App.defaultLocale');
    $languages = [\Locale::parseLocale($defaultLocale)['language'] => ['prefix' => false, 'locale' => $defaultLocale]];
}
foreach ($languages as $options) {
    $lang = $options['prefix'];
    Router::scope('/' . $lang, ['lang' => $lang], function (RouteBuilder $routes) {
        /**
         * Here, we are connecting '/' (base path) to a controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, src/Template/Pages/home.ctp)...
         */
        $routes->scope('/send-question',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendQuestions']);
        });

        $routes->scope('/sendReviews',  ['controller' => 'Reviews'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendReviews']);
        });

        $routes->scope('/sendCallModal',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendCallModal']);
        });

        $routes->scope('/sendAppointment',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendAppointment']);
        });

        $routes->scope('/sendTimetable',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendTimetable']);
        });

        $routes->scope('/sendMailModal',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendMailModal']);
        });

        $routes->scope('/sendJob',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendJob']);
        });

        $routes->scope('/sendQuestion',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendQuestion']);
        });

        $routes->scope('/sendContactForm',  ['controller' => 'Contacts'],  function (RouteBuilder $routes) {
            $routes->setExtensions(['json']);
            $routes->connect('/', ['action' => 'sendContactForm']);
        });

        $routes->connect('/', ['controller' => 'Pages', 'action' => 'mainpage']);
        $routes->connect('/abouts', ['controller' => 'Abouts']);
        $routes->connect('/experts', ['controller' => 'Pages', 'action' => 'experts']);
        $routes->connect('/experts/:slug', ['controller' => 'Experts', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
        $routes->connect('/timetable', ['controller' => 'Pages', 'action' => 'timetable']);
        $routes->connect('/costs', ['controller' => 'Pages', 'action' => 'costs']);
        $routes->connect('/questionnaire', ['controller' => 'Pages', 'action' => 'questionnaire']);
        $routes->connect('/reviews', ['controller' => 'Pages', 'action' => 'reviews']);
        $routes->connect('/politics', ['controller' => 'Pages', 'action' => 'politics']);


        $routes->scope('/abouts', function (RouteBuilder $routes) {
            $routes->connect('/about-medical-activities', ['controller' => 'Pages', 'action' => 'aboutMedicalActivities']);
            $routes->connect('/regulations', ['controller' => 'Pages', 'action' => 'regulations']);
            $routes->connect('/order-rules', ['controller' => 'Pages', 'action' => 'orderRules']);
            $routes->connect('/rights-obligations', ['controller' => 'Pages', 'action' => 'rightsObligations']);
            $routes->connect('/jobs', ['controller' => 'Pages', 'action' => 'jobs']);
            $routes->connect('/gallery', ['controller' => 'Pages', 'action' => 'gallery']);
        });

        $routes->scope('/services', function (RouteBuilder $routes) {
            $routes->connect('/', ['controller' => 'Pages', 'action' => 'services']);
            $routes->connect('/rubric/:slug', ['controller' => 'ItemRubrics', 'action' => 'index'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
            $routes->connect('/item/:slug', ['controller' => 'Items', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
        });

        $routes->scope('/articles', function (RouteBuilder $routes) {
            $routes->connect('/', ['controller' => 'Pages', 'action' => 'articles']);
            $routes->connect('/rubric/:slug', ['controller' => 'ArticleRubrics', 'action' => 'index'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
            $routes->connect('/item/:slug', ['controller' => 'Articles', 'action' => 'view'])->setPass(['slug'])->setPatterns(['slug' => '[\w\-]+']);
        });

        $routes->connect('/sitemap', ['controller' => 'Misc', 'action' => 'sitemap']);
        $routes->connect('/sitemap.xml', ['controller' => 'Misc', 'action' => 'sitemap', 'xml' => true]);



        /**
         * Connect catchall routes for all controllers.
         *
         * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
         *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
         *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
         *
         * Any route class can be used with this method, such as:
         * - DashedRoute
         * - InflectedRoute
         * - Route
         * - Or your own route class
         *
         * You can remove these routes once you've connected the
         * routes you want in your application.
         */
        $routes->fallbacks(DashedRoute::class);
    });
}

Router::addUrlFilter(function ($params, $request) {
    if (!isset($params['lang'])) {
        $params['lang'] = null;
    }
    if (!$request instanceof \Cake\Http\ServerRequest) {
        return $params;
    }
    if ($request->getParam('lang') and $params['lang'] !== false) {
        $params['lang'] = $request->getParam('lang');
    }
    return $params;
});
/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
