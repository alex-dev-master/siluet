<div class="page-top">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            $rubrics_items['caption'],
            ['controller' => 'Portfolios', 'action' => 'index', $rubrics_items['slug']]
        );
        $cat_prod = \Cake\ORM\TableRegistry::getTableLocator()->get('Portfolios')->find()->first();
        $this->Breadcrumbs->prepend(
//            __('Наша продукция'),
            $cat_prod['caption'],
            ['controller' => 'Portfolios', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$item['caption']?></h1>
        <div class="main-image portfolio-image">
            <img src="/files/items/<?=$item['image_main_case']?>">
            <div class="order-block">
                <span class="price">₽ <?=$item['price']?></span>
                <a href="#order-modal" style="background: #<?=$rubrics_items['color']?>"  data-toggle="modal" class="btn arr-btn"><?=__('Заказать')?></a>
            </div>
        </div>
    </div>
</div>
<div class="portfolio__top portfolio">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6 col-sm-6 portfolio__img-col">

                <div class="sol_slider-block">
                    <div class="sol_slider">
                        <?php foreach ($item['attachment_images'] as $attachment_image) { ?>
                        <div class="slide_one">
                            <img class="slide_img" src="/files/attachment-images/<?=$attachment_image['thumbnail_560x560']?>" alt="">
                        </div>
                        <?php } ?>
                    </div>
                    <div class="sol_slider__nav">
                        <a href="#" class="sol_slider-left"><img src="/img/jumb-arr-l.png"> </a>
                        <a href="#" class="sol_slider-right"><img src="/img/jumb-arr-r.png"> </a>
                    </div>
                </div>
                <div class="ctrl">

                    <div class="sol_slider-other">
                        <?php foreach ($item['attachment_images'] as $k=>$attachment_image) { ?>
                        <div class="slide_one">
                            <a href="javascript:void();" data-n="<?=$k?>">
                                <?php if (!empty($attachment_image['thumbnail_100x100'])) { ?>
                                    <img src="/files/attachment-images/<?=$attachment_image['thumbnail_100x100']?>"/>
                                <?php } else { ?>
                                    <img src="/files/attachment-images/<?=$attachment_image['thumbnail_560x560']?>"/>
                                <?php } ?>
                            </a>
                        </div>
                        <?php } ?>
                    </div>

                </div>
                <div class="clear"></div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="portfolio__info">
                    <p class="title"><?=$item['caption']?></p>
                    <span class="price">₽ <?=$item['price']?></span>
                    <a href="#order-modal" style="background: #<?=$rubrics_items['color']?>"  data-toggle="modal" class="btn arr-btn portf-btn"><?=__('Заказать')?></a>
                    <div class="text">
                        <?=$item['content']?>
                    </div>
                    <div class="charac">
                        <ul>
                            <?php foreach ($item['params'] as $param) { ?>
                            <li>
                                <p class="title"><?=$param['caption']?></p>
                                <p><?= implode(", ", $param['_data']); ?></p>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio__zagotovka">
    <div class="wrapper">
        <h2 class="h2-sml"><?=__('Сделаем на любой заготовке')?></h2>
        <div class="portfolio__slider-block">
            <div class="portfolio__slider">
                <?php foreach ($rubrics_items['attachment_blanks'] as $attachment_blank) { ?>
                <div class="slide">
                    <div class="img-container">
                        <img src="/files/attachment-blanks/<?=$attachment_blank['image']?>">
                    </div>
                    <div class="text">
                        <p><?=$attachment_blank['caption']?></p>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="portfolio__slider__nav">
                <a href="#" class="slide-nav-arrow left"><img src="/img/slide__arr-l.png"> </a>
                <a href="#" class="slide-nav-arrow right"><img src="/img/slide__arr-r.png"> </a>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($item['standard_solutions'])) { ?>
<div class="portfolio__text">
    <div class="wrapper">
        <h2 class="h2-sml">
            <?=__('Стандартные решения')?>
        </h2>
        <div class="text styled-center-text">
            <?=$item['standard_solutions']?>
        </div>
        <div class="center">
            <a  href="#order-modal" data-toggle="modal" class="btn want-btn" style="background: #<?=$rubrics_items['color']?>" > <?=__('Хочу также!')?></a>
        </div>
    </div>
</div>
<?php } ?>
<div class="how-works portfolio__how-works">
    <div class="wrapper">
        <h2><?=__('Как мы работаем?')?></h2>
        <div class="how-works__list">
            <div class="row list">
                <?php foreach ($works as $work) { ?>
                <div class="item">
                    <div class="img-container">
                        <img src="/files/works/<?=$work['image_icon']?>">
                    </div>
                    <div class="text">
                        <p><?=$work['caption_3']?></p>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>
<div id="order-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create(['schema' => []], ['id' => 'order-modal-form', 'class' => '']); ?>
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="/img/form-close.png"></button>
                <h4 class="modal-title"><?=__('Заявка на расчет')?></h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <input type="hidden" name="towar" value="<?=$item['caption']?>">
                <label for="name2"><?=__('ФИО')?><span>*</span></label>
                <input class="modal-input" id="name2" name="name">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Телефон')?><span>*</span></label>
                <input class="modal-input phone" id="organization2" name="phone">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2">E-mail<span>*</span></label>
                <input class="modal-input" id="organization2" name="email">
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>
                <label for="organization2"><?=__('Комментарий')?><span>*</span></label>
                <textarea name="msg"></textarea>
                <p class="error-message" style="display: none"><?=__('Это поле обязательно для заполнения')?></p>

                <label for="msg"><?=__('Файлы (не более 50Мб)')?></label>

                <div class="files">
                    <div class="file-input">
                        <div class="file_upload">
                            <div class="file-name"><?=__('Файл не выбран')?></div>
                            <button><?=__('Обзор')?></button>
                            <input type="file">
                        </div>
                        <a class="delete-place"><img src="/img/delete.png"> </a>
                    </div>
                    <a href="#" class="add-more__btn"><?=__('Добавить файл')?></a>
                </div>

            </div>
            <div class="modal-footer">
                <div class="btns">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" value="Заказ в портфолио">
                            <label class="checkbox">
                                <input type="checkbox" name="privacy-policy">
                                <div class="checkbox__text"><?=__('Я принимаю')?> <a href="<?= $this->Url->build(["controller" => "Pages","action" => "politics"])?>"><?=__('Политику конфиденциальности')?></a></div>
                            </label>
                        </div>
                        <div class="col-md-12">
                            <button data-tab="#success_order-modal-form2" class="btn btn-modal"><?=__('Отправить')?></button>
                        </div>
                    </div>
                </div>
                <img src="/img/doll.png" class="modal-doll">
                <h4 id="success_order-modal-form2" style="display: none; color: green;" class="success" ><?=__('Ваша заявка успешно отправлена!')?></h4>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
