<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('caption')
                ->setPrimaryKey('id')
                ->setOrder(['Items.position' => 'desc', 'Items.id' => 'desc'])
                ->setNeighborhood(['item_rubric_id'])
                ->paginateLimit(20);

        $this->setFormSchema([
            'image' => ['params' => ['copy' => '']],
            'thumbnail_470x270' => ['type' => 'image','params' => ['resize' => 'crop', 'x' => 470, 'y' => 270, 'source' => 'image']],
            'thumbnail_1170x480' => ['type' => 'image','params' => ['resize' => 'crop', 'x' => 1170, 'y' => 480, 'source' => 'image']],
            'content_full' => ['label' => 'Полное описание', 'tab' => 'inner_content'],
            'content_not_full' => ['label' => 'Краткое описание', 'tab' => 'inner_content'],
            'content_advantages' => ['label' => 'Блок с преимуществами', 'type' => 'wysiwyg', 'tab' => 'inner_content'],
            'content' => ['label' => 'Краткое содержимое', 'type' => 'wysiwyg'],

        ]);

        $this->setTabSchema([
            'inner_content' => ['label' => 'Внутреннее описание'],
            'attachment_infos' => ['label' => 'Полезно знать']
        ]);



        if ($config['alias'] == 'LinkedItem') {
            $this->setOrder([
                'LinkedItem.position' => 'DESC'
            ]);
        }

        $this->belongsTo('ItemRubrics');
        $this->hasMany('AttachmentImages', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentImages.model' => 'Items']]);
        $this->hasMany('AttachmentInfos', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentInfos.model' => 'Items']]);
//        $this->hasMany('AttachmentPictures', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['model' => 'Items']]);
        $this->belongsToMany('Articles');
        $this->belongsToMany('Experts');
//        $this->belongsToMany('Params');
//
//        $this->belongsToMany('LinkedItem', [
//            'className' => 'Items',
//            'targetForeignKey' => 'linked_item_id',
//            'propertyName' => 'linked_items',
//            'joinTable' => 'items_items'
//        ]);

//        $this->addBehavior('Translate', ['fields' => ['caption', 'content', 'meta_title', 'meta_description']]);
    }
}
