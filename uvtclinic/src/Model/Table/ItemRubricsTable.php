<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemRubrics Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\HasMany $Items
 *
 * @method \App\Model\Entity\ItemRubric get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemRubric newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemRubric[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemRubric patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemRubric findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemRubricsTable extends Table
{
    use \SiluetCms\Traits\TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this
                ->setTable('item_rubrics')
                ->setDisplayField('caption')
                ->setPrimaryKey('id')
                ->setOrder(['ItemRubrics.position' => 'DESC', 'ItemRubrics.id' => 'DESC'])
                ->setFormSchema([
                    'thumbnail' => ['params' => ['resize' => 'crop', 'x' => 100, 'y' => 100]],
                    'image' => ['params' => ['resize' => 'crop', 'x' => 170, 'y' => 170]],
                    'image_2' => ['label' => 'Изоображение при наведении', 'type' => 'image','params' => ['resize' => 'crop', 'x' => 170, 'y' => 170]],
                    'image_inner' => ['label' => 'Внутреннее изоображение', 'type' => 'image','params' => ['resize' => 'crop', 'x' => 1440, 'y' => 500]],
                    'content_advantages' => ['label' => 'Блок с преимуществами', 'type' => 'wysiwyg'],
                    'caption_sub' => ['label' => 'Название в родительном падеже (для заголовка таблицы стоимости)'],
                ]);
        $this->setTabSchema([
            'attachment_costs' => ['label' => 'Стоимость лечения'],
            'attachment_infos' => ['label' => 'Полезно знать']
        ]);

        $this->addBehavior('Tree');
        $this->addBehavior('Timestamp');


        $this->hasMany('Items', [
            'foreignKey' => 'item_rubric_id'
        ]);
        $this->hasMany('AttachmentCosts', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentCosts.model' => 'ItemRubrics']]);
        $this->hasMany('AttachmentInfos', ['dependent' => true, 'foreignKey' => 'foreign_key', 'conditions' => ['AttachmentInfos.model' => 'ItemRubrics']]);
        $this->belongsToMany('Articles');
        $this->belongsToMany('Experts');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    /*public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 100)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->scalar('caption')
            ->maxLength('caption', 255)
            ->requirePresence('caption', 'create')
            ->notEmpty('caption');

        $validator
            ->integer('position')
            ->allowEmpty('position');

        $validator
            ->boolean('disabled')
            ->requirePresence('disabled', 'create')
            ->notEmpty('disabled');

        return $validator;
    }*/
}
