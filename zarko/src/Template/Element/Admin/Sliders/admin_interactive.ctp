<?php
use Cake\I18n\I18n;
use Cake\Utility\Inflector;
use Cake\I18n\Number;


$field_description['input']['type'] = 'file';
$attrs = $field_description['input'];
if ($this->request->is('ajax')) {
    $attrs['data-ajax'] = 1;
    $attrs['data-accept'] = 'image/jpeg,image/jpg,image/gif,image/png';
    $attrs['data-name'] = 'upload_' . $field_name;
}
echo $this->Form->input('upload_' . $field_name, $attrs);

//echo '<pre>';
//print_r($row);
//echo '</pre>';

if (isset($row[$field_name . '-uploaded'])) {
    $img = $row[$field_name . '-uploaded'];
    $image_info = getimagesize($img['filename']);
    $width = $image_info[0];
    $height = $image_info[1];
    echo $this->Form->input('delete_' . $field_name, array('label' => __d('admin', 'delete_image_label'), 'type' => 'checkbox'));
    ?>
    <div class="" id="img-container-drag">
        <div id="img-wrapper" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; position: relative;">

            <?php echo $this->Html->image($row[$field_name . '-uploaded']['url']); ?>
        </div>
        <div class="dragElement ui-widget ui-corner-all ui-state-error"
                style="<?php if (isset($row['point_x']) && isset($row['point_y']) ) {echo 'left:'.$row['point_x'].';top:'.$row['point_y'].';';} ?>"></div>
    </div>
    <?php
}
