<div class="page-content about-content about-testimonials-content">
    <div class="wrapper">
        <?php
        $this->Breadcrumbs->prepend(
            __('О клинике'),
            ['controller' => 'Abouts', 'action' => 'index']
        );
        ?>
        <?= $this->element('breadcrumbs') ?>
        <div class="border-top mh-block">
            <h1><?=$row['caption']?></h1>
            <?php if(count($reviews)==0) { ?>
            <p class="no-reviews">Отзызвов пока нет</p>
              <?php } ?>
            <?php foreach ($reviews as $review) { ?>
            <div class="testimonials-item">
                <img class="quot" src="/img/quotes.png" alt="">
                <p class="text"><?=$review['content']?></p>
                <h5 class="name"><?=$review['caption']?></h5>
                <?php if (!empty($review->published)) { ?>
                <p class="date sml-text"><?=$review->published->format('d.m.Y')?></p>
                <?php } else { ?>
                    <p class="date"><?=$review->created->format('d.m.Y')?></p>
                <?php } ?>
            </div>
            <?php } ?>
            <?= $this->element('pagination') ?>
        </div>
    </div>
    <?= $this->Form->create(['schema' => []], ['id' => 'sendReviews', 'class' => 'form']); ?>
    <div class="testimonials-form">
        <div class="wrapper">
            <h3 class="title">Оставьте свой отзыв!</h3>
            <div class="row list">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <label for="name2">ФИО<span>*</span></label>
                    <input class="modal-input required" name="name" id="name2" placeholder="Как к вам обратиться?">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <label for="organization2">Контактные данные<span>*</span></label>
                    <input class="modal-input required" name="phone_email" id="organization2" placeholder="Ваш номер телефона или e-mail">
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <label for="organization2">Текст отзыва<span>*</span></label>
                    <textarea class="modal-input required" name="text" id="organization2" placeholder="Поделитесь вашими мыслями"></textarea>
                    <p class="error-message" style="display: none;"><?=__('Это поле обязательно для заполнения')?></p>
                </div>
            </div>
            <div class="row list">
                <div class="col-md-9 col-sm-9 col-xs-12 ">
                    <label class="checkbox">
                        <input name="privacy-policy" type="checkbox">
                        <div class="checkbox__text">Я принимаю <a href="/politics">Политику
                                конфиденциальности</a></div>
                    </label>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 ">
                    <div class="btns">
                        <button data-tab="#success_msg" class="btn btn-form"><img src="/img/mail.png">Отправить</button>
                    </div>
                </div>
                 <div class="col-md-12 ">
                <h4 id="success_msg" class="success" ><?=__('Форма успешно отправлена!')?></h4>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end(); ?>
</div>
