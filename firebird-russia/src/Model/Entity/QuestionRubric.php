<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemRubric Entity
 *
 * @property int $id
 * @property string $slug
 * @property string $caption
 * @property int $position
 * @property bool $disabled
 *
 * @property \App\Model\Entity\Item[] $items
 */
class QuestionRubric extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'slug' => true,
        'caption' => true,
        'position' => true,
        'disabled' => true,
        'items' => true
    ];
}
