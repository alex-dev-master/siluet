<p><?php echo $this->Html->link('Список', ['controller' => 'ItemRubrics', 'action' => 'index']); ?></p>
<ul>
    <?php foreach ($rows as $row) { ?>
    <li><a href="<?php echo $row->_url; ?>"><?php echo $row->caption; ?></a></li>
    <?php } ?>
</ul>
<?php if ($this->Paginator->hasPages()) { ?>
<?php echo $this->Paginator->numbers(); ?>
<?php } ?>
