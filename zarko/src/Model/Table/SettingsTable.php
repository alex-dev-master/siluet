<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Validation\Validator;
use SiluetCms\Traits\TableTrait;
use Cake\Cache\Cache;

/**
 * Settings Model
 *
 * @method \App\Model\Entity\Setting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Setting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Setting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Setting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Setting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Setting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Setting findOrCreate($search, callable $callback = null, $options = [])
 */
class SettingsTable extends Table
{
    use TableTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settings')
                ->setDisplayField('project_title')
                ->setPrimaryKey('id')
                ->setFormSchema([
                    'email' => ['label'=>'E-mail (на сайте)'],
                    'phone' => ['label'=>'Телефон (на сайте)'],
                    'address' => ['label'=>'Адрес (на сайте)'],
                    'favicon' => ['params' => ['path' => WWW_ROOT, 'name' => 'favicon.ico']]
                ]);
    }
    
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options) {
        Cache::clear(false, 'settings');
    }
}
