<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function edit($id = null) {
        if ($id == 4 || $id == 6 || $id == 5 || $id == 1) {
            parent::edit($id);
            $View = $this->createView();
            $tab_schema = $View->get('tab_schema');
            unset($tab_schema['attachment_images']);
            $schema = $this->Pages->getFormSchema();
            $this->Pages->addBehavior('Translate', ['fields' =>
                ['caption', 'content', 'meta_title', 'meta_description', 'meta_keywords']
            ]);
            $this->Pages->setFormSchema($schema);
            $this->set('tab_schema', $tab_schema);
        }
    }
}
