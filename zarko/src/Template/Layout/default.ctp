<?php
/**
 * settings block
 */
if (isset($settings)) {
    foreach (['code_bh', 'code_ab', 'code_bb'] as $prop) {
        $this->start($prop);
        echo $settings->get($prop);
        $this->end();
    }
}
$this->Html->css('/css/screen-kit', ['type' => 'text/css', 'media' => 'all', 'block' => true]);
$this->Html->css('/css/jquery.fancybox', ['type' => 'text/css', 'media' => 'all', 'block' => true]);

// $this->Html->css('/css/screen-kit.map', ['type' => 'text/css', 'media' => 'all', 'block' => true]);


$this->append('js', $this->Html->script('jquery.3.2.1.min'));
$this->append('js', $this->Html->script('popper.min'));
$this->append('js', $this->Html->script('bootstrap.min'));
$this->append('js', $this->Html->script('slick.min'));
$this->append('js', $this->Html->script('featherlight.min'));
$this->append('js', $this->Html->script('common'));
$this->append('js', $this->Html->script('jquery.selectric.min'));

$this->append('js', $this->Html->script('jquery.fancybox.min'));

$this->append('js', $this->Html->script('jquery-ready'));
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $title_for_layout; ?></title>
    <?php if (isset($description_for_layout) and !empty($description_for_layout)) {
        echo $this->Html->meta('description', $description_for_layout);
    } ?>
    <?php if (isset($keywords_for_layout) and !empty($keywords_for_layout)) {
        echo $this->Html->meta('keywords', $keywords_for_layout);
    } ?>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($href = $settings->get('favicon-uploaded')) { ?>
    <?= $this->Html->meta('icon', $href['url']) ?>
    <?php } ?>

    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#262626">
    <meta name="theme-color" content="#ffffff">
    
    <?= $this->fetch('css') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <?= $this->fetch('code_bh') ?>
</head>
<body>
    <?= $this->fetch('code_ab') ?>

    <div class="page-wrap">
        <!-- header -->
        <?= $this->element('header')?>
        <!-- header end -->
             <?= $this->fetch('content') ?>
        <!-- footer -->
        <?= $this->element('footer')?>
        <!-- /footer -->
    </div>

    <?= $this->fetch('js') ?>
    <?= $this->fetch('code_bb') ?>
</body>
</html>
