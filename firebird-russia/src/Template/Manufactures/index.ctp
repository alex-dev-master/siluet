<div class="page-top">
    <div class="wrapper">
        <?= $this->element('breadcrumbs') ?>
        <h1><?=$manufactures['caption']?></h1>
        <div class="main-image">
            <img src="/files/manufactures/<?=$manufactures['image']?>">
        </div>
        <div class="styled-text text">
            <?=$manufactures['content']?>
        </div>
    </div>
</div>
<div class="productiion">

    <div class="production__list">
        <?php foreach ($manufactures['attachment_manufactures'] as $attachment_manufacture) { ?>
        <div class="step">
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1 col-md-push-8">
                        <div class="img-container">
                            <img src="/files/attachment-manufactures/<?=$attachment_manufacture['image']?>">
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-7 col-md-pull-4">
                        <div class=" text">
                            <p class="title"><?=$attachment_manufacture['caption']?></p>
                            <?=$attachment_manufacture['content']?>
                        </div>
                    </div>

                </div>
                <?php if ($attachment_manufacture['big_text_status']) { ?>
                <div class="full-text text">
                    <?php if (!empty($attachment_manufacture['big_text_image'])) { ?>
                    <img src="/files/attachment-manufactures/<?=$attachment_manufacture['big_text_image']?>">
                    <?php } ?>
                    <?=$attachment_manufacture['big_text']?>
                    <?php if (!empty($attachment_manufacture['big_text_video'])) { ?>
                    <div class="video-container">
                        <div class="thumb-wrap">
                            <?=$attachment_manufacture['big_text_video']?>
                        </div>
                    </div>
                    <?php } ?>
                    <?=$attachment_manufacture['big_text_2']?>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="production__masters">
        <div class="wrapper">
            <h2>Наши мастера</h2>
            <div class="styled-text text">
                <?=$manufactures['masters_text']?>
            </div>
            <div class="masters__list">
                <?php foreach ($manufactures['attachment_masters'] as $master)  { ?>
                <div class="master">

                    <div class="img-container">
                        <img src="/files/attachment-masters/<?=$master['image']?>">
                    </div>
                    <div class="text">
                        <p class="title"><?=$master['caption']?></p>
                        <p class="descr"><?=$master['post']?></p>
                        <div class="comment">
                            <?=$master['content']?>
                        </div>

                    </div>

                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('makeOrder')?>
